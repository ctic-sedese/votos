from selenium import webdriver

driver = webdriver.Chrome()  # Inicia o browser
driver.get('https://127.0.0.1:8000/assistente_eleicao')  # Acessar a URL especificada

#login
driver.find_element_by_css_selector('#auth_user_username').send_keys("51829634089")  # Digita o texto "python" no input
driver.find_element_by_css_selector('#auth_user_password').send_keys("123456789")  # Digita o texto "python" no input
driver.find_element_by_css_selector('input[type=submit]').click()  # Clica no botão de submit

#seleciona entidade
driver.find_element_by_css_selector('#entidade-18').click()  # Clica no botão de submit

#dados_eleição
driver.find_element_by_css_selector('#eleicao_nome').send_keys("Eleicao Selenium 1234")  # Digita o texto "python" no input
driver.find_element_by_css_selector('#eleicao_inicio_alistamento').send_keys("25-08-2021 08:00:00")  # Digita o texto "python" no input
driver.find_element_by_css_selector('#eleicao_fim_alistamento').send_keys("29-08-2021 08:00:00")  # Digita o texto "python" no input
driver.find_element_by_css_selector('#eleicao_inicio_eleicao').send_keys("30-08-2021 08:00:00")  # Digita o texto "python" no input
driver.find_element_by_css_selector('#eleicao_fim_eleicao').send_keys("30-08-2021 18:00:00")  # Digita o texto "python" no input
driver.find_element_by_css_selector('input[type=submit]').click()  # Clica no botão de submit

#documentos
driver.find_element_by_css_selector('#obrigatorioCPF').click()  # Digita o texto "python" no input
driver.find_element_by_css_selector('input[type=submit]').click()  # Clica no botão de submit


import time
#cargos
driver.find_element_by_css_selector('#btn-cadastrar-cargo').click()  # Clica no botão de submit
time.sleep(1)
driver.find_element_by_css_selector('#cargo_nome').send_keys("Melhor programador")
driver.find_element_by_css_selector('#cargo_atribuicoes').send_keys("Eleição de Selenium")
driver.find_element_by_css_selector('input[type=submit]').click()
time.sleep(3)

driver.find_element_by_css_selector('#btn-cadastrar-cargo').click()  # Clica no botão de submit
time.sleep(1)
driver.find_element_by_css_selector('#cargo_nome').send_keys("Melhor programador de testes")
driver.find_element_by_css_selector('#cargo_atribuicoes').send_keys("Eleição de Selenium")
driver.find_element_by_css_selector('input[type=submit]').click()
time.sleep(2)

driver.find_element_by_css_selector('#btn-cadastrar-cargo').click()  # Clica no botão de submit
time.sleep(1)
driver.find_element_by_css_selector('#cargo_nome').send_keys("Melhor do Melhor do Mundo")
driver.find_element_by_css_selector('#cargo_atribuicoes').send_keys("Eleição de Selenium")
driver.find_element_by_css_selector('input[type=submit]').click()
time.sleep(3)

#prosseguir
driver.find_element_by_css_selector('#btn-prosseguir').click()
driver.quit()  # Encerra o browser
