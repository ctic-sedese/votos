#-*- coding: utf-8 -*-

# primeiro está logado?
# Se está logado é usuário para inscrição?
# Se não está logado faz login ou cadastro



@auth.requires_login()
def index():
    # request.vars.eleicao or redirect(URL(c='default', f='eleicoes'))

    eleicao = db.eleicao(request.get_vars.eleicao)
    
    # exige_entidade_eleitora = db((db.cargo.eleicao==eleicao.id)
    #         & (db.cargo.tipo_eleitor=='entidade')).select()
            

    # exige_entidade_candidata = db((db.cargo.eleicao==eleicao.id)
    #         & (db.cargo.tipo_eleitor=='entidade')).select()

    if eleicao:
        inscrito = db((db.eleitor.eleitor == auth.user_id)
                        & (db.eleitor.eleicao == eleicao.id)).select().first()
        candidaturas = db((db.candidato.candidato==auth.user_id) 
                        & (db.candidato.eleicao== eleicao.id)
                        # & (db.candidato.status == 'aprovado')
                        ).select()
        habilitacoes = db((db.habilitacao.eleitor == auth.user_id) & (db.habilitacao.eleicao==eleicao.id)).select()
    else:
        inscrito = None
        candidaturas = None
        habilitacoes = None
    fields = db.auth_user.fields
    # del fields[fields.index('entidade')]
    del fields[fields.index('registration_key')]
    del fields[fields.index('reset_password_key')]
    del fields[fields.index('registration_id')]
    del fields[fields.index('is_active')]
    del fields[fields.index('created_on')]
    del fields[fields.index('created_by')]
    del fields[fields.index('modified_on')]
    del fields[fields.index('modified_by')]

    form = SQLFORM(db.auth_user, # Edita se já tiver logado
                    auth.user_id,
                    fields = fields,
                    showid=False, submit_button='Atualizar',
                    # formstyle='bootstrap3_stacked',
                    separator=':  ')
    if form.process().accepted:
        if not db((db.eleitor.eleicao==request.vars.eleicao) & (db.eleitor.eleitor==auth.user_id)).select():
            db.eleitor.insert(eleicao=request.vars.eleicao
                , eleitor=auth.user_id
                , status="nao_inscrito")

        redirect(URL('habilitar_eleitor', 'cargos', vars=dict(eleicao=request.vars.eleicao, mode='list')))

    # form = auth.register()
    # se está inscrito precisa continuar
    if inscrito:
        db.auth_user.username.writable = False
        db.auth_user.first_name.writable = False
        # db.auth_user.data_nascimento.writable = False
        db.auth_user.identidade.writable = False
        nome_eleitor = db.auth_user(db.auth_user.id == auth.user_id).first_name
        eleitor_insc = inscrito.id
        docs_registrados = db(db.documentos.user_id == eleitor_insc).select(
            'tipo_arquivo').column()
        db.documentos.user_id.default = eleitor_insc
        
        
    # else:
    #     auth.messages.verify_email = '''
    #     <html> <html class="no-js" lang="{{='pt-br'}}"><head>
    #     <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge{{=not request.is_local and ',chrome=1' or ''}}"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
    #     <title>{{=response.title or request.application}}</title>
    #     <meta name="application-name" content="{{=request.application}}"> <meta name="google-site-verification" content="">
    #     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    #     <link rel="stylesheet" href="{{=URL('static','css/bootstrap.min.css')}}" />
    #     <link rel="stylesheet" href="{{=URL('static','css/web2py-bootstrap4.css')}}" />
    #     <script src="{{=URL('static','js/modernizr-2.8.3.min.js')}}"></script><body>
    #     <div class="card" style="width: 50rem background-color: rgb(10, 70, 250); color:white; padding:20px;word-wrap:break-word;">
    #     <div class="card-body" align="center">
    #     <h1 class="card-title" > Eleições Virtuais da SEDESE</h1> </head> <br><br>
    #     <h3 class="card-text"> Bem vindo %(first_name)s, Click no link abaixo:<br><br>
    #     <a href=%(link)s class="btn btn-primary"> Verificar e-mail </a> <br><br> para verificar seu email </h3><br><br></div>    </div> </body>
    #     <div class="card-footer"> <div class="container center" align="center" style="background-color: rgb(32, 32, 32); color:white; padding:20px;word-wrap:break-word;"> <br><br>
    #     <a class="display-3" href="{{=URL(c='default',f='index')}}"">Clique para ir até Eleições Virtuais</a>
    #     <h3 class="display-3" > Contato: sistemavoto@social.mg.gov.br </h3> </div> </div> </html> '''
    #     auth.settings.registration_requires_verification = False
    #     # auth.settings.registration_requires_approval = False 
    #     #auth.settings.register_next=URL('default', 'eleitor_docs', args = eleicao)

    # return dict(form = form, eleicao=eleicao, eleitor=inscrito, candidaturas=candidaturas)
    return locals()

@auth.requires_login()
def form_entidade():
    db.entidade.legislacao_criacao.writable=False
    db.entidade.nome.label=ob("Entidade")
    db.entidade.cnpj.label=ob("CNPJ")
    db.entidade.representante_legal.readable=True
    db.entidade.representante_legal.default=auth.user_id
    db.entidade.representante_legal.comment=''
    db.entidade.representante_legal.writable=False

    form = SQLFORM(db.entidade)
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        
        # redirect(URL('habilitar_eleitor', 'cargos', vars=request.vars))
    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        pass
    return {"form": form}

@auth.requires_login()
def entidades():
    request.vars.eleicao or redirect(URL('default', 'index'))
    eleicao = db.eleicao(request.vars.eleicao)
        
    query = db.entidade.id > 0
    query &= db.entidade.representante_legal==auth.user_id
    if request.vars.q:
        query = db.entidade.nome.contains(request.vars.q)
        
    entidades = db(query).select(db.entidade.nome, db.entidade.id, orderby=db.entidade.nome)
    return dict(entidades=entidades, eleicao=eleicao)

@auth.requires_login()
def cargos():
    print('vamos começar')
    eleicao = db.eleicao(request.vars.eleicao)

    if eleicao:
        session.eleicao = eleicao
    elif session.eleicao: 
        eleicao = session.eleicao
    else:
        redirect(URL('index'))
    eleitor = db((db.eleitor.eleitor==auth.user_id) & (db.eleitor.eleicao==eleicao.id)).select().first()




    if request.vars.entidade_representada and int(request.vars.entidade_representada) > 0:
        entidade = db((db.entidade.id == int(request.vars.entidade_representada))
                    & (db.entidade.representante_legal==auth.user.id)
                    ).select().first()
        eleitor.update_record(entidade_representada=entidade.id)

    if eleitor.entidade_representada:
        entidade = db.entidade(eleitor.entidade_representada)
        entidade_id = entidade.id
    else:
        entidade = None
        entidade_id = 0
    # print(entidade, 'toqui')
    if entidade:
        if eleicao.tipo_eleitor == 'entidade':
            habilitacoes = db( (db.habilitacao.eleitor==auth.user_id) 
                & (db.habilitacao.eleicao == eleicao.id)
                & (db.habilitacao.entidade_representada==None)
                ).select(db.habilitacao.ALL)    
            print('para atualizar', len(habilitacoes))
            for h in habilitacoes:    
                h.update_record(entidade_representada=entidade.id)     
                print(h, 'atualizado')   



    print('passou atualização')
    # response.flash = request.vars
    
    if request.post_vars:
        print('tem post_vars')
        # response.view = 'generic.html'
        h = []
        c = []
        for v in request.post_vars:
            if 'eleitor' in str(v):
                i = v.split('[')[1].split(']')[0]
                h.append(int(i))
            if 'candidato' in v:
                i = v.split('[')[1].split(']')[0]
                c.append(int(i))
        # return str(candidato)

        # habilitar()
        
        for v in h:
            r = db((db.habilitacao.eleitor == auth.user_id) 
                & (db.habilitacao.eleicao==eleicao.id)
                & (db.habilitacao.cargo==v)
            ).select().first()
            if not r: # Cria eleitor para prosseguir
                id = db.habilitacao.insert(eleicao=eleicao.id, 
                                           eleitor=auth.user_id, 
                                           entidade=entidade.id if (entidade and eleicao.tipo_eleitor == 'entidade') else None, 
                                           cargo=v,
                                           status="inscricao_solicitada")
            
        for v in c:
            # session.flash = request.vars.eleitor
            # for e in request.vars.eleitor:
            r = db((db.candidato.candidato == auth.user_id) 
                & (db.candidato.eleicao==eleicao.id)
                & (db.candidato.cargo==v)
            ).select().first()
            if not r: # Cria eleitor para prosseguir
                id = db.candidato.insert(eleicao=eleicao.id, 
                    candidato=auth.user_id,
                    entidade=entidade.id if (entidade and eleicao.tipo_candidato== 'entidade') else None, 
                    cargo=v,
                    status='nao_inscrito'
                    )

        #clean habilitacoes desmarcadas
        if h and len(h) > 0:
            n = db((db.habilitacao.eleitor == auth.user_id) 
                    & (db.habilitacao.eleicao==eleicao.id)
                    & (~db.habilitacao.cargo.belongs(h))
                    ).delete()
        elif not h:
            n = db((db.habilitacao.eleitor == auth.user_id) 
                    & (db.habilitacao.eleicao==eleicao.id)
                    ).delete()        

        #clean candidaturas desmarcadas
        if c and len(c) > 0:
            n = db((db.candidato.candidato == auth.user_id) 
                    & (db.candidato.eleicao==eleicao.id)
                    & (~db.candidato.cargo.belongs(c))
                    ).delete()
        elif not c:
            n = db((db.candidato.candidato == auth.user_id) 
                    & (db.candidato.eleicao==eleicao.id)
                    ).delete()
        # n = db._lastsql

    print('passou post_vars')
    if request.vars.eleitor:
        redirect(URL(f='documentos',vars =dict(eleicao=eleicao.id,entidade_representada=entidade.id)))
        
    def habilitacao(l):
        h = db( (db.habilitacao.eleitor==auth.user_id) 
            & (db.habilitacao.eleicao == eleicao.id)
            & (db.habilitacao.cargo == l.id)
            ).select(db.habilitacao.ALL).first()
        return h

    def candidatado(l):
        candidato = db( (db.candidato.candidato==auth.user_id)
                 & (db.candidato.cargo==l.id)
                ).select().first()
        return candidato
                
    habilitacoes = db( (db.habilitacao.eleitor==auth.user_id) 
            & (db.habilitacao.eleicao == eleicao.id)
            # & (db.habilitacao.entidade_representada==None)
            ).select(db.habilitacao.ALL)  
    print('habilitacoes no fim', len(habilitacoes))
    query = db.cargo.eleicao==eleicao.id
    tbl = db.cargo
    lista = db(query).select(tbl.id, tbl.nome, tbl.atribuicoes, tbl.documentos_requeridos_eleitor, tbl.documentos_requeridos_cargo)
    print('lista', len(lista))
    # return dict(lista=lista, eleicao=eleicao, eleitor=eleitor, habilitado=habilitado, entidade=entidade,candidatado=candidatado)   
    return locals()




@auth.requires_login()
def documentos():
    if request.vars.eleicao and int(request.vars.eleicao) > 0:
        eleicao = db.eleicao(int(request.vars.eleicao))
    if not request.vars.eleicao:
        session.flash = "Você precisa escolher uma eleição"
        redirect(URL('habilitar_eleitor','cargos', vars=dict(eleicao=request.vars.eleicao)))

    #cargo = db.cargo(request.vars.cargo)
    eleitor = db((db.habilitacao.eleicao==eleicao.id) & (db.habilitacao.eleitor==auth.user_id)).select().first()
    # return db._lastsql
    candidato = db((db.candidato.candidato == auth.user_id) 
                & (db.candidato.eleicao == eleicao.id)).select().first()
    # eleicao = cargo.eleicao
    entidade = eleicao.entidade
    # return eleitor.id
    if candidato:
        # return 'sou candidato'
        docs = documentos_requeridos(eleicao, 'candidatar')
    elif eleitor:
        docs = documentos_requeridos(eleicao, 'votar')
    # return docs
    docs_cadastrados = documentos_cadastrados(docs, auth.user_id)

    documentos = [] # form list
    
    fields = ["nome_arquivo", "tipo_arquivo", "arquivo", "status", "observacoes", "modified_on"]
        
    for d in docs_cadastrados:
        db.documentos.status.writable = False
        db.documentos.tipo_arquivo.writable = False
        db.documentos.nome_arquivo.writable = False
        db.documentos.modified_on.writable = False
        
        form = SQLFORM(db.documentos, record=d.id, deletable=True, upload=URL('download'), fields=fields, submit_button="Anexar")
        if form.process().accepted:
            db(db.documentos.id==form.vars.id).update(status="aguardando_aprovacao")
            redirect(URL('habilitar_eleitor', 'documentos', args=request.args
                    , vars=dict(eleicao=request.vars.eleicao, eleitor=eleitor.id, entidade=entidade.id)))
            
            response.flash = "Atualizado"        

        elif form.errors:
            response.flash = 'há erros no formulário'
        else:
            pass
        
        documentos.append(form)
    
    return dict(documentos=documentos, eleicao=eleicao)

@auth.requires_login()
def finalizar():
    eleicao = db.eleicao(request.vars.eleicao)
    eleitor = db((db.eleitor.eleitor == auth.user_id)
                &(db.eleitor.eleicao==eleicao.id)).select().first()
    if eleitor:
        eleitor.update_record(status='inscricao_solicitada')
    c = db((db.candidato.candidato == auth.user_id)
                & (db.candidato.eleicao==eleicao.id)).select().first()
    if c:
        c.update_record(status='inscricao_solicitada')



    return locals()

# ---- action to server uploaded static content (required) ---
@ cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)

@auth.requires_login()
def habilitar():
    eleitor = db.eleitor(db.eleitor.eleitor==auth.user_id)    
    eleicao = db.eleicao(int(request.vars.eleicao))
    

        # Checa se o eleitor está habilitado para o cargo escolhido
    candidaturas = db((db.candidato.candidato == auth.user_id)
                    & (db.candidato.eleicao==eleicao.id)
                ).select()

    habilitacoes = db(
        (db.habilitacao.eleicao==eleicao.id)
        & (db.habilitacao.eleitor==auth.user_id)

        ).select()
    if candidaturas:
        candidato = candidaturas[0]
        if candidato.entidade:
            nome_candidato = db.entidade(candidato.entidade).nome
        else:
            nome_candidato = auth.user.first_name
        mensagem = f"""
        <p class="text-center"> Foi solicitida a inscrição de {nome_candidato} para o processo eleitoral {db.eleicao(candidato.eleicao).nome}<br>
        Agora você precisa aguardar aprovação da comissão eleitoral. <br>
        Fique atento ao sistema e seu email.<p>
        <br><br>
        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
        <p class="text-danger">Não responda este email</p>
        """
        envia_emails(auth.user.email, "Candidatura para processo eleitoral", mensagem)

    #TODO
    if habilitacoes:
        habilitacao = habilitacoes[0]
        if habilitacao.entidade_representada:
            nome_eleitor = db.entidade(habilitacao.entidade_representada).nome
        else:
            nome_eleitor = auth.user.first_name

        # precisa enviar um email
        mensagem = f"""
        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral {eleicao.nome}<br>
        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>
        Fique atento ao sistema e seu email.<p>
        <br><br>
        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
        <p class="text-danger">Não responda este email</p>
        """
        envia_emails(auth.user.email, "Inscrição Solicitada para processo eleitoral", mensagem)
        # precisa enviar um email

        # habilitacao_id = habilitacao.id
    


    redirect(URL('habilitar_eleitor', 'finalizar', vars=request.vars))
                
