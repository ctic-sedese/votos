--
-- PostgreSQL database dump
--

-- Dumped from database version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_cas; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_cas (
    id integer NOT NULL,
    user_id integer,
    created_on timestamp without time zone,
    service character varying(512),
    ticket character varying(512),
    renew character(1)
);


ALTER TABLE public.auth_cas OWNER TO x;

--
-- Name: auth_cas_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_cas_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_cas_id_seq OWNER TO x;

--
-- Name: auth_cas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_cas_id_seq OWNED BY public.auth_cas.id;


--
-- Name: auth_event; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_event (
    id integer NOT NULL,
    time_stamp timestamp without time zone,
    client_ip character varying(512),
    user_id integer,
    origin character varying(512),
    description text
);


ALTER TABLE public.auth_event OWNER TO x;

--
-- Name: auth_event_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_event_id_seq OWNER TO x;

--
-- Name: auth_event_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_event_id_seq OWNED BY public.auth_event.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    role character varying(512),
    description text
);


ALTER TABLE public.auth_group OWNER TO x;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO x;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_membership; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_membership (
    id integer NOT NULL,
    user_id integer,
    group_id integer
);


ALTER TABLE public.auth_membership OWNER TO x;

--
-- Name: auth_membership_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_membership_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_membership_id_seq OWNER TO x;

--
-- Name: auth_membership_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_membership_id_seq OWNED BY public.auth_membership.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    group_id integer,
    name character varying(512),
    table_name character varying(512),
    record_id integer
);


ALTER TABLE public.auth_permission OWNER TO x;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO x;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    first_name character varying(128),
    last_name character varying(128),
    email character varying(128),
    username character varying(200),
    entidade integer,
    mothers_name character varying(128),
    "gênero" character varying(512),
    cor character varying(512),
    data_nascimento date,
    identidade character varying(512),
    arquivo_identidade character varying(512),
    escolaridade character varying(512),
    password character varying(512),
    registration_key character varying(512),
    reset_password_key character varying(512),
    registration_id character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    municipio integer,
    foto character varying(512)
);


ALTER TABLE public.auth_user OWNER TO x;

--
-- Name: auth_user_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.auth_user_archive (
    current_record integer,
    id integer NOT NULL,
    first_name character varying(128),
    last_name character varying(128),
    email character varying(128),
    username character varying(200),
    entidade integer,
    municipio integer,
    mothers_name character varying(128),
    "gênero" character varying(512),
    cor character varying(512),
    data_nascimento date,
    identidade character varying(512),
    escolaridade character varying(512),
    password character varying(512),
    registration_key character varying(512),
    reset_password_key character varying(512),
    registration_id character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.auth_user_archive OWNER TO x;

--
-- Name: auth_user_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_user_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_archive_id_seq OWNER TO x;

--
-- Name: auth_user_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_user_archive_id_seq OWNED BY public.auth_user_archive.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO x;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: candidato; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.candidato (
    id integer NOT NULL,
    entidade integer,
    eleicao integer,
    cargo integer,
    pdf character varying(512),
    foto character varying(512),
    perfil text,
    propostas text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    candidato integer,
    status character varying(512)
);


ALTER TABLE public.candidato OWNER TO x;

--
-- Name: candidato_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.candidato_archive (
    current_record integer,
    id integer NOT NULL,
    candidato integer,
    entidade integer,
    eleicao integer,
    cargo integer NOT NULL,
    status character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.candidato_archive OWNER TO x;

--
-- Name: candidato_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.candidato_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.candidato_archive_id_seq OWNER TO x;

--
-- Name: candidato_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.candidato_archive_id_seq OWNED BY public.candidato_archive.id;


--
-- Name: candidato_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.candidato_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.candidato_id_seq OWNER TO x;

--
-- Name: candidato_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.candidato_id_seq OWNED BY public.candidato.id;


--
-- Name: cargo; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.cargo (
    id integer NOT NULL,
    entidade integer,
    eleicao integer,
    nome character varying(512),
    atribuicoes text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    documentos_requeridos_eleitor text,
    documentos_requeridos_cargo text,
    tipo_eleitor character varying(512),
    tipo_candidato character varying(512),
    cadeiras integer
);


ALTER TABLE public.cargo OWNER TO x;

--
-- Name: cargo_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.cargo_archive (
    current_record integer,
    id integer NOT NULL,
    entidade integer,
    eleicao integer,
    tipo_eleitor character varying(512),
    tipo_candidato character varying(512),
    documentos_requeridos_eleitor text,
    documentos_requeridos_cargo text,
    nome character varying(512) NOT NULL,
    atribuicoes text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.cargo_archive OWNER TO x;

--
-- Name: cargo_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.cargo_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargo_archive_id_seq OWNER TO x;

--
-- Name: cargo_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.cargo_archive_id_seq OWNED BY public.cargo_archive.id;


--
-- Name: cargo_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.cargo_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargo_id_seq OWNER TO x;

--
-- Name: cargo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.cargo_id_seq OWNED BY public.cargo.id;


--
-- Name: documento_recurso; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.documento_recurso (
    id integer NOT NULL,
    recurso integer,
    arquivo character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    nome character varying(512)
);


ALTER TABLE public.documento_recurso OWNER TO x;

--
-- Name: documento_recurso_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.documento_recurso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documento_recurso_id_seq OWNER TO x;

--
-- Name: documento_recurso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.documento_recurso_id_seq OWNED BY public.documento_recurso.id;


--
-- Name: documentos; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.documentos (
    id integer NOT NULL,
    user_id integer,
    nome_arquivo character varying(512),
    tipo_arquivo integer,
    arquivo character varying(512),
    status character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    observacoes text
);


ALTER TABLE public.documentos OWNER TO x;

--
-- Name: documentos_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.documentos_archive (
    current_record integer,
    id integer NOT NULL,
    user_id integer,
    nome_arquivo character varying(512),
    tipo_arquivo integer,
    arquivo character varying(512),
    status character varying(512),
    observacoes text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.documentos_archive OWNER TO x;

--
-- Name: documentos_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.documentos_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentos_archive_id_seq OWNER TO x;

--
-- Name: documentos_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.documentos_archive_id_seq OWNED BY public.documentos_archive.id;


--
-- Name: documentos_eleitor; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.documentos_eleitor (
    id integer NOT NULL,
    eleitor integer,
    nome_arquivo character varying(512),
    tipo_arquivo character varying(512),
    arquivo character varying(512),
    status character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.documentos_eleitor OWNER TO x;

--
-- Name: documentos_eleitor_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.documentos_eleitor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentos_eleitor_id_seq OWNER TO x;

--
-- Name: documentos_eleitor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.documentos_eleitor_id_seq OWNED BY public.documentos_eleitor.id;


--
-- Name: documentos_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.documentos_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentos_id_seq OWNER TO x;

--
-- Name: documentos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.documentos_id_seq OWNED BY public.documentos.id;


--
-- Name: documentos_obrigatorios_eleicao; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.documentos_obrigatorios_eleicao (
    id integer NOT NULL,
    eleicao integer,
    tipo_documento integer
);


ALTER TABLE public.documentos_obrigatorios_eleicao OWNER TO x;

--
-- Name: documentos_obrigatorios_eleicao_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.documentos_obrigatorios_eleicao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documentos_obrigatorios_eleicao_id_seq OWNER TO x;

--
-- Name: documentos_obrigatorios_eleicao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.documentos_obrigatorios_eleicao_id_seq OWNED BY public.documentos_obrigatorios_eleicao.id;


--
-- Name: eleicao; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.eleicao (
    id integer NOT NULL,
    entidade integer,
    nome character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    edital character varying(512),
    inicio_alistamento timestamp without time zone,
    fim_alistamento timestamp without time zone,
    inicio_eleicao timestamp without time zone,
    fim_eleicao timestamp without time zone,
    obrigatorio text,
    prazo_correcao_documentos integer,
    segmentos integer,
    segmentos_candidatura integer,
    documentos_requeridos_eleitor text,
    documentos_requeridos_cargo text,
    tipo_eleitor character varying(512),
    tipo_candidato character varying(512),
    publicar character(1)
);


ALTER TABLE public.eleicao OWNER TO x;

--
-- Name: eleicao_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.eleicao_archive (
    current_record integer,
    id integer NOT NULL,
    entidade integer,
    nome character varying(512),
    inicio_alistamento timestamp without time zone,
    fim_alistamento timestamp without time zone,
    inicio_eleicao timestamp without time zone,
    fim_eleicao timestamp without time zone,
    edital character varying(512),
    obrigatorio text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    prazo_correcao_documentos integer
);


ALTER TABLE public.eleicao_archive OWNER TO x;

--
-- Name: eleicao_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.eleicao_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eleicao_archive_id_seq OWNER TO x;

--
-- Name: eleicao_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.eleicao_archive_id_seq OWNED BY public.eleicao_archive.id;


--
-- Name: eleicao_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.eleicao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eleicao_id_seq OWNER TO x;

--
-- Name: eleicao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.eleicao_id_seq OWNED BY public.eleicao.id;


--
-- Name: eleitor; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.eleitor (
    id integer NOT NULL,
    eleitor integer,
    eleicao integer,
    status character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    entidade_representada integer
);


ALTER TABLE public.eleitor OWNER TO x;

--
-- Name: eleitor_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.eleitor_archive (
    current_record integer,
    id integer NOT NULL,
    eleicao integer,
    eleitor integer,
    status character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.eleitor_archive OWNER TO x;

--
-- Name: eleitor_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.eleitor_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eleitor_archive_id_seq OWNER TO x;

--
-- Name: eleitor_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.eleitor_archive_id_seq OWNED BY public.eleitor_archive.id;


--
-- Name: eleitor_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.eleitor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eleitor_id_seq OWNER TO x;

--
-- Name: eleitor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.eleitor_id_seq OWNED BY public.eleitor.id;


--
-- Name: entidade; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.entidade (
    id integer NOT NULL,
    nome character varying(512),
    cnpj character varying(200),
    data_criacao_entidade date,
    carta_apresentacao text,
    segmento_atuacao text,
    municipio integer,
    representante_legal integer,
    cartao_cnpj character varying(512),
    comprovante_endereco character varying(512),
    concordancia character varying(512),
    orgao_colegiado character(1),
    legislacao_criacao text,
    texto_concordancia character varying(512)
);


ALTER TABLE public.entidade OWNER TO x;

--
-- Name: entidade_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.entidade_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entidade_id_seq OWNER TO x;

--
-- Name: entidade_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.entidade_id_seq OWNED BY public.entidade.id;


--
-- Name: estado; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.estado (
    id integer NOT NULL,
    uf character varying(512),
    nome character varying(512),
    latitude double precision,
    longitude double precision
);


ALTER TABLE public.estado OWNER TO x;

--
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.estado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_id_seq OWNER TO x;

--
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.estado_id_seq OWNED BY public.estado.id;


--
-- Name: habilitacao; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.habilitacao (
    id integer NOT NULL,
    cargo integer,
    status character varying(512),
    entidade_representada integer,
    eleicao integer,
    eleitor integer
);


ALTER TABLE public.habilitacao OWNER TO x;

--
-- Name: habilitacao_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.habilitacao_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.habilitacao_id_seq OWNER TO x;

--
-- Name: habilitacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.habilitacao_id_seq OWNED BY public.habilitacao.id;


--
-- Name: municipio; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.municipio (
    id integer NOT NULL,
    nome character varying(512),
    latitude double precision,
    longitude double precision,
    estado integer,
    uf character varying(512),
    capital character varying(512)
);


ALTER TABLE public.municipio OWNER TO x;

--
-- Name: municipio_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.municipio_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.municipio_id_seq OWNER TO x;

--
-- Name: municipio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.municipio_id_seq OWNED BY public.municipio.id;


--
-- Name: queue; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.queue (
    id integer NOT NULL,
    status character varying(512),
    email character varying(512),
    subject character varying(512),
    msg text
);


ALTER TABLE public.queue OWNER TO x;

--
-- Name: queue_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.queue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.queue_id_seq OWNER TO x;

--
-- Name: queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.queue_id_seq OWNED BY public.queue.id;


--
-- Name: recurso; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.recurso (
    id integer NOT NULL,
    titulo character varying(512),
    tipo character varying(512),
    eleicao integer,
    descricao text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.recurso OWNER TO x;

--
-- Name: recurso_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.recurso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recurso_id_seq OWNER TO x;

--
-- Name: recurso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.recurso_id_seq OWNED BY public.recurso.id;


--
-- Name: resposta_recurso; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.resposta_recurso (
    id integer NOT NULL,
    resposta text,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    recurso integer
);


ALTER TABLE public.resposta_recurso OWNER TO x;

--
-- Name: resposta_recurso_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.resposta_recurso_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resposta_recurso_id_seq OWNER TO x;

--
-- Name: resposta_recurso_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.resposta_recurso_id_seq OWNED BY public.resposta_recurso.id;


--
-- Name: tipo_documento; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_documento (
    id integer NOT NULL,
    tipo character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer,
    relativo_a character varying(512)
);


ALTER TABLE public.tipo_documento OWNER TO postgres;

--
-- Name: tipo_documento_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.tipo_documento_archive (
    current_record integer,
    id integer NOT NULL,
    tipo character varying(512),
    relativo_a character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.tipo_documento_archive OWNER TO x;

--
-- Name: tipo_documento_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.tipo_documento_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_documento_archive_id_seq OWNER TO x;

--
-- Name: tipo_documento_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.tipo_documento_archive_id_seq OWNED BY public.tipo_documento_archive.id;


--
-- Name: tipo_documento_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_documento_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_documento_id_seq OWNER TO postgres;

--
-- Name: tipo_documento_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_documento_id_seq OWNED BY public.tipo_documento.id;


--
-- Name: voto; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.voto (
    id integer NOT NULL,
    entidade integer,
    eleicao integer,
    cargo integer,
    candidato integer,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.voto OWNER TO x;

--
-- Name: voto_archive; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.voto_archive (
    current_record integer,
    id integer NOT NULL,
    entidade integer,
    eleicao integer,
    cargo integer,
    candidato integer,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.voto_archive OWNER TO x;

--
-- Name: voto_archive_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.voto_archive_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.voto_archive_id_seq OWNER TO x;

--
-- Name: voto_archive_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.voto_archive_id_seq OWNED BY public.voto_archive.id;


--
-- Name: voto_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.voto_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.voto_id_seq OWNER TO x;

--
-- Name: voto_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.voto_id_seq OWNED BY public.voto.id;


--
-- Name: wiki_media; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.wiki_media (
    id integer NOT NULL,
    wiki_page integer,
    title character varying(512),
    filename character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.wiki_media OWNER TO x;

--
-- Name: wiki_media_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.wiki_media_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_media_id_seq OWNER TO x;

--
-- Name: wiki_media_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.wiki_media_id_seq OWNED BY public.wiki_media.id;


--
-- Name: wiki_page; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.wiki_page (
    id integer NOT NULL,
    slug character varying(512),
    title character varying(255),
    body text NOT NULL,
    tags text,
    can_read text,
    can_edit text,
    changelog character varying(512),
    html text,
    render character varying(512),
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.wiki_page OWNER TO x;

--
-- Name: wiki_page_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.wiki_page_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_page_id_seq OWNER TO x;

--
-- Name: wiki_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.wiki_page_id_seq OWNED BY public.wiki_page.id;


--
-- Name: wiki_tag; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.wiki_tag (
    id integer NOT NULL,
    name character varying(512),
    wiki_page integer,
    is_active character(1),
    created_on timestamp without time zone,
    created_by integer,
    modified_on timestamp without time zone,
    modified_by integer
);


ALTER TABLE public.wiki_tag OWNER TO x;

--
-- Name: wiki_tag_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.wiki_tag_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.wiki_tag_id_seq OWNER TO x;

--
-- Name: wiki_tag_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.wiki_tag_id_seq OWNED BY public.wiki_tag.id;


--
-- Name: auth_cas id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_cas ALTER COLUMN id SET DEFAULT nextval('public.auth_cas_id_seq'::regclass);


--
-- Name: auth_event id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_event ALTER COLUMN id SET DEFAULT nextval('public.auth_event_id_seq'::regclass);


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_membership id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_membership ALTER COLUMN id SET DEFAULT nextval('public.auth_membership_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user_archive ALTER COLUMN id SET DEFAULT nextval('public.auth_user_archive_id_seq'::regclass);


--
-- Name: candidato id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato ALTER COLUMN id SET DEFAULT nextval('public.candidato_id_seq'::regclass);


--
-- Name: candidato_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive ALTER COLUMN id SET DEFAULT nextval('public.candidato_archive_id_seq'::regclass);


--
-- Name: cargo id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo ALTER COLUMN id SET DEFAULT nextval('public.cargo_id_seq'::regclass);


--
-- Name: cargo_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive ALTER COLUMN id SET DEFAULT nextval('public.cargo_archive_id_seq'::regclass);


--
-- Name: documento_recurso id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documento_recurso ALTER COLUMN id SET DEFAULT nextval('public.documento_recurso_id_seq'::regclass);


--
-- Name: documentos id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos ALTER COLUMN id SET DEFAULT nextval('public.documentos_id_seq'::regclass);


--
-- Name: documentos_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive ALTER COLUMN id SET DEFAULT nextval('public.documentos_archive_id_seq'::regclass);


--
-- Name: documentos_eleitor id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_eleitor ALTER COLUMN id SET DEFAULT nextval('public.documentos_eleitor_id_seq'::regclass);


--
-- Name: documentos_obrigatorios_eleicao id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_obrigatorios_eleicao ALTER COLUMN id SET DEFAULT nextval('public.documentos_obrigatorios_eleicao_id_seq'::regclass);


--
-- Name: eleicao id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao ALTER COLUMN id SET DEFAULT nextval('public.eleicao_id_seq'::regclass);


--
-- Name: eleicao_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive ALTER COLUMN id SET DEFAULT nextval('public.eleicao_archive_id_seq'::regclass);


--
-- Name: eleitor id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor ALTER COLUMN id SET DEFAULT nextval('public.eleitor_id_seq'::regclass);


--
-- Name: eleitor_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive ALTER COLUMN id SET DEFAULT nextval('public.eleitor_archive_id_seq'::regclass);


--
-- Name: entidade id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.entidade ALTER COLUMN id SET DEFAULT nextval('public.entidade_id_seq'::regclass);


--
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.estado ALTER COLUMN id SET DEFAULT nextval('public.estado_id_seq'::regclass);


--
-- Name: habilitacao id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao ALTER COLUMN id SET DEFAULT nextval('public.habilitacao_id_seq'::regclass);


--
-- Name: municipio id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.municipio ALTER COLUMN id SET DEFAULT nextval('public.municipio_id_seq'::regclass);


--
-- Name: queue id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.queue ALTER COLUMN id SET DEFAULT nextval('public.queue_id_seq'::regclass);


--
-- Name: recurso id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.recurso ALTER COLUMN id SET DEFAULT nextval('public.recurso_id_seq'::regclass);


--
-- Name: resposta_recurso id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.resposta_recurso ALTER COLUMN id SET DEFAULT nextval('public.resposta_recurso_id_seq'::regclass);


--
-- Name: tipo_documento id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento ALTER COLUMN id SET DEFAULT nextval('public.tipo_documento_id_seq'::regclass);


--
-- Name: tipo_documento_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.tipo_documento_archive ALTER COLUMN id SET DEFAULT nextval('public.tipo_documento_archive_id_seq'::regclass);


--
-- Name: voto id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto ALTER COLUMN id SET DEFAULT nextval('public.voto_id_seq'::regclass);


--
-- Name: voto_archive id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive ALTER COLUMN id SET DEFAULT nextval('public.voto_archive_id_seq'::regclass);


--
-- Name: wiki_media id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_media ALTER COLUMN id SET DEFAULT nextval('public.wiki_media_id_seq'::regclass);


--
-- Name: wiki_page id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_page ALTER COLUMN id SET DEFAULT nextval('public.wiki_page_id_seq'::regclass);


--
-- Name: wiki_tag id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_tag ALTER COLUMN id SET DEFAULT nextval('public.wiki_tag_id_seq'::regclass);


--
-- Data for Name: auth_cas; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_cas (id, user_id, created_on, service, ticket, renew) FROM stdin;
\.


--
-- Data for Name: auth_event; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_event (id, time_stamp, client_ip, user_id, origin, description) FROM stdin;
1	2020-08-05 19:05:35	127.0.0.1	\N	auth	User 1 Registered
2	2020-08-05 19:29:05	127.0.0.1	\N	auth	User 2 Registered
3	2020-08-05 19:36:36	127.0.0.1	1	auth	User 1 Logged-in
5	2020-08-06 16:03:24	127.0.0.1	1	auth	User 1 Logged-in
6	2020-08-06 16:20:34	127.0.0.1	\N	auth	User 3 Registered
7	2020-08-06 19:58:43	127.0.0.1	1	auth	User 1 Logged-in
664	2021-11-09 18:08:56	127.0.0.1	1	auth	User 1 Logged-out
9	2020-08-07 13:08:09	127.0.0.1	\N	auth	User 4 Registered
10	2020-08-07 13:34:58	127.0.0.1	\N	auth	User 5 Registered
12	2020-08-10 22:04:56	127.0.0.1	1	auth	User 1 Logged-in
17	2020-08-19 15:30:27	127.0.0.1	1	auth	User 1 Logged-in
18	2020-08-19 15:31:33	127.0.0.1	1	auth	User 1 Profile updated
19	2020-08-19 16:31:39	127.0.0.1	1	auth	User 1 Profile updated
20	2020-08-21 14:17:06	127.0.0.1	\N	auth	User 6 Registered
21	2020-08-27 18:50:38	127.0.0.1	1	auth	User 1 Logged-in
22	2020-08-28 16:13:39	127.0.0.1	1	auth	User 1 Logged-out
23	2020-08-28 17:14:35	127.0.0.1	1	auth	User 1 Logged-in
24	2020-08-28 18:10:36	127.0.0.1	1	auth	User 1 Logged-out
25	2020-08-31 14:31:03	127.0.0.1	1	auth	User 1 Logged-in
26	2020-09-01 01:05:55	127.0.0.1	1	auth	User 1 Logged-in
27	2020-09-01 05:55:09	127.0.0.1	1	auth	User 1 Logged-in
28	2020-09-01 23:46:55	127.0.0.1	1	auth	User 1 Logged-in
29	2020-09-09 17:52:09	127.0.0.1	1	auth	User 1 Logged-in
30	2020-09-10 01:49:45	127.0.0.1	1	auth	User 1 Logged-in
31	2020-09-11 20:46:26	127.0.0.1	\N	auth	User 7 Registered
32	2020-09-12 23:30:56	127.0.0.1	1	auth	User 1 Logged-in
33	2020-09-15 14:23:37	127.0.0.1	1	auth	User 1 Logged-in
34	2020-09-15 14:48:22	127.0.0.1	1	auth	User 1 Logged-in
35	2020-09-15 19:06:24	127.0.0.1	1	auth	User 1 Logged-in
36	2020-09-15 21:03:30	127.0.0.1	1	auth	User 1 Logged-in
37	2020-09-16 00:00:32	127.0.0.1	1	auth	User 1 Logged-in
38	2020-09-16 01:22:10	127.0.0.1	1	auth	User 1 Logged-in
39	2020-09-16 14:15:34	127.0.0.1	1	auth	User 1 Logged-in
40	2020-09-17 14:48:08	127.0.0.1	1	auth	User 1 Logged-in
665	2021-11-09 18:10:24	127.0.0.1	\N	auth	User 69 Registered
42	2020-09-17 16:55:13	127.0.0.1	1	auth	User 1 Logged-in
43	2020-09-20 04:47:10	127.0.0.1	1	auth	User 1 Logged-in
44	2020-09-21 14:33:33	127.0.0.1	1	auth	User 1 Logged-in
45	2020-09-21 18:37:51	127.0.0.1	1	auth	User 1 Logged-in
46	2020-09-22 01:36:04	127.0.0.1	1	auth	User 1 Logged-in
666	2021-11-09 18:12:11	127.0.0.1	\N	auth	User 69 Verification email sent
667	2021-11-09 18:12:32	127.0.0.1	1	auth	User 1 Logged-in
668	2021-11-09 18:12:41	127.0.0.1	\N	auth	User 70 Registered
51	2020-09-22 03:32:53	127.0.0.1	1	auth	User 1 Logged-in
669	2021-11-09 18:13:00	127.0.0.1	\N	auth	User 71 Registered
53	2020-09-22 15:26:14	127.0.0.1	\N	auth	User 8 Registered
54	2020-09-30 18:17:32	127.0.0.1	\N	auth	User 1 Password reset
55	2020-10-02 01:40:09	127.0.0.1	1	auth	User 1 Logged-in
56	2020-10-02 03:51:41	127.0.0.1	1	auth	User 1 Logged-in
57	2020-10-02 20:58:19	127.0.0.1	\N	auth	User 9 Registered
58	2020-10-02 20:59:36	127.0.0.1	1	auth	User 1 Logged-in
670	2021-11-09 18:13:25	127.0.0.1	\N	auth	User 72 Registered
60	2020-10-05 22:53:41	127.0.0.1	1	auth	User 1 Logged-in
671	2021-11-09 18:13:29	127.0.0.1	\N	auth	User 73 Registered
62	2020-10-09 12:44:47	127.0.0.1	1	auth	User 1 Logged-in
63	2020-10-09 14:54:41	127.0.0.1	\N	auth	User 10 Registered
64	2020-10-09 14:55:55	127.0.0.1	\N	auth	User 11 Registered
65	2020-10-09 18:23:55	127.0.0.1	\N	auth	User 12 Registered
66	2020-10-09 18:24:53	127.0.0.1	\N	auth	User 12 Verification email sent
67	2020-10-13 14:27:45	127.0.0.1	\N	auth	User 13 Registered
68	2020-10-13 15:06:16	127.0.0.1	\N	auth	User 13 Verification email sent
69	2020-10-13 16:21:43	127.0.0.1	1	auth	User 1 Logged-in
672	2021-11-09 18:13:55	127.0.0.1	69	auth	User 69 Logged-in
673	2021-11-09 18:15:25	127.0.0.1	71	auth	User 71 Logged-in
72	2020-10-22 18:10:07	127.0.0.1	1	auth	User 1 Logged-in
73	2020-10-22 20:23:16	127.0.0.1	1	auth	User 1 Logged-in
74	2020-10-22 23:08:33	127.0.0.1	1	auth	User 1 Logged-in
75	2020-10-23 14:59:40	127.0.0.1	1	auth	User 1 Logged-in
76	2020-10-23 15:00:31	127.0.0.1	1	auth	User 1 Logged-out
77	2020-10-23 15:00:41	127.0.0.1	\N	auth	User 12 Password reset
78	2020-10-23 15:15:18	127.0.0.1	\N	auth	User 14 Registered
79	2020-10-23 15:15:41	127.0.0.1	\N	auth	User 14 Verification email sent
674	2021-11-09 18:15:30	127.0.0.1	\N	auth	User 74 Registered
81	2020-10-23 17:37:42	127.0.0.1	1	auth	User 1 Logged-in
82	2020-10-23 15:31:07	127.0.0.1	1	auth	User 1 Logged-in
675	2021-11-09 18:16:02	127.0.0.1	\N	auth	User 75 Registered
676	2021-11-09 18:17:08	127.0.0.1	74	auth	User 74 Logged-in
677	2021-11-09 18:18:15	127.0.0.1	75	auth	User 75 Logged-in
86	2020-10-29 16:59:56	127.0.0.1	1	auth	User 1 Logged-in
678	2021-11-09 18:18:36	127.0.0.1	\N	auth	User 76 Registered
88	2020-11-02 19:34:36	127.0.0.1	\N	auth	User 15 Registered
89	2020-11-02 19:35:04	127.0.0.1	\N	auth	User 15 Verification email sent
90	2020-11-03 18:33:47	127.0.0.1	1	auth	User 1 Logged-in
92	2020-11-05 14:38:04	127.0.0.1	1	auth	User 1 Logged-in
680	2021-11-09 18:20:47	127.0.0.1	\N	auth	User 77 Registered
681	2021-11-09 18:20:58	127.0.0.1	\N	auth	User 78 Registered
682	2021-11-09 18:21:15	127.0.0.1	72	auth	User 72 Logged-in
683	2021-11-09 18:21:15	127.0.0.1	\N	auth	User 79 Registered
684	2021-11-09 18:22:17	127.0.0.1	\N	auth	User 80 Registered
685	2021-11-09 18:23:19	127.0.0.1	\N	auth	User 80 Verification email sent
99	2020-11-09 10:02:23	127.0.0.1	1	auth	User 1 Logged-in
722	2021-11-09 19:11:45	127.0.0.1	1	auth	User 1 Logged-out
101	2020-11-09 10:18:52	127.0.0.1	1	auth	User 1 Logged-in
102	2020-11-11 08:59:52	127.0.0.1	1	auth	User 1 Logged-in
723	2021-11-09 19:12:02	127.0.0.1	1	auth	User 1 Logged-in
686	2021-11-09 18:23:31	127.0.0.1	78	auth	User 78 Logged-in
105	2020-11-11 09:43:16	127.0.0.1	\N	auth	User 11 Password reset
687	2021-11-09 18:23:36	127.0.0.1	80	auth	User 80 Logged-in
107	2020-11-11 10:21:46	127.0.0.1	1	auth	User 1 Logged-in
688	2021-11-09 18:24:02	127.0.0.1	77	auth	User 77 Logged-in
689	2021-11-09 18:24:04	127.0.0.1	1	auth	User 1 Logged-out
110	2020-11-13 17:06:04	127.0.0.1	1	auth	User 1 Logged-in
111	2020-11-17 11:31:38	127.0.0.1	\N	auth	User 16 Registered
112	2020-11-17 11:33:45	127.0.0.1	\N	auth	User 16 Verification email sent
113	2020-11-17 11:37:19	127.0.0.1	1	auth	User 1 Logged-in
114	2020-11-18 13:12:53	127.0.0.1	\N	auth	User 17 Registered
115	2020-11-18 13:14:39	127.0.0.1	\N	auth	User 17 Verification email sent
690	2021-11-09 18:24:09	127.0.0.1	78	auth	User 78 Profile updated
691	2021-11-09 18:30:12	127.0.0.1	\N	auth	User 81 Registered
692	2021-11-09 18:33:59	127.0.0.1	\N	auth	User 82 Registered
119	2020-11-19 17:38:33	127.0.0.1	\N	auth	User 18 Registered
120	2020-11-19 17:39:48	127.0.0.1	\N	auth	User 18 Verification email sent
693	2021-11-09 18:34:24	127.0.0.1	\N	auth	User 83 Registered
694	2021-11-09 18:34:34	127.0.0.1	\N	auth	User 84 Registered
695	2021-11-09 18:36:27	127.0.0.1	82	auth	User 82 Logged-in
696	2021-11-09 18:36:34	127.0.0.1	79	auth	User 79 Logged-in
697	2021-11-09 18:37:31	127.0.0.1	\N	auth	User 81 Password reset
126	2020-11-25 11:37:25	127.0.0.1	\N	auth	User 7 Password reset
698	2021-11-09 18:37:52	127.0.0.1	\N	auth	User 81 Password reset
128	2020-11-25 13:35:06	127.0.0.1	\N	auth	User 13 Password reset
129	2020-11-25 14:16:36	127.0.0.1	\N	auth	User 19 Registered
130	2020-11-25 14:35:09	127.0.0.1	\N	auth	User 19 Verification email sent
699	2021-11-09 18:38:09	127.0.0.1	\N	auth	User 70 Password reset
700	2021-11-09 18:38:15	127.0.0.1	\N	auth	User 76 Password reset
133	2020-11-25 14:38:47	127.0.0.1	\N	auth	User 20 Registered
701	2021-11-09 18:42:58	127.0.0.1	84	auth	User 84 Logged-in
135	2020-11-25 14:39:46	127.0.0.1	\N	auth	User 20 Verification email sent
702	2021-11-09 18:48:20	127.0.0.1	70	auth	User 70 Logged-out
703	2021-11-09 18:48:41	127.0.0.1	70	auth	User 70 Logged-in
138	2020-11-25 14:48:43	127.0.0.1	\N	auth	User 7 Password reset
139	2020-11-25 16:17:02	127.0.0.1	\N	auth	User 21 Registered
704	2021-11-09 18:50:22	127.0.0.1	1	auth	User 1 Logged-in
141	2020-11-25 20:09:28	127.0.0.1	\N	auth	User 13 Password reset
705	2021-11-09 18:51:27	127.0.0.1	74	auth	User 74 Logged-in
706	2021-11-09 18:51:48	127.0.0.1	1	auth	User 1 Logged-out
144	2020-11-26 11:00:57	127.0.0.1	\N	auth	User 22 Registered
707	2021-11-09 18:52:08	127.0.0.1	1	auth	User 1 Logged-in
708	2021-11-09 18:52:18	127.0.0.1	1	auth	User 1 Logged-out
709	2021-11-09 18:52:37	127.0.0.1	81	auth	User 81 Logged-out
710	2021-11-09 18:52:48	127.0.0.1	76	auth	User 76 Logged-out
711	2021-11-09 18:52:49	127.0.0.1	1	auth	User 1 Logged-in
712	2021-11-09 18:53:02	127.0.0.1	81	auth	User 81 Logged-in
713	2021-11-09 18:53:07	127.0.0.1	76	auth	User 76 Logged-in
714	2021-11-09 18:53:27	127.0.0.1	1	auth	User 1 Logged-out
715	2021-11-09 18:55:50	127.0.0.1	80	auth	User 80 Logged-out
154	2020-11-26 17:56:16	127.0.0.1	\N	auth	User 21 Password reset
716	2021-11-09 18:56:30	127.0.0.1	1	auth	User 1 Logged-in
717	2021-11-09 18:56:32	127.0.0.1	80	auth	User 80 Logged-in
718	2021-11-09 18:57:30	127.0.0.1	1	auth	User 1 Logged-out
719	2021-11-09 18:57:52	127.0.0.1	1	auth	User 1 Logged-in
720	2021-11-09 18:58:16	127.0.0.1	1	auth	User 1 Logged-out
721	2021-11-09 18:59:05	127.0.0.1	1	auth	User 1 Logged-in
735	2021-11-11 19:24:22	127.0.0.1	1	auth	User 1 Logged-in
780	2021-11-23 15:14:19	127.0.0.1	1	auth	User 1 Logged-in
793	2021-11-25 03:31:01	127.0.0.1	1	auth	User 1 Logged-in
794	2021-11-25 12:59:41	127.0.0.1	1	auth	User 1 Logged-in
795	2021-11-25 13:01:44	127.0.0.1	1	auth	User 1 Logged-out
796	2021-11-25 13:08:24	127.0.0.1	\N	auth	User 86 Registered
797	2021-11-25 13:09:39	127.0.0.1	\N	auth	User 86 Verification email sent
798	2021-11-25 13:10:00	127.0.0.1	86	auth	User 86 Logged-in
799	2021-11-25 13:14:01	127.0.0.1	1	auth	User 1 Logged-in
800	2021-11-25 14:48:16	127.0.0.1	1	auth	User 1 Logged-in
835	2021-11-27 01:17:01	127.0.0.1	1	auth	User 1 Logged-in
836	2021-11-29 14:34:02	127.0.0.1	1	auth	User 1 Logged-in
837	2021-11-29 19:46:09	127.0.0.1	69	auth	User 69 Logged-in
838	2021-11-29 21:14:25	127.0.0.1	69	auth	User 69 Logged-in
839	2021-11-29 21:16:07	127.0.0.1	69	auth	Group 40 created
840	2021-11-30 13:46:55	127.0.0.1	1	auth	User 1 Logged-in
841	2021-11-30 14:10:39	127.0.0.1	1	auth	User 1 Logged-out
842	2021-11-30 14:20:05	127.0.0.1	1	auth	User 1 Logged-in
843	2021-11-30 14:21:46	127.0.0.1	1	auth	User 1 Logged-out
844	2021-11-30 14:21:56	127.0.0.1	1	auth	User 1 Logged-in
851	2021-11-30 16:07:39	127.0.0.1	\N	auth	User 89 Registered
852	2021-11-30 16:11:34	127.0.0.1	\N	auth	User 89 Verification email sent
853	2021-11-30 16:11:58	127.0.0.1	89	auth	User 89 Logged-in
854	2021-11-30 16:41:47	127.0.0.1	1	auth	User 1 Logged-in
855	2021-11-30 17:33:03	127.0.0.1	1	auth	User 1 Logged-in
856	2021-11-30 17:33:16	127.0.0.1	69	auth	User 1 is impersonating 69
857	2021-11-30 17:39:01	127.0.0.1	1	auth	User 1 Logged-in
926	2021-12-03 18:40:18	127.0.0.1	92	auth	User 92 Logged-out
724	2021-11-09 19:32:30	127.0.0.1	75	auth	User 75 Logged-in
736	2021-11-11 20:50:37	127.0.0.1	1	auth	User 1 Logged-in
737	2021-11-12 12:21:20	127.0.0.1	1	auth	User 1 Logged-in
738	2021-11-12 13:00:54	127.0.0.1	1	auth	User 1 Logged-out
739	2021-11-12 13:10:53	127.0.0.1	1	auth	User 1 Logged-in
781	2021-11-23 20:12:08	127.0.0.1	1	auth	User 1 Logged-in
801	2021-11-25 20:52:29	127.0.0.1	1	auth	User 1 Logged-in
845	2021-11-30 14:47:34	127.0.0.1	69	auth	User 1 is impersonating 69
846	2021-11-30 14:49:25	127.0.0.1	69	auth	User 69 Logged-out
847	2021-11-30 14:49:38	127.0.0.1	1	auth	User 1 Logged-in
848	2021-11-30 15:26:52	127.0.0.1	86	auth	User 86 Logged-out
849	2021-11-30 15:26:59	127.0.0.1	1	auth	User 1 Logged-in
850	2021-11-30 15:27:23	127.0.0.1	69	auth	User 1 is impersonating 69
927	2021-12-03 19:32:19	127.0.0.1	\N	auth	User 92 Password reset
928	2021-12-03 19:50:24	127.0.0.1	92	auth	User 92 Logged-out
929	2021-12-03 19:50:41	127.0.0.1	\N	auth	User 92 Password reset
930	2021-12-03 19:51:17	127.0.0.1	\N	auth	User 92 Password reset
931	2021-12-03 19:55:36	127.0.0.1	\N	auth	User 92 Password reset
932	2021-12-03 19:55:48	127.0.0.1	\N	auth	User 92 Password reset
219	2020-12-02 09:50:49	127.0.0.1	1	auth	User 1 Logged-in
933	2021-12-03 20:01:49	127.0.0.1	92	auth	User 92 Logged-in
934	2021-12-03 20:20:31	127.0.0.1	1	auth	User 1 Password reset
222	2020-12-03 10:51:33	127.0.0.1	1	auth	User 1 Logged-in
223	2020-12-03 11:03:56	127.0.0.1	1	auth	User 1 Logged-in
224	2020-12-03 11:20:30	127.0.0.1	1	auth	User 1 Logged-out
225	2020-12-03 11:20:45	127.0.0.1	1	auth	User 1 Logged-in
226	2020-12-03 14:31:39	127.0.0.1	1	auth	User 1 Logged-in
227	2020-12-03 15:39:16	127.0.0.1	\N	auth	User 26 Registered
228	2020-12-03 15:41:19	127.0.0.1	\N	auth	User 26 Verification email sent
229	2020-12-03 16:18:44	127.0.0.1	1	auth	User 1 Logged-in
935	2021-12-03 20:26:22	127.0.0.1	92	auth	User 92 Logged-out
231	2020-12-03 18:57:12	127.0.0.1	\N	auth	User 27 Registered
232	2020-12-03 18:57:31	127.0.0.1	\N	auth	User 27 Verification email sent
233	2020-12-03 21:10:56	127.0.0.1	1	auth	User 1 Logged-in
234	2020-12-04 06:36:28	127.0.0.1	1	auth	User 1 Logged-in
235	2020-12-04 06:38:19	127.0.0.1	1	auth	User 1 Logged-in
236	2020-12-04 14:42:21	127.0.0.1	1	auth	User 1 Logged-in
936	2021-12-03 20:26:45	127.0.0.1	92	auth	User 92 Logged-in
238	2020-12-17 12:39:23	127.0.0.1	\N	auth	User 15 Password reset
937	2021-12-03 20:33:21	127.0.0.1	82	auth	User 92 is impersonating 82
938	2021-12-03 21:29:45	127.0.0.1	82	auth	User 82 Logged-out
241	2021-02-26 11:09:53	127.0.0.1	\N	auth	User 28 Registered
242	2021-02-26 11:12:19	127.0.0.1	1	auth	User 1 Logged-in
243	2021-02-26 11:14:00	127.0.0.1	1	auth	User 1 Logged-in
244	2021-02-26 15:02:06	127.0.0.1	\N	auth	User 29 Registered
245	2021-02-26 15:03:02	127.0.0.1	\N	auth	User 29 Verification email sent
246	2021-03-01 12:39:59	127.0.0.1	1	auth	User 1 Logged-in
247	2021-03-03 13:16:33	127.0.0.1	1	auth	User 1 Logged-out
248	2021-03-03 13:16:42	127.0.0.1	\N	auth	User 14 Password reset
249	2021-03-03 13:30:17	127.0.0.1	\N	auth	User 1 Password reset
250	2021-03-04 10:39:25	127.0.0.1	\N	auth	User 19 Password reset
939	2021-12-03 21:30:03	127.0.0.1	92	auth	User 92 Logged-in
252	2021-03-10 16:33:09	127.0.0.1	\N	auth	User 13 Password reset
253	2021-03-16 14:21:00	127.0.0.1	1	auth	User 1 Logged-in
254	2021-03-17 10:51:29	127.0.0.1	\N	auth	User 30 Registered
255	2021-03-17 10:52:21	127.0.0.1	\N	auth	User 30 Verification email sent
256	2021-03-17 17:27:50	127.0.0.1	1	auth	User 1 Logged-in
940	2021-12-03 21:31:56	127.0.0.1	82	auth	User 92 is impersonating 82
258	2021-03-17 17:51:30	127.0.0.1	\N	auth	User 31 Registered
259	2021-03-17 17:53:51	127.0.0.1	\N	auth	User 31 Verification email sent
941	2021-12-03 21:32:42	127.0.0.1	82	auth	User 82 Logged-out
261	2021-03-18 08:43:57	127.0.0.1	\N	auth	User 32 Registered
262	2021-03-18 08:51:19	127.0.0.1	\N	auth	User 32 Verification email sent
942	2021-12-03 21:32:59	127.0.0.1	92	auth	User 92 Logged-in
943	2021-12-03 21:35:51	127.0.0.1	82	auth	User 92 is impersonating 82
944	2021-12-03 21:38:45	127.0.0.1	82	auth	User 82 Logged-out
945	2021-12-03 21:39:03	127.0.0.1	92	auth	User 92 Logged-in
946	2021-12-06 11:09:16	127.0.0.1	\N	auth	User 92 Password reset
947	2021-12-06 11:13:17	127.0.0.1	92	auth	User 92 Logged-in
948	2021-12-06 11:18:26	127.0.0.1	82	auth	User 92 is impersonating 82
949	2021-12-06 11:21:13	127.0.0.1	82	auth	User 82 Logged-out
271	2021-03-18 15:24:11	127.0.0.1	\N	auth	User 33 Registered
272	2021-03-18 15:24:30	127.0.0.1	\N	auth	User 33 Verification email sent
273	2021-03-18 15:26:32	127.0.0.1	\N	auth	User 34 Registered
274	2021-03-18 15:28:38	127.0.0.1	\N	auth	User 35 Registered
275	2021-03-18 15:59:42	127.0.0.1	\N	auth	User 37 Registered
276	2021-03-18 16:00:27	127.0.0.1	\N	auth	User 38 Registered
950	2021-12-06 11:21:29	127.0.0.1	92	auth	User 92 Logged-in
278	2021-03-18 16:02:43	127.0.0.1	\N	auth	User 39 Registered
279	2021-03-18 16:04:40	127.0.0.1	\N	auth	User 39 Verification email sent
951	2021-12-06 14:35:27	127.0.0.1	1	auth	User 1 Logged-in
281	2021-03-18 16:07:47	127.0.0.1	\N	auth	User 40 Registered
282	2021-03-18 16:10:59	127.0.0.1	\N	auth	User 41 Registered
283	2021-03-18 16:13:09	127.0.0.1	\N	auth	User 41 Verification email sent
952	2021-12-06 14:37:16	127.0.0.1	1	auth	User 1 Logged-out
953	2021-12-06 14:38:03	127.0.0.1	1	auth	User 1 Logged-in
286	2021-03-18 16:18:38	127.0.0.1	\N	auth	User 42 Registered
954	2021-12-06 14:40:23	127.0.0.1	1	auth	User 1 Logged-out
288	2021-03-18 16:20:13	127.0.0.1	\N	auth	User 42 Verification email sent
955	2021-12-06 14:40:36	127.0.0.1	1	auth	User 1 Logged-in
956	2021-12-06 14:42:00	127.0.0.1	1	auth	User 1 Logged-out
291	2021-03-18 16:41:59	127.0.0.1	1	auth	User 1 Logged-out
292	2021-03-18 16:42:34	127.0.0.1	1	auth	User 1 Logged-in
293	2021-03-18 16:43:14	127.0.0.1	1	auth	User 1 Logged-out
294	2021-03-18 16:44:08	127.0.0.1	1	auth	User 1 Logged-in
295	2021-03-18 16:44:56	127.0.0.1	1	auth	User 1 Logged-out
725	2021-11-09 19:49:42	127.0.0.1	69	auth	Group 38 created
726	2021-11-09 19:50:02	127.0.0.1	76	auth	Group 39 created
740	2021-11-12 13:14:56	127.0.0.1	69	auth	User 69 Logged-in
741	2021-11-12 13:16:11	127.0.0.1	1	auth	User 1 Logged-out
742	2021-11-12 13:19:55	127.0.0.1	1	auth	User 1 Logged-in
301	2021-03-18 16:54:31	127.0.0.1	1	auth	User 1 Logged-in
743	2021-11-12 14:58:31	127.0.0.1	71	auth	User 71 Logged-in
744	2021-11-12 14:59:12	127.0.0.1	82	auth	User 82 Logged-in
745	2021-11-12 15:01:39	127.0.0.1	76	auth	User 76 Logged-in
305	2021-03-18 18:59:40	127.0.0.1	1	auth	User 1 Logged-in
746	2021-11-12 15:03:28	127.0.0.1	81	auth	User 81 Logged-in
747	2021-11-12 15:12:02	127.0.0.1	1	auth	User 1 Logged-out
308	2021-03-18 22:05:45	127.0.0.1	\N	auth	User 43 Registered
309	2021-03-18 22:06:38	127.0.0.1	\N	auth	User 43 Verification email sent
748	2021-11-12 15:13:09	127.0.0.1	1	auth	User 1 Logged-in
750	2021-11-12 15:14:11	127.0.0.1	1	auth	User 1 Logged-in
751	2021-11-12 15:18:28	127.0.0.1	\N	auth	User 85 Registered
752	2021-11-12 15:33:16	127.0.0.1	76	auth	User 76 Logged-out
753	2021-11-12 15:33:45	127.0.0.1	76	auth	User 76 Logged-in
754	2021-11-12 15:41:45	127.0.0.1	85	auth	User 85 Logged-in
755	2021-11-16 19:27:09	127.0.0.1	69	auth	User 69 Logged-in
756	2021-11-17 11:29:56	127.0.0.1	1	auth	User 1 Logged-in
757	2021-11-17 11:31:26	127.0.0.1	1	auth	User 1 Logged-out
782	2021-11-24 11:36:10	127.0.0.1	1	auth	User 1 Logged-in
802	2021-11-26 00:41:47	127.0.0.1	1	auth	User 1 Logged-in
321	2021-03-19 09:34:49	127.0.0.1	\N	auth	User 44 Registered
803	2021-11-26 00:57:46	127.0.0.1	69	auth	User 1 is impersonating 69
858	2021-11-30 18:15:06	127.0.0.1	1	auth	User 1 Logged-out
859	2021-11-30 18:23:43	127.0.0.1	69	auth	User 69 Logged-in
860	2021-11-30 18:25:24	127.0.0.1	1	auth	User 1 Logged-in
861	2021-11-30 18:25:39	127.0.0.1	1	auth	User 1 Logged-out
862	2021-11-30 18:25:48	127.0.0.1	1	auth	User 1 Logged-in
863	2021-11-30 18:26:57	127.0.0.1	1	auth	User 1 Logged-out
864	2021-11-30 18:30:03	127.0.0.1	1	auth	User 1 Logged-in
865	2021-11-30 18:31:04	127.0.0.1	1	auth	User 1 Logged-out
866	2021-11-30 18:31:22	127.0.0.1	86	auth	User 86 Logged-in
867	2021-11-30 18:31:52	127.0.0.1	86	auth	User 86 Logged-out
868	2021-11-30 18:32:54	127.0.0.1	86	auth	User 86 Logged-in
869	2021-11-30 18:56:26	127.0.0.1	89	auth	User 89 Logged-out
335	2021-03-19 10:51:58	127.0.0.1	\N	auth	User 47 Registered
336	2021-03-19 10:53:08	127.0.0.1	\N	auth	User 47 Verification email sent
870	2021-11-30 18:56:34	127.0.0.1	89	auth	User 89 Logged-in
871	2021-11-30 19:43:15	127.0.0.1	89	auth	User 89 Logged-out
339	2021-03-19 11:04:35	127.0.0.1	\N	auth	User 48 Registered
340	2021-03-19 11:05:15	127.0.0.1	\N	auth	User 48 Verification email sent
872	2021-11-30 19:54:25	127.0.0.1	89	auth	User 89 Logged-in
873	2021-11-30 20:03:11	127.0.0.1	1	auth	User 1 Logged-in
874	2021-11-30 20:13:21	127.0.0.1	86	auth	User 86 Logged-out
875	2021-11-30 20:13:28	127.0.0.1	1	auth	User 1 Logged-in
876	2021-11-30 20:13:39	127.0.0.1	82	auth	User 1 is impersonating 82
957	2021-12-06 14:43:09	127.0.0.1	1	auth	User 1 Logged-in
958	2021-12-06 14:44:05	127.0.0.1	74	auth	User 1 is impersonating 74
959	2021-12-06 14:51:47	127.0.0.1	75	auth	User 1 is impersonating 75
960	2021-12-06 15:01:51	127.0.0.1	74	auth	User 1 is impersonating 74
350	2021-03-19 13:56:13	127.0.0.1	1	auth	User 1 Logged-in
961	2021-12-06 15:22:24	127.0.0.1	70	auth	User 1 is impersonating 70
962	2021-12-06 15:51:49	127.0.0.1	92	auth	User 92 Logged-in
982	2021-12-06 20:53:19	127.0.0.1	92	auth	Group 43 created
354	2021-03-19 14:05:14	127.0.0.1	\N	auth	User 7 Password reset
983	2021-12-06 20:56:50	127.0.0.1	82	auth	User 92 is impersonating 82
984	2021-12-06 20:57:30	127.0.0.1	82	auth	User 82 Logged-out
985	2021-12-06 20:57:40	127.0.0.1	92	auth	User 92 Logged-in
986	2021-12-06 20:58:14	127.0.0.1	82	auth	User 92 is impersonating 82
987	2021-12-06 21:01:31	127.0.0.1	82	auth	User 82 Logged-out
988	2021-12-06 21:01:43	127.0.0.1	92	auth	User 92 Logged-in
989	2021-12-06 21:02:57	127.0.0.1	92	auth	Group 44 created
362	2021-03-19 14:19:07	127.0.0.1	\N	auth	User 7 Password reset
363	2021-03-19 14:26:03	127.0.0.1	\N	auth	User 21 Password reset
990	2021-12-06 21:05:07	127.0.0.1	82	auth	User 92 is impersonating 82
365	2021-03-19 16:01:32	127.0.0.1	1	auth	User 1 Logged-in
991	2021-12-07 04:11:50	127.0.0.1	1	auth	User 1 Logged-in
367	2021-03-21 14:30:51	127.0.0.1	\N	auth	User 50 Registered
368	2021-03-21 14:32:37	127.0.0.1	\N	auth	User 41 Password reset
992	2021-12-07 16:07:04	127.0.0.1	\N	auth	User 93 Registered
993	2021-12-07 16:07:27	127.0.0.1	\N	auth	User 93 Verification email sent
995	2021-12-07 16:33:30	127.0.0.1	92	auth	User 92 Logged-in
373	2021-03-22 07:27:18	127.0.0.1	\N	auth	User 50 Verification email sent
1013	2021-12-07 18:27:19	127.0.0.1	72	auth	User 1 is impersonating 72
375	2021-03-22 08:09:55	127.0.0.1	\N	auth	User 50 Password reset
1014	2021-12-07 18:29:18	127.0.0.1	86	auth	User 1 is impersonating 86
727	2021-11-09 20:09:05	127.0.0.1	70	auth	User 70 Logged-out
392	2021-03-22 09:14:39	127.0.0.1	\N	auth	User 30 Password reset
393	2021-03-22 09:15:44	127.0.0.1	\N	auth	User 50 Password reset
728	2021-11-09 20:09:37	127.0.0.1	70	auth	User 70 Logged-in
729	2021-11-09 20:13:10	127.0.0.1	70	auth	User 70 Logged-out
730	2021-11-09 20:20:23	127.0.0.1	80	auth	User 80 Logged-out
749	2021-11-12 15:13:50	127.0.0.1	1	auth	User 1 Logged-out
758	2021-11-17 12:27:45	127.0.0.1	1	auth	User 1 Logged-in
759	2021-11-18 14:21:31	127.0.0.1	1	auth	User 1 Logged-in
783	2021-11-24 13:04:12	127.0.0.1	1	auth	User 1 Logged-out
784	2021-11-24 13:04:31	127.0.0.1	1	auth	User 1 Logged-in
402	2021-03-22 10:35:00	127.0.0.1	\N	auth	User 37 Password reset
785	2021-11-24 13:12:18	127.0.0.1	69	auth	User 69 Logged-in
786	2021-11-24 13:12:26	127.0.0.1	1	auth	User 1 Logged-out
787	2021-11-24 13:13:45	127.0.0.1	1	auth	User 1 Logged-in
406	2021-03-22 11:24:47	127.0.0.1	\N	auth	User 51 Registered
407	2021-03-22 11:25:25	127.0.0.1	\N	auth	User 51 Verification email sent
804	2021-11-26 12:52:28	127.0.0.1	1	auth	User 1 Logged-in
805	2021-11-26 15:13:46	127.0.0.1	1	auth	User 1 Logged-in
410	2021-03-22 14:25:26	127.0.0.1	1	auth	User 1 Logged-in
806	2021-11-26 17:50:36	127.0.0.1	1	auth	User 1 Logged-in
807	2021-11-26 18:42:42	127.0.0.1	1	auth	User 1 Logged-out
808	2021-11-26 18:43:21	127.0.0.1	1	auth	User 1 Logged-in
809	2021-11-26 18:50:42	127.0.0.1	1	auth	User 1 Logged-out
415	2021-03-22 14:59:44	127.0.0.1	\N	auth	User 41 Password reset
810	2021-11-26 18:51:23	127.0.0.1	1	auth	User 1 Logged-in
811	2021-11-26 19:02:26	127.0.0.1	1	auth	User 1 Logged-out
812	2021-11-26 19:04:43	127.0.0.1	71	auth	User 71 Logged-in
813	2021-11-26 19:07:08	127.0.0.1	1	auth	User 1 Logged-in
814	2021-11-26 19:07:40	127.0.0.1	69	auth	User 69 Logged-in
421	2021-03-22 15:52:06	127.0.0.1	\N	auth	User 52 Registered
422	2021-03-22 15:52:53	127.0.0.1	\N	auth	User 20 Password reset
423	2021-03-22 16:02:32	127.0.0.1	\N	auth	User 53 Registered
424	2021-03-22 16:04:15	127.0.0.1	\N	auth	User 53 Verification email sent
815	2021-11-26 19:08:32	127.0.0.1	1	auth	User 1 Logged-out
823	2021-11-26 19:32:21	127.0.0.1	\N	auth	User 88 Registered
824	2021-11-26 19:32:50	127.0.0.1	\N	auth	User 88 Verification email sent
877	2021-11-30 20:43:59	127.0.0.1	89	auth	User 89 Logged-out
900	2021-12-01 22:16:35	127.0.0.1	1	auth	User 1 Logged-in
963	2021-12-06 18:49:22	127.0.0.1	92	auth	Group 41 created
964	2021-12-06 19:21:05	127.0.0.1	92	auth	Group 42 created
965	2021-12-06 19:35:23	127.0.0.1	1	auth	User 1 Logged-in
966	2021-12-06 19:53:00	127.0.0.1	71	auth	User 1 is impersonating 71
967	2021-12-06 19:59:10	127.0.0.1	1	auth	User 1 Logged-in
435	2021-03-23 07:24:44	127.0.0.1	\N	auth	User 52 Verification email sent
968	2021-12-06 19:59:37	127.0.0.1	1	auth	User 1 Logged-out
969	2021-12-06 19:59:49	127.0.0.1	1	auth	User 1 Logged-in
438	2021-03-23 08:49:51	127.0.0.1	\N	auth	User 54 Registered
439	2021-03-23 08:50:54	127.0.0.1	\N	auth	User 54 Verification email sent
970	2021-12-06 20:00:47	127.0.0.1	1	auth	User 1 Logged-out
971	2021-12-06 20:01:05	127.0.0.1	1	auth	User 1 Logged-in
972	2021-12-06 20:01:22	127.0.0.1	1	auth	User 1 Logged-out
973	2021-12-06 20:01:35	127.0.0.1	1	auth	User 1 Logged-in
974	2021-12-06 20:02:02	127.0.0.1	1	auth	User 1 Logged-in
975	2021-12-06 20:02:31	127.0.0.1	1	auth	User 1 Logged-out
976	2021-12-06 20:03:00	127.0.0.1	1	auth	User 1 Logged-in
977	2021-12-06 20:05:24	127.0.0.1	1	auth	User 1 Logged-out
978	2021-12-06 20:05:48	127.0.0.1	1	auth	User 1 Logged-in
979	2021-12-06 20:06:24	127.0.0.1	1	auth	User 1 Logged-out
980	2021-12-06 20:06:55	127.0.0.1	1	auth	User 1 Logged-in
981	2021-12-06 20:07:20	127.0.0.1	71	auth	User 1 is impersonating 71
996	2021-12-07 16:37:33	127.0.0.1	92	auth	Group 45 created
997	2021-12-07 17:03:40	127.0.0.1	82	auth	User 92 is impersonating 82
998	2021-12-07 17:03:52	127.0.0.1	82	auth	User 82 Logged-out
999	2021-12-07 17:04:10	127.0.0.1	92	auth	User 92 Logged-in
1000	2021-12-07 17:05:47	127.0.0.1	92	auth	Group 46 created
1001	2021-12-07 17:08:26	127.0.0.1	92	auth	Group 47 created
1002	2021-12-07 17:11:09	127.0.0.1	92	auth	Group 48 created
1003	2021-12-07 17:13:41	127.0.0.1	92	auth	Group 49 created
1004	2021-12-07 17:20:01	127.0.0.1	92	auth	Group 50 created
1005	2021-12-07 17:28:33	127.0.0.1	92	auth	Group 51 created
1006	2021-12-07 17:32:42	127.0.0.1	82	auth	User 92 is impersonating 82
1007	2021-12-07 17:35:09	127.0.0.1	82	auth	User 82 Logged-out
1008	2021-12-07 17:35:24	127.0.0.1	92	auth	User 92 Logged-in
1009	2021-12-07 17:37:47	127.0.0.1	82	auth	User 92 is impersonating 82
1010	2021-12-07 17:49:41	127.0.0.1	1	auth	User 1 Logged-in
467	2021-03-23 14:15:58	127.0.0.1	\N	auth	User 55 Registered
468	2021-03-23 14:16:27	127.0.0.1	\N	auth	User 55 Verification email sent
1011	2021-12-07 17:50:04	127.0.0.1	72	auth	User 1 is impersonating 72
1012	2021-12-07 18:17:59	127.0.0.1	71	auth	User 1 is impersonating 71
483	2021-03-23 17:14:01	127.0.0.1	\N	auth	User 56 Registered
484	2021-03-23 17:16:13	127.0.0.1	\N	auth	User 57 Registered
731	2021-11-09 21:52:59	127.0.0.1	82	auth	User 82 Logged-in
760	2021-11-20 13:44:36	127.0.0.1	1	auth	User 1 Logged-in
761	2021-11-21 18:53:38	127.0.0.1	1	auth	User 1 Logged-in
762	2021-11-22 12:43:58	127.0.0.1	1	auth	User 1 Logged-in
763	2021-11-22 13:02:15	127.0.0.1	1	auth	User 1 Logged-out
493	2021-03-26 11:17:12	127.0.0.1	1	auth	User 1 Logged-in
764	2021-11-22 13:03:39	127.0.0.1	82	auth	User 82 Logged-in
765	2021-11-22 13:41:28	127.0.0.1	1	auth	User 1 Logged-in
766	2021-11-22 13:56:51	127.0.0.1	1	auth	User 1 Logged-out
767	2021-11-22 14:02:17	127.0.0.1	82	auth	User 82 Logged-out
768	2021-11-22 14:08:43	127.0.0.1	1	auth	User 1 Logged-in
769	2021-11-22 14:09:10	127.0.0.1	1	auth	User 1 Logged-out
770	2021-11-22 14:09:37	127.0.0.1	1	auth	User 1 Logged-in
771	2021-11-22 14:10:01	127.0.0.1	1	auth	User 1 Logged-out
772	2021-11-22 14:11:01	127.0.0.1	1	auth	User 1 Logged-in
773	2021-11-22 14:11:20	127.0.0.1	1	auth	User 1 Logged-out
774	2021-11-22 14:11:51	127.0.0.1	1	auth	User 1 Logged-in
788	2021-11-24 14:18:29	127.0.0.1	1	auth	User 1 Logged-out
789	2021-11-24 14:19:30	127.0.0.1	1	auth	User 1 Logged-in
790	2021-11-24 16:31:08	127.0.0.1	1	auth	User 1 Logged-in
791	2021-11-24 18:06:33	127.0.0.1	1	auth	User 1 Logged-in
816	2021-11-26 19:09:19	127.0.0.1	1	auth	User 1 Logged-in
817	2021-11-26 19:09:30	127.0.0.1	82	auth	User 82 Logged-in
818	2021-11-26 19:15:48	127.0.0.1	75	auth	User 75 Logged-in
819	2021-11-26 19:17:45	127.0.0.1	1	auth	User 1 Logged-out
821	2021-11-26 19:18:23	127.0.0.1	1	auth	User 1 Logged-in
515	2021-04-07 17:44:40	127.0.0.1	1	auth	User 1 Logged-in
822	2021-11-26 19:26:08	127.0.0.1	\N	auth	User 87 Registered
517	2021-04-13 16:46:35	127.0.0.1	\N	auth	User 58 Registered
518	2021-04-13 16:49:19	127.0.0.1	\N	auth	User 58 Verification email sent
878	2021-11-30 20:45:17	127.0.0.1	69	auth	User 69 Logged-in
520	2021-04-13 17:01:52	127.0.0.1	\N	auth	User 22 Password reset
521	2021-04-13 17:02:27	127.0.0.1	\N	auth	User 22 Password reset
879	2021-11-30 20:47:27	127.0.0.1	89	auth	User 89 Logged-in
880	2021-12-01 13:29:07	127.0.0.1	86	auth	User 86 Logged-in
881	2021-12-01 13:30:24	127.0.0.1	86	auth	User 86 Logged-out
882	2021-12-01 13:31:50	127.0.0.1	1	auth	User 1 Logged-in
526	2021-04-13 17:35:14	127.0.0.1	\N	auth	User 59 Registered
527	2021-04-13 17:35:51	127.0.0.1	\N	auth	User 59 Verification email sent
883	2021-12-01 19:59:13	127.0.0.1	86	auth	User 86 Logged-in
884	2021-12-01 19:59:46	127.0.0.1	86	auth	User 86 Logged-out
530	2021-04-13 18:50:03	127.0.0.1	1	auth	User 1 Logged-in
531	2021-04-14 09:13:52	127.0.0.1	\N	auth	User 52 Password reset
885	2021-12-01 19:59:55	127.0.0.1	86	auth	User 86 Logged-in
886	2021-12-01 20:00:02	127.0.0.1	86	auth	User 86 Logged-out
534	2021-04-14 09:39:50	127.0.0.1	\N	auth	User 4 Password reset
887	2021-12-01 20:00:22	127.0.0.1	86	auth	User 86 Logged-in
888	2021-12-01 20:00:38	127.0.0.1	86	auth	User 86 Logged-out
889	2021-12-01 20:00:47	127.0.0.1	1	auth	User 1 Logged-in
890	2021-12-01 20:03:16	127.0.0.1	82	auth	User 1 is impersonating 82
891	2021-12-01 20:03:35	127.0.0.1	82	auth	User 82 Logged-out
892	2021-12-01 20:04:57	127.0.0.1	1	auth	User 1 Logged-in
893	2021-12-01 20:05:24	127.0.0.1	82	auth	User 1 is impersonating 82
542	2021-04-14 11:10:41	127.0.0.1	1	auth	User 1 Logged-in
894	2021-12-01 20:06:06	127.0.0.1	82	auth	User 82 Logged-out
895	2021-12-01 20:06:17	127.0.0.1	1	auth	User 1 Logged-in
896	2021-12-01 20:09:08	127.0.0.1	1	auth	User 1 Logged-in
897	2021-12-01 20:09:16	127.0.0.1	69	auth	User 1 is impersonating 69
898	2021-12-01 21:47:00	127.0.0.1	1	auth	User 1 Logged-out
899	2021-12-01 21:47:13	127.0.0.1	86	auth	User 86 Logged-in
901	2021-12-02 14:15:26	127.0.0.1	1	auth	User 1 Logged-in
902	2021-12-02 14:17:01	127.0.0.1	1	auth	User 1 Logged-in
903	2021-12-02 14:17:24	127.0.0.1	69	auth	User 1 is impersonating 69
904	2021-12-02 14:37:38	127.0.0.1	\N	auth	User 90 Registered
905	2021-12-02 14:40:30	127.0.0.1	\N	auth	User 91 Registered
906	2021-12-02 14:43:51	127.0.0.1	69	auth	User 1 is impersonating 69
907	2021-12-02 15:34:35	127.0.0.1	69	auth	User 69 Logged-in
908	2021-12-02 15:49:14	127.0.0.1	69	auth	User 69 Logged-out
557	2021-04-14 15:10:35	127.0.0.1	\N	auth	User 58 Password reset
909	2021-12-02 15:49:26	127.0.0.1	1	auth	User 1 Logged-in
910	2021-12-02 15:50:17	127.0.0.1	82	auth	User 1 is impersonating 82
911	2021-12-02 15:50:54	127.0.0.1	82	auth	User 82 Logged-out
912	2021-12-02 15:51:06	127.0.0.1	1	auth	User 1 Logged-in
562	2021-04-14 15:27:17	127.0.0.1	\N	auth	User 58 Password reset
913	2021-12-02 18:06:59	127.0.0.1	1	auth	User 1 Logged-in
1015	2021-12-07 18:39:16	127.0.0.1	1	auth	User 1 Logged-out
565	2021-04-14 16:44:07	127.0.0.1	1	auth	User 1 Logged-in
566	2021-04-15 08:15:40	127.0.0.1	\N	auth	User 60 Registered
567	2021-04-15 08:16:23	127.0.0.1	\N	auth	User 60 Verification email sent
1016	2021-12-07 18:41:18	127.0.0.1	1	auth	User 1 Logged-in
1017	2021-12-07 18:44:27	127.0.0.1	1	auth	User 1 Logged-out
1018	2021-12-07 18:52:00	127.0.0.1	1	auth	User 1 Logged-in
571	2021-04-16 09:01:50	127.0.0.1	\N	auth	User 27 Password reset
1019	2021-12-07 19:34:02	127.0.0.1	1	auth	User 1 Logged-out
1020	2021-12-07 20:53:42	127.0.0.1	\N	auth	User 94 Registered
1021	2021-12-07 20:53:59	127.0.0.1	\N	auth	User 94 Verification email sent
1022	2021-12-07 20:54:04	127.0.0.1	94	auth	User 94 Logged-in
1023	2021-12-07 21:25:25	127.0.0.1	1	auth	User 1 Logged-in
1024	2021-12-09 14:22:16	127.0.0.1	1	auth	User 1 Logged-in
1025	2021-12-09 14:22:51	127.0.0.1	71	auth	User 1 is impersonating 71
580	2021-04-19 08:56:01	127.0.0.1	\N	auth	User 61 Registered
582	2021-04-19 15:19:55	127.0.0.1	1	auth	User 1 Logged-in
733	2021-11-10 21:00:46	127.0.0.1	1	auth	User 1 Logged-in
734	2021-11-10 21:11:11	127.0.0.1	69	auth	User 69 Logged-in
775	2021-11-22 15:07:23	127.0.0.1	71	auth	User 71 Logged-in
586	2021-04-20 14:31:44	127.0.0.1	\N	auth	User 31 Password reset
776	2021-11-22 19:13:34	127.0.0.1	1	auth	User 1 Logged-in
588	2021-04-20 14:46:11	127.0.0.1	1	auth	User 1 Logged-in
777	2021-11-22 19:16:32	127.0.0.1	1	auth	User 1 Logged-out
778	2021-11-22 19:16:46	127.0.0.1	1	auth	User 1 Logged-in
591	2021-04-20 15:39:43	127.0.0.1	1	auth	User 1 Logged-in
779	2021-11-23 12:27:14	127.0.0.1	1	auth	User 1 Logged-in
792	2021-11-25 01:04:10	127.0.0.1	1	auth	User 1 Logged-in
825	2021-11-26 19:33:20	127.0.0.1	88	auth	User 88 Logged-in
595	2021-04-20 15:45:32	127.0.0.1	\N	auth	User 62 Registered
826	2021-11-26 19:35:10	127.0.0.1	82	auth	User 1 is impersonating 82
597	2021-04-20 15:48:49	127.0.0.1	\N	auth	User 63 Registered
827	2021-11-26 19:36:13	127.0.0.1	71	auth	User 71 Logged-in
828	2021-11-26 20:25:05	127.0.0.1	82	auth	User 82 Logged-out
829	2021-11-26 20:25:12	127.0.0.1	1	auth	User 1 Logged-in
830	2021-11-26 21:31:47	127.0.0.1	82	auth	User 1 is impersonating 82
831	2021-11-26 21:32:18	127.0.0.1	82	auth	User 82 Logged-out
603	2021-04-20 16:20:17	127.0.0.1	\N	auth	User 64 Registered
604	2021-04-20 16:25:34	127.0.0.1	\N	auth	User 65 Registered
605	2021-04-20 16:28:12	127.0.0.1	\N	auth	User 65 Verification email sent
832	2021-11-26 21:32:26	127.0.0.1	1	auth	User 1 Logged-in
833	2021-11-26 21:35:53	127.0.0.1	82	auth	User 82 Logged-in
834	2021-11-26 21:37:33	127.0.0.1	82	auth	User 1 is impersonating 82
914	2021-12-02 18:26:40	127.0.0.1	70	auth	User 1 is impersonating 70
915	2021-12-02 18:51:56	127.0.0.1	1	auth	User 1 Logged-in
916	2021-12-02 19:18:54	127.0.0.1	\N	auth	User 92 Registered
612	2021-04-20 16:52:20	127.0.0.1	\N	auth	User 66 Registered
613	2021-04-20 16:52:35	127.0.0.1	\N	auth	User 66 Verification email sent
614	2021-04-20 17:01:39	127.0.0.1	1	auth	User 1 Logged-in
917	2021-12-02 19:29:47	127.0.0.1	\N	auth	User 92 Verification email sent
616	2021-04-20 17:21:59	127.0.0.1	\N	auth	User 66 Password reset
918	2021-12-02 19:30:05	127.0.0.1	92	auth	User 92 Logged-in
618	2021-04-21 15:10:04	127.0.0.1	\N	auth	User 61 Password reset
919	2021-12-02 19:34:39	127.0.0.1	1	auth	User 1 Logged-in
920	2021-12-02 19:38:58	127.0.0.1	\N	auth	User 76 Password reset
621	2021-04-25 19:40:26	127.0.0.1	1	auth	User 1 Logged-in
622	2021-05-20 04:14:47	177.182.92.23	1	auth	User 1 Logged-in
623	2021-05-31 16:02:54	189.48.190.211	\N	auth	User 67 Registered
624	2021-05-31 16:04:47	189.48.190.211	\N	auth	User 67 Verification email sent
921	2021-12-02 23:39:53	127.0.0.1	1	auth	User 1 Logged-in
922	2021-12-03 17:57:37	127.0.0.1	\N	auth	User 92 Password reset
627	2021-07-23 19:22:33	127.0.0.1	1	auth	User 1 Logged-in
628	2021-07-26 17:10:47	127.0.0.1	1	auth	User 1 Logged-in
629	2021-07-26 18:45:07	127.0.0.1	1	auth	User 1 Logged-in
630	2021-08-17 15:58:48	127.0.0.1	1	auth	User 1 Logged-in
631	2021-08-18 06:04:45	127.0.0.1	1	auth	User 1 Logged-in
632	2021-08-18 22:15:17	127.0.0.1	1	auth	User 1 Logged-in
633	2021-08-19 00:42:59	127.0.0.1	1	auth	User 1 Logged-in
634	2021-08-19 12:49:08	127.0.0.1	1	auth	User 1 Logged-in
635	2021-08-19 15:42:03	127.0.0.1	1	auth	User 1 Logged-in
636	2021-08-23 14:11:40	127.0.0.1	1	auth	User 1 Logged-in
637	2021-08-25 23:18:24	127.0.0.1	1	auth	User 1 Logged-in
638	2021-08-30 05:13:46	127.0.0.1	1	auth	User 1 Logged-in
639	2021-08-30 05:14:32	127.0.0.1	1	auth	Group 26 created
640	2021-08-30 05:15:46	127.0.0.1	1	auth	Group 27 created
641	2021-09-01 18:16:16	127.0.0.1	1	auth	User 1 Logged-in
642	2021-09-01 18:17:03	127.0.0.1	1	auth	Group 28 created
643	2021-09-01 19:28:11	127.0.0.1	1	auth	User 1 Logged-in
644	2021-09-02 20:17:43	127.0.0.1	1	auth	User 1 Logged-in
645	2021-09-02 22:14:29	127.0.0.1	1	auth	Group 29 created
646	2021-09-03 20:56:32	127.0.0.1	1	auth	Group 30 created
647	2021-09-03 20:59:47	127.0.0.1	1	auth	Group 31 created
648	2021-09-08 01:16:54	127.0.0.1	1	auth	User 1 Logged-in
649	2021-09-08 01:18:14	127.0.0.1	1	auth	Group 32 created
650	2021-09-08 01:20:49	127.0.0.1	1	auth	Group 33 created
651	2021-09-10 14:25:03	127.0.0.1	1	auth	Group 34 created
652	2021-09-13 13:57:14	127.0.0.1	1	auth	Group 35 created
653	2021-09-13 13:58:45	127.0.0.1	1	auth	Group 36 created
654	2021-09-13 20:02:51	127.0.0.1	1	auth	Group 37 created
655	2021-09-14 19:54:05	127.0.0.1	1	auth	User 1 Logged-out
656	2021-09-14 19:54:15	127.0.0.1	1	auth	User 1 Logged-in
657	2021-09-14 19:57:15	127.0.0.1	\N	auth	User 68 Registered
923	2021-12-03 18:15:09	127.0.0.1	\N	auth	User 1 Password reset
659	2021-09-17 14:00:03	127.0.0.1	1	auth	User 1 Logged-in
660	2021-09-22 20:59:03	127.0.0.1	1	auth	User 1 Logged-in
661	2021-09-29 23:00:58	127.0.0.1	1	auth	User 1 Logged-in
662	2021-10-13 15:51:44	127.0.0.1	1	auth	User 1 Logged-in
663	2021-11-09 17:53:55	127.0.0.1	1	auth	User 1 Logged-in
924	2021-12-03 18:15:44	127.0.0.1	92	auth	User 92 Logged-out
925	2021-12-03 18:30:59	127.0.0.1	\N	auth	User 92 Password reset
1026	2021-12-09 18:59:53	127.0.0.1	71	auth	User 1 is impersonating 71
1027	2021-12-09 19:01:23	127.0.0.1	92	auth	User 1 is impersonating 92
1028	2021-12-09 19:02:07	127.0.0.1	76	auth	User 1 is impersonating 76
\.


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_group (id, role, description) FROM stdin;
1	admin	\N
2	dev	\N
3	eleicao_1	\N
4	eleicao_2	\N
5	eleicao_3	\N
6	eleicao_4	\N
7	eleicao_5	\N
8	eleicao_6	\N
9	eleicao_7	\N
10	eleicao_8	grupo de admin da eleicao de id = 8
11	eleicao_9	grupo de admin da eleicao de id = 9
12	eleicao_10	grupo de admin da eleicao de id = 10
13	eleicao_11	grupo de admin da eleicao de id = 11
14	eleicao_12	grupo de admin da eleicao de id = 12
15	eleicao_13	grupo de admin da eleicao de id = 13
16	eleicao_14	grupo de admin da eleicao de id = 14
17	eleicao_15	grupo de admin da eleicao de id = 15
18	eleicao_16	grupo de admin da eleicao de id = 16
19	eleicao_17	grupo de admin da eleicao de id = 17
20	eleicao_18	grupo de admin da eleicao de id = 18
21	eleicao_19	grupo de admin da eleicao de id = 19
22	eleicao_20	grupo de admin da eleicao de id = 20
23	eleicao_21	grupo de admin da eleicao de id = 21
24	eleicao_22	grupo de admin da eleicao de id = 22
25	eleicao_23	grupo de admin da eleicao de id = 23
26	eleicao_25	grupo de admin da eleicao de id = 25
27	eleicao_26	grupo de admin da eleicao de id = 26
28	eleicao_27	grupo de admin da eleicao de id = 27
29	eleicao_28	grupo de admin da eleicao de id = 28
30	eleicao_4	grupo de admin da eleicao de id = 4
31	eleicao_4	grupo de admin da eleicao de id = 4
32	eleicao_4	grupo de admin da eleicao de id = 4
33	eleicao_4	grupo de admin da eleicao de id = 4
34	eleicao_4	grupo de admin da eleicao de id = 4
35	eleicao_4	grupo de admin da eleicao de id = 4
36	eleicao_4	grupo de admin da eleicao de id = 4
37	eleicao_4	grupo de admin da eleicao de id = 4
38	eleicao_29	grupo de admin da eleicao de id = 29
39	eleicao_30	grupo de admin da eleicao de id = 30
40	eleicao_31	grupo de admin da eleicao de id = 31
41	eleicao_32	grupo de admin da eleicao de id = 32
42	eleicao_33	grupo de admin da eleicao de id = 33
43	eleicao_34	grupo de admin da eleicao de id = 34
44	eleicao_35	grupo de admin da eleicao de id = 35
45	eleicao_36	grupo de admin da eleicao de id = 36
46	eleicao_37	grupo de admin da eleicao de id = 37
47	eleicao_38	grupo de admin da eleicao de id = 38
48	eleicao_39	grupo de admin da eleicao de id = 39
49	eleicao_40	grupo de admin da eleicao de id = 40
50	eleicao_41	grupo de admin da eleicao de id = 41
51	eleicao_42	grupo de admin da eleicao de id = 42
\.


--
-- Data for Name: auth_membership; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_membership (id, user_id, group_id) FROM stdin;
1	1	1
175	82	43
4	1	2
176	92	43
101	1	38
102	69	38
178	92	44
180	84	45
181	70	45
182	89	45
183	92	45
17	1	3
185	82	46
186	92	46
188	82	47
189	92	47
191	82	48
192	92	48
194	82	49
195	92	49
197	82	50
198	92	50
125	86	40
34	1	15
126	69	40
127	72	40
128	85	40
129	71	40
130	89	1
200	82	51
201	92	51
203	94	1
50	1	20
51	1	26
52	1	27
53	1	28
54	1	29
55	1	30
56	1	31
57	1	32
59	1	33
60	1	34
61	1	35
62	1	36
63	1	37
64	1	6
67	71	1
68	69	1
69	70	1
70	84	1
71	72	1
72	83	1
75	74	1
76	81	1
77	75	1
78	80	1
79	79	1
80	76	1
81	78	1
82	77	1
150	92	1
151	92	2
159	86	39
160	1	39
161	69	39
162	75	39
163	89	39
164	76	39
165	92	39
170	90	41
172	92	42
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_permission (id, group_id, name, table_name, record_id) FROM stdin;
1	2	impersonate	auth_user	0
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_user (id, first_name, last_name, email, username, entidade, mothers_name, "gênero", cor, data_nascimento, identidade, arquivo_identidade, escolaridade, password, registration_key, reset_password_key, registration_id, is_active, created_on, created_by, modified_on, modified_by, municipio, foto) FROM stdin;
79	Michel	Cândido	michelalaphoenix@hotmail.com	10767621662	12	Michel Vercélis Cândido	Masculino	Parda	1990-02-28	MG16297920	\N	Superior Completo	Mvc021324				T	2021-11-09 18:21:15	\N	2021-11-09 18:21:50	71	3106200	
76	Elenir Rios dos Santos	\N	elenirriossantos31@gmail.com	85907790687	7	Maria da Conceição dos Santos	\N	\N	1968-09-20	278536	\N	Superior Completo	Manuela123				T	2021-11-09 18:18:36	\N	2021-12-02 19:39:43	\N	3106200	
70	Cláudia	Ribeiro	claudia.ribeiro@social.mg.gov.br	67999379620	\N	Ivone	Feminino	Branca	1967-03-23	3981502	\N	Médio Completo	123456				T	2021-11-09 18:12:41	\N	2021-11-09 18:39:19	\N	3106200	
80	Fernanda Márcia de Lima Jales	\N	fernanda.lima@social.mg.gov.br	06949572670	8	FERNANDA MARCIA DE LIMA JALES	Feminino	Branca	1986-03-08	7142478	\N	Superior Completo	Sophia123				T	2021-11-09 18:22:17	\N	2021-11-09 18:23:19	71	3101102	
71	Wallison Francisco	\N	wallison.francisco@social.mg.gov.br	08292171690	12	Maria Augusta	Masculino	\N	1988-07-04	7817372	\N	Superior Completo	34537786				T	2021-11-09 18:13:00	\N	2021-11-09 18:13:42	1	3106200	
78	Elizabeth Regina	Rainha Elizabeth	luizamascarello@gmail.com	08873235611	12	Rainha Mãe	Feminino	Branca	1935-11-24	9999999	\N	Médio Completo	123456				T	2021-11-09 18:20:58	\N	2021-11-09 18:24:09	78	3100104	
87	Walbert Martins 	\N	walbert.martins@fiat.com	02737645603	\N	\N	\N	\N	\N	7331362	\N	\N	uemg@102030				T	2021-11-26 19:26:08	\N	2021-11-30 16:42:10	1	3157807	\N
77	Louis Allanic	\N	louis.allanic@social.mg.gov.br	96068868672	1	Jacqueline	Masculino	Não Desejo Informar	1980-01-01	010203	\N	Superior Completo	CEJUVEweb*				T	2021-11-09 18:20:47	\N	2021-11-09 18:21:44	71	3106200	
89	Claudia Rodrigues César	Cláudia	claudia.damc123456@gmail.com	87236192600	\N	\N	\N	\N	\N	MG4343130	\N	\N	Marina042731				T	2021-11-30 16:07:39	\N	2021-11-30 16:11:34	\N	3106200	\N
69	Alexandre Moreira Vertelo	\N	alexandre.vertelo@social.mg.gov.br	48508772653	\N	Conceição Moreira Vertelo	Masculino	Preta	1967-03-23	MG3.616.868	\N	Superior Completo	Hemo1989				T	2021-11-09 18:10:24	\N	2021-11-09 18:13:31	1	3106200	
85	Vânia Lúcia de Oliveira Guimarães	\N	vanialuciadeoliveiraguimaraes@social.mg.gov.br	58121269687	10	Evanildes Maria de O. Guimarães	Feminino	Branca	1965-10-02	M-3.953.284	\N	Superior Completo	@melina2021				T	2021-11-12 15:18:28	\N	2021-11-12 15:38:55	71	3106200	
92	Talles Roque de Freitas	Talles Roque de Freitas	talles.freitas@social.mg.gov.br	11343879620	\N	\N	\N	\N	\N	15913965	\N	\N	senhateste2		1638785357-89f96228-82f5-4841-b4f9-c633b03683f0		T	2021-12-02 19:18:54	\N	2021-12-06 11:09:16	\N	3106200	\N
90	Testudo Testudão	Testudo Testudão	teste@teste.com	34842352094	\N	\N	\N	\N	\N	4444	\N	\N	senhateste				T	2021-12-02 14:37:38	\N	2021-12-02 18:07:32	1	3105707	\N
72	Juliana de Melo Cordeiro	\N	jcnoivas@hotmail.com	04747761667	10	\N	Feminino	Branca	1977-06-27	MG 9 349041	\N	Superior Completo	Edu.030409				T	2021-11-09 18:13:25	\N	2021-11-09 18:15:44	71	3106200	
75	Beatriz Trindade	\N	beatrizetrindade@gmail.com	66350549600	\N	Odila trindade	Feminino	Preta	1966-04-11	3328369	\N	Superior Completo	MALBEC-116805				T	2021-11-09 18:16:02	\N	2021-11-09 18:16:23	71	3106200	
74	Paulo Henrique Martins	\N	paulo.martins@social.mg.gov.br	09909598647	1	Dircilene Aparecida Faria Martins	Masculino	Não Desejo Informar	1989-04-05	14377089	\N	Superior Incompleto	paulo713				T	2021-11-09 18:15:30	\N	2021-11-09 18:15:51	71	3106200	
81	Tânia Mara	\N	tania.farnese@social.mg.gov.br	59750650620	\N	Alice	Feminino	Parda	1963-08-05	3120906	\N	Superior Completo	mg123456				T	2021-11-09 18:30:12	\N	2021-11-09 18:38:24	\N	3106200	
83	Camila Felix Araujo 	\N	camila-felix-araujo@hotmail.com	10386663688	12	Sônia	Feminino	Parda	1990-02-07	99999999	\N	Superior Completo	camila123				T	2021-11-09 18:34:24	\N	2021-11-09 18:35:43	71	3106200	
84	christiane machado	\N	chrisbh2014@gmail.com	04068334609	2	christiane machado	Feminino	Branca	1978-09-05	mg10920489	\N	Superior Completo	eleicao				T	2021-11-09 18:34:34	\N	2021-11-09 18:35:50	71	3106200	
86	Alexandre Miguel de Andrade Souza	\N	wymored@outlook.com	11111111111	\N	\N	\N	\N	\N	7362444	\N	\N	egcawx				T	2021-11-25 13:08:24	\N	2021-11-25 13:09:39	\N	3106200	\N
1	Alexandre Miguel de Andrade Souza	\N	alexandremasbr@gmail.com	42137276291	4	Hilda de Andrade Souza	Masculino	Parda	1973-01-23	MG 7362442		Superior Completo	egcawx		1638559231-f9b708e5-0292-4957-bd1c-5feb02c45eb9	\N	T	2020-08-05 19:05:35	\N	2021-12-03 20:20:31	1	3106200	
82	Dirlene Ribeiro Lopes	Dirlene	dilene.lopes@social.mg.gov.br	00140084673	\N	Dalva Ribeiro Lopes	Feminino	Branca	1974-02-12	m 7 130 365	\N	Superior Completo	@bebella01				T	2021-11-09 18:33:59	\N	2021-11-09 18:35:37	71	3118601	
88	Walbert Martins	\N	walbert.martins@social.mg.gov.br	07055507618	\N	\N	\N	\N	\N	m1395781	\N	\N	uemg@102030				T	2021-11-26 19:32:21	\N	2021-11-26 19:32:50	\N	3157807	\N
94	Isla Rosa	\N	isla.rosa@social.mg.gov.br	13048560629	\N	\N	\N	\N	\N	000000000	\N	\N	votarvotar				T	2021-12-07 20:53:42	\N	2021-12-07 20:53:59	\N	3106200	\N
\.


--
-- Data for Name: auth_user_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.auth_user_archive (current_record, id, first_name, last_name, email, username, entidade, municipio, mothers_name, "gênero", cor, data_nascimento, identidade, escolaridade, password, registration_key, reset_password_key, registration_id, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: candidato; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.candidato (id, entidade, eleicao, cargo, pdf, foto, perfil, propostas, is_active, created_on, created_by, modified_on, modified_by, candidato, status) FROM stdin;
71	20	30	55	\N	\N	\N	\N	T	2021-11-30 20:54:31	82	2021-12-06 21:06:14	82	82	aprovado
78	52	30	42	\N	\N	\N	\N	T	2021-12-06 17:11:03	92	2021-12-07 16:34:43	92	92	inscricao_solicitada
80	52	36	65	\N	\N	\N	\N	T	2021-12-07 16:59:26	92	2021-12-07 16:59:45	92	92	inscricao_solicitada
82	53	42	70	\N	\N	\N	\N	T	2021-12-07 17:29:44	92	2021-12-07 17:30:44	92	92	inscricao_solicitada
83	56	30	55	\N	\N	\N	\N	T	2021-12-07 17:53:32	72	2021-12-07 17:58:20	72	72	aprovado
67	6	29	40	\N	\N	\N	\N	T	2021-11-26 19:06:21	71	2021-11-26 19:09:42	71	71	aprovado
68	20	29	39	\N	\N	\N	\N	T	2021-11-26 19:41:15	82	2021-11-26 20:21:34	82	82	aprovado
69	4	31	46	\N	\N	\N	\N	T	2021-11-30 14:23:52	1	2021-11-30 14:23:52	1	1	nao_inscrito
70	4	29	40	\N	\N	\N	\N	T	2021-11-30 14:30:26	1	2021-11-30 14:35:00	1	1	aprovado
63	22	29	40	\N	\N	\N	\N	T	2021-11-25 13:12:16	86	2021-11-30 18:35:38	86	86	inscricao_solicitada
72	22	30	36	\N	\N	\N	\N	T	2021-12-01 21:50:36	86	2021-12-01 22:14:46	86	86	aprovado
73	29	30	53	\N	\N	\N	\N	T	2021-12-02 14:15:49	1	2021-12-02 14:16:23	1	1	aprovado
75	44	30	43	\N	\N	\N	\N	T	2021-12-06 14:56:24	75	2021-12-06 14:59:12	75	75	aprovado
76	43	30	54	\N	\N	\N	\N	T	2021-12-06 15:13:42	74	2021-12-06 15:17:16	74	74	aprovado
77	45	30	42	\N	\N	\N	\N	T	2021-12-06 15:26:22	70	2021-12-06 15:27:45	70	70	aprovado
79	6	30	36	\N	\N	\N	\N	T	2021-12-06 20:08:11	71	2021-12-06 20:48:23	71	71	aprovado
\.


--
-- Data for Name: candidato_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.candidato_archive (current_record, id, candidato, entidade, eleicao, cargo, status, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: cargo; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.cargo (id, entidade, eleicao, nome, atribuicoes, is_active, created_on, created_by, modified_on, modified_by, documentos_requeridos_eleitor, documentos_requeridos_cargo, tipo_eleitor, tipo_candidato, cadeiras) FROM stdin;
40	10	29	Representante do Segmento X	\N	T	2021-11-09 21:04:38	1	2021-11-30 18:58:46	69	||	||	entidade	entidade	1
39	10	29	Representante do Segmento das Mulheres Negras	\N	T	2021-11-09 21:02:52	1	2021-11-30 18:59:03	69	||	||	entidade	entidade	1
38	10	29	Representante segmento da população indígena	\N	T	2021-11-09 20:29:31	69	2021-11-30 18:59:16	69	||	||	entidade	entidade	1
41	10	29	Representante do Segmento do movimento negro	\N	T	2021-11-09 21:05:16	69	2021-11-30 18:59:28	69	\N	\N	entidade	entidade	1
36	5	30	Mães	Feminnista	T	2021-11-09 19:50:54	76	2021-11-30 20:21:52	1	\N	\N	usuario	entidade	1
53	5	30	Estudantes	\N	T	2021-11-30 20:22:01	1	2021-11-30 20:22:01	1	\N	\N	\N	\N	1
54	5	30	Empreendedoras	\N	T	2021-11-30 20:22:10	1	2021-11-30 20:22:10	1	\N	\N	\N	\N	1
55	5	30	Setor Rural	\N	T	2021-11-30 20:22:21	1	2021-11-30 20:22:21	1	\N	\N	\N	\N	1
56	53	32	SegmentoTeste 001	\N	T	2021-12-06 18:50:41	92	2021-12-06 18:50:41	92	\N	\N	\N	\N	1
57	53	32	SegmentoTeste 002	\N	T	2021-12-06 18:50:50	92	2021-12-06 18:50:50	92	\N	\N	\N	\N	1
58	53	32	SegmentoTeste 003	\N	T	2021-12-06 18:50:58	92	2021-12-06 18:50:58	92	\N	\N	\N	\N	1
59	54	33	SegmentoTeste 001	\N	T	2021-12-06 19:24:09	92	2021-12-06 19:24:09	92	\N	\N	\N	\N	1
60	54	33	SegmentoTeste 002	\N	T	2021-12-06 19:24:18	92	2021-12-06 19:24:18	92	\N	\N	\N	\N	2
61	55	34	SegmentoTeste 001	\N	T	2021-12-06 20:54:18	92	2021-12-06 20:54:18	92	\N	\N	\N	\N	1
62	55	34	SegmentoTeste 002	\N	T	2021-12-06 20:54:25	92	2021-12-06 20:54:25	92	\N	\N	\N	\N	1
63	55	34	SegmentoTeste 003	\N	T	2021-12-06 20:54:31	92	2021-12-06 20:54:31	92	\N	\N	\N	\N	1
64	53	35	SegmentoTeste 001	\N	T	2021-12-06 21:03:48	92	2021-12-06 21:03:48	92	\N	\N	\N	\N	1
65	53	36	SegmentoTeste001	\N	T	2021-12-07 16:38:17	92	2021-12-07 16:38:17	92	\N	\N	\N	\N	1
66	53	37	SegmentoTeste 001	\N	T	2021-12-07 17:06:23	92	2021-12-07 17:06:23	92	\N	\N	\N	\N	1
67	53	38	SegmentoTeste 001	\N	T	2021-12-07 17:08:44	92	2021-12-07 17:08:44	92	\N	\N	\N	\N	1
68	53	39	SegmentoTeste 001	\N	T	2021-12-07 17:11:33	92	2021-12-07 17:11:33	92	\N	\N	\N	\N	1
69	53	41	SegmentoTeste 001	\N	T	2021-12-07 17:20:39	92	2021-12-07 17:20:39	92	\N	\N	\N	\N	1
70	54	42	SegmentoTeste 001	\N	T	2021-12-07 17:28:54	92	2021-12-07 17:28:54	92	\N	\N	\N	\N	1
42	5	30	Feminista	\N	T	2021-11-12 15:12:11	76	2021-12-09 17:02:40	1	|15|18|16|11|13|12|17|14|9|	|10|20|	entidade	usuario	1
44	10	31	Representantes da população negra - segmento quilombola	\N	T	2021-11-29 21:48:10	69	2021-11-29 21:48:10	69	\N	\N	\N	\N	1
45	10	31	Representantes da população negra - segmento mulheres negras	\N	T	2021-11-29 21:48:27	69	2021-11-29 21:48:27	69	\N	\N	\N	\N	1
46	10	31	Representantes da população negra - segmento movimento negro	\N	T	2021-11-29 21:48:44	69	2021-11-29 21:48:44	69	\N	\N	\N	\N	1
47	10	31	Representantes da população negra - segmento juventude negra	\N	T	2021-11-29 21:49:01	69	2021-11-29 21:49:01	69	\N	\N	\N	\N	1
48	10	31	Representantes da população negra - segmento da  comunidade LGBTQIA+ negra	\N	T	2021-11-29 21:49:38	69	2021-11-29 21:49:38	69	\N	\N	\N	\N	1
49	10	31	Representantes da população negra - segmento de religiões de matriz africana	\N	T	2021-11-29 21:51:04	69	2021-11-29 21:51:04	69	\N	\N	\N	\N	1
51	10	31	Representante da comunidade cigana	\N	T	2021-11-29 21:52:00	69	2021-11-29 21:52:00	69	\N	\N	\N	\N	1
43	5	30	Mulheres Negras	\N	T	2021-11-12 15:17:57	76	2021-12-09 17:03:07	1	\N	\N	usuario	usuario	1
50	10	31	Representantes dos povos indígenas	\N	T	2021-11-29 21:51:42	69	2021-11-29 21:55:51	69	\N	\N	\N	\N	2
52	10	31	Representantes de outras etnias	\N	T	2021-11-29 21:52:22	69	2021-11-29 22:04:36	69	\N	\N	\N	\N	2
37	10	29	Representante do Segmento Quilombola	\N	T	2021-11-09 20:28:23	1	2021-11-30 18:58:23	69	||	||	entidade	entidade	1
\.


--
-- Data for Name: cargo_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.cargo_archive (current_record, id, entidade, eleicao, tipo_eleitor, tipo_candidato, documentos_requeridos_eleitor, documentos_requeridos_cargo, nome, atribuicoes, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: documento_recurso; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.documento_recurso (id, recurso, arquivo, is_active, created_on, created_by, modified_on, modified_by, nome) FROM stdin;
1	1	documento_recurso.arquivo.a2f6907dcf2501f2.74657374652e706e67.png	T	2021-12-09 22:17:05	76	2021-12-09 22:17:05	76	teste
\.


--
-- Data for Name: documentos; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.documentos (id, user_id, nome_arquivo, tipo_arquivo, arquivo, status, is_active, created_on, created_by, modified_on, modified_by, observacoes) FROM stdin;
10	1	14	14	\N	nao_enviado	T	2021-11-09 20:52:54	1	2021-11-09 20:52:54	1	\N
13	71	10	10	documentos.arquivo.b560a37e2f676a20.74657374652e706e67.png	aprovado	T	2021-11-12 14:58:52	71	\N	\N	\N
43	82	17	17	documentos.arquivo.b87c5e3d29ccb2e0.746f7070632e6a7067.jpg	aprovado	T	2021-11-26 00:42:55	1	2021-11-26 21:21:55	1	\N
34	86	17	17	documentos.arquivo.9fdebe0db1a23527.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	2021-11-26 02:25:39	69	\N
59	86	18	18	documentos.arquivo.8b84367737853111.74657374652e706e67.png	aprovado	T	2021-12-01 22:06:11	86	\N	\N	\N
46	82	12	12	documentos.arquivo.b74f398c448f89c2.746f7063656c756c61722e6a7067.jpg	aprovado	T	2021-11-26 19:54:55	82	\N	\N	\N
37	86	12	12	documentos.arquivo.8a45c856c8f1f566.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	\N	\N	\N
18	69	1	1	documentos.arquivo.ada61bb5d6aca5f1.546573746520646f63756d656e746f7320456c6569c3a7c3a36f20434f4e455049522e706466.pdf	aprovado	T	2021-11-12 15:01:45	69	2021-11-21 19:13:10	1	\N
38	86	13	13	documentos.arquivo.bc2d850deffa39aa.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	\N	\N	\N
19	69	10	10	documentos.arquivo.9f5a2ccade05efc7.546573746520646f63756d656e746f7320456c6569c3a7c3a36f20434f4e455049522e706466.pdf	aprovado	T	2021-11-12 15:01:45	69	2021-11-21 19:13:22	1	\N
35	86	19	19	documentos.arquivo.bf7b55f08619a6f6.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	\N	\N	\N
20	69	13	13	documentos.arquivo.8b4876b7aaecf946.546573746520646f63756d656e746f7320456c6569c3a7c3a36f20434f4e455049522e706466.pdf	aprovado	T	2021-11-12 15:01:45	69	2021-11-21 19:13:31	1	\N
54	82	15	15	documentos.arquivo.84c2db5903b86ad4.74657374652e706e67.png	aprovado	T	2021-11-30 20:31:24	82	2021-11-30 20:32:34	1	\N
3	1	1	1	documentos.arquivo.97b4f6efcf10dfdf.746f7063656c756c61722e6a7067.jpg	aprovado	F	2021-09-21 13:13:40	1	2021-11-30 14:48:45	69	\N
17	82	13	13	documentos.arquivo.9bb5ed30b585e450.74657374652e706466.pdf	aprovado	T	2021-11-12 15:00:03	82	\N	\N	\N
14	71	13	13	documentos.arquivo.944f9173224985e2.74657374652e706e67.png	aprovado	T	2021-11-12 14:58:52	71	2021-11-12 15:21:26	1	\N
32	86	16	16	documentos.arquivo.a0652c61f7345466.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	\N	\N	\N
21	1	17	17	documentos.arquivo.b9fda196ca79b578.74657374652e706e67.png	aprovado	T	2021-11-23 13:34:43	1	2021-11-26 01:09:30	69	\N
44	82	16	16	documentos.arquivo.9fefb9a5f8a0ea51.746f7070632e6a7067.jpg	aprovado	T	2021-11-26 19:54:55	82	\N	\N	\N
49	1	21	21	documentos.arquivo.a2e3ab8508ba025e.74657374652e706e67.png	aprovado	T	2021-11-30 20:24:59	1	2021-12-01 20:11:59	69	\N
16	82	10	10	documentos.arquivo.9fb1e7dc53047ddc.5465737465203232323232322e706466.pdf	aprovado	T	2021-11-12 15:00:03	82	2021-11-26 00:43:47	1	\N
58	86	15	15	documentos.arquivo.80f361fb45db76ea.74657374652e706e67.png	aprovado	T	2021-12-01 22:06:11	86	2021-12-01 22:20:56	1	\N
8	1	10	10	documentos.arquivo.ab4ab00a710496f6.746f7070632e6a7067.jpg	aprovado	T	2021-11-09 20:52:54	1	2021-11-30 14:48:50	69	\N
27	1	12	12	documentos.arquivo.b6079cf3a7a92044.74657374652e706e67.png	aprovado	T	2021-11-24 22:36:24	1	\N	\N	\N
36	86	10	10	documentos.arquivo.a990f15561abe4bd.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	\N	\N	\N
45	82	19	19	documentos.arquivo.b71c27622c8f3806.746f7070632e6a7067.jpg	aprovado	T	2021-11-26 19:54:55	82	\N	\N	\N
12	71	1	1	documentos.arquivo.9e154cf70a6b3c24.54455354452e706466.pdf	aprovado	T	2021-11-12 14:58:52	71	2021-11-26 20:31:04	1	\N
47	69	17	17	\N	nao_enviado	T	2021-11-26 21:30:11	1	2021-11-26 21:30:11	1	\N
11	1	13	13	documentos.arquivo.9fb0edbb39ad814e.65646974616c2e706466.pdf	aprovado	T	2021-11-12 13:52:42	1	\N	\N	\N
7	1	16	16	documentos.arquivo.848fbccc2eb9e999.74657374652e706e67.png	aprovado	T	2021-11-09 20:52:54	1	\N	\N	\N
29	71	17	17	documentos.arquivo.8ad83bacfc207cc2.54455354452e706466.pdf	aprovado	T	2021-11-24 22:40:14	1	2021-11-26 20:31:11	1	\N
26	1	19	19	documentos.arquivo.b8b8e2dcf6e21bc8.74657374652e706e67.png	aprovado	T	2021-11-24 22:36:24	1	\N	\N	\N
31	71	12	12	documentos.arquivo.8cad2a911c471540.54455354452e706466.pdf	aprovado	T	2021-11-24 22:40:14	1	\N	\N	\N
33	86	1	1	documentos.arquivo.ae0fef747679667f.74657374652e706e67.png	aprovado	T	2021-11-25 13:12:29	86	2021-11-26 02:25:32	69	\N
28	71	16	16	documentos.arquivo.a10bd7629f300b72.54455354452e706466.pdf	aprovado	T	2021-11-24 22:40:14	1	\N	\N	\N
30	71	19	19	documentos.arquivo.8156f14ecf87b731.54455354452e706466.pdf	aprovado	T	2021-11-24 22:40:14	1	\N	\N	\N
15	82	1	1	documentos.arquivo.a2f9e276b7eb2c18.746f7070632e6a7067.jpg	aprovado	T	2021-11-12 15:00:03	82	2021-11-26 21:21:48	1	\N
6	1	1	1	documentos.arquivo.b013fc539e827698.746f7070632e6a7067.jpg	aprovado	T	2021-09-29 23:21:01	1	2021-11-30 14:48:39	69	\N
9	1	11	11	documentos.arquivo.ba3d4311e34bdcba.74657374652e706e67.png	aprovado	T	2021-11-09 20:52:54	1	2021-12-01 20:11:47	69	\N
51	82	9	9	documentos.arquivo.9c51c4cf7fcda545.74657374652e706e67.png	aprovado	T	2021-11-30 20:31:24	82	2021-11-30 20:32:21	1	\N
53	82	21	21	documentos.arquivo.9bd46e37ab8461ca.74657374652e706e67.png	aprovado	T	2021-11-30 20:31:24	82	2021-11-30 20:32:42	1	\N
55	82	18	18	documentos.arquivo.a22fbddfc4a70315.74657374652e706e67.png	aprovado	T	2021-11-30 20:54:40	82	\N	\N	\N
52	82	11	11	documentos.arquivo.90c96f963681a694.74657374652e706e67.png	aprovado	T	2021-11-30 20:31:24	82	2021-11-30 20:32:28	1	\N
50	1	15	15	documentos.arquivo.9436de68c19bd63d.74657374652e706e67.png	aprovado	T	2021-11-30 20:24:59	1	2021-12-01 20:11:53	69	\N
48	1	9	9	documentos.arquivo.b1f89c12870e481f.74657374652e706e67.png	aprovado	T	2021-11-30 20:24:59	1	2021-12-01 20:11:41	69	\N
60	86	21	21	documentos.arquivo.bd9294cf7da0da5f.74657374652e706e67.png	aprovado	T	2021-12-01 22:06:11	86	2021-12-01 22:21:03	1	\N
61	1	18	18	documentos.arquivo.a876879751e1efc5.74657374652e706e67.png	aprovado	T	2021-12-02 14:15:54	1	\N	\N	\N
57	86	11	11	documentos.arquivo.ab05a77a121e12fa.74657374652e706e67.png	aprovado	T	2021-12-01 22:06:11	86	2021-12-01 22:20:50	1	\N
56	86	9	9	documentos.arquivo.9296266924ca4331.74657374652e706e67.png	aprovado	T	2021-12-01 22:06:11	86	2021-12-01 22:20:44	1	\N
62	82	14	14	\N	nao_enviado	T	2021-12-02 15:35:19	69	2021-12-02 15:35:19	69	\N
90	92	15	15	documentos.arquivo.991d6ff66aa039bb.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:17:50	92	\N
83	70	15	15	documentos.arquivo.ae13cba17f5eb7c6.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:35	1	\N
76	75	15	15	documentos.arquivo.8eeab2d94e1a01d5.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:28	1	\N
102	72	13	13	documentos.arquivo.b64bb2204ccd3e0a.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	\N	\N	\N
84	92	18	18	documentos.arquivo.abd9bf894b889a2b.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:18:10	92	\N
93	71	15	15	documentos.arquivo.9dde3c5afec522e3.74657374652e706e67.png	aprovado	T	2021-12-06 20:08:14	71	2021-12-06 20:49:07	1	\N
77	70	18	18	documentos.arquivo.9ed3318ab1d5308e.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:41	1	\N
97	72	18	18	documentos.arquivo.92444c5494fcf85b.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:36	1	\N
70	75	18	18	documentos.arquivo.9ed1964c09bd2f71.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:34	1	\N
85	92	21	21	documentos.arquivo.8f80b0122d02c2d8.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:18:27	92	\N
78	70	21	21	documentos.arquivo.b0fe6bc5b1f919dc.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:48	1	\N
101	72	11	11	documentos.arquivo.9756ca6cfd30a481.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:24	1	\N
71	75	21	21	documentos.arquivo.a02bf9a7bf8d82e9.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:40	1	\N
75	75	13	13	documentos.arquivo.a6308b11d231e588.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	\N	\N	\N
94	71	18	18	documentos.arquivo.babf90fc63e1869f.74657374652e706e67.png	aprovado	T	2021-12-06 20:08:14	71	2021-12-06 20:49:13	1	\N
98	72	21	21	documentos.arquivo.ab318906d972f58e.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:41	1	\N
68	74	13	13	documentos.arquivo.9ee5039055ba2137.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	\N	\N	\N
87	92	10	10	documentos.arquivo.920146d0c577a725.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:20:17	92	\N
82	70	13	13	documentos.arquivo.be50438310133b06.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	\N	\N	\N
103	72	15	15	documentos.arquivo.a2107f37a1067817.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:30	1	\N
65	74	9	9	documentos.arquivo.a58d7108177a2c93.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 15:00:54	1	\N
66	74	10	10	documentos.arquivo.a67e2de248f36117.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 15:00:59	1	\N
67	74	11	11	documentos.arquivo.9476cf286b36f96c.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 15:01:04	1	\N
69	74	15	15	documentos.arquivo.ac5ab3ebbc7790bd.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 15:01:10	1	\N
64	74	21	21	documentos.arquivo.97ee381331887e97.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 15:01:15	1	\N
63	74	18	18	documentos.arquivo.a07cc7ca4a726821.74657374652e706e67.png	aprovado	T	2021-12-06 14:49:22	74	2021-12-06 19:50:02	1	\N
86	92	9	9	documentos.arquivo.b29bf8cabf5df52c.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:15:16	92	\N
72	75	9	9	documentos.arquivo.8fdd185109df5b42.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:12	1	\N
88	92	11	11	documentos.arquivo.bcb7ea1cdd64f80d.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:16:02	92	\N
89	92	13	13	documentos.arquivo.85ac18ac64370168.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-06 17:11:12	92	2021-12-06 17:17:39	92	\N
79	70	9	9	documentos.arquivo.80ca5b008d7c486e.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:19	1	\N
73	75	10	10	documentos.arquivo.8b95b55a09ebcbf8.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:16	1	\N
74	75	11	11	documentos.arquivo.86d4227591235ca1.74657374652e706e67.png	aprovado	T	2021-12-06 14:56:27	75	2021-12-06 19:49:22	1	\N
80	70	10	10	documentos.arquivo.a64d222e60180c7b.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:24	1	\N
81	70	11	11	documentos.arquivo.8c4eeb23d5f1061a.74657374652e706e67.png	aprovado	T	2021-12-06 15:26:29	70	2021-12-06 19:50:29	1	\N
91	71	9	9	documentos.arquivo.a6ae22fb68080831.74657374652e706e67.png	aprovado	T	2021-12-06 20:08:14	71	2021-12-06 20:48:54	1	\N
95	71	21	21	documentos.arquivo.a5ad1a4a761b085a.74657374652e706e67.png	aprovado	T	2021-12-06 20:08:14	71	2021-12-06 20:49:20	1	\N
100	72	10	10	documentos.arquivo.b585b0f926652860.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:18	1	\N
92	71	11	11	documentos.arquivo.b5d69f0351f34875.74657374652e706e67.png	aprovado	T	2021-12-06 20:08:14	71	2021-12-06 20:49:01	1	\N
96	92	1	1	documentos.arquivo.95465d216f58e460.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	aguardando_aprovacao	T	2021-12-07 17:25:49	92	2021-12-07 17:26:08	92	\N
99	72	9	9	documentos.arquivo.b14aed65273df208.74657374652e706e67.png	aprovado	T	2021-12-07 17:53:37	72	2021-12-07 18:07:13	1	\N
\.


--
-- Data for Name: documentos_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.documentos_archive (current_record, id, user_id, nome_arquivo, tipo_arquivo, arquivo, status, observacoes, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
3	3	1	1	1	documentos.arquivo.81c67d18f9b2cad9.4f52c387414d454e544f2053722020416c6578616e6472655e2e706466.pdf	aguardando_aprovacao	\N	T	2021-09-21 13:13:40	1	2021-09-21 13:13:40	1
6	6	1	1	1	documentos.arquivo.b8d7e36b5ef64458.436f6e76656e63c3a36f2e706466.pdf	nao_enviado	\N	T	2021-09-29 23:21:01	1	2021-09-29 23:21:01	1
\.


--
-- Data for Name: documentos_eleitor; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.documentos_eleitor (id, eleitor, nome_arquivo, tipo_arquivo, arquivo, status, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: documentos_obrigatorios_eleicao; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.documentos_obrigatorios_eleicao (id, eleicao, tipo_documento) FROM stdin;
\.


--
-- Data for Name: eleicao; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.eleicao (id, entidade, nome, is_active, created_on, created_by, modified_on, modified_by, edital, inicio_alistamento, fim_alistamento, inicio_eleicao, fim_eleicao, obrigatorio, prazo_correcao_documentos, segmentos, segmentos_candidatura, documentos_requeridos_eleitor, documentos_requeridos_cargo, tipo_eleitor, tipo_candidato, publicar) FROM stdin;
32	53	COLEGIADOTESTE001 - ELEIÇÃO 001	T	2021-12-06 18:49:22	92	2021-12-06 18:50:19	92	\N	2021-11-30 00:00:00	2021-12-06 14:50:00	2021-12-06 14:50:01	2021-12-09 00:00:00	|10|	5	2	1	\N	||	usuario	usuario	T
33	54	COLEGIADOTESTE002 - ELEIÇÃO 001	T	2021-12-06 19:21:05	92	2021-12-06 19:23:55	92	eleicao.edital.873e4a5e61237940.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	2021-11-30 00:00:00	2021-12-06 15:25:00	2021-12-06 15:25:01	2021-12-06 15:30:00	|10|	5	2	1	\N	||	usuario	usuario	T
29	10	CONSELHO ESTADUAL DE PROMOÇÃO DA IGUALDADE RACIAL 2021	T	2021-11-09 19:49:42	69	2021-11-30 21:40:48	1	eleicao.edital.a4475d725fbd7fe3.65646974616c2e706466.pdf	2021-11-09 15:00:00	2021-11-29 18:00:00	2021-11-30 07:00:00	2021-12-28 18:00:00	|10|1|	5	1	1	|14|	|16|19|13|12|	entidade	entidade	T
34	55	COLEGIADOTESTE003	T	2021-12-06 20:53:19	92	2021-12-06 20:53:33	92	\N	2021-11-30 00:00:00	2021-12-06 17:00:00	2021-12-06 17:01:00	2021-12-06 18:00:00	|10|	5	2	1	\N	||	usuario	usuario	T
42	54	002	T	2021-12-07 17:28:33	92	2021-12-07 17:37:06	92	\N	2021-11-30 00:00:00	2021-12-08 17:35:00	2021-12-08 17:40:00	2021-12-08 17:50:00	|10|	1	1	1	\N	||	usuario	usuario	T
35	53	COLEGIADOTESTE001 - ELEIÇÃO 002	T	2021-12-06 21:02:57	92	2021-12-06 21:03:28	92	\N	2021-11-30 00:00:00	2021-12-06 20:10:00	2021-12-06 20:11:00	2021-12-06 21:00:00	|10|	5	\N	\N	\N	||	usuario	usuario	T
36	53	COLEGIADOTESTE001 - ELEIÇÃO003	T	2021-12-07 16:37:33	92	2021-12-07 16:37:42	92	\N	2021-11-30 00:00:00	2021-12-08 17:00:00	2021-12-08 18:00:00	2021-12-10 18:00:00	|10|	5	1	1	\N	||	usuario	usuario	T
31	10	Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR	T	2021-11-29 21:16:07	69	2021-11-30 14:26:03	1	eleicao.edital.8434a7cd80cea194.54657374652045646974616c20434f4e455049522e706466.pdf	2021-11-29 08:00:00	2021-12-10 23:59:00	2021-12-20 08:00:00	2021-12-21 18:00:00	|21|11|10|14|9|	5	1	1	\N	\N	entidade	entidade	\N
37	53	COLEGIADOTESTE001 - ELEIÇÃO004	T	2021-12-07 17:05:47	92	2021-12-07 17:06:05	92	\N	2021-11-30 00:00:00	2021-12-07 13:15:00	2021-12-07 13:20:00	2021-12-07 13:30:00	|10|	1	1	1	\N	||	usuario	usuario	T
30	5	CONSELHO ESTADUAL DA MULHER - 2021	T	2021-11-09 19:50:02	76	2021-12-09 14:24:39	71	eleicao.edital.b3cfcb007e87f285.65646974616c2e706466.pdf	2021-11-12 15:47:00	2021-12-05 18:00:00	2021-12-06 08:00:00	2021-12-09 23:00:00	|15|18|21|11|10|9|	5	3	1	\N	|13|	entidade	entidade	T
38	53	COLEGIADOTESTE001 - ELEIÇÃO005	T	2021-12-07 17:08:26	92	2021-12-07 17:08:33	92	eleicao.edital.b16f97964c441495.566f6c63616e6f2d57616c6c7061706572732d48442d446f776e6c6f61642d467265652e6a7067.jpg	2021-11-30 00:00:00	2021-12-07 16:15:00	2021-12-07 16:20:00	2021-12-07 16:40:00	||	5	1	1	\N	||	usuario	usuario	T
39	53	SEGMENTOTESTE 001	T	2021-12-07 17:11:09	92	2021-12-07 17:11:21	92	\N	2021-11-30 00:00:00	2021-12-07 17:00:00	2021-12-07 18:00:00	2021-12-07 19:00:00	||	5	1	1	\N	||	usuario	usuario	T
40	53	COLEGIADOTESTE001 - ELEIÇÃO006	T	2021-12-07 17:13:41	92	2021-12-07 17:14:08	92	\N	2021-11-30 00:00:00	2021-12-07 17:20:00	2021-12-07 17:25:00	2021-12-07 17:40:00	|1|	5	1	1	\N	||	usuario	usuario	T
41	53	COLEGIADOTESTE001	T	2021-12-07 17:20:01	92	2021-12-07 17:20:11	92	\N	2021-11-30 00:00:00	2021-12-07 17:25:00	2021-12-07 17:30:00	2021-12-07 17:40:00	|1|	5	1	1	\N	\N	usuario	usuario	T
\.


--
-- Data for Name: eleicao_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.eleicao_archive (current_record, id, entidade, nome, inicio_alistamento, fim_alistamento, inicio_eleicao, fim_eleicao, edital, obrigatorio, is_active, created_on, created_by, modified_on, modified_by, prazo_correcao_documentos) FROM stdin;
\.


--
-- Data for Name: eleitor; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.eleitor (id, eleitor, eleicao, status, is_active, created_on, created_by, modified_on, modified_by, entidade_representada) FROM stdin;
111	71	30	aprovado	T	2021-12-06 20:07:35	71	2021-12-06 20:48:23	71	6
103	82	30	aprovado	T	2021-11-30 20:31:03	82	2021-12-06 21:06:14	82	20
96	81	29	nao_inscrito	T	2021-11-12 15:07:52	81	2021-11-12 15:07:52	81	19
110	92	30	inscricao_solicitada	T	2021-12-06 15:59:10	92	2021-12-07 16:34:43	92	52
112	92	36	inscricao_solicitada	T	2021-12-07 16:42:29	92	2021-12-07 16:59:45	92	52
113	92	40	nao_inscrito	T	2021-12-07 17:15:44	92	2021-12-07 17:16:09	92	53
114	92	41	nao_inscrito	T	2021-12-07 17:22:55	92	2021-12-07 17:24:24	92	53
92	1	29	aprovado	T	2021-11-09 20:52:45	1	2021-11-30 14:43:43	1	4
101	89	31	nao_inscrito	T	2021-11-30 18:17:55	89	2021-11-30 18:17:55	89	\N
115	92	42	inscricao_solicitada	T	2021-12-07 17:29:23	92	2021-12-07 17:30:44	92	53
116	82	42	nao_inscrito	T	2021-12-07 17:32:52	82	2021-12-07 17:33:01	82	20
104	69	31	nao_inscrito	T	2021-11-30 20:45:52	69	2021-11-30 20:45:52	69	\N
97	86	29	inscricao_solicitada	T	2021-11-25 13:10:15	86	2021-11-30 18:35:38	86	22
117	72	30	aprovado	T	2021-12-07 17:50:15	72	2021-12-07 17:58:20	72	56
105	69	30	nao_inscrito	T	2021-11-30 20:59:45	69	2021-11-30 20:59:45	69	\N
99	88	29	nao_inscrito	T	2021-11-26 19:55:39	88	2021-11-26 19:55:39	88	\N
94	71	29	aprovado	T	2021-11-12 14:58:43	71	2021-11-26 19:09:42	71	6
95	69	29	aprovado	T	2021-11-12 15:01:32	69	2021-11-12 15:01:32	69	10
106	86	30	aprovado	T	2021-12-01 21:47:19	86	2021-12-01 22:14:46	86	22
102	1	30	aprovado	T	2021-11-30 20:24:13	1	2021-12-02 14:16:23	1	29
93	82	31	aprovado	T	2021-11-09 21:54:11	82	2021-11-26 21:35:53	82	20
100	1	31	nao_inscrito	T	2021-11-30 14:22:22	1	2021-11-30 14:43:29	1	4
108	75	30	aprovado	T	2021-12-06 14:51:58	75	2021-12-06 14:59:12	75	44
107	74	30	aprovado	T	2021-12-06 14:45:41	74	2021-12-06 15:17:16	74	43
109	70	30	aprovado	T	2021-12-06 15:22:39	70	2021-12-06 15:27:45	70	45
\.


--
-- Data for Name: eleitor_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.eleitor_archive (current_record, id, eleicao, eleitor, status, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: entidade; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.entidade (id, nome, cnpj, data_criacao_entidade, carta_apresentacao, segmento_atuacao, municipio, representante_legal, cartao_cnpj, comprovante_endereco, concordancia, orgao_colegiado, legislacao_criacao, texto_concordancia) FROM stdin;
9	ONG CICLANO	030303	1990-01-01	Ong Ciclano	Juventude	3106200	\N	\N	\N	\N	\N	\N	\N
7	ONG FULANO	010101	1980-01-01	Ong Fulano	Juventude	3106200	37	\N	\N	\N	\N	\N	\N
12	Casa	999999999999999999999999	2000-03-12	Entidade que atua com pessoas com deficiência intelectual.\r\n	Pessoas com deficiência intelectual.\r\n	3118601	44	\N	\N	\N	\N	\N	\N
3	Conselho Municipal de Defesa dos de todos de Oliveira Fortes	12345646464	1980-03-20	Somos uma entidade que cuida de todos	Atuamos no segmento de acompanhamento	3145703	43	\N	\N	\N	\N	\N	\N
15	Instituto Mineiro de Estudos e Pesquisas Juventude - IMEPJ	184205700001-00	1999-01-20	\N	ESPORTISTAS	3136702	41	\N	\N	\N	\N	\N	\N
13	VILA VICENTINA DA SOCIEDADE SÃO VICENTE DE PAULO 	18420570000100	2014-12-11	Declaro para os devidos fins e sob as penas da Lei que a entidade/instituição está apta a participar do processo de escolha dos representantes da .sociedade civil que ~ ocuparão assento no CEI/MG, na qualidade .de eleitora nos termos do presente , edital, conforme informações acima apresentadas e documentação que segue em anexo, Declaro, ainda, ter conhecimento de que a função de Conselheiro Estadual da Pessoa Idosa de MG, não corresponde qualquer tipo de remuneração ou ajuda de custo, estando o candidato e a instituição cientes de seus deveres junto ao CEI/MG de acordo com a Lei 13176 de 20 de janeiro de 1999).	Instituição de assistência social onde são abrigados pessoas  idosas etc.\r\nO	3100203	50	\N	\N	\N	\N	\N	\N
14	LAR SÃO VICENTE DE PAULO	17171220000100	2015-04-12	Declaro para os devidos fins e sob as penas da Lei que a eS1tidade/instituição está apta a participar do processo de escolha dos representantes da .sociedade civil que ~ ocuparão assento no CEI/MG, na qualidade .de eleitora nos termos do presente , edital, conforme informações acima apresentadas e documentação que segue em anexo, Declaro, ainda, ter conhecimento de que a função de Conselheiro Estadual da Pessoa Idosa de MG, não corresponde qualquer tipo de remuneração ou ajuda de custo, estando o candidato e a instituição cientes de seus deveres junto ao CEI/MG de acordo com a Lei 13176 de 20 de janeiro de 1999).	Instituição de assistência social onde são abrigados pessoas idosas etc.\r\n	3100807	50	\N	\N	\N	\N	\N	\N
17	Marlene Normândia Jorge	0000000000	2020-04-14	Quero participar do processo eleitoral do CONPED	Pessoa com Deficiência	3106200	52	\N	\N	\N	\N	\N	\N
8	ONG BELTRANA	020202	1985-01-01	Ong Beltrana atua há mais de 30 anos em defesa dos direitos dos jovens de povos e comunidades tradicionais, como foco no médio e baixo jequitinhonha.	Juventudes de povos e comunidades tradicionais	3106200	59	\N	\N	\N	\N	\N	\N
18	Conselho Estadual de Juventude	05465167000141	2010-01-01	O Conselho Estadual da Juventude (Cejuve) é um órgão colegiado de caráter deliberativo, consultivo e propositivo, subordinado à Secretaria de Estado de Desenvolvimento Social (SEDESE). Tem por finalidade formular diretrizes de ações governamentais voltadas para jovens de 15 a 29 anos. É composto por 36 conselheiros, sendo 12 representantes governamentais e seus respectivos suplentes e 24 membros e seus suplentes representantes de entidades da sociedade civil em atividade há, pelo menos um ano no Estado, com atuação na promoção, atendimento, defesa, garantia, estudos ou pesquisas dos direitos das juventudes. Os mandatos têm duração de dois anos, sendo possível uma recondução.	Jovens	3100104	78			Concordo	T	\N	\N
5	CONSELHO ESTADUAL DA MULHER - CEM	49120567000190	2014-12-11	teste	Feminista	3106200	76			Concordo	T	\N	\N
19	Conselho Estadual de Defesa dos Direitos Humanos - CONEDH	05465167000141	\N	\N	\N	3106200	81			Concordo	T	\N	\N
10	CONEPIR/Conselho Estadual de Promoção da Igualdade Racial 	48323258000155	2009-08-26	xxxxx	Promoção da Igualdade Racial	3106200	69			Concordo	T	\N	\N
25	Movimento Negro Unificado de Belo Horizonte	08715327000151	1989-08-15	\N	01 (um) do segmento dos quilombolas;\r\n01 (um) do segmento das mulheres negras; \r\n01 (um) do segmento do movimento negro;\r\n 01 (um) do segmento da juventude negra;\r\n01 (um) do segmento da comunidade LGBTQIA+ negra; \r\n01 (um) do segmento de religiões de matriz africana\r\n	3106200	69	\N	\N	\N	F	Ato constitutivo registro cartorio 	T
26	Movimento negro "Elas lutam e labutam"	08715327000151	1888-01-01	\N	Mulheres Negras	3101607	69	\N	\N	\N	F	Registro Cartório	T
20	Conselho Estadual de Defesa dos Direitos da Pessoa com Deficiência - CONPED	42761694000120	\N	\N	\N	3106200	82			Concordo	T	\N	\N
27	Movimento Elas por Elas +	08715327000151	1989-01-01	\N	Mulheres negras	3102100	69	\N	\N	\N	F	\N	T
2	Conselho Estadual dos Direitos da Criança e do Adolescente - CEDCA	35205424000177	\N	\N	\N	3106200	84			Concordo	T	\N	\N
16	Conselho Estadual da Pessoa Idosa - CEI	07910791000136	2018-02-24	BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB	TTTTTTTTTTTTTTTTTTTT	3106200	83			Concordo	T	\N	\N
21	Conselho Estadual da mulher	\N	2013-10-13	\N	\N	3106200	\N	\N	\N	\N	T	\N	\N
22	Associação dos Pré-Aposentados	68367953000126	2011-11-11	\N	empregados	3106200	86	\N	\N	\N	F	\N	T
6	ASILO DE VELHOS JESUS NAZARENO	90733160000132	2013-10-11	Declaro para os devidos fins e sob as penas da Lei que a eS1tidade/instituição está apta a participar do processo de escolha dos representantes da .sociedade civil que ~ ocuparão assento no CEI/MG, na qualidade .de eleitora nos termos do presente , edital, conforme informações acima apresentadas e documentação que segue em anexo, Declaro, ainda, ter conhecimento de que a função de Conselheiro Estadual da Pessoa Idosa de MG, não corresponde qualquer tipo de remuneração ou ajuda de custo, estando o candidato e a instituição cientes de seus deveres junto ao CEI/MG de acordo com a Lei 13176 de 20 de janeiro de 1999).	Abrigamento de pessoas idosas	3101102	71	\N	\N	\N	F	\N	T
23	MOVIMENTO ITABIRITENSE DE LESBICAS GAYS BISSEXUAIS E TRAVESTIS - ITA LGBT	18715383000140	1999-01-20	\N	\N	3106200	73	\N	\N	\N	F	\N	T
24	Instituto Mineiro de Promoção da Igualdade Racial - IMPIR	08715327000151	1989-08-15	\N	Promoção da igualdade racial, do movimento negro, de homens e  mulheres negras, quilombolas, juventude negra, LGBTQIA+ negra; religiões de matriz africana	3106200	69	\N	\N	\N	F	Ato registro cartório de notas 	T
28	Instituto Shalon MG	26388330000190	2009-07-07	\N	Etnias de raiz hebraica  	3100609	69	\N	\N	\N	F	Registro Cartorio	T
29	Associação Cigana de Minas Gerais	08715327000151	1989-08-15	\N	Defesa da causa cigana	3101904	1	\N	\N	\N	F	\N	T
30	Associação Cigana de Minas Gerais	08715327000151	1989-01-01	\N	Defesa da causa cigana	3104601	69	\N	\N	\N	F	fajslkfjlkasjlfk	T
31	Associação Cigana de Belo Horizonte	08715327000151	1989-08-15	\N	Defesa da causa cigana	3101805	69	\N	\N	\N	F	asfsaf	T
32	Associação Cigana do Brasil	08715327000151	1989-08-15	\N	Defesa da causa cigana	3105608	69	\N	\N	\N	F	Cartório de.... 	T
33	Associaçao de Defesa da Causa Indigenista de MG	08715327000151	1989-08-15	\N	Indigenista	3162922	69	\N	\N	\N	F	\N	T
34	Indígenas Unidos de MG	08715327000151	1888-01-01	\N	Defesa da Causa Indigena	3106606	69	\N	\N	\N	F	registro ato em cartorio	T
35	Associação Dignidade para todos e todas	08715327000151	1989-08-15	\N	Causa Indigenista; Quilombolas, Raizeiros, ciganos, religião de matriz africana	3128709	69	\N	\N	\N	F	registr	T
36	Parem de nos matar - jovens negros sim!	08715327000151	2018-11-29	\N	Juventude Negra	3106200	69	\N	\N	\N	F	ksafjkhsakjfha	T
37	Movimento Mineiro "Mãe África"	08715327000151	1989-01-01	\N	representantes da população negra em toda sua amplitude e aspectos sociais.	3100104	69	\N	\N	\N	F	Cartório 	T
38	Movimento sem luta não há vitória #Lute!	08715327000151	1989-08-15	\N	01 (um) do segmento dos quilombolas;\r\n01 (um) do segmento das mulheres negras; \r\n01 (um) do segmento do movimento negro;\r\n 01 (um) do segmento da juventude negra;\r\n01 (um) do segmento da comunidade LGBTQIA+ negra; \r\n01 (um) do segmento de religiões de matriz africana\r\n	3101631	69	\N	\N	\N	F	Registro Cartório	T
39	Indígenas de Caldas MG 	08715327000151	1989-01-01	\N	Causa Indígena\r\n	3110301	69	\N	\N	\N	F	Cartorio	T
40	Representante da Comunidade Cigana da Região Metropolitana de BH	08715327000151	1989-08-15	\N	Causa Cigana	3129806	69	\N	\N	\N	F	Cartorio	T
41	União e força raça negra 	08715327000151	1989-08-15	\N	Causa quilombola, religião de matriz africana, juventude negra	3171808	69	\N	\N	\N	F	Cartório	T
42	Movimento Elas por Elas +	08715327000151	1989-01-01	\N	Mulheres	3101706	69	\N	\N	\N	F	\N	T
43	Associação das Mulheres Negras de Varginha	07737631000137	2014-04-25	\N	Mulheres negras	3170701	74	\N	\N	\N	F	\N	T
44	Associação das Mulheres Negras de Belo Horizonte	03057322000165	2018-05-12	\N	Mulheres Negras	3106200	75	\N	\N	\N	F	\N	T
45	Associação das Feministas de Minas Gerais	95936005000137	2017-06-25	\N	Feminista	3106200	70	\N	\N	\N	F	\N	T
46	0EntidadeTesteUH"!@#$%¨&*()_+	61681303000101	1000-11-30	\N	\N	3100807	92	\N	\N	\N	F	\N	T
47	0EntidadeTesteUH"!@#$%¨&*()_+	61681303000101	2000-11-30	\N	\N	3101607	92	\N	\N	\N	F	\N	T
48	0EntidadeTesteUH"!@#$%¨&*()_+	61681303000101	2000-11-30	\N	a	3101409	92	\N	\N	\N	F	\N	T
49	EntidadeTeste	61681303000101	2000-11-30	\N	aaaaaaaaa	3101409	92	\N	\N	\N	F	\N	T
50	EntidadeTeste	61681303000101	2000-11-30	\N	aaaaaaaaaaaaaaaaaaaaaa                aaaaaaaaaaaaaaaaaaaaaaaaaaaa                          aaaaaaaaaaaa	3106200	92	\N	\N	\N	F	\N	T
51	EntidadeTeste	61681303000101	2020-11-30	\N	aaaaaaaaaaa aaaaaaaaaaaa aaaaaaaaaaaaa aaaaaaaaaaa aaaaaaaaaaaa	3101631	92	\N	\N	\N	F	\N	T
52	0EntidadeTesteUH"!@#$%¨&*()_+	61681303000101	2000-11-30	\N	\N	3101631	92	\N	\N	\N	F	\N	T
53	ColegiadoTeste001	61681303000101	2000-11-30	\N	\N	3101003	92	\N	\N	\N	T	\N	T
54	ColegiadoTeste002	61681303000101	2000-11-30	\N	\N	3101631	92	\N	\N	\N	T	\N	T
55	ColegiadoTeste 003	61681303000101	2000-11-30	\N	\N	3101508	92	\N	\N	\N	T	\N	T
4	Associação dos Jovens Desempregados	32001515000100	2020-08-10	\N	\N	3106200	1			Concordo	\N	\N	T
56	Associação de Mulheres do Norte de Minas	10552620000197	1953-12-15	\N	\N	3101706	72	\N	\N	\N	F	\N	T
\.


--
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.estado (id, uf, nome, latitude, longitude) FROM stdin;
11	RO	Rondônia	-10.83	-63.34
12	AC	Acre	-8.77	-70.55
13	AM	Amazonas	-3.47	-65.1
14	RR	Roraima	1.99	-61.33
15	PA	Pará	-3.79	-52.48
16	AP	Amapá	1.41	-51.77
17	TO	Tocantins	-9.46	-48.26
21	MA	Maranhão	-5.42	-45.44
22	PI	Piauí	-6.6	-42.28
23	CE	Ceará	-5.2	-39.53
24	RN	Rio Grande do Norte	-5.81	-36.59
25	PB	Paraíba	-7.28	-36.72
26	PE	Pernambuco	-8.38	-37.86
27	AL	Alagoas	-9.62	-36.82
28	SE	Sergipe	-10.57	-37.45
29	BA	Bahia	-13.29	-41.71
31	MG	Minas Gerais	-18.1	-44.38
32	ES	Espírito Santo	-19.19	-40.34
33	RJ	Rio de Janeiro	-22.25	-42.66
35	SP	São Paulo	-22.19	-48.79
41	PR	Paraná	-24.89	-51.55
42	SC	Santa Catarina	-27.45	-50.95
43	RS	Rio Grande do Sul	-30.17	-53.5
50	MS	Mato Grosso do Sul	-20.51	-54.54
51	MT	Mato Grosso	-12.64	-55.42
52	GO	Goiás	-15.98	-49.86
53	DF	Distrito Federal	-15.83	-47.86
\.


--
-- Data for Name: habilitacao; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.habilitacao (id, cargo, status, entidade_representada, eleicao, eleitor) FROM stdin;
53	\N	Aguardando	\N	\N	\N
54	\N	Aguardando	\N	\N	\N
59	40	inscricao_solicitada	\N	29	71
61	46	inscricao_solicitada	\N	31	1
62	40	aprovado	4	29	1
55	40	aprovado	22	29	86
76	43	aprovado	\N	30	75
77	36	aprovado	\N	30	75
73	43	aprovado	43	30	74
75	53	aprovado	43	30	74
78	54	aprovado	43	30	74
79	55	aprovado	\N	30	70
60	39	inscricao_solicitada	20	29	82
80	54	aprovado	\N	30	70
81	42	aprovado	\N	30	70
85	36	aprovado	6	30	71
86	42	aprovado	6	30	71
67	36	aprovado	20	30	82
68	55	aprovado	20	30	82
69	53	aprovado	20	30	82
82	43	inscricao_solicitada	52	30	92
83	36	inscricao_solicitada	52	30	92
84	42	inscricao_solicitada	52	30	92
87	65	inscricao_solicitada	\N	36	92
88	69	inscricao_solicitada	53	41	92
70	36	aprovado	22	30	86
71	55	aprovado	22	30	86
72	53	aprovado	22	30	86
63	36	aprovado	29	30	1
64	55	aprovado	29	30	1
66	53	aprovado	29	30	1
90	70	inscricao_solicitada	20	42	82
89	70	inscricao_solicitada	53	42	92
91	55	aprovado	56	30	72
92	54	aprovado	56	30	72
93	42	aprovado	56	30	72
\.


--
-- Data for Name: municipio; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.municipio (id, nome, latitude, longitude, estado, uf, capital) FROM stdin;
4301958	Barra Funda	-27.9205	-53.0391	43	RS	false
4304200	Candelária	-29.6684	-52.7895	43	RS	false
3514403	Dracena	-21.4843	-51.535	35	SP	false
2305407	Icó	-6.39627	-38.8554	23	CE	false
5200050	Abadia de Goiás	-16.7573	-49.4412	52	GO	false
3100104	Abadia dos Dourados	-18.4831	-47.3916	31	MG	false
5200100	Abadiânia	-16.197	-48.7057	52	GO	false
3100203	Abaeté	-19.1551	-45.4444	31	MG	false
1500107	Abaetetuba	-1.72183	-48.8788	15	PA	false
2300101	Abaiara	-7.34588	-39.0416	23	CE	false
2900108	Abaíra	-13.2488	-41.6619	29	BA	false
2900207	Abaré	-8.72073	-39.1162	29	BA	false
4100103	Abatiá	-23.3049	-50.3133	41	PR	false
4200051	Abdon Batista	-27.6126	-51.0233	42	SC	false
1500131	Abel Figueiredo	-4.95333	-48.3933	15	PA	false
4200101	Abelardo Luz	-26.5716	-52.3229	42	SC	false
3100302	Abre Campo	-20.2996	-42.4743	31	MG	false
2600054	Abreu e Lima	-7.90072	-34.8984	26	PE	false
1700251	Abreulândia	-9.62101	-49.1518	17	TO	false
3100401	Acaiaca	-20.359	-43.1439	31	MG	false
2100055	Açailândia	-4.94714	-47.5004	21	MA	false
2900306	Acajutiba	-11.6575	-38.0197	29	BA	false
1500206	Acará	-1.95383	-48.1985	15	PA	false
2300150	Acarape	-4.22083	-38.7055	23	CE	false
2300200	Acaraú	-2.88769	-40.1183	23	CE	false
2400109	Acari	-6.4282	-36.6347	24	RN	false
2200053	Acauã	-8.21954	-41.0831	22	PI	false
4300034	Aceguá	-31.8665	-54.1615	43	RS	false
2300309	Acopiara	-6.08911	-39.448	23	CE	false
5100102	Acorizal	-15.194	-56.3632	51	MT	false
1200013	Acrelândia	-9.82581	-66.8972	12	AC	false
5200134	Acreúna	-17.396	-50.3749	52	GO	false
2400208	Açu	-5.58362	-36.914	24	RN	false
3100500	Açucena	-19.0671	-42.5419	31	MG	false
3500105	Adamantina	-21.682	-51.0737	35	SP	false
5200159	Adelândia	-16.4127	-50.1657	52	GO	false
3500204	Adolfo	-21.2325	-49.6451	35	SP	false
4100202	Adrianópolis	-24.6606	-48.9922	41	PR	false
2900355	Adustina	-10.5437	-38.1113	29	BA	false
2600104	Afogados da Ingazeira	-7.74312	-37.631	26	PE	false
2400307	Afonso Bezerra	-5.49229	-36.5075	24	RN	false
3200102	Afonso Cláudio	-20.0778	-41.1261	32	ES	false
2100105	Afonso Cunha	-4.13631	-43.3275	21	MA	false
2600203	Afrânio	-8.51136	-41.0095	26	PE	false
1500305	Afuá	-0.154874	-50.3861	15	PA	false
2600302	Agrestina	-8.45966	-35.9447	26	PE	false
2200103	Agricolândia	-5.79676	-42.6664	22	PI	false
4200200	Agrolândia	-27.4087	-49.822	42	SC	false
4200309	Agronômica	-27.2662	-49.708	42	SC	false
1500347	Água Azul do Norte	-6.79053	-50.4791	15	PA	false
3100609	Água Boa	-17.9914	-42.3806	31	MG	false
5100201	Água Boa	-14.051	-52.1601	51	MT	false
2200202	Água Branca	-5.88856	-42.637	22	PI	false
2500106	Água Branca	-7.51144	-37.6357	25	PB	false
2700102	Água Branca	-9.262	-37.938	27	AL	false
5000203	Água Clara	-20.4452	-52.879	50	MS	false
3100708	Água Comprida	-20.0576	-48.1069	31	MG	false
4200408	Água Doce	-26.9985	-51.5528	42	SC	false
2100154	Água Doce do Maranhão	-2.84048	-42.1189	21	MA	false
3200169	Água Doce do Norte	-18.5482	-40.9854	32	ES	false
2900405	Água Fria	-11.8618	-38.7639	29	BA	false
5200175	Água Fria de Goiás	-14.9778	-47.7823	52	GO	false
5200209	Água Limpa	-18.0771	-48.7603	52	GO	false
2400406	Água Nova	-6.20351	-38.2941	24	RN	false
2600401	Água Preta	-8.70609	-35.5263	26	PE	false
4300059	Água Santa	-28.1672	-52.031	43	RS	false
3500303	Aguaí	-22.0572	-46.9735	35	SP	false
3100807	Aguanil	-20.9439	-45.3915	31	MG	false
2600500	Águas Belas	-9.11125	-37.1226	26	PE	false
3500402	Águas da Prata	-21.9319	-46.7176	35	SP	false
4200507	Águas de Chapecó	-27.0754	-52.9808	42	SC	false
3500501	Águas de Lindóia	-22.4733	-46.6314	35	SP	false
3500550	Águas de Santa Bárbara	-22.8812	-49.2421	35	SP	false
3500600	Águas de São Pedro	-22.5977	-47.8734	35	SP	false
3100906	Águas Formosas	-17.0802	-40.9384	31	MG	false
4200556	Águas Frias	-26.8794	-52.8568	42	SC	false
5200258	Águas Lindas de Goiás	-15.7617	-48.2816	52	GO	false
4200606	Águas Mornas	-27.6963	-48.8243	42	SC	false
3101003	Águas Vermelhas	-15.7431	-41.4571	31	MG	false
4300109	Agudo	-29.6447	-53.2515	43	RS	false
3500709	Agudos	-22.4694	-48.9863	35	SP	false
4100301	Agudos do Sul	-25.9899	-49.3343	41	PR	false
3200136	Águia Branca	-18.9846	-40.7437	32	ES	false
2500205	Aguiar	-7.0918	-38.1681	25	PB	false
1700301	Aguiarnópolis	-6.55409	-47.4702	17	TO	false
3101102	Aimorés	-19.5007	-41.0746	31	MG	false
2900603	Aiquara	-14.1269	-39.8937	29	BA	false
2300408	Aiuaba	-6.57122	-40.1178	23	CE	false
3101201	Aiuruoca	-21.9736	-44.6042	31	MG	false
4300208	Ajuricaba	-28.2342	-53.7757	43	RS	false
3101300	Alagoa	-22.171	-44.6413	31	MG	false
2500304	Alagoa Grande	-7.03943	-35.6206	25	PB	false
2500403	Alagoa Nova	-7.05377	-35.7591	25	PB	false
2500502	Alagoinha	-6.94657	-35.5332	25	PB	false
2600609	Alagoinha	-8.4665	-36.7788	26	PE	false
2200251	Alagoinha do Piauí	-7.00039	-40.9282	22	PI	false
2900702	Alagoinhas	-12.1335	-38.4208	29	BA	false
3500758	Alambari	-23.5503	-47.898	35	SP	false
3101409	Albertina	-22.2018	-46.6139	31	MG	false
2100204	Alcântara	-2.39574	-44.4062	21	MA	false
2300507	Alcântaras	-3.58537	-40.5479	23	CE	false
2500536	Alcantil	-7.73668	-36.0511	25	PB	false
5000252	Alcinópolis	-18.3255	-53.7042	50	MS	false
2900801	Alcobaça	-17.5195	-39.2036	29	BA	false
2100303	Aldeias Altas	-4.62621	-43.4689	21	MA	false
4300307	Alecrim	-27.6579	-54.7649	43	RS	false
3200201	Alegre	-20.758	-41.5382	32	ES	false
4300406	Alegrete	-29.7902	-55.7949	43	RS	false
2200277	Alegrete do Piauí	-7.24196	-40.8566	22	PI	false
4300455	Alegria	-27.8345	-54.0557	43	RS	false
3101508	Além Paraíba	-21.8797	-42.7176	31	MG	false
1500404	Alenquer	-1.94623	-54.7384	15	PA	false
2400505	Alexandria	-6.40533	-38.0142	24	RN	false
5200308	Alexânia	-16.0834	-48.5076	52	GO	false
3101607	Alfenas	-21.4256	-45.9477	31	MG	false
3200300	Alfredo Chaves	-20.6396	-40.7543	32	ES	false
3500808	Alfredo Marcondes	-21.9527	-51.414	35	SP	false
3101631	Alfredo Vasconcelos	-21.1535	-43.7718	31	MG	false
4200705	Alfredo Wagner	-27.7001	-49.3273	42	SC	false
2500577	Algodão de Jandaíra	-6.89292	-36.0129	25	PB	false
2500601	Alhandra	-7.42977	-34.9057	25	PB	false
2600708	Aliança	-7.60398	-35.2227	26	PE	false
1700350	Aliança do Tocantins	-11.3056	-48.9361	17	TO	false
2900900	Almadina	-14.7089	-39.6415	29	BA	false
1700400	Almas	-11.5706	-47.1792	17	TO	false
1500503	Almeirim	-1.52904	-52.5788	15	PA	false
3101706	Almenara	-16.1785	-40.6942	31	MG	false
2400604	Almino Afonso	-6.1475	-37.7636	24	RN	false
4100400	Almirante Tamandaré	-25.3188	-49.3037	41	PR	false
4300471	Almirante Tamandaré do Sul	-28.1149	-52.9142	43	RS	false
5200506	Aloândia	-17.7292	-49.4769	52	GO	false
3101805	Alpercata	-18.974	-41.97	31	MG	false
4300505	Alpestre	-27.2502	-53.0341	43	RS	false
3101904	Alpinópolis	-20.8631	-46.3878	31	MG	false
5100250	Alta Floresta	-9.86674	-56.0867	51	MT	false
1100015	Alta Floresta D'Oeste	-11.9283	-61.9953	11	RO	false
3500907	Altair	-20.5242	-49.0571	35	SP	false
1500602	Altamira	-3.20407	-52.21	15	PA	false
2100402	Altamira do Maranhão	-4.16598	-45.4706	21	MA	false
4100459	Altamira do Paraná	-24.7983	-52.7128	41	PR	false
2300606	Altaneira	-6.99837	-39.7356	23	CE	false
3102001	Alterosa	-21.2488	-46.1387	31	MG	false
2600807	Altinho	-8.48482	-36.0644	26	PE	false
3501004	Altinópolis	-21.0214	-47.3712	35	SP	false
3501103	Alto Alegre	-21.5811	-50.168	35	SP	false
1400050	Alto Alegre	2.98858	-61.3072	14	RR	false
4300554	Alto Alegre	-28.7769	-52.9893	43	RS	false
2100436	Alto Alegre do Maranhão	-4.213	-44.446	21	MA	false
2100477	Alto Alegre do Pindaré	-3.66689	-45.8421	21	MA	false
1100379	Alto Alegre dos Parecis	-12.132	-61.835	11	RO	false
5100300	Alto Araguaia	-17.3153	-53.2181	51	MT	false
4200754	Alto Bela Vista	-27.4333	-51.9044	42	SC	false
5100359	Alto Boa Vista	-11.6732	-51.3883	51	MT	false
3102050	Alto Caparaó	-20.431	-41.8738	31	MG	false
2400703	Alto do Rodrigues	-5.28186	-36.75	24	RN	false
4300570	Alto Feliz	-29.3919	-51.3123	43	RS	false
5100409	Alto Garças	-16.9462	-53.5272	51	MT	false
5200555	Alto Horizonte	-14.1978	-49.3378	52	GO	false
3153509	Alto Jequitibá	-20.4208	-41.967	31	MG	false
2200301	Alto Longá	-5.25634	-42.2096	22	PI	false
5100508	Alto Paraguai	-14.5137	-56.4776	51	MT	false
4128625	Alto Paraíso	-26.1146	-52.7469	41	PR	false
1100403	Alto Paraíso	-9.71429	-63.3188	11	RO	false
5200605	Alto Paraíso de Goiás	-14.1305	-47.51	52	GO	false
4100608	Alto Paraná	-23.1312	-52.3189	41	PR	false
2100501	Alto Parnaíba	-9.10273	-45.9303	21	MA	false
4100707	Alto Piquiri	-24.0224	-53.44	41	PR	false
3102100	Alto Rio Doce	-21.0281	-43.4067	31	MG	false
3200359	Alto Rio Novo	-19.0618	-41.0209	32	ES	false
2300705	Alto Santo	-5.50894	-38.2743	23	CE	false
5100607	Alto Taquari	-17.8241	-53.2792	51	MT	false
4100509	Altônia	-23.8759	-53.8958	41	PR	false
2200400	Altos	-5.03888	-42.4612	22	PI	false
3501152	Alumínio	-23.5306	-47.2546	35	SP	false
1300029	Alvarães	-3.22727	-64.8007	13	AM	false
3102209	Alvarenga	-19.4174	-41.7317	31	MG	false
3501202	Álvares Florence	-20.3203	-49.9141	35	SP	false
3501301	Álvares Machado	-22.0764	-51.4722	35	SP	false
3501400	Álvaro de Carvalho	-22.0841	-49.719	35	SP	false
3501509	Alvinlândia	-22.4435	-49.7623	35	SP	false
3102308	Alvinópolis	-20.1098	-43.0535	31	MG	false
1700707	Alvorada	-12.4785	-49.1249	17	TO	false
4300604	Alvorada	-29.9914	-51.0809	43	RS	false
1100346	Alvorada D'Oeste	-11.3463	-62.2847	11	RO	false
3102407	Alvorada de Minas	-18.7334	-43.3638	31	MG	false
2200459	Alvorada do Gurguéia	-8.42418	-43.777	22	PI	false
5200803	Alvorada do Norte	-14.4797	-46.491	52	GO	false
4100806	Alvorada do Sul	-22.7813	-51.2297	41	PR	false
1400027	Amajari	3.64571	-61.3692	14	RR	false
5000609	Amambai	-23.1058	-55.2253	50	MS	false
1600105	Amapá	2.05267	-50.7957	16	AP	false
2100550	Amapá do Maranhão	-1.67524	-46.0024	21	MA	false
4100905	Amaporã	-23.0943	-52.7866	41	PR	false
2600906	Amaraji	-8.37691	-35.4501	26	PE	false
4300638	Amaral Ferrador	-30.8756	-52.2509	43	RS	false
5200829	Amaralina	-13.9236	-49.2962	52	GO	false
2200509	Amarante	-6.24304	-42.8433	22	PI	false
2100600	Amarante do Maranhão	-5.56913	-46.7473	21	MA	false
2901007	Amargosa	-13.0215	-39.602	29	BA	false
1300060	Amaturá	-3.37455	-68.2005	13	AM	false
2901106	Amélia Rodrigues	-12.3914	-38.7563	29	BA	false
2901155	América Dourada	-11.4429	-41.439	29	BA	false
3501608	Americana	-22.7374	-47.3331	35	SP	false
5200852	Americano do Brasil	-16.2514	-49.9831	52	GO	false
3501707	Américo Brasiliense	-21.7288	-48.1147	35	SP	false
3501806	Américo de Campos	-20.2985	-49.7359	35	SP	false
4300646	Ametista do Sul	-27.3607	-53.183	43	RS	false
2300754	Amontada	-3.36017	-39.8288	23	CE	false
5200902	Amorinópolis	-16.6151	-51.0919	52	GO	false
2500734	Amparo	-7.55502	-37.0628	25	PB	false
3501905	Amparo	-22.7088	-46.772	35	SP	false
2800100	Amparo de São Francisco	-10.1348	-36.935	28	SE	false
3102506	Amparo do Serra	-20.5051	-42.8009	31	MG	false
4101002	Ampére	-25.9168	-53.4686	41	PR	false
2700201	Anadia	-9.68489	-36.3078	27	AL	false
2901205	Anagé	-14.6151	-41.1356	29	BA	false
4101051	Anahy	-24.6449	-53.1332	41	PR	false
1500701	Anajás	-0.996811	-49.9354	15	PA	false
2100709	Anajatuba	-3.26269	-44.6126	21	MA	false
3502002	Analândia	-22.1289	-47.6619	35	SP	false
1300086	Anamã	-3.56697	-61.3963	13	AM	false
1701002	Ananás	-6.36437	-48.0735	17	TO	false
1500800	Ananindeua	-1.36391	-48.3743	15	PA	false
5201108	Anápolis	-16.3281	-48.953	52	GO	false
1500859	Anapu	-3.46985	-51.2003	15	PA	false
2100808	Anapurus	-3.67577	-43.1014	21	MA	false
5000708	Anastácio	-20.4823	-55.8104	50	MS	false
5000807	Anaurilândia	-22.1852	-52.7191	50	MS	false
4200804	Anchieta	-26.5382	-53.3319	42	SC	false
3200409	Anchieta	-20.7955	-40.6425	32	ES	false
2901304	Andaraí	-12.8049	-41.3297	29	BA	false
4101101	Andirá	-23.0533	-50.2304	41	PR	false
2901353	Andorinha	-10.3482	-39.8391	29	BA	false
3102605	Andradas	-22.0695	-46.5724	31	MG	false
3502101	Andradina	-20.8948	-51.3786	35	SP	false
4300661	André da Rocha	-28.6283	-51.5797	43	RS	false
3102803	Andrelândia	-21.7411	-44.3117	31	MG	false
3502200	Angatuba	-23.4917	-48.4139	35	SP	false
3102852	Angelândia	-17.7279	-42.2641	31	MG	false
5000856	Angélica	-22.1527	-53.7708	50	MS	false
2601003	Angelim	-8.88429	-36.2902	26	PE	false
4200903	Angelina	-27.5704	-48.9879	42	SC	false
2901403	Angical	-12.0063	-44.7003	29	BA	false
2200608	Angical do Piauí	-6.08786	-42.74	22	PI	false
1701051	Angico	-6.39179	-47.8611	17	TO	false
2400802	Angicos	-5.65792	-36.6094	24	RN	false
3300100	Angra dos Reis	-23.0011	-44.3196	33	RJ	false
2901502	Anguera	-12.1462	-39.2462	29	BA	false
4101150	Ângulo	-23.1946	-51.9154	41	PR	false
5201207	Anhanguera	-18.3339	-48.2204	52	GO	false
3502309	Anhembi	-22.793	-48.1336	35	SP	false
3502408	Anhumas	-22.2934	-51.3895	35	SP	false
5201306	Anicuns	-16.4642	-49.9617	52	GO	false
2200707	Anísio de Abreu	-9.18564	-43.0494	22	PI	false
4201000	Anita Garibaldi	-27.6897	-51.1271	42	SC	false
4201109	Anitápolis	-27.9012	-49.1316	42	SC	false
1300102	Anori	-3.74603	-61.6575	13	AM	false
4300703	Anta Gorda	-28.9698	-52.0102	43	RS	false
2901601	Antas	-10.3856	-38.3401	29	BA	false
4101200	Antonina	-25.4386	-48.7191	41	PR	false
2300804	Antonina do Norte	-6.76919	-39.987	23	CE	false
2200806	Antônio Almeida	-7.21276	-44.1889	22	PI	false
2901700	Antônio Cardoso	-12.4335	-39.1176	29	BA	false
4201208	Antônio Carlos	-27.5191	-48.766	42	SC	false
3102902	Antônio Carlos	-21.321	-43.7451	31	MG	false
3103009	Antônio Dias	-19.6491	-42.8732	31	MG	false
2901809	Antônio Gonçalves	-10.5767	-40.2785	29	BA	false
5000906	Antônio João	-22.1927	-55.9517	50	MS	false
2400901	Antônio Martins	-6.21367	-37.8834	24	RN	false
4101309	Antônio Olinto	-25.9804	-50.1972	41	PR	false
4300802	Antônio Prado	-28.8565	-51.2883	43	RS	false
3103108	Antônio Prado de Minas	-21.0192	-42.1109	31	MG	false
2500775	Aparecida	-6.78466	-38.0803	25	PB	false
3502507	Aparecida	-22.8495	-45.2325	35	SP	false
3502606	Aparecida d'Oeste	-20.4487	-50.8835	35	SP	false
5201405	Aparecida de Goiânia	-16.8198	-49.2469	52	GO	false
5201454	Aparecida do Rio Doce	-18.2941	-51.1516	52	GO	false
1701101	Aparecida do Rio Negro	-9.94139	-47.9638	17	TO	false
5001003	Aparecida do Taboado	-20.0873	-51.0961	50	MS	false
3300159	Aperibé	-21.6252	-42.1017	33	RJ	false
3200508	Apiacá	-21.1523	-41.5693	32	ES	false
5100805	Apiacás	-9.53981	-57.4587	51	MT	false
3502705	Apiaí	-24.5108	-48.8443	35	SP	false
2100832	Apicum-Açu	-1.45862	-45.0864	21	MA	false
4201257	Apiúna	-27.0375	-49.3885	42	SC	false
2401008	Apodi	-5.65349	-37.7946	24	RN	false
2901908	Aporá	-11.6577	-38.0814	29	BA	false
5201504	Aporé	-18.9607	-51.9232	52	GO	false
2901957	Apuarema	-13.8542	-39.7501	29	BA	false
4101408	Apucarana	-23.55	-51.4635	41	PR	false
1300144	Apuí	-7.19409	-59.896	13	AM	false
2300903	Apuiarés	-3.94506	-39.4359	23	CE	false
2800209	Aquidabã	-10.278	-37.0148	28	SE	false
5001102	Aquidauana	-20.4666	-55.7868	50	MS	false
2301000	Aquiraz	-3.89929	-38.3896	23	CE	false
4201273	Arabutã	-27.1587	-52.1423	42	SC	false
2500809	Araçagi	-6.84374	-35.3737	25	PB	false
3103207	Araçaí	-19.1955	-44.2493	31	MG	false
2800308	Aracaju	-10.9091	-37.0677	28	SE	true 
3502754	Araçariguama	-23.4366	-47.0608	35	SP	false
2902054	Araças	-12.22	-38.2027	29	BA	false
2301109	Aracati	-4.55826	-37.7679	23	CE	false
2902005	Aracatu	-14.428	-41.4648	29	BA	false
3502804	Araçatuba	-21.2076	-50.4401	35	SP	false
2902104	Araci	-11.3253	-38.9584	29	BA	false
3103306	Aracitaba	-21.3446	-43.3736	31	MG	false
2601052	Araçoiaba	-7.78391	-35.0809	26	PE	false
2301208	Aracoiaba	-4.36872	-38.8125	23	CE	false
3502903	Araçoiaba da Serra	-23.5029	-47.6166	35	SP	false
3200607	Aracruz	-19.82	-40.2764	32	ES	false
5201603	Araçu	-16.3563	-49.6804	52	GO	false
3103405	Araçuaí	-16.8523	-42.0637	31	MG	false
5201702	Aragarças	-15.8955	-52.2372	52	GO	false
5201801	Aragoiânia	-16.9087	-49.4476	52	GO	false
1701309	Aragominas	-7.16005	-48.5291	17	TO	false
1701903	Araguacema	-8.80755	-49.5569	17	TO	false
1702000	Araguaçu	-12.9289	-49.8231	17	TO	false
5101001	Araguaiana	-15.7291	-51.8341	51	MT	false
1702109	Araguaína	-7.19238	-48.2044	17	TO	false
5101209	Araguainha	-16.857	-53.0318	51	MT	false
1702158	Araguanã	-6.58225	-48.6395	17	TO	false
2100873	Araguanã	-2.94644	-45.6589	21	MA	false
5202155	Araguapaz	-15.0909	-50.6315	52	GO	false
3103504	Araguari	-18.6456	-48.1934	31	MG	false
1702208	Araguatins	-5.64659	-48.1232	17	TO	false
2100907	Araioses	-2.89091	-41.905	21	MA	false
5001243	Aral Moreira	-22.9385	-55.6334	50	MS	false
2902203	Aramari	-12.0884	-38.4969	29	BA	false
4300851	Arambaré	-30.9092	-51.5046	43	RS	false
2100956	Arame	-4.88347	-46.0032	21	MA	false
3503000	Aramina	-20.0882	-47.7873	35	SP	false
3503109	Arandu	-23.1386	-49.0487	35	SP	false
3103603	Arantina	-21.9102	-44.2555	31	MG	false
3503158	Arapeí	-22.6717	-44.4441	35	SP	false
2700300	Arapiraca	-9.75487	-36.6615	27	AL	false
1702307	Arapoema	-7.65463	-49.0637	17	TO	false
3103702	Araponga	-20.6686	-42.5178	31	MG	false
4101507	Arapongas	-23.4153	-51.4259	41	PR	false
3103751	Araporã	-18.4357	-49.1847	31	MG	false
4101606	Arapoti	-24.1548	-49.8285	41	PR	false
4101655	Arapuã	-24.3132	-51.7856	41	PR	false
3103801	Arapuá	-19.0268	-46.1484	31	MG	false
5101258	Araputanga	-15.4641	-58.3425	51	MT	false
4201307	Araquari	-26.3754	-48.7188	42	SC	false
2500908	Arara	-6.82813	-35.7552	25	PB	false
4201406	Araranguá	-28.9356	-49.4918	42	SC	false
3503208	Araraquara	-21.7845	-48.178	35	SP	false
3503307	Araras	-22.3572	-47.3842	35	SP	false
2301257	Ararendá	-4.74567	-40.831	23	CE	false
2101004	Arari	-3.45214	-44.7665	21	MA	false
4300877	Araricá	-29.6168	-50.9291	43	RS	false
2301307	Araripe	-7.21319	-40.1359	23	CE	false
2601102	Araripina	-7.57073	-40.494	26	PE	false
3300209	Araruama	-22.8697	-42.3326	33	RJ	false
4101705	Araruna	-23.9315	-52.5021	41	PR	false
2501005	Araruna	-6.54848	-35.7498	25	PB	false
2902252	Arataca	-15.2651	-39.419	29	BA	false
4300901	Aratiba	-27.3978	-52.2975	43	RS	false
2301406	Aratuba	-4.41229	-39.0471	23	CE	false
2902302	Aratuípe	-13.0716	-39.0038	29	BA	false
2800407	Arauá	-11.2614	-37.6201	28	SE	false
4101804	Araucária	-25.5859	-49.4047	41	PR	false
3103900	Araújos	-19.9405	-45.1671	31	MG	false
3104007	Araxá	-19.5902	-46.9438	31	MG	false
3104106	Arceburgo	-21.359	-46.9401	31	MG	false
3503356	Arco-Íris	-21.7728	-50.466	35	SP	false
3104205	Arcos	-20.2863	-45.5373	31	MG	false
2601201	Arcoverde	-8.41519	-37.0577	26	PE	false
3104304	Areado	-21.3572	-46.1421	31	MG	false
3300225	Areal	-22.2283	-43.1118	33	RJ	false
3503406	Arealva	-22.031	-48.9135	35	SP	false
2501104	Areia	-6.96396	-35.6977	25	PB	false
2401107	Areia Branca	-4.95254	-37.1252	24	RN	false
2800506	Areia Branca	-10.758	-37.3251	28	SE	false
2501153	Areia de Baraúnas	-7.11702	-36.9404	25	PB	false
2501203	Areial	-7.04789	-35.9313	25	PB	false
3503505	Areias	-22.5786	-44.6992	35	SP	false
3503604	Areiópolis	-22.6672	-48.6681	35	SP	false
5101308	Arenápolis	-14.4472	-56.8437	51	MT	false
5202353	Arenópolis	-16.3837	-51.5563	52	GO	false
2401206	Arês	-6.18831	-35.1608	24	RN	false
3104403	Argirita	-21.6083	-42.8292	31	MG	false
3104452	Aricanduva	-17.8666	-42.5533	31	MG	false
3104502	Arinos	-15.9187	-46.1043	31	MG	false
5101407	Aripuanã	-10.1723	-59.4568	51	MT	false
1100023	Ariquemes	-9.90571	-63.0325	11	RO	false
3503703	Ariranha	-21.1872	-48.7904	35	SP	false
4101853	Ariranha do Ivaí	-24.3857	-51.5839	41	PR	false
3300233	Armação dos Búzios	-22.7528	-41.8846	33	RJ	false
4201505	Armazém	-28.2448	-49.0215	42	SC	false
2301505	Arneiroz	-6.3165	-40.1653	23	CE	false
2200905	Aroazes	-6.11022	-41.7822	22	PI	false
2501302	Aroeiras	-7.54473	-35.7066	25	PB	false
2200954	Aroeiras do Itaim	-7.24502	-41.5325	22	PI	false
2201002	Arraial	-6.65075	-42.5418	22	PI	false
3300258	Arraial do Cabo	-22.9774	-42.0267	33	RJ	false
1702406	Arraias	-12.9287	-46.9359	17	TO	false
4301008	Arroio do Meio	-29.4014	-51.9557	43	RS	false
4301073	Arroio do Padre	-31.4389	-52.4246	43	RS	false
4301057	Arroio do Sal	-29.5439	-49.8895	43	RS	false
4301206	Arroio do Tigre	-29.3348	-53.0966	43	RS	false
4301107	Arroio dos Ratos	-30.0875	-51.7275	43	RS	false
4301305	Arroio Grande	-32.2327	-53.0862	43	RS	false
4201604	Arroio Trinta	-26.9257	-51.3407	42	SC	false
3503802	Artur Nogueira	-22.5727	-47.1727	35	SP	false
5202502	Aruanã	-14.9166	-51.075	52	GO	false
3503901	Arujá	-23.3965	-46.32	35	SP	false
4201653	Arvoredo	-27.0748	-52.4543	42	SC	false
4301404	Arvorezinha	-28.8737	-52.1781	43	RS	false
4201703	Ascurra	-26.9548	-49.3783	42	SC	false
3503950	Aspásia	-20.16	-50.728	35	SP	false
4101903	Assaí	-23.3697	-50.8459	41	PR	false
2301604	Assaré	-6.8669	-39.8689	23	CE	false
3504008	Assis	-22.66	-50.4183	35	SP	false
1200054	Assis Brasil	-10.9298	-69.5738	12	AC	false
4102000	Assis Chateaubriand	-24.4168	-53.5213	41	PR	false
2501351	Assunção	-7.07231	-36.725	25	PB	false
2201051	Assunção do Piauí	-5.865	-41.0389	22	PI	false
3104601	Astolfo Dutra	-21.3184	-42.8572	31	MG	false
4102109	Astorga	-23.2318	-51.6668	41	PR	false
4102208	Atalaia	-23.1517	-52.0551	41	PR	false
2700409	Atalaia	-9.5119	-36.0086	27	AL	false
1300201	Atalaia do Norte	-4.37055	-70.1967	13	AM	false
4201802	Atalanta	-27.4219	-49.7789	42	SC	false
3104700	Ataléia	-18.0438	-41.1149	31	MG	false
3504107	Atibaia	-23.1171	-46.5563	35	SP	false
3200706	Atilio Vivacqua	-20.913	-41.1986	32	ES	false
1702554	Augustinópolis	-5.46863	-47.8863	17	TO	false
1500909	Augusto Corrêa	-1.05109	-46.6147	15	PA	false
3104809	Augusto de Lima	-18.0997	-44.2655	31	MG	false
4301503	Augusto Pestana	-28.5172	-53.9883	43	RS	false
2401305	Augusto Severo (Campo Grande)	-5.86206	-37.3135	24	RN	false
4301552	Áurea	-27.6936	-52.0505	43	RS	false
2902401	Aurelino Leal	-14.321	-39.329	29	BA	false
3504206	Auriflama	-20.6836	-50.5572	35	SP	false
5202601	Aurilândia	-16.6773	-50.4641	52	GO	false
2301703	Aurora	-6.93349	-38.9742	23	CE	false
4201901	Aurora	-27.3098	-49.6295	42	SC	false
1500958	Aurora do Pará	-2.14898	-47.5677	15	PA	false
1702703	Aurora do Tocantins	-12.7105	-46.4076	17	TO	false
1300300	Autazes	-3.58574	-59.1256	13	AM	false
3504305	Avaí	-22.1514	-49.3356	35	SP	false
3504404	Avanhandava	-21.4584	-49.9509	35	SP	false
3504503	Avaré	-23.1067	-48.9251	35	SP	false
1501006	Aveiro	-3.60841	-55.3199	15	PA	false
2201101	Avelino Lopes	-10.1345	-43.9563	22	PI	false
5202809	Avelinópolis	-16.4672	-49.7579	52	GO	false
2101103	Axixá	-2.83939	-44.062	21	MA	false
1702901	Axixá do Tocantins	-5.61275	-47.7701	17	TO	false
1703008	Babaçulândia	-7.20923	-47.7613	17	TO	false
2101202	Bacabal	-4.22447	-44.7832	21	MA	false
2101251	Bacabeira	-2.96452	-44.3164	21	MA	false
2101301	Bacuri	-1.6965	-45.1328	21	MA	false
2101350	Bacurituba	-2.71	-44.7329	21	MA	false
3504602	Bady Bassitt	-20.9197	-49.4385	35	SP	false
3104908	Baependi	-21.957	-44.8874	31	MG	false
4301602	Bagé	-31.3297	-54.0999	43	RS	false
1501105	Bagre	-1.90057	-50.1987	15	PA	false
2501401	Baía da Traição	-6.69209	-34.9381	25	PB	false
2401404	Baía Formosa	-6.37161	-35.0033	24	RN	false
2902500	Baianópolis	-12.3016	-44.5388	29	BA	false
1501204	Baião	-2.79021	-49.6694	15	PA	false
2902609	Baixa Grande	-11.9519	-40.169	29	BA	false
2201150	Baixa Grande do Ribeiro	-7.84903	-45.219	22	PI	false
2301802	Baixio	-6.71945	-38.7134	23	CE	false
3200805	Baixo Guandu	-19.5213	-41.0109	32	ES	false
3504701	Balbinos	-21.8963	-49.3619	35	SP	false
3105004	Baldim	-19.2832	-43.9613	31	MG	false
5203104	Baliza	-16.1966	-52.5393	52	GO	false
4201950	Balneário Arroio do Silva	-28.9806	-49.4237	42	SC	false
4202057	Balneário Barra do Sul	-26.4597	-48.6123	42	SC	false
4202008	Balneário Camboriú	-26.9926	-48.6352	42	SC	false
4202073	Balneário Gaivota	-29.1527	-49.5841	42	SC	false
4212809	Balneário Piçarras	-26.7639	-48.6717	42	SC	false
4301636	Balneário Pinhal	-30.2419	-50.2337	43	RS	false
4220000	Balneário Rincão	-28.8314	-49.2352	42	SC	false
4102307	Balsa Nova	-25.5804	-49.6291	41	PR	false
3504800	Bálsamo	-20.7348	-49.5865	35	SP	false
2101400	Balsas	-7.53214	-46.0372	21	MA	false
3105103	Bambuí	-20.0166	-45.9754	31	MG	false
2301851	Banabuiú	-5.30454	-38.9132	23	CE	false
3504909	Bananal	-22.6819	-44.3281	35	SP	false
2501500	Bananeiras	-6.74775	-35.6246	25	PB	false
3105202	Bandeira	-15.8783	-40.5622	31	MG	false
3105301	Bandeira do Sul	-21.7308	-46.3833	31	MG	false
4202081	Bandeirante	-26.7705	-53.6413	42	SC	false
5001508	Bandeirantes	-19.9275	-54.3585	50	MS	false
4102406	Bandeirantes	-23.1078	-50.3704	41	PR	false
1703057	Bandeirantes do Tocantins	-7.75612	-48.5836	17	TO	false
1501253	Bannach	-7.34779	-50.3959	15	PA	false
2902658	Banzaê	-10.5788	-38.6212	29	BA	false
4301651	Barão	-29.3725	-51.4949	43	RS	false
3505005	Barão de Antonina	-23.6284	-49.5634	35	SP	false
3105400	Barão de Cocais	-19.9389	-43.4755	31	MG	false
4301701	Barão de Cotegipe	-27.6208	-52.3798	43	RS	false
2101509	Barão de Grajaú	-6.74463	-43.0261	21	MA	false
5101605	Barão de Melgaço	-16.2067	-55.9623	51	MT	false
3105509	Barão de Monte Alto	-21.2444	-42.2372	31	MG	false
4301750	Barão do Triunfo	-30.3891	-51.7384	43	RS	false
2401453	Baraúna	-5.06977	-37.6129	24	RN	false
2501534	Baraúna	-6.63484	-36.2601	25	PB	false
3105608	Barbacena	-21.2214	-43.7703	31	MG	false
2301901	Barbalha	-7.2982	-39.3021	23	CE	false
3505104	Barbosa	-21.2657	-49.9518	35	SP	false
4102505	Barbosa Ferraz	-24.0334	-52.004	41	PR	false
1501303	Barcarena	-1.51187	-48.6195	15	PA	false
2401503	Barcelona	-5.94284	-35.9247	24	RN	false
1300409	Barcelos	-0.983373	-62.9311	13	AM	false
3505203	Bariri	-22.073	-48.7438	35	SP	false
2902708	Barra	-11.0859	-43.1459	29	BA	false
4202099	Barra Bonita	-26.654	-53.44	42	SC	false
3505302	Barra Bonita	-22.4909	-48.5583	35	SP	false
2201176	Barra D'Alcântara	-6.51645	-42.1146	22	PI	false
2902807	Barra da Estiva	-13.6237	-41.3347	29	BA	false
2601300	Barra de Guabiraba	-8.42075	-35.6585	26	PE	false
2501609	Barra de Santa Rosa	-6.71816	-36.0671	25	PB	false
2501575	Barra de Santana	-7.51809	-35.9913	25	PB	false
2700508	Barra de Santo Antônio	-9.4023	-35.5101	27	AL	false
3200904	Barra de São Francisco	-18.7548	-40.8965	32	ES	false
2501708	Barra de São Miguel	-7.74603	-36.3209	25	PB	false
2700607	Barra de São Miguel	-9.83842	-35.9057	27	AL	false
5101704	Barra do Bugres	-15.0702	-57.1878	51	MT	false
3505351	Barra do Chapéu	-24.4722	-49.0238	35	SP	false
2902906	Barra do Choça	-14.8654	-40.5791	29	BA	false
2101608	Barra do Corda	-5.49682	-45.2485	21	MA	false
5101803	Barra do Garças	-15.8804	-52.264	51	MT	false
4301859	Barra do Guarita	-27.1927	-53.7109	43	RS	false
4102703	Barra do Jacaré	-23.116	-50.1842	41	PR	false
2903003	Barra do Mendes	-11.81	-42.059	29	BA	false
1703073	Barra do Ouro	-7.69593	-47.6776	17	TO	false
3300308	Barra do Piraí	-22.4715	-43.8269	33	RJ	false
4301875	Barra do Quaraí	-30.2029	-57.5497	43	RS	false
4301909	Barra do Ribeiro	-30.2939	-51.3014	43	RS	false
4301925	Barra do Rio Azul	-27.4069	-52.4084	43	RS	false
2903102	Barra do Rocha	-14.2	-39.5991	29	BA	false
3505401	Barra do Turvo	-24.759	-48.5013	35	SP	false
2800605	Barra dos Coqueiros	-10.8996	-37.0323	28	SE	false
3105707	Barra Longa	-20.2869	-43.0402	31	MG	false
3300407	Barra Mansa	-22.5481	-44.1752	33	RJ	false
4202107	Barra Velha	-26.637	-48.6933	42	SC	false
4301800	Barracão	-27.6739	-51.4585	43	RS	false
4102604	Barracão	-26.2502	-53.6324	41	PR	false
2201200	Barras	-4.24468	-42.2922	22	PI	false
2301950	Barreira	-4.28921	-38.6429	23	CE	false
2903201	Barreiras	-12.1439	-44.9968	29	BA	false
2201309	Barreiras do Piauí	-9.9296	-45.4702	22	PI	false
1300508	Barreirinha	-2.79886	-57.0679	13	AM	false
2101707	Barreirinhas	-2.75863	-42.8232	21	MA	false
2601409	Barreiros	-8.81605	-35.1832	26	PE	false
3505500	Barretos	-20.5531	-48.5698	35	SP	false
3505609	Barrinha	-21.1864	-48.1636	35	SP	false
2302008	Barro	-7.17188	-38.7741	23	CE	false
2903235	Barro Alto	-11.7605	-41.9054	29	BA	false
5203203	Barro Alto	-14.9658	-48.9086	52	GO	false
2201408	Barro Duro	-5.81673	-42.5147	22	PI	false
2903300	Barro Preto	-14.7948	-39.476	29	BA	false
2903276	Barrocas	-11.5272	-39.0776	29	BA	false
1703107	Barrolândia	-9.83404	-48.7252	17	TO	false
2302057	Barroquinha	-3.02051	-41.1358	23	CE	false
4302006	Barros Cassal	-29.0947	-52.5836	43	RS	false
3105905	Barroso	-21.1907	-43.972	31	MG	false
3505708	Barueri	-23.5057	-46.879	35	SP	false
3505807	Bastos	-21.921	-50.7357	35	SP	false
5001904	Bataguassu	-21.7159	-52.4221	50	MS	false
2201507	Batalha	-4.0223	-42.0787	22	PI	false
2700706	Batalha	-9.6742	-37.133	27	AL	false
3505906	Batatais	-20.8929	-47.5921	35	SP	false
5002001	Batayporã	-22.2944	-53.2705	50	MS	false
2302107	Baturité	-4.32598	-38.8812	23	CE	false
3506003	Bauru	-22.3246	-49.0871	35	SP	false
2501807	Bayeux	-7.1238	-34.9293	25	PB	false
3506102	Bebedouro	-20.9491	-48.4791	35	SP	false
2302206	Beberibe	-4.17741	-38.1271	23	CE	false
2302305	Bela Cruz	-3.04996	-40.1671	23	CE	false
5002100	Bela Vista	-22.1073	-56.5263	50	MS	false
4102752	Bela Vista da Caroba	-25.8842	-53.6725	41	PR	false
5203302	Bela Vista de Goiás	-16.9693	-48.9513	52	GO	false
3106002	Bela Vista de Minas	-19.8302	-43.0922	31	MG	false
2101772	Bela Vista do Maranhão	-3.72618	-45.3075	21	MA	false
4102802	Bela Vista do Paraíso	-22.9937	-51.1927	41	PR	false
2201556	Bela Vista do Piauí	-7.98809	-41.8675	22	PI	false
4202131	Bela Vista do Toldo	-26.2746	-50.4664	42	SC	false
2101731	Belágua	-3.15485	-43.5122	21	MA	false
1501402	Belém	-1.4554	-48.4898	15	PA	true 
2501906	Belém	-6.74261	-35.5166	25	PB	false
2700805	Belém	-9.57047	-36.4904	27	AL	false
2601508	Belém de Maria	-8.62504	-35.8335	26	PE	false
2502003	Belém do Brejo do Cruz	-6.18515	-37.5348	25	PB	false
2201572	Belém do Piauí	-7.36652	-40.9688	22	PI	false
2601607	Belém do São Francisco	-8.75046	-38.9623	26	PE	false
3300456	Belford Roxo	-22.764	-43.3992	33	RJ	false
3106101	Belmiro Braga	-21.944	-43.4084	31	MG	false
4202156	Belmonte	-26.843	-53.5758	42	SC	false
2903409	Belmonte	-15.8608	-38.8758	29	BA	false
2903508	Belo Campo	-15.0334	-41.2652	29	BA	false
3106200	Belo Horizonte	-19.9102	-43.9266	31	MG	true 
2601706	Belo Jardim	-8.3313	-36.4258	26	PE	false
2700904	Belo Monte	-9.82272	-37.277	27	AL	false
3106309	Belo Oriente	-19.2199	-42.4828	31	MG	false
3106408	Belo Vale	-20.4077	-44.0275	31	MG	false
1501451	Belterra	-2.63609	-54.9374	15	PA	false
2201606	Beneditinos	-5.45676	-42.3638	22	PI	false
2101806	Benedito Leite	-7.21037	-44.5577	21	MA	false
4202206	Benedito Novo	-26.781	-49.3593	42	SC	false
1501501	Benevides	-1.36183	-48.2434	15	PA	false
1300607	Benjamin Constant	-4.37768	-70.0342	13	AM	false
4302055	Benjamin Constant do Sul	-27.5086	-52.5995	43	RS	false
3506201	Bento de Abreu	-21.2686	-50.814	35	SP	false
2401602	Bento Fernandes	-5.69906	-35.813	24	RN	false
4302105	Bento Gonçalves	-29.1662	-51.5165	43	RS	false
2101905	Bequimão	-2.44162	-44.7842	21	MA	false
3106507	Berilo	-16.9567	-42.4606	31	MG	false
3106655	Berizal	-15.61	-41.7432	31	MG	false
2502052	Bernardino Batista	-6.44572	-38.5521	25	PB	false
3506300	Bernardino de Campos	-23.0164	-49.4679	35	SP	false
2101939	Bernardo do Mearim	-4.62666	-44.7608	21	MA	false
1703206	Bernardo Sayão	-7.87481	-48.8893	17	TO	false
3506359	Bertioga	-23.8486	-46.1396	35	SP	false
2201705	Bertolínia	-7.63338	-43.9498	22	PI	false
3106606	Bertópolis	-17.059	-40.58	31	MG	false
1300631	Beruri	-3.89874	-61.3616	13	AM	false
2601805	Betânia	-8.26787	-38.0345	26	PE	false
2201739	Betânia do Piauí	-8.14376	-40.7989	22	PI	false
3106705	Betim	-19.9668	-44.2008	31	MG	false
2601904	Bezerros	-8.2328	-35.796	26	PE	false
3106804	Bias Fortes	-21.602	-43.7574	31	MG	false
3106903	Bicas	-21.7232	-43.056	31	MG	false
4202305	Biguaçu	-27.496	-48.6598	42	SC	false
3506409	Bilac	-21.404	-50.4746	35	SP	false
3107000	Biquinhas	-18.7754	-45.4974	31	MG	false
3506508	Birigui	-21.291	-50.3432	35	SP	false
3506607	Biritiba-Mirim	-23.5698	-46.0407	35	SP	false
2903607	Biritinga	-11.6072	-38.8051	29	BA	false
4102901	Bituruna	-26.1607	-51.5518	41	PR	false
4202404	Blumenau	-26.9155	-49.0709	42	SC	false
4103008	Boa Esperança	-24.2467	-52.7876	41	PR	false
3107109	Boa Esperança	-21.0927	-45.5612	31	MG	false
3201001	Boa Esperança	-18.5395	-40.3025	32	ES	false
4103024	Boa Esperança do Iguaçu	-25.6324	-53.2108	41	PR	false
3506706	Boa Esperança do Sul	-21.9918	-48.3906	35	SP	false
2201770	Boa Hora	-4.41404	-42.1357	22	PI	false
2903706	Boa Nova	-14.3598	-40.2064	29	BA	false
2502102	Boa Ventura	-7.40982	-38.2113	25	PB	false
4103040	Boa Ventura de São Roque	-24.8688	-51.6276	41	PR	false
2302404	Boa Viagem	-5.11258	-39.7337	23	CE	false
1400100	Boa Vista	2.82384	-60.6753	14	RR	true 
2502151	Boa Vista	-7.26365	-36.2357	25	PB	false
4103057	Boa Vista da Aparecida	-25.4308	-53.4117	41	PR	false
4302154	Boa Vista das Missões	-27.6671	-53.3102	43	RS	false
4302204	Boa Vista do Buricá	-27.6693	-54.1082	43	RS	false
4302220	Boa Vista do Cadeado	-28.5791	-53.8108	43	RS	false
2101970	Boa Vista do Gurupi	-1.77614	-46.3002	21	MA	false
4302238	Boa Vista do Incra	-28.8185	-53.391	43	RS	false
1300680	Boa Vista do Ramos	-2.97409	-57.5873	13	AM	false
4302253	Boa Vista do Sul	-29.3544	-51.6687	43	RS	false
2903805	Boa Vista do Tupim	-12.6498	-40.6064	29	BA	false
2701001	Boca da Mata	-9.64308	-36.2125	27	AL	false
1300706	Boca do Acre	-8.74232	-67.3919	13	AM	false
2201804	Bocaina	-6.94124	-41.3168	22	PI	false
3506805	Bocaina	-22.1365	-48.523	35	SP	false
3107208	Bocaina de Minas	-22.1697	-44.3972	31	MG	false
4202438	Bocaina do Sul	-27.7455	-49.9423	42	SC	false
3107307	Bocaiúva	-17.1135	-43.8104	31	MG	false
4103107	Bocaiúva do Sul	-25.2066	-49.1141	41	PR	false
2401651	Bodó	-5.98027	-36.4167	24	RN	false
2602001	Bodocó	-7.77759	-39.9338	26	PE	false
5002159	Bodoquena	-20.537	-56.7127	50	MS	false
3506904	Bofete	-23.1055	-48.2582	35	SP	false
3507001	Boituva	-23.2855	-47.6786	35	SP	false
2602100	Bom Conselho	-9.16919	-36.6857	26	PE	false
3107406	Bom Despacho	-19.7386	-45.2622	31	MG	false
3300506	Bom Jardim	-22.1545	-42.4251	33	RJ	false
2602209	Bom Jardim	-7.79695	-35.5784	26	PE	false
2102002	Bom Jardim	-3.54129	-45.606	21	MA	false
4202503	Bom Jardim da Serra	-28.3377	-49.6373	42	SC	false
5203401	Bom Jardim de Goiás	-16.2063	-52.1728	52	GO	false
3107505	Bom Jardim de Minas	-21.9479	-44.1885	31	MG	false
4202537	Bom Jesus	-26.7326	-52.3919	42	SC	false
4302303	Bom Jesus	-28.6697	-50.4295	43	RS	false
2201903	Bom Jesus	-9.07124	-44.359	22	PI	false
2401701	Bom Jesus	-5.98648	-35.5792	24	RN	false
2502201	Bom Jesus	-6.81601	-38.6453	25	PB	false
2903904	Bom Jesus da Lapa	-13.2506	-43.4108	29	BA	false
3107604	Bom Jesus da Penha	-21.0148	-46.5174	31	MG	false
2903953	Bom Jesus da Serra	-14.3663	-40.5126	29	BA	false
2102036	Bom Jesus das Selvas	-4.47638	-46.8641	21	MA	false
5203500	Bom Jesus de Goiás	-18.2173	-49.74	52	GO	false
3107703	Bom Jesus do Amparo	-19.7054	-43.4782	31	MG	false
5101852	Bom Jesus do Araguaia	-12.1706	-51.5032	51	MT	false
3107802	Bom Jesus do Galho	-19.836	-42.3165	31	MG	false
3300605	Bom Jesus do Itabapoana	-21.1449	-41.6822	33	RJ	false
3201100	Bom Jesus do Norte	-21.1173	-41.6731	32	ES	false
4202578	Bom Jesus do Oeste	-26.6927	-53.0967	42	SC	false
4103156	Bom Jesus do Sul	-26.1958	-53.5955	41	PR	false
1501576	Bom Jesus do Tocantins	-5.0424	-48.6047	15	PA	false
1703305	Bom Jesus do Tocantins	-8.96306	-48.165	17	TO	false
3507100	Bom Jesus dos Perdões	-23.1356	-46.4675	35	SP	false
2102077	Bom Lugar	-4.37311	-45.0326	21	MA	false
4302352	Bom Princípio	-29.4856	-51.3548	43	RS	false
2201919	Bom Princípio do Piauí	-3.19631	-41.6403	22	PI	false
4302378	Bom Progresso	-27.5399	-53.8716	43	RS	false
3107901	Bom Repouso	-22.4675	-46.144	31	MG	false
4202602	Bom Retiro	-27.799	-49.487	42	SC	false
4302402	Bom Retiro do Sul	-29.6071	-51.9456	43	RS	false
3108008	Bom Sucesso	-21.0329	-44.7537	31	MG	false
4103206	Bom Sucesso	-23.7063	-51.7671	41	PR	false
2502300	Bom Sucesso	-6.44176	-37.9234	25	PB	false
3507159	Bom Sucesso de Itararé	-24.3155	-49.1451	35	SP	false
4103222	Bom Sucesso do Sul	-26.0731	-52.8353	41	PR	false
4202453	Bombinhas	-27.1382	-48.5146	42	SC	false
1400159	Bonfim	3.36161	-59.8333	14	RR	false
3108107	Bonfim	-20.3302	-44.2366	31	MG	false
2201929	Bonfim do Piauí	-9.1605	-42.8865	22	PI	false
5203559	Bonfinópolis	-16.6173	-48.9616	52	GO	false
3108206	Bonfinópolis de Minas	-16.568	-45.9839	31	MG	false
2904001	Boninal	-12.7069	-41.8286	29	BA	false
2602308	Bonito	-8.47163	-35.7292	26	PE	false
2904050	Bonito	-11.9668	-41.2647	29	BA	false
1501600	Bonito	-1.36745	-47.3066	15	PA	false
5002209	Bonito	-21.1261	-56.4836	50	MS	false
3108255	Bonito de Minas	-15.3231	-44.7543	31	MG	false
2502409	Bonito de Santa Fé	-7.31341	-38.5133	25	PB	false
5203575	Bonópolis	-13.6329	-49.8106	52	GO	false
2502508	Boqueirão	-7.487	-36.1309	25	PB	false
4302451	Boqueirão do Leão	-29.3046	-52.4284	43	RS	false
2201945	Boqueirão do Piauí	-4.48181	-42.1212	22	PI	false
2800670	Boquim	-11.1397	-37.6195	28	SE	false
2904100	Boquira	-12.8205	-42.7324	29	BA	false
3507209	Borá	-22.2696	-50.5409	35	SP	false
3507308	Boracéia	-22.1926	-48.7808	35	SP	false
1300805	Borba	-4.39154	-59.5874	13	AM	false
2502706	Borborema	-6.80199	-35.6187	25	PB	false
3507407	Borborema	-21.6214	-49.0741	35	SP	false
3108305	Borda da Mata	-22.2707	-46.1653	31	MG	false
3507456	Borebi	-22.5728	-48.9707	35	SP	false
4103305	Borrazópolis	-23.9366	-51.5875	41	PR	false
4302501	Bossoroca	-28.7291	-54.9035	43	RS	false
3108404	Botelhos	-21.6412	-46.391	31	MG	false
3507506	Botucatu	-22.8837	-48.4437	35	SP	false
3108503	Botumirim	-16.8657	-43.0086	31	MG	false
2904209	Botuporã	-13.3772	-42.5163	29	BA	false
4202701	Botuverá	-27.2007	-49.0689	42	SC	false
4302584	Bozano	-28.3659	-53.772	43	RS	false
4202800	Braço do Norte	-28.2681	-49.1701	42	SC	false
4202859	Braço do Trombudo	-27.3586	-49.8821	42	SC	false
4302600	Braga	-27.6173	-53.7405	43	RS	false
1501709	Bragança	-1.06126	-46.7826	15	PA	false
3507605	Bragança Paulista	-22.9527	-46.5419	35	SP	false
4103354	Braganey	-24.8173	-53.1218	41	PR	false
2701100	Branquinha	-9.23342	-36.0162	27	AL	false
3108701	Brás Pires	-20.8419	-43.2406	31	MG	false
1501725	Brasil Novo	-3.29792	-52.534	15	PA	false
5002308	Brasilândia	-21.2544	-52.0365	50	MS	false
3108552	Brasilândia de Minas	-16.9999	-46.0081	31	MG	false
4103370	Brasilândia do Sul	-24.1978	-53.5275	41	PR	false
1703602	Brasilândia do Tocantins	-8.38918	-48.4822	17	TO	false
1200104	Brasiléia	-10.995	-68.7497	12	AC	false
2201960	Brasileira	-4.1337	-41.7859	22	PI	false
5300108	Brasília	-15.7795	-47.9297	53	DF	true 
3108602	Brasília de Minas	-16.2104	-44.4299	31	MG	false
5101902	Brasnorte	-12.1474	-57.9833	51	MT	false
3507704	Braúna	-21.499	-50.3175	35	SP	false
3108800	Braúnas	-19.0562	-42.7099	31	MG	false
5203609	Brazabrantes	-16.4281	-49.3863	52	GO	false
3108909	Brazópolis	-22.4743	-45.6166	31	MG	false
2602407	Brejão	-9.02915	-36.566	26	PE	false
3201159	Brejetuba	-20.1395	-41.2954	32	ES	false
2401800	Brejinho	-6.18566	-35.3591	24	RN	false
2602506	Brejinho	-7.34694	-37.2865	26	PE	false
1703701	Brejinho de Nazaré	-11.0058	-48.5683	17	TO	false
2102101	Brejo	-3.67796	-42.7527	21	MA	false
3507753	Brejo Alegre	-21.1651	-50.1861	35	SP	false
2602605	Brejo da Madre de Deus	-8.14933	-36.3741	26	PE	false
2102150	Brejo de Areia	-4.334	-45.581	21	MA	false
2502805	Brejo do Cruz	-6.34185	-37.4943	25	PB	false
2201988	Brejo do Piauí	-8.20314	-42.8229	22	PI	false
2502904	Brejo dos Santos	-6.37065	-37.8253	25	PB	false
2800704	Brejo Grande	-10.4297	-36.4611	28	SE	false
1501758	Brejo Grande do Araguaia	-5.69822	-48.4103	15	PA	false
2302503	Brejo Santo	-7.48469	-38.9799	23	CE	false
2904308	Brejões	-13.1039	-39.7988	29	BA	false
2904407	Brejolândia	-12.4815	-43.9679	29	BA	false
1501782	Breu Branco	-3.77191	-49.5735	15	PA	false
1501808	Breves	-1.68036	-50.4791	15	PA	false
5203807	Britânia	-15.2428	-51.1602	52	GO	false
4302659	Brochier	-29.5501	-51.5945	43	RS	false
3507803	Brodowski	-20.9845	-47.6572	35	SP	false
3507902	Brotas	-22.2795	-48.1251	35	SP	false
2904506	Brotas de Macaúbas	-11.9915	-42.6326	29	BA	false
3109006	Brumadinho	-20.151	-44.2007	31	MG	false
2904605	Brumado	-14.2021	-41.6696	29	BA	false
4202875	Brunópolis	-27.3058	-50.8684	42	SC	false
4202909	Brusque	-27.0977	-48.9107	42	SC	false
3109105	Bueno Brandão	-22.4383	-46.3491	31	MG	false
3109204	Buenópolis	-17.8744	-44.1775	31	MG	false
2602704	Buenos Aires	-7.72449	-35.3182	26	PE	false
2904704	Buerarema	-14.9595	-39.3028	29	BA	false
3109253	Bugre	-19.4231	-42.2552	31	MG	false
2602803	Buíque	-8.61954	-37.1606	26	PE	false
1200138	Bujari	-9.81528	-67.955	12	AC	false
1501907	Bujaru	-1.51762	-48.0381	15	PA	false
3508009	Buri	-23.7977	-48.5958	35	SP	false
3508108	Buritama	-21.0661	-50.1475	35	SP	false
2102200	Buriti	-3.94169	-42.9179	21	MA	false
5203906	Buriti Alegre	-18.1378	-49.0404	52	GO	false
2102309	Buriti Bravo	-5.83239	-43.8353	21	MA	false
5203939	Buriti de Goiás	-16.1792	-50.4302	52	GO	false
1703800	Buriti do Tocantins	-5.31448	-48.2271	17	TO	false
2202000	Buriti dos Lopes	-3.18259	-41.8695	22	PI	false
2202026	Buriti dos Montes	-5.30584	-41.0933	22	PI	false
2102325	Buriticupu	-4.32375	-46.4409	21	MA	false
5203962	Buritinópolis	-14.4772	-46.4076	52	GO	false
2904753	Buritirama	-10.7171	-43.6302	29	BA	false
2102358	Buritirana	-5.59823	-47.0131	21	MA	false
1100452	Buritis	-10.1943	-63.8324	11	RO	false
3109303	Buritis	-15.6218	-46.4221	31	MG	false
3508207	Buritizal	-20.1911	-47.7096	35	SP	false
3109402	Buritizeiro	-17.3656	-44.9606	31	MG	false
4302709	Butiá	-30.1179	-51.9601	43	RS	false
1300839	Caapiranga	-3.31537	-61.2206	13	AM	false
2503001	Caaporã	-7.51351	-34.9055	25	PB	false
5002407	Caarapó	-22.6368	-54.8209	50	MS	false
2904803	Caatiba	-14.9699	-40.4092	29	BA	false
2503100	Cabaceiras	-7.48899	-36.287	25	PB	false
2904852	Cabaceiras do Paraguaçu	-12.5317	-39.1902	29	BA	false
3109451	Cabeceira Grande	-16.0335	-47.0862	31	MG	false
5204003	Cabeceiras	-15.7995	-46.9265	52	GO	false
2202059	Cabeceiras do Piauí	-4.4773	-42.3069	22	PI	false
2503209	Cabedelo	-6.98731	-34.8284	25	PB	false
1100031	Cabixi	-13.4945	-60.552	11	RO	false
2602902	Cabo de Santo Agostinho	-8.28218	-35.0253	26	PE	false
3300704	Cabo Frio	-22.8894	-42.0286	33	RJ	false
3109501	Cabo Verde	-21.4699	-46.3919	31	MG	false
3508306	Cabrália Paulista	-22.4576	-49.3393	35	SP	false
3508405	Cabreúva	-23.3053	-47.1362	35	SP	false
2603009	Cabrobó	-8.50548	-39.3094	26	PE	false
4203006	Caçador	-26.7757	-51.012	42	SC	false
3508504	Caçapava	-23.0992	-45.7076	35	SP	false
4302808	Caçapava do Sul	-30.5144	-53.4827	43	RS	false
1100601	Cacaulândia	-10.349	-62.9043	11	RO	false
4302907	Cacequi	-29.8883	-54.822	43	RS	false
5102504	Cáceres	-16.0764	-57.6818	51	MT	false
2904902	Cachoeira	-12.5994	-38.9587	29	BA	false
5204102	Cachoeira Alta	-18.7618	-50.9432	52	GO	false
3109600	Cachoeira da Prata	-19.521	-44.4544	31	MG	false
5204201	Cachoeira de Goiás	-16.6635	-50.646	52	GO	false
3109709	Cachoeira de Minas	-22.3511	-45.7809	31	MG	false
3102704	Cachoeira de Pajeú	-15.9688	-41.4948	31	MG	false
1502004	Cachoeira do Arari	-1.01226	-48.9503	15	PA	false
1501956	Cachoeira do Piriá	-1.75974	-46.5459	15	PA	false
4303004	Cachoeira do Sul	-30.033	-52.8928	43	RS	false
2503308	Cachoeira dos Índios	-6.91353	-38.676	25	PB	false
5204250	Cachoeira Dourada	-18.4859	-49.4766	52	GO	false
3109808	Cachoeira Dourada	-18.5161	-49.5039	31	MG	false
2102374	Cachoeira Grande	-2.93074	-44.0528	21	MA	false
3508603	Cachoeira Paulista	-22.6665	-45.0154	35	SP	false
3300803	Cachoeiras de Macacu	-22.4658	-42.6523	33	RJ	false
1703826	Cachoeirinha	-6.1156	-47.9234	17	TO	false
2603108	Cachoeirinha	-8.48668	-36.2402	26	PE	false
4303103	Cachoeirinha	-29.9472	-51.1016	43	RS	false
3201209	Cachoeiro de Itapemirim	-20.8462	-41.1198	32	ES	false
2503407	Cacimba de Areia	-7.12128	-37.1563	25	PB	false
2503506	Cacimba de Dentro	-6.6386	-35.7778	25	PB	false
2503555	Cacimbas	-7.20721	-37.0604	25	PB	false
2701209	Cacimbinhas	-9.40121	-36.9911	27	AL	false
4303202	Cacique Doble	-27.767	-51.6597	43	RS	false
1100049	Cacoal	-11.4343	-61.4562	11	RO	false
3508702	Caconde	-21.528	-46.6437	35	SP	false
5204300	Caçu	-18.5594	-51.1328	52	GO	false
2905008	Caculé	-14.5003	-42.2229	29	BA	false
2905107	Caém	-11.0677	-40.432	29	BA	false
3109907	Caetanópolis	-19.2971	-44.4189	31	MG	false
2905156	Caetanos	-14.3347	-40.9175	29	BA	false
3110004	Caeté	-19.8826	-43.6704	31	MG	false
2603207	Caetés	-8.7803	-36.6268	26	PE	false
2905206	Caetité	-14.0684	-42.4861	29	BA	false
2905305	Cafarnaum	-11.6914	-41.4688	29	BA	false
4103404	Cafeara	-22.789	-51.7142	41	PR	false
3508801	Cafelândia	-21.8031	-49.6092	35	SP	false
4103453	Cafelândia	-24.6189	-53.3207	41	PR	false
4103479	Cafezal do Sul	-23.9005	-53.5124	41	PR	false
3508900	Caiabu	-22.0127	-51.2394	35	SP	false
3110103	Caiana	-20.6956	-41.9292	31	MG	false
5204409	Caiapônia	-16.9539	-51.8091	52	GO	false
4303301	Caibaté	-28.2905	-54.6454	43	RS	false
4203105	Caibi	-27.0741	-53.2458	42	SC	false
4303400	Caiçara	-27.2791	-53.4257	43	RS	false
2503605	Caiçara	-6.62115	-35.4581	25	PB	false
2401859	Caiçara do Norte	-5.07091	-36.0717	24	RN	false
2401909	Caiçara do Rio do Vento	-5.76541	-35.9938	24	RN	false
2402006	Caicó	-6.45441	-37.1067	24	RN	false
3509007	Caieiras	-23.3607	-46.7397	35	SP	false
2905404	Cairu	-13.4904	-39.0465	29	BA	false
3509106	Caiuá	-21.8322	-51.9969	35	SP	false
3509205	Cajamar	-23.355	-46.8781	35	SP	false
2102408	Cajapió	-2.87326	-44.6741	21	MA	false
2102507	Cajari	-3.32742	-45.0145	21	MA	false
3509254	Cajati	-24.7324	-48.1223	35	SP	false
2503704	Cajazeiras	-6.88004	-38.5577	25	PB	false
2202075	Cajazeiras do Piauí	-6.79667	-42.3903	22	PI	false
2503753	Cajazeirinhas	-6.96016	-37.8009	25	PB	false
3509304	Cajobi	-20.8773	-48.8063	35	SP	false
2701308	Cajueiro	-9.3994	-36.1559	27	AL	false
2202083	Cajueiro da Praia	-2.93111	-41.3408	22	PI	false
3110202	Cajuri	-20.7903	-42.7925	31	MG	false
3509403	Cajuru	-21.2749	-47.303	35	SP	false
2603306	Calçado	-8.73108	-36.3366	26	PE	false
1600204	Calçoene	2.50475	-50.9512	16	AP	false
3110301	Caldas	-21.9183	-46.3843	31	MG	false
2503803	Caldas Brandão	-7.1025	-35.3272	25	PB	false
5204508	Caldas Novas	-17.7441	-48.6246	52	GO	false
5204557	Caldazinha	-16.7117	-49.0013	52	GO	false
2905503	Caldeirão Grande	-11.0208	-40.2956	29	BA	false
2202091	Caldeirão Grande do Piauí	-7.3314	-40.6366	22	PI	false
4103503	Califórnia	-23.6566	-51.3574	41	PR	false
4203154	Calmon	-26.5942	-51.095	42	SC	false
2603405	Calumbi	-7.93551	-38.1482	26	PE	false
2905602	Camacan	-15.4142	-39.4919	29	BA	false
2905701	Camaçari	-12.6996	-38.3263	29	BA	false
3110400	Camacho	-20.6294	-45.1593	31	MG	false
2503902	Camalaú	-7.88503	-36.8242	25	PB	false
2905800	Camamu	-13.9398	-39.1071	29	BA	false
3110509	Camanducaia	-22.7515	-46.1494	31	MG	false
5002605	Camapuã	-19.5347	-54.0431	50	MS	false
4303509	Camaquã	-30.8489	-51.8043	43	RS	false
2603454	Camaragibe	-8.02351	-34.9782	26	PE	false
4303558	Camargo	-28.588	-52.2003	43	RS	false
4103602	Cambará	-23.0423	-50.0753	41	PR	false
4303608	Cambará do Sul	-29.0474	-50.1465	43	RS	false
4103701	Cambé	-23.2766	-51.2798	41	PR	false
4103800	Cambira	-23.589	-51.5792	41	PR	false
4203204	Camboriú	-27.0241	-48.6503	42	SC	false
3300902	Cambuci	-21.5691	-41.9187	33	RJ	false
3110608	Cambuí	-22.6115	-46.0572	31	MG	false
3110707	Cambuquira	-21.854	-45.2896	31	MG	false
1502103	Cametá	-2.24295	-49.4979	15	PA	false
2302602	Camocim	-2.9005	-40.8544	23	CE	false
2603504	Camocim de São Félix	-8.35865	-35.7653	26	PE	false
3110806	Campanário	-18.2427	-41.7355	31	MG	false
3110905	Campanha	-21.836	-45.4004	31	MG	false
3111002	Campestre	-21.7079	-46.2381	31	MG	false
2701357	Campestre	-8.84723	-35.5685	27	AL	false
4303673	Campestre da Serra	-28.7926	-51.0941	43	RS	false
5204607	Campestre de Goiás	-16.7624	-49.695	52	GO	false
2102556	Campestre do Maranhão	-6.17075	-47.3625	21	MA	false
4103909	Campina da Lagoa	-24.5893	-52.7976	41	PR	false
4303707	Campina das Missões	-27.9888	-54.8416	43	RS	false
3509452	Campina do Monte Alegre	-23.5895	-48.4758	35	SP	false
4103958	Campina do Simão	-25.0802	-51.8237	41	PR	false
2504009	Campina Grande	-7.22196	-35.8731	25	PB	false
4104006	Campina Grande do Sul	-25.3044	-49.0551	41	PR	false
3111101	Campina Verde	-19.5382	-49.4862	31	MG	false
5204656	Campinaçu	-13.787	-48.5704	52	GO	false
5102603	Campinápolis	-14.5162	-52.893	51	MT	false
3509502	Campinas	-22.9053	-47.0659	35	SP	false
2202109	Campinas do Piauí	-7.6593	-41.8775	22	PI	false
4303806	Campinas do Sul	-27.7174	-52.6248	43	RS	false
5204706	Campinorte	-14.3137	-49.1511	52	GO	false
4203303	Campo Alegre	-26.195	-49.2676	42	SC	false
2701407	Campo Alegre	-9.78451	-36.3525	27	AL	false
5204805	Campo Alegre de Goiás	-17.6363	-47.7768	52	GO	false
2905909	Campo Alegre de Lourdes	-9.52221	-43.0126	29	BA	false
2202117	Campo Alegre do Fidalgo	-8.38236	-41.8344	22	PI	false
3111150	Campo Azul	-16.5028	-44.8096	31	MG	false
3111200	Campo Belo	-20.8932	-45.2699	31	MG	false
4203402	Campo Belo do Sul	-27.8975	-50.7595	42	SC	false
4303905	Campo Bom	-29.6747	-51.0606	43	RS	false
4104055	Campo Bonito	-25.0294	-52.9939	41	PR	false
2801009	Campo do Brito	-10.7392	-37.4954	28	SE	false
3111309	Campo do Meio	-21.1127	-45.8273	31	MG	false
4104105	Campo do Tenente	-25.98	-49.6844	41	PR	false
4203501	Campo Erê	-26.3931	-53.0856	42	SC	false
3111408	Campo Florido	-19.7631	-48.5716	31	MG	false
2906006	Campo Formoso	-10.5105	-40.32	29	BA	false
2701506	Campo Grande	-9.95542	-36.7926	27	AL	false
5002704	Campo Grande	-20.4486	-54.6295	50	MS	true 
2202133	Campo Grande do Piauí	-7.12827	-41.0315	22	PI	false
4104204	Campo Largo	-25.4525	-49.529	41	PR	false
2202174	Campo Largo do Piauí	-3.80441	-42.64	22	PI	false
5204854	Campo Limpo de Goiás	-16.2971	-49.0895	52	GO	false
3509601	Campo Limpo Paulista	-23.2078	-46.7889	35	SP	false
4104253	Campo Magro	-25.3687	-49.4501	41	PR	false
2202208	Campo Maior	-4.8217	-42.1641	22	PI	false
4104303	Campo Mourão	-24.0463	-52.378	41	PR	false
4304002	Campo Novo	-27.6792	-53.8052	43	RS	false
1100700	Campo Novo de Rondônia	-10.5712	-63.6266	11	RO	false
5102637	Campo Novo do Parecis	-13.6587	-57.8907	51	MT	false
2402105	Campo Redondo	-6.23829	-36.1888	24	RN	false
5102678	Campo Verde	-15.545	-55.1626	51	MT	false
3111507	Campos Altos	-19.6914	-46.1725	31	MG	false
5204904	Campos Belos	-13.035	-46.7681	52	GO	false
4304101	Campos Borges	-28.8871	-53.0008	43	RS	false
5102686	Campos de Júlio	-13.7242	-59.2858	51	MT	false
3509700	Campos do Jordão	-22.7296	-45.5833	35	SP	false
3301009	Campos dos Goytacazes	-21.7622	-41.3181	33	RJ	false
3111606	Campos Gerais	-21.237	-45.7569	31	MG	false
1703842	Campos Lindos	-7.98956	-46.8645	17	TO	false
4203600	Campos Novos	-27.4002	-51.2276	42	SC	false
3509809	Campos Novos Paulista	-22.602	-49.9987	35	SP	false
2302701	Campos Sales	-7.06761	-40.3687	23	CE	false
5204953	Campos Verdes	-14.2442	-49.6528	52	GO	false
2603603	Camutanga	-7.40545	-35.2664	26	PE	false
3111903	Cana Verde	-21.0232	-45.1801	31	MG	false
3111705	Canaã	-20.6869	-42.6167	31	MG	false
1502152	Canaã dos Carajás	-6.49659	-49.8776	15	PA	false
5102694	Canabrava do Norte	-11.0556	-51.8209	51	MT	false
3509908	Cananéia	-25.0144	-47.9341	35	SP	false
2701605	Canapi	-9.11932	-37.5967	27	AL	false
2906105	Canápolis	-13.0725	-44.201	29	BA	false
3111804	Canápolis	-18.7212	-49.2035	31	MG	false
2906204	Canarana	-11.6858	-41.7677	29	BA	false
5102702	Canarana	-13.5515	-52.2705	51	MT	false
3509957	Canas	-22.7003	-45.0521	35	SP	false
2202251	Canavieira	-7.68821	-43.7233	22	PI	false
2906303	Canavieiras	-15.6722	-38.9536	29	BA	false
2906402	Candeal	-11.8049	-39.1203	29	BA	false
2906501	Candeias	-12.6716	-38.5472	29	BA	false
3112000	Candeias	-20.7692	-45.2765	31	MG	false
1100809	Candeias do Jamari	-8.7907	-63.7005	11	RO	false
2906600	Candiba	-14.4097	-42.8667	29	BA	false
4104402	Cândido de Abreu	-24.5649	-51.3372	41	PR	false
4304309	Cândido Godói	-27.9515	-54.7517	43	RS	false
2102606	Cândido Mendes	-1.43265	-45.7161	21	MA	false
3510005	Cândido Mota	-22.7471	-50.3873	35	SP	false
3510104	Cândido Rodrigues	-21.3275	-48.6327	35	SP	false
2906709	Cândido Sales	-15.4993	-41.2414	29	BA	false
4304358	Candiota	-31.5516	-53.6773	43	RS	false
4104428	Candói	-25.5758	-52.0409	41	PR	false
4304408	Canela	-29.356	-50.8119	43	RS	false
4203709	Canelinha	-27.2616	-48.7658	42	SC	false
2402204	Canguaretama	-6.37193	-35.1281	24	RN	false
4304507	Canguçu	-31.396	-52.6783	43	RS	false
2801108	Canhoba	-10.1365	-36.9806	28	SE	false
2603702	Canhotinho	-8.87652	-36.1979	26	PE	false
2302800	Canindé	-4.35162	-39.3155	23	CE	false
2801207	Canindé de São Francisco	-9.64882	-37.7923	28	SE	false
3510153	Canitar	-23.004	-49.7839	35	SP	false
4304606	Canoas	-29.9128	-51.1857	43	RS	false
4203808	Canoinhas	-26.1766	-50.395	42	SC	false
2906808	Cansanção	-10.6647	-39.4944	29	BA	false
1400175	Cantá	2.60994	-60.6058	14	RR	false
3301108	Cantagalo	-21.9797	-42.3664	33	RJ	false
4104451	Cantagalo	-25.3734	-52.1198	41	PR	false
3112059	Cantagalo	-18.5248	-42.6223	31	MG	false
2102705	Cantanhede	-3.63757	-44.383	21	MA	false
2202307	Canto do Buriti	-8.1111	-42.9517	22	PI	false
2906824	Canudos	-9.90014	-39.1471	29	BA	false
4304614	Canudos do Vale	-29.3271	-52.2374	43	RS	false
1300904	Canutama	-6.52582	-64.3953	13	AM	false
1502202	Capanema	-1.20529	-47.1778	15	PA	false
4104501	Capanema	-25.6691	-53.8055	41	PR	false
4203253	Capão Alto	-27.9389	-50.5098	42	SC	false
3510203	Capão Bonito	-24.0113	-48.3482	35	SP	false
4304622	Capão Bonito do Sul	-28.1254	-51.3961	43	RS	false
4304630	Capão da Canoa	-29.7642	-50.0282	43	RS	false
4304655	Capão do Cipó	-28.9312	-54.5558	43	RS	false
4304663	Capão do Leão	-31.7565	-52.4889	43	RS	false
3112109	Caparaó	-20.5289	-41.9061	31	MG	false
2701704	Capela	-9.41504	-36.0826	27	AL	false
2801306	Capela	-10.5069	-37.0628	28	SE	false
4304689	Capela de Santana	-29.6961	-51.328	43	RS	false
3510302	Capela do Alto	-23.4685	-47.7388	35	SP	false
2906857	Capela do Alto Alegre	-11.6658	-39.8349	29	BA	false
3112208	Capela Nova	-20.9179	-43.622	31	MG	false
3112307	Capelinha	-17.6888	-42.5147	31	MG	false
3112406	Capetinga	-20.6163	-47.0571	31	MG	false
2504033	Capim	-6.91624	-35.1673	25	PB	false
3112505	Capim Branco	-19.5471	-44.1304	31	MG	false
2906873	Capim Grosso	-11.3797	-40.0089	29	BA	false
3112604	Capinópolis	-18.6862	-49.5706	31	MG	false
4203907	Capinzal	-27.3473	-51.6057	42	SC	false
2102754	Capinzal do Norte	-4.7236	-44.328	21	MA	false
2302909	Capistrano	-4.45569	-38.9048	23	CE	false
4304697	Capitão	-29.2674	-51.9853	43	RS	false
3112653	Capitão Andrade	-19.0748	-41.8614	31	MG	false
2202406	Capitão de Campos	-4.457	-41.944	22	PI	false
3112703	Capitão Enéas	-16.3265	-43.7084	31	MG	false
2202455	Capitão Gervásio Oliveira	-8.49655	-41.814	22	PI	false
4104600	Capitão Leônidas Marques	-25.4816	-53.6112	41	PR	false
1502301	Capitão Poço	-1.74785	-47.0629	15	PA	false
3112802	Capitólio	-20.6164	-46.0493	31	MG	false
3510401	Capivari	-22.9951	-47.5071	35	SP	false
4203956	Capivari de Baixo	-28.4498	-48.9631	42	SC	false
4304671	Capivari do Sul	-30.1383	-50.5152	43	RS	false
1200179	Capixaba	-10.566	-67.686	12	AC	false
2603801	Capoeiras	-8.73423	-36.6306	26	PE	false
3112901	Caputira	-20.1703	-42.2683	31	MG	false
4304713	Caraá	-29.7869	-50.4316	43	RS	false
1400209	Caracaraí	1.82766	-61.1304	14	RR	false
2202505	Caracol	-9.27933	-43.329	22	PI	false
5002803	Caracol	-22.011	-57.0277	50	MS	false
3510500	Caraguatatuba	-23.6125	-45.4125	35	SP	false
3113008	Caraí	-17.1862	-41.7004	31	MG	false
2906899	Caraíbas	-14.7177	-41.2603	29	BA	false
4104659	Carambeí	-24.9152	-50.0986	41	PR	false
3113107	Caranaíba	-20.8707	-43.7417	31	MG	false
3113206	Carandaí	-20.9566	-43.811	31	MG	false
3113305	Carangola	-20.7343	-42.0313	31	MG	false
3300936	Carapebus	-22.1821	-41.663	33	RJ	false
3510609	Carapicuíba	-23.5235	-46.8407	35	SP	false
3113404	Caratinga	-19.7868	-42.1292	31	MG	false
1301001	Carauari	-4.88161	-66.9086	13	AM	false
2402303	Caraúbas	-5.78387	-37.5586	24	RN	false
2504074	Caraúbas	-7.72049	-36.492	25	PB	false
2202539	Caraúbas do Piauí	-3.47525	-41.8425	22	PI	false
2906907	Caravelas	-17.7268	-39.2597	29	BA	false
4304705	Carazinho	-28.2958	-52.7933	43	RS	false
3113503	Carbonita	-17.5255	-43.0137	31	MG	false
2907004	Cardeal da Silva	-11.9472	-37.9469	29	BA	false
3510708	Cardoso	-20.08	-49.9183	35	SP	false
3301157	Cardoso Moreira	-21.4846	-41.6165	33	RJ	false
3113602	Careaçu	-22.0424	-45.696	31	MG	false
1301100	Careiro	-3.76803	-60.369	13	AM	false
1301159	Careiro da Várzea	-3.314	-59.5557	13	AM	false
3201308	Cariacica	-20.2632	-40.4165	32	ES	false
2303006	Caridade	-4.22514	-39.1912	23	CE	false
2202554	Caridade do Piauí	-7.73435	-40.9848	22	PI	false
2907103	Carinhanha	-14.2985	-43.7724	29	BA	false
2801405	Carira	-10.3524	-37.7002	28	SE	false
2303105	Cariré	-3.94858	-40.476	23	CE	false
1703867	Cariri do Tocantins	-11.8881	-49.1609	17	TO	false
2303204	Caririaçu	-7.02808	-39.2828	23	CE	false
2303303	Cariús	-6.52428	-39.4916	23	CE	false
5102793	Carlinda	-9.94912	-55.8417	51	MT	false
4104709	Carlópolis	-23.4269	-49.7235	41	PR	false
4304804	Carlos Barbosa	-29.2969	-51.5028	43	RS	false
3113701	Carlos Chagas	-17.6973	-40.7723	31	MG	false
4304853	Carlos Gomes	-27.7167	-51.9121	43	RS	false
3113800	Carmésia	-19.0877	-43.1382	31	MG	false
3301207	Carmo	-21.931	-42.6046	33	RJ	false
3113909	Carmo da Cachoeira	-21.4633	-45.2201	31	MG	false
3114006	Carmo da Mata	-20.5575	-44.8735	31	MG	false
3114105	Carmo de Minas	-22.1204	-45.1307	31	MG	false
3114204	Carmo do Cajuru	-20.1912	-44.7664	31	MG	false
3114303	Carmo do Paranaíba	-18.991	-46.3167	31	MG	false
3114402	Carmo do Rio Claro	-20.9736	-46.1149	31	MG	false
5205000	Carmo do Rio Verde	-15.3549	-49.708	52	GO	false
1703883	Carmolândia	-7.03262	-48.3978	17	TO	false
2801504	Carmópolis	-10.6449	-36.9887	28	SE	false
3114501	Carmópolis de Minas	-20.5396	-44.6336	31	MG	false
2603900	Carnaíba	-7.79342	-37.7946	26	PE	false
2402402	Carnaúba dos Dantas	-6.55015	-36.5868	24	RN	false
2402501	Carnaubais	-5.34181	-36.8335	24	RN	false
2303402	Carnaubal	-4.15985	-40.9413	23	CE	false
2603926	Carnaubeira da Penha	-8.31799	-38.7512	26	PE	false
3114550	Carneirinho	-19.6987	-50.6894	31	MG	false
2701803	Carneiros	-9.48476	-37.3773	27	AL	false
1400233	Caroebe	0.884203	-59.6959	14	RR	false
2102804	Carolina	-7.33584	-47.4634	21	MA	false
2604007	Carpina	-7.84566	-35.2514	26	PE	false
3114600	Carrancas	-21.4898	-44.6446	31	MG	false
2504108	Carrapateira	-7.03414	-38.3399	25	PB	false
1703891	Carrasco Bonito	-5.31415	-48.0314	17	TO	false
2604106	Caruaru	-8.28455	-35.9699	26	PE	false
2102903	Carutapera	-1.19696	-46.0085	21	MA	false
3114709	Carvalhópolis	-21.7735	-45.8421	31	MG	false
3114808	Carvalhos	-22	-44.4632	31	MG	false
3510807	Casa Branca	-21.7708	-47.0852	35	SP	false
3114907	Casa Grande	-20.7925	-43.9343	31	MG	false
2907202	Casa Nova	-9.16408	-40.974	29	BA	false
4304903	Casca	-28.5605	-51.9815	43	RS	false
3115003	Cascalho Rico	-18.5772	-47.8716	31	MG	false
4104808	Cascavel	-24.9573	-53.459	41	PR	false
2303501	Cascavel	-4.12967	-38.2412	23	CE	false
1703909	Caseara	-9.27612	-49.9521	17	TO	false
4304952	Caseiros	-28.2582	-51.6861	43	RS	false
3301306	Casimiro de Abreu	-22.4812	-42.2066	33	RJ	false
2604155	Casinhas	-7.74084	-35.7206	26	PE	false
2504157	Casserengue	-6.77954	-35.8179	25	PB	false
3115102	Cássia	-20.5831	-46.9201	31	MG	false
3510906	Cássia dos Coqueiros	-21.2801	-47.1643	35	SP	false
5002902	Cassilândia	-19.1179	-51.7313	50	MS	false
1502400	Castanhal	-1.29797	-47.9167	15	PA	false
5102850	Castanheira	-11.1251	-58.6081	51	MT	false
1100908	Castanheiras	-11.4253	-61.9482	11	RO	false
5205059	Castelândia	-18.0921	-50.203	52	GO	false
3201407	Castelo	-20.6033	-41.2031	32	ES	false
2202604	Castelo do Piauí	-5.31869	-41.5499	22	PI	false
3511003	Castilho	-20.8689	-51.4884	35	SP	false
4104907	Castro	-24.7891	-50.0108	41	PR	false
2907301	Castro Alves	-12.7579	-39.4248	29	BA	false
3115300	Cataguases	-21.3924	-42.6896	31	MG	false
5205109	Catalão	-18.1656	-47.944	52	GO	false
3511102	Catanduva	-21.1314	-48.977	35	SP	false
4105003	Catanduvas	-25.2044	-53.1548	41	PR	false
4204004	Catanduvas	-27.069	-51.6602	42	SC	false
2303600	Catarina	-6.12291	-39.8736	23	CE	false
3115359	Catas Altas	-20.0734	-43.4061	31	MG	false
3115409	Catas Altas da Noruega	-20.6901	-43.4939	31	MG	false
2604205	Catende	-8.67509	-35.7024	26	PE	false
3511201	Catiguá	-21.0519	-49.0616	35	SP	false
2504207	Catingueira	-7.12008	-37.6064	25	PB	false
2907400	Catolândia	-12.31	-44.8648	29	BA	false
2504306	Catolé do Rocha	-6.34062	-37.747	25	PB	false
2907509	Catu	-12.3513	-38.3791	29	BA	false
4305009	Catuípe	-28.2554	-54.0132	43	RS	false
3115458	Catuji	-17.3018	-41.5276	31	MG	false
2303659	Catunda	-4.64336	-40.2	23	CE	false
5205208	Caturaí	-16.4447	-49.4936	52	GO	false
2907558	Caturama	-13.3239	-42.2904	29	BA	false
2504355	Caturité	-7.41659	-36.0306	25	PB	false
3115474	Catuti	-15.3616	-42.9627	31	MG	false
2303709	Caucaia	-3.72797	-38.6619	23	CE	false
5205307	Cavalcante	-13.7976	-47.4566	52	GO	false
3115508	Caxambu	-21.9753	-44.9319	31	MG	false
4204103	Caxambu do Sul	-27.1624	-52.8807	42	SC	false
2103000	Caxias	-4.86505	-43.3617	21	MA	false
4305108	Caxias do Sul	-29.1629	-51.1792	43	RS	false
2202653	Caxingó	-3.41904	-41.8955	22	PI	false
2402600	Ceará-Mirim	-5.64323	-35.4247	24	RN	false
2103109	Cedral	-2.00027	-44.5281	21	MA	false
3511300	Cedral	-20.9009	-49.2664	35	SP	false
2303808	Cedro	-6.60034	-39.0609	23	CE	false
2604304	Cedro	-7.71179	-39.2367	26	PE	false
2801603	Cedro de São João	-10.2534	-36.8856	28	SE	false
3115607	Cedro do Abaeté	-19.1458	-45.712	31	MG	false
4204152	Celso Ramos	-27.6327	-51.335	42	SC	false
4305116	Centenário	-27.7615	-51.9984	43	RS	false
1704105	Centenário	-8.96103	-47.3304	17	TO	false
4105102	Centenário do Sul	-22.8188	-51.5973	41	PR	false
2907608	Central	-11.1376	-42.1116	29	BA	false
3115706	Central de Minas	-18.7612	-41.3143	31	MG	false
2103125	Central do Maranhão	-2.19831	-44.8254	21	MA	false
3115805	Centralina	-18.5852	-49.2014	31	MG	false
2103158	Centro do Guilherme	-2.44891	-46.0345	21	MA	false
2103174	Centro Novo do Maranhão	-2.12696	-46.1228	21	MA	false
1100056	Cerejeiras	-13.187	-60.8168	11	RO	false
5205406	Ceres	-15.3061	-49.6	52	GO	false
3511409	Cerqueira César	-23.038	-49.1655	35	SP	false
3511508	Cerquilho	-23.1665	-47.7459	35	SP	false
4305124	Cerrito	-31.8419	-52.8004	43	RS	false
4105201	Cerro Azul	-26.0891	-52.8691	41	PR	false
4305132	Cerro Branco	-29.657	-52.9406	43	RS	false
2402709	Cerro Corá	-6.03503	-36.3503	24	RN	false
4305157	Cerro Grande	-27.6106	-53.1672	43	RS	false
4305173	Cerro Grande do Sul	-30.5905	-51.7418	43	RS	false
4305207	Cerro Largo	-28.1463	-54.7428	43	RS	false
4204178	Cerro Negro	-27.7942	-50.8673	42	SC	false
3511607	Cesário Lange	-23.226	-47.9545	35	SP	false
4105300	Céu Azul	-25.1489	-53.8415	41	PR	false
5205455	Cezarina	-16.9718	-49.7758	52	GO	false
2604403	Chã de Alegria	-8.00679	-35.204	26	PE	false
2604502	Chã Grande	-8.23827	-35.4571	26	PE	false
2701902	Chã Preta	-9.2556	-36.2983	27	AL	false
3115904	Chácara	-21.6733	-43.215	31	MG	false
3116001	Chalé	-20.0453	-41.6897	31	MG	false
4305306	Chapada	-28.0559	-53.0665	43	RS	false
1705102	Chapada da Natividade	-11.6175	-47.7486	17	TO	false
1704600	Chapada de Areia	-10.1419	-49.1403	17	TO	false
3116100	Chapada do Norte	-17.0881	-42.5392	31	MG	false
5103007	Chapada dos Guimarães	-15.4643	-55.7499	51	MT	false
3116159	Chapada Gaúcha	-15.3014	-45.6116	31	MG	false
5205471	Chapadão do Céu	-18.4073	-52.549	52	GO	false
4204194	Chapadão do Lageado	-27.5905	-49.5539	42	SC	false
5002951	Chapadão do Sul	-18.788	-52.6263	50	MS	false
2103208	Chapadinha	-3.73875	-43.3538	21	MA	false
4204202	Chapecó	-27.1004	-52.6152	42	SC	false
3511706	Charqueada	-22.5096	-47.7755	35	SP	false
4305355	Charqueadas	-29.9625	-51.6289	43	RS	false
4305371	Charrua	-27.9493	-52.015	43	RS	false
2303907	Chaval	-3.03571	-41.2435	23	CE	false
3557204	Chavantes	-23.0366	-49.7096	35	SP	false
1502509	Chaves	-0.164154	-49.987	15	PA	false
3116209	Chiador	-21.9996	-43.0617	31	MG	false
4305405	Chiapetta	-27.923	-53.9419	43	RS	false
4105409	Chopinzinho	-25.8515	-52.5173	41	PR	false
2303931	Choró	-4.83906	-39.1344	23	CE	false
2303956	Chorozinho	-4.28873	-38.4986	23	CE	false
2907707	Chorrochó	-8.9695	-39.0979	29	BA	false
4305439	Chuí	-33.6866	-53.4594	43	RS	false
1100924	Chupinguaia	-12.5611	-60.8877	11	RO	false
4305447	Chuvisca	-30.7504	-51.9737	43	RS	false
4105508	Cianorte	-23.6599	-52.6054	41	PR	false
2907806	Cícero Dantas	-10.5897	-38.3794	29	BA	false
4105607	Cidade Gaúcha	-23.3772	-52.9436	41	PR	false
5205497	Cidade Ocidental	-16.0765	-47.9252	52	GO	false
2103257	Cidelândia	-5.17465	-47.7781	21	MA	false
4305454	Cidreira	-30.1604	-50.2337	43	RS	false
2907905	Cipó	-11.1032	-38.5179	29	BA	false
3116308	Cipotânea	-20.9026	-43.3629	31	MG	false
4305504	Ciríaco	-28.3419	-51.8741	43	RS	false
3116407	Claraval	-20.397	-47.2768	31	MG	false
3116506	Claro dos Poções	-17.082	-44.2061	31	MG	false
5103056	Cláudia	-11.5075	-54.8835	51	MT	false
3116605	Cláudio	-20.4437	-44.7673	31	MG	false
3511904	Clementina	-21.5604	-50.4525	35	SP	false
4105706	Clevelândia	-26.4043	-52.3508	41	PR	false
2908002	Coaraci	-14.637	-39.5556	29	BA	false
1301209	Coari	-4.09412	-63.1441	13	AM	false
2202703	Cocal	-3.47279	-41.5546	22	PI	false
2202711	Cocal de Telha	-4.5571	-41.9587	22	PI	false
4204251	Cocal do Sul	-28.5986	-49.3335	42	SC	false
2202729	Cocal dos Alves	-3.62047	-41.4402	22	PI	false
5103106	Cocalinho	-14.3903	-51.0001	51	MT	false
5205513	Cocalzinho de Goiás	-15.7914	-48.7747	52	GO	false
2908101	Cocos	-14.1814	-44.5352	29	BA	false
1301308	Codajás	-3.83053	-62.0658	13	AM	false
2103307	Codó	-4.45562	-43.8924	21	MA	false
2103406	Coelho Neto	-4.25245	-43.0108	21	MA	false
3116704	Coimbra	-20.8535	-42.8008	31	MG	false
2702009	Coité do Nóia	-9.63348	-36.5845	27	AL	false
2202737	Coivaras	-5.09224	-42.208	22	PI	false
1502608	Colares	-0.936423	-48.2803	15	PA	false
3201506	Colatina	-19.5493	-40.6269	32	ES	false
5103205	Colíder	-10.8135	-55.461	51	MT	false
3512001	Colina	-20.7114	-48.5387	35	SP	false
4305587	Colinas	-29.3948	-51.8556	43	RS	false
2103505	Colinas	-6.03199	-44.2543	21	MA	false
5205521	Colinas do Sul	-14.1528	-48.076	52	GO	false
1705508	Colinas do Tocantins	-8.05764	-48.4757	17	TO	false
1716703	Colméia	-8.72463	-48.7638	17	TO	false
5103254	Colniza	-9.46121	-59.2252	51	MT	false
3512100	Colômbia	-20.1768	-48.6865	35	SP	false
4105805	Colombo	-25.2925	-49.2262	41	PR	false
2202752	Colônia do Gurguéia	-8.1837	-43.794	22	PI	false
2202778	Colônia do Piauí	-7.22651	-42.1756	22	PI	false
2702108	Colônia Leopoldina	-8.91806	-35.7214	27	AL	false
4305603	Colorado	-28.5258	-52.9928	43	RS	false
4105904	Colorado	-22.8374	-51.9743	41	PR	false
1100064	Colorado do Oeste	-13.1174	-60.5454	11	RO	false
3116803	Coluna	-18.2311	-42.8352	31	MG	false
1705557	Combinado	-12.7917	-46.5388	17	TO	false
3116902	Comendador Gomes	-19.6973	-49.0789	31	MG	false
3300951	Comendador Levy Gasparian	-22.0404	-43.214	33	RJ	false
3117009	Comercinho	-16.2963	-41.7945	31	MG	false
5103304	Comodoro	-13.6614	-59.7848	51	MT	false
2504405	Conceição	-7.55106	-38.5014	25	PB	false
3117108	Conceição da Aparecida	-21.096	-46.2049	31	MG	false
3201605	Conceição da Barra	-18.5883	-39.7362	32	ES	false
3115201	Conceição da Barra de Minas	-21.1316	-44.4729	31	MG	false
2908200	Conceição da Feira	-12.5078	-38.9978	29	BA	false
3117306	Conceição das Alagoas	-19.9172	-48.3839	31	MG	false
3117207	Conceição das Pedras	-22.1576	-45.4562	31	MG	false
3117405	Conceição de Ipanema	-19.9326	-41.6908	31	MG	false
3301405	Conceição de Macabu	-22.0834	-41.8719	33	RJ	false
2908309	Conceição do Almeida	-12.7836	-39.1715	29	BA	false
1502707	Conceição do Araguaia	-8.26136	-49.2689	15	PA	false
2202802	Conceição do Canindé	-7.87638	-41.5942	22	PI	false
3201704	Conceição do Castelo	-20.3639	-41.2417	32	ES	false
2908408	Conceição do Coité	-11.56	-39.2808	29	BA	false
2908507	Conceição do Jacuípe	-12.3268	-38.7684	29	BA	false
2103554	Conceição do Lago-Açu	-3.85142	-44.8895	21	MA	false
3117504	Conceição do Mato Dentro	-19.0344	-43.4221	31	MG	false
3117603	Conceição do Pará	-19.7456	-44.8945	31	MG	false
3117702	Conceição do Rio Verde	-21.8778	-45.087	31	MG	false
1705607	Conceição do Tocantins	-12.2209	-47.2951	17	TO	false
3117801	Conceição dos Ouros	-22.4078	-45.7996	31	MG	false
3512209	Conchal	-22.3375	-47.1729	35	SP	false
3512308	Conchas	-23.0154	-48.0134	35	SP	false
4204301	Concórdia	-27.2335	-52.026	42	SC	false
1502756	Concórdia do Pará	-1.99238	-47.9422	15	PA	false
2504504	Condado	-6.89831	-37.606	25	PB	false
2604601	Condado	-7.58787	-35.0999	26	PE	false
2504603	Conde	-7.25746	-34.8999	25	PB	false
2908606	Conde	-11.8179	-37.6131	29	BA	false
2908705	Condeúba	-14.9022	-41.9718	29	BA	false
4305702	Condor	-28.2075	-53.4905	43	RS	false
3117836	Cônego Marinho	-15.2892	-44.4181	31	MG	false
3117876	Confins	-19.6282	-43.9931	31	MG	false
5103353	Confresa	-10.6437	-51.5699	51	MT	false
2504702	Congo	-7.79078	-36.6581	25	PB	false
3117900	Congonhal	-22.1488	-46.043	31	MG	false
3118007	Congonhas	-20.4958	-43.851	31	MG	false
3118106	Congonhas do Norte	-18.8021	-43.6767	31	MG	false
4106001	Congonhinhas	-23.5493	-50.5569	41	PR	false
3118205	Conquista	-19.9312	-47.5492	31	MG	false
5103361	Conquista D'Oeste	-14.5381	-59.5444	51	MT	false
3118304	Conselheiro Lafaiete	-20.6634	-43.7846	31	MG	false
4106100	Conselheiro Mairinck	-23.623	-50.1707	41	PR	false
3118403	Conselheiro Pena	-19.1789	-41.4736	31	MG	false
3118502	Consolação	-22.5493	-45.9255	31	MG	false
4305801	Constantina	-27.732	-52.9938	43	RS	false
3118601	Contagem	-19.9321	-44.0539	31	MG	false
4106209	Contenda	-25.6788	-49.535	41	PR	false
2908804	Contendas do Sincorá	-13.7537	-41.048	29	BA	false
3118700	Coqueiral	-21.1858	-45.4366	31	MG	false
4305835	Coqueiro Baixo	-29.1802	-52.0942	43	RS	false
2702207	Coqueiro Seco	-9.63715	-35.7994	27	AL	false
4305850	Coqueiros do Sul	-28.1194	-52.7842	43	RS	false
3118809	Coração de Jesus	-16.6841	-44.3635	31	MG	false
2908903	Coração de Maria	-12.2333	-38.7487	29	BA	false
4106308	Corbélia	-24.7971	-53.3006	41	PR	false
3301504	Cordeiro	-22.0267	-42.3648	33	RJ	false
3512407	Cordeirópolis	-22.4778	-47.4519	35	SP	false
2909000	Cordeiros	-15.0356	-41.9308	29	BA	false
4204350	Cordilheira Alta	-26.9844	-52.6056	42	SC	false
3118908	Cordisburgo	-19.1224	-44.3224	31	MG	false
3119005	Cordislândia	-21.7891	-45.6999	31	MG	false
2304004	Coreaú	-3.5415	-40.6587	23	CE	false
2504801	Coremas	-7.00712	-37.9346	25	PB	false
5003108	Corguinho	-19.8243	-54.8281	50	MS	false
2909109	Coribe	-13.8232	-44.4586	29	BA	false
3119104	Corinto	-18.369	-44.4542	31	MG	false
4106407	Cornélio Procópio	-23.1829	-50.6498	41	PR	false
3119203	Coroaci	-18.6156	-42.2791	31	MG	false
3512506	Coroados	-21.3521	-50.2859	35	SP	false
2103604	Coroatá	-4.13442	-44.1244	21	MA	false
3119302	Coromandel	-18.4734	-47.1933	31	MG	false
4305871	Coronel Barros	-28.3921	-54.0686	43	RS	false
4305900	Coronel Bicaco	-27.7197	-53.7022	43	RS	false
4106456	Coronel Domingos Soares	-26.2277	-52.0356	41	PR	false
2402808	Coronel Ezequiel	-6.3748	-36.2223	24	RN	false
3119401	Coronel Fabriciano	-19.5179	-42.6276	31	MG	false
4204400	Coronel Freitas	-26.9057	-52.7011	42	SC	false
2402907	Coronel João Pessoa	-6.24974	-38.4441	24	RN	false
2909208	Coronel João Sá	-10.2847	-37.9198	29	BA	false
2202851	Coronel José Dias	-8.81397	-42.5232	22	PI	false
3512605	Coronel Macedo	-23.6261	-49.31	35	SP	false
4204459	Coronel Martins	-26.511	-52.6694	42	SC	false
3119500	Coronel Murta	-16.6148	-42.184	31	MG	false
3119609	Coronel Pacheco	-21.5898	-43.256	31	MG	false
4305934	Coronel Pilar	-29.2695	-51.6847	43	RS	false
5003157	Coronel Sapucaia	-23.2724	-55.5278	50	MS	false
4106506	Coronel Vivida	-25.9767	-52.5641	41	PR	false
3119708	Coronel Xavier Chaves	-21.0277	-44.2206	31	MG	false
3119807	Córrego Danta	-19.8198	-45.9032	31	MG	false
3119906	Córrego do Bom Jesus	-22.6269	-46.0241	31	MG	false
5205703	Córrego do Ouro	-16.2918	-50.5503	52	GO	false
3119955	Córrego Fundo	-20.4474	-45.5617	31	MG	false
3120003	Córrego Novo	-19.8361	-42.3988	31	MG	false
4204558	Correia Pinto	-27.5877	-50.3614	42	SC	false
2202901	Corrente	-10.4333	-45.1633	22	PI	false
2604700	Correntes	-9.12117	-36.3244	26	PE	false
2909307	Correntina	-13.3477	-44.6333	29	BA	false
2604809	Cortês	-8.47443	-35.5468	26	PE	false
5003207	Corumbá	-19.0077	-57.651	50	MS	false
5205802	Corumbá de Goiás	-15.9245	-48.8117	52	GO	false
5205901	Corumbaíba	-18.1415	-48.5626	52	GO	false
3512704	Corumbataí	-22.2213	-47.6215	35	SP	false
4106555	Corumbataí do Sul	-24.101	-52.1177	41	PR	false
1100072	Corumbiara	-12.9551	-60.8947	11	RO	false
4204509	Corupá	-26.4246	-49.246	42	SC	false
2702306	Coruripe	-10.1276	-36.1717	27	AL	false
3512803	Cosmópolis	-22.6419	-47.1926	35	SP	false
3512902	Cosmorama	-20.4755	-49.7827	35	SP	false
1100080	Costa Marques	-12.4367	-64.228	11	RO	false
5003256	Costa Rica	-18.5432	-53.1287	50	MS	false
2909406	Cotegipe	-12.0228	-44.2566	29	BA	false
3513009	Cotia	-23.6022	-46.919	35	SP	false
4305959	Cotiporã	-28.9891	-51.6971	43	RS	false
5103379	Cotriguaçu	-9.85656	-58.4192	51	MT	false
3120102	Couto de Magalhães de Minas	-18.0727	-43.4648	31	MG	false
1706001	Couto Magalhães	-8.28411	-49.2473	17	TO	false
4305975	Coxilha	-28.128	-52.3023	43	RS	false
5003306	Coxim	-18.5013	-54.751	50	MS	false
2504850	Coxixola	-7.62365	-36.6064	25	PB	false
2702355	Craíbas	-9.6178	-36.7697	27	AL	false
2304103	Crateús	-5.16768	-40.6536	23	CE	false
2304202	Crato	-7.2153	-39.4103	23	CE	false
3513108	Cravinhos	-21.338	-47.7324	35	SP	false
2909505	Cravolândia	-13.3531	-39.8031	29	BA	false
4204608	Criciúma	-28.6723	-49.3729	42	SC	false
3120151	Crisólita	-17.2381	-40.9184	31	MG	false
2909604	Crisópolis	-11.5059	-38.1515	29	BA	false
4306007	Crissiumal	-27.4999	-54.0994	43	RS	false
3120201	Cristais	-20.8733	-45.5167	31	MG	false
3513207	Cristais Paulista	-20.4036	-47.4209	35	SP	false
4306056	Cristal	-31.0046	-52.0436	43	RS	false
4306072	Cristal do Sul	-27.452	-53.2422	43	RS	false
1706100	Cristalândia	-10.5985	-49.1942	17	TO	false
2203008	Cristalândia do Piauí	-10.6443	-45.1893	22	PI	false
3120300	Cristália	-16.716	-42.8571	31	MG	false
5206206	Cristalina	-16.7676	-47.6131	52	GO	false
3120409	Cristiano Otoni	-20.8324	-43.8166	31	MG	false
5206305	Cristianópolis	-17.1987	-48.7034	52	GO	false
3120508	Cristina	-22.208	-45.2673	31	MG	false
2801702	Cristinápolis	-11.4668	-37.7585	28	SE	false
2203107	Cristino Castro	-8.82273	-44.223	22	PI	false
2909703	Cristópolis	-12.2249	-44.4214	29	BA	false
5206404	Crixás	-14.5412	-49.974	52	GO	false
1706258	Crixás do Tocantins	-11.0994	-48.9152	17	TO	false
2304236	Croatá	-4.40481	-40.9022	23	CE	false
5206503	Cromínia	-17.2883	-49.3798	52	GO	false
3120607	Crucilândia	-20.3923	-44.3334	31	MG	false
2304251	Cruz	-2.91813	-40.176	23	CE	false
4306106	Cruz Alta	-28.645	-53.6048	43	RS	false
2909802	Cruz das Almas	-12.6675	-39.1008	29	BA	false
2504900	Cruz do Espírito Santo	-7.13902	-35.0857	25	PB	false
4106803	Cruz Machado	-26.0166	-51.343	41	PR	false
3513306	Cruzália	-22.7373	-50.7909	35	SP	false
4306130	Cruzaltense	-27.6672	-52.6522	43	RS	false
3513405	Cruzeiro	-22.5728	-44.969	35	SP	false
3120706	Cruzeiro da Fortaleza	-18.944	-46.6669	31	MG	false
4106571	Cruzeiro do Iguaçu	-25.6192	-53.1285	41	PR	false
4106605	Cruzeiro do Oeste	-23.7799	-53.0774	41	PR	false
4106704	Cruzeiro do Sul	-22.9624	-52.1622	41	PR	false
4306205	Cruzeiro do Sul	-29.5148	-51.9928	43	RS	false
1200203	Cruzeiro do Sul	-7.62762	-72.6756	12	AC	false
2403004	Cruzeta	-6.40894	-36.7782	24	RN	false
3120805	Cruzília	-21.84	-44.8067	31	MG	false
4106852	Cruzmaltina	-24.0132	-51.4563	41	PR	false
3513504	Cubatão	-23.8911	-46.424	35	SP	false
2505006	Cubati	-6.86686	-36.3619	25	PB	false
5103403	Cuiabá	-15.601	-56.0974	51	MT	true 
2505105	Cuité	-6.47647	-36.1515	25	PB	false
2505238	Cuité de Mamanguape	-6.91292	-35.2502	25	PB	false
2505204	Cuitegi	-6.89058	-35.5215	25	PB	false
1100940	Cujubim	-9.36065	-62.5846	11	RO	false
5206602	Cumari	-18.2644	-48.1511	52	GO	false
2604908	Cumaru	-8.00827	-35.6957	26	PE	false
1502764	Cumaru do Norte	-7.81097	-50.7698	15	PA	false
2801900	Cumbe	-10.352	-37.1846	28	SE	false
3513603	Cunha	-23.0731	-44.9576	35	SP	false
4204707	Cunha Porã	-26.895	-53.1662	42	SC	false
4204756	Cunhataí	-26.9709	-53.0895	42	SC	false
3120839	Cuparaque	-18.9648	-41.0986	31	MG	false
2605004	Cupira	-8.62432	-35.9518	26	PE	false
2909901	Curaçá	-8.98458	-39.8997	29	BA	false
2203206	Curimatá	-10.0326	-44.3002	22	PI	false
1502772	Curionópolis	-6.09965	-49.6068	15	PA	false
4106902	Curitiba	-25.4195	-49.2646	41	PR	true 
4204806	Curitibanos	-27.2824	-50.5816	42	SC	false
4107009	Curiúva	-24.0362	-50.4576	41	PR	false
2203230	Currais	-9.01175	-44.4062	22	PI	false
2403103	Currais Novos	-6.25484	-36.5146	24	RN	false
2505279	Curral de Cima	-6.72325	-35.2639	25	PB	false
3120870	Curral de Dentro	-15.9327	-41.8557	31	MG	false
2203271	Curral Novo do Piauí	-7.8313	-40.8957	22	PI	false
2505303	Curral Velho	-7.53075	-38.1962	25	PB	false
1502806	Curralinho	-1.81179	-49.7952	15	PA	false
2203255	Curralinhos	-5.60825	-42.8376	22	PI	false
1502855	Curuá	-1.88775	-55.1168	15	PA	false
1502905	Curuçá	-0.733214	-47.8515	15	PA	false
2103703	Cururupu	-1.81475	-44.8644	21	MA	false
5103437	Curvelândia	-15.6084	-57.9133	51	MT	false
3120904	Curvelo	-18.7527	-44.4303	31	MG	false
2605103	Custódia	-8.08546	-37.6443	26	PE	false
1600212	Cutias	0.970761	-50.8005	16	AP	false
5206701	Damianópolis	-14.5604	-46.178	52	GO	false
2505352	Damião	-6.63161	-35.9101	25	PB	false
5206800	Damolândia	-16.2544	-49.3631	52	GO	false
1706506	Darcinópolis	-6.71591	-47.7597	17	TO	false
2910008	Dário Meira	-14.4229	-39.9031	29	BA	false
3121001	Datas	-18.4478	-43.6591	31	MG	false
4306304	David Canabarro	-28.3849	-51.8482	43	RS	false
2103752	Davinópolis	-5.54637	-47.4217	21	MA	false
5206909	Davinópolis	-18.1501	-47.5568	52	GO	false
3121100	Delfim Moreira	-22.5036	-45.2792	31	MG	false
3121209	Delfinópolis	-20.3468	-46.8456	31	MG	false
2702405	Delmiro Gouveia	-9.38534	-37.9987	27	AL	false
3121258	Delta	-19.9721	-47.7841	31	MG	false
2203305	Demerval Lobão	-5.35875	-42.6776	22	PI	false
5103452	Denise	-14.7324	-57.0583	51	MT	false
5003454	Deodápolis	-22.2763	-54.1682	50	MS	false
2304269	Deputado Irapuan Pinheiro	-5.91485	-39.257	23	CE	false
4306320	Derrubadas	-27.2642	-53.8645	43	RS	false
3513702	Descalvado	-21.9002	-47.6181	35	SP	false
4204905	Descanso	-26.827	-53.5034	42	SC	false
3121308	Descoberto	-21.46	-42.9618	31	MG	false
2505402	Desterro	-7.287	-37.0925	25	PB	false
3121407	Desterro de Entre Rios	-20.665	-44.3334	31	MG	false
3121506	Desterro do Melo	-21.143	-43.5178	31	MG	false
4306353	Dezesseis de Novembro	-28.219	-55.0617	43	RS	false
3513801	Diadema	-23.6813	-46.6205	35	SP	false
2505600	Diamante	-7.41738	-38.2615	25	PB	false
4107157	Diamante D'Oeste	-24.9419	-54.1052	41	PR	false
4107108	Diamante do Norte	-22.655	-52.8617	41	PR	false
4107124	Diamante do Sul	-25.035	-52.6768	41	PR	false
3121605	Diamantina	-18.2413	-43.6031	31	MG	false
5103502	Diamantino	-14.4037	-56.4366	51	MT	false
1707009	Dianópolis	-11.624	-46.8198	17	TO	false
2910057	Dias d'Ávila	-12.6187	-38.2926	29	BA	false
4306379	Dilermando de Aguiar	-29.7054	-54.2122	43	RS	false
3121704	Diogo de Vasconcelos	-20.4879	-43.1953	31	MG	false
3121803	Dionísio	-19.8433	-42.7701	31	MG	false
4205001	Dionísio Cerqueira	-26.2648	-53.6351	42	SC	false
5207105	Diorama	-16.2329	-51.2543	52	GO	false
3513850	Dirce Reis	-20.4642	-50.6073	35	SP	false
2203354	Dirceu Arcoverde	-9.33939	-42.4348	22	PI	false
2802007	Divina Pastora	-10.6782	-37.1506	28	SE	false
3121902	Divinésia	-20.9917	-43.0003	31	MG	false
3122009	Divino	-20.6134	-42.1438	31	MG	false
3122108	Divino das Laranjeiras	-18.7755	-41.4781	31	MG	false
3201803	Divino de São Lourenço	-20.6229	-41.6937	32	ES	false
3513900	Divinolândia	-21.6637	-46.7361	35	SP	false
3122207	Divinolândia de Minas	-18.8004	-42.6103	31	MG	false
3122306	Divinópolis	-20.1446	-44.8912	31	MG	false
5208301	Divinópolis de Goiás	-13.2853	-46.3999	52	GO	false
1707108	Divinópolis do Tocantins	-9.80018	-49.2169	17	TO	false
3122355	Divisa Alegre	-15.7221	-41.3463	31	MG	false
3122405	Divisa Nova	-21.5092	-46.1904	31	MG	false
3122454	Divisópolis	-15.7254	-40.9997	31	MG	false
3514007	Dobrada	-21.5155	-48.3935	35	SP	false
3514106	Dois Córregos	-22.3673	-48.3819	35	SP	false
4306403	Dois Irmãos	-29.5836	-51.0898	43	RS	false
4306429	Dois Irmãos das Missões	-27.6621	-53.5304	43	RS	false
5003488	Dois Irmãos do Buriti	-20.6848	-55.2915	50	MS	false
1707207	Dois Irmãos do Tocantins	-9.25534	-49.0638	17	TO	false
4306452	Dois Lajeados	-28.983	-51.8396	43	RS	false
2702504	Dois Riachos	-9.38465	-37.0965	27	AL	false
4107207	Dois Vizinhos	-25.7407	-53.057	41	PR	false
3514205	Dolcinópolis	-20.124	-50.5149	35	SP	false
5103601	Dom Aquino	-15.8099	-54.9223	51	MT	false
2910107	Dom Basílio	-13.7565	-41.7677	29	BA	false
3122470	Dom Bosco	-16.652	-46.2597	31	MG	false
3122504	Dom Cavati	-19.3735	-42.1121	31	MG	false
1502939	Dom Eliseu	-4.19944	-47.8245	15	PA	false
2203404	Dom Expedito Lopes	-6.95332	-41.6396	22	PI	false
4306502	Dom Feliciano	-30.7004	-52.1026	43	RS	false
2203453	Dom Inocêncio	-9.00516	-41.9697	22	PI	false
3122603	Dom Joaquim	-18.961	-43.2544	31	MG	false
2910206	Dom Macedo Costa	-12.9016	-39.1923	29	BA	false
4306601	Dom Pedrito	-30.9756	-54.6694	43	RS	false
2103802	Dom Pedro	-5.03518	-44.4409	21	MA	false
4306551	Dom Pedro de Alcântara	-29.3639	-49.853	43	RS	false
3122702	Dom Silvério	-20.1627	-42.9627	31	MG	false
3122801	Dom Viçoso	-22.2511	-45.1643	31	MG	false
3201902	Domingos Martins	-20.3603	-40.6594	32	ES	false
2203420	Domingos Mourão	-4.2495	-41.2683	22	PI	false
4205100	Dona Emma	-26.981	-49.7261	42	SC	false
3122900	Dona Eusébia	-21.319	-42.807	31	MG	false
4306700	Dona Francisca	-29.6195	-53.3617	43	RS	false
2505709	Dona Inês	-6.61566	-35.6205	25	PB	false
3123007	Dores de Campos	-21.1139	-44.0207	31	MG	false
3123106	Dores de Guanhães	-19.0516	-42.9254	31	MG	false
3123205	Dores do Indaiá	-19.4628	-45.5927	31	MG	false
3202009	Dores do Rio Preto	-20.6931	-41.8405	32	ES	false
3123304	Dores do Turvo	-20.9785	-43.1834	31	MG	false
3123403	Doresópolis	-20.2868	-45.9007	31	MG	false
2605152	Dormentes	-8.44116	-40.7662	26	PE	false
5003504	Douradina	-22.0405	-54.6158	50	MS	false
4107256	Douradina	-23.3807	-53.2918	41	PR	false
3514304	Dourado	-22.1044	-48.3178	35	SP	false
3123502	Douradoquara	-18.4338	-47.5993	31	MG	false
5003702	Dourados	-22.2231	-54.812	50	MS	false
4107306	Doutor Camargo	-23.5582	-52.2178	41	PR	false
4306734	Doutor Maurício Cardoso	-27.5103	-54.3577	43	RS	false
4205159	Doutor Pedrinho	-26.7174	-49.4795	42	SC	false
4306759	Doutor Ricardo	-29.084	-51.9972	43	RS	false
2403202	Doutor Severiano	-6.08082	-38.3794	24	RN	false
4128633	Doutor Ulysses	-24.5665	-49.4219	41	PR	false
5207253	Doverlândia	-16.7188	-52.3189	52	GO	false
3514502	Duartina	-22.4146	-49.4084	35	SP	false
3301603	Duas Barras	-22.0536	-42.5232	33	RJ	false
2505808	Duas Estradas	-6.68499	-35.418	25	PB	false
1707306	Dueré	-11.3416	-49.2716	17	TO	false
3514601	Dumont	-21.2324	-47.9756	35	SP	false
2103901	Duque Bacelar	-4.15002	-42.9477	21	MA	false
3301702	Duque de Caxias	-22.7858	-43.3049	33	RJ	false
3123528	Durandé	-20.2058	-41.7977	31	MG	false
3514700	Echaporã	-22.4326	-50.2038	35	SP	false
3202108	Ecoporanga	-18.3702	-40.836	32	ES	false
5207352	Edealina	-17.4239	-49.6644	52	GO	false
5207402	Edéia	-17.3406	-49.9295	52	GO	false
1301407	Eirunepé	-6.65677	-69.8662	13	AM	false
5003751	Eldorado	-23.7868	-54.2838	50	MS	false
3514809	Eldorado	-24.5281	-48.1141	35	SP	false
1502954	Eldorado do Carajás	-6.10389	-49.3553	15	PA	false
4306767	Eldorado do Sul	-30.0847	-51.6187	43	RS	false
2203503	Elesbão Veloso	-6.19947	-42.1355	22	PI	false
3514908	Elias Fausto	-23.0428	-47.3682	35	SP	false
2203602	Eliseu Martins	-8.09629	-43.6705	22	PI	false
3514924	Elisiário	-21.1678	-49.1146	35	SP	false
2910305	Elísio Medrado	-12.9417	-39.5191	29	BA	false
3123601	Elói Mendes	-21.6088	-45.5691	31	MG	false
2505907	Emas	-7.09964	-37.7163	25	PB	false
3514957	Embaúba	-20.9796	-48.8325	35	SP	false
3515004	Embu das Artes	-23.6437	-46.8579	35	SP	false
3515103	Embu-Guaçu	-23.8297	-46.8136	35	SP	false
3515129	Emilianópolis	-21.8314	-51.4832	35	SP	false
4306809	Encantado	-29.2351	-51.8703	43	RS	false
2403301	Encanto	-6.10691	-38.3033	24	RN	false
2910404	Encruzilhada	-15.5302	-40.9124	29	BA	false
4306908	Encruzilhada do Sul	-30.543	-52.5204	43	RS	false
4107405	Enéas Marques	-25.9445	-53.1659	41	PR	false
4107504	Engenheiro Beltrão	-23.797	-52.2659	41	PR	false
3123700	Engenheiro Caldas	-19.2065	-42.0503	31	MG	false
3515152	Engenheiro Coelho	-22.4836	-47.211	35	SP	false
3123809	Engenheiro Navarro	-17.2831	-43.947	31	MG	false
3301801	Engenheiro Paulo de Frontin	-22.5498	-43.6827	33	RJ	false
4306924	Engenho Velho	-27.706	-52.9145	43	RS	false
3123858	Entre Folhas	-19.6218	-42.2306	31	MG	false
2910503	Entre Rios	-11.9392	-38.0871	29	BA	false
4205175	Entre Rios	-26.7225	-52.5585	42	SC	false
3123908	Entre Rios de Minas	-20.6706	-44.0654	31	MG	false
4107538	Entre Rios do Oeste	-24.7042	-54.2385	41	PR	false
4306957	Entre Rios do Sul	-27.5298	-52.7347	43	RS	false
4306932	Entre-Ijuís	-28.3686	-54.2686	43	RS	false
1301506	Envira	-7.43789	-70.0281	13	AM	false
1200252	Epitaciolândia	-11.0188	-68.7341	12	AC	false
2403400	Equador	-6.93835	-36.717	24	RN	false
4306973	Erebango	-27.8544	-52.3005	43	RS	false
4307005	Erechim	-27.6364	-52.2697	43	RS	false
2304277	Ererê	-6.02751	-38.3461	23	CE	false
2900504	Érico Cardoso	-13.4215	-42.1352	29	BA	false
4205191	Ermo	-28.9869	-49.643	42	SC	false
4307054	Ernestina	-28.4977	-52.5836	43	RS	false
4307203	Erval Grande	-27.3926	-52.574	43	RS	false
4307302	Erval Seco	-27.5443	-53.5005	43	RS	false
4205209	Erval Velho	-27.2743	-51.443	42	SC	false
3124005	Ervália	-20.8403	-42.6544	31	MG	false
2605202	Escada	-8.35672	-35.2241	26	PE	false
4307401	Esmeralda	-28.0518	-51.1933	43	RS	false
3124104	Esmeraldas	-19.764	-44.3065	31	MG	false
3124203	Espera Feliz	-20.6508	-41.9119	31	MG	false
2506004	Esperança	-7.02278	-35.8597	25	PB	false
4307450	Esperança do Sul	-27.3603	-53.9891	43	RS	false
4107520	Esperança Nova	-23.7238	-53.811	41	PR	false
1707405	Esperantina	-5.36593	-48.5378	17	TO	false
2203701	Esperantina	-3.88863	-42.2324	22	PI	false
2104008	Esperantinópolis	-4.87938	-44.6926	21	MA	false
4107546	Espigão Alto do Iguaçu	-25.4216	-52.8348	41	PR	false
1100098	Espigão D'Oeste	-11.5266	-61.0252	11	RO	false
3124302	Espinosa	-14.9249	-42.809	31	MG	false
2403509	Espírito Santo	-6.33563	-35.3052	24	RN	false
3124401	Espírito Santo do Dourado	-22.0454	-45.9548	31	MG	false
3515186	Espírito Santo do Pinhal	-22.1909	-46.7477	35	SP	false
3515194	Espírito Santo do Turvo	-22.6925	-49.4341	35	SP	false
2910602	Esplanada	-11.7942	-37.9432	29	BA	false
4307500	Espumoso	-28.7286	-52.8461	43	RS	false
4307559	Estação	-27.9135	-52.2635	43	RS	false
2802106	Estância	-11.2659	-37.4484	28	SE	false
4307609	Estância Velha	-29.6535	-51.1843	43	RS	false
4307708	Esteio	-29.852	-51.1841	43	RS	false
3124500	Estiva	-22.4577	-46.0191	31	MG	false
3557303	Estiva Gerbi	-22.2713	-46.9481	35	SP	false
2104057	Estreito	-6.56077	-47.4431	21	MA	false
4307807	Estrela	-29.5002	-51.9495	43	RS	false
3515202	Estrela d'Oeste	-20.2875	-50.4049	35	SP	false
3124609	Estrela Dalva	-21.7412	-42.4574	31	MG	false
2702553	Estrela de Alagoas	-9.39089	-36.7644	27	AL	false
3124708	Estrela do Indaiá	-19.5169	-45.7859	31	MG	false
5207501	Estrela do Norte	-13.8665	-49.0716	52	GO	false
3515301	Estrela do Norte	-22.4859	-51.6632	35	SP	false
3124807	Estrela do Sul	-18.7399	-47.6956	31	MG	false
4307815	Estrela Velha	-29.1713	-53.1639	43	RS	false
2910701	Euclides da Cunha	-10.5078	-39.0153	29	BA	false
3515350	Euclides da Cunha Paulista	-22.5545	-52.5928	35	SP	false
4307831	Eugênio de Castro	-28.5315	-54.1506	43	RS	false
3124906	Eugenópolis	-21.1002	-42.1878	31	MG	false
2910727	Eunápolis	-16.3715	-39.5821	29	BA	false
2304285	Eusébio	-3.8925	-38.4559	23	CE	false
3125002	Ewbank da Câmara	-21.5498	-43.5068	31	MG	false
3125101	Extrema	-22.854	-46.3178	31	MG	false
2403608	Extremoz	-5.70143	-35.3048	24	RN	false
2605301	Exu	-7.50364	-39.7238	26	PE	false
2506103	Fagundes	-7.34454	-35.7931	25	PB	false
4307864	Fagundes Varela	-28.8794	-51.7014	43	RS	false
5207535	Faina	-15.4473	-50.3622	52	GO	false
3125200	Fama	-21.4089	-45.8286	31	MG	false
3125309	Faria Lemos	-20.8097	-42.0213	31	MG	false
2304301	Farias Brito	-6.92146	-39.5651	23	CE	false
1503002	Faro	-2.16805	-56.7405	15	PA	false
4107553	Farol	-24.0958	-52.6217	41	PR	false
4307906	Farroupilha	-29.2227	-51.3419	43	RS	false
3515400	Fartura	-23.3916	-49.5124	35	SP	false
2203750	Fartura do Piauí	-9.48342	-42.7912	22	PI	false
1707553	Fátima	-10.7603	-48.9076	17	TO	false
2910750	Fátima	-10.616	-38.2239	29	BA	false
5003801	Fátima do Sul	-22.3789	-54.5131	50	MS	false
4107603	Faxinal	-24.0077	-51.3227	41	PR	false
4308003	Faxinal do Soturno	-29.5788	-53.4484	43	RS	false
4205308	Faxinal dos Guedes	-26.8451	-52.2596	42	SC	false
4308052	Faxinalzinho	-27.4238	-52.6789	43	RS	false
5207600	Fazenda Nova	-16.1834	-50.7781	52	GO	false
4107652	Fazenda Rio Grande	-25.6624	-49.3073	41	PR	false
4308078	Fazenda Vilanova	-29.5885	-51.8217	43	RS	false
1200302	Feijó	-8.17054	-70.351	12	AC	false
2910776	Feira da Mata	-14.2044	-44.2744	29	BA	false
2910800	Feira de Santana	-12.2664	-38.9663	29	BA	false
2702603	Feira Grande	-9.89859	-36.6815	27	AL	false
2605400	Feira Nova	-7.94704	-35.3801	26	PE	false
2802205	Feira Nova	-10.2616	-37.3147	28	SE	false
2104073	Feira Nova do Maranhão	-6.96508	-46.6786	21	MA	false
3125408	Felício dos Santos	-18.0755	-43.2422	31	MG	false
2403707	Felipe Guerra	-5.59274	-37.6875	24	RN	false
3125606	Felisburgo	-16.6348	-40.7605	31	MG	false
3125705	Felixlândia	-18.7507	-44.9004	31	MG	false
4308102	Feliz	-29.4527	-51.3032	43	RS	false
2702702	Feliz Deserto	-10.2935	-36.3028	27	AL	false
5103700	Feliz Natal	-12.385	-54.9227	51	MT	false
4107702	Fênix	-23.9135	-51.9805	41	PR	false
4107736	Fernandes Pinheiro	-25.4107	-50.5456	41	PR	false
3125804	Fernandes Tourinho	-19.1541	-42.0803	31	MG	false
2605459	Fernando de Noronha	-3.8396	-32.4107	26	PE	false
2104081	Fernando Falcão	-6.16207	-44.8979	21	MA	false
2403756	Fernando Pedroza	-5.69096	-36.5282	24	RN	false
3515608	Fernando Prestes	-21.2661	-48.6874	35	SP	false
3515509	Fernandópolis	-20.2806	-50.2471	35	SP	false
3515657	Fernão	-22.3607	-49.5187	35	SP	false
3515707	Ferraz de Vasconcelos	-23.5411	-46.371	35	SP	false
1600238	Ferreira Gomes	0.857256	-51.1795	16	AP	false
2605509	Ferreiros	-7.44666	-35.2373	26	PE	false
3125903	Ferros	-19.2343	-43.0192	31	MG	false
3125952	Fervedouro	-20.726	-42.279	31	MG	false
4107751	Figueira	-23.8455	-50.4031	41	PR	false
5003900	Figueirão	-18.6782	-53.638	50	MS	false
1707652	Figueirópolis	-12.1312	-49.1748	17	TO	false
5103809	Figueirópolis D'Oeste	-15.4439	-58.7391	51	MT	false
1707702	Filadélfia	-7.33501	-47.4954	17	TO	false
2910859	Filadélfia	-10.7405	-40.1437	29	BA	false
2910909	Firmino Alves	-14.9823	-39.9269	29	BA	false
5207808	Firminópolis	-16.5778	-50.304	52	GO	false
2702801	Flexeiras	-9.27281	-35.7139	27	AL	false
4107850	Flor da Serra do Sul	-26.2523	-53.3092	41	PR	false
4205357	Flor do Sertão	-26.7811	-53.3505	42	SC	false
3515806	Flora Rica	-21.6727	-51.3821	35	SP	false
4107801	Floraí	-23.3178	-52.3029	41	PR	false
2403806	Florânia	-6.12264	-36.8226	24	RN	false
3515905	Floreal	-20.6752	-50.1513	35	SP	false
2605608	Flores	-7.85842	-37.9715	26	PE	false
4308201	Flores da Cunha	-29.0261	-51.1875	43	RS	false
5207907	Flores de Goiás	-14.4451	-47.0417	52	GO	false
2203800	Flores do Piauí	-7.78793	-42.918	22	PI	false
4107900	Floresta	-23.6031	-52.0807	41	PR	false
2605707	Floresta	-8.60307	-38.5687	26	PE	false
2911006	Floresta Azul	-14.8629	-39.6579	29	BA	false
1503044	Floresta do Araguaia	-7.55335	-49.7125	15	PA	false
2203859	Floresta do Piauí	-7.46682	-41.7883	22	PI	false
3126000	Florestal	-19.888	-44.4318	31	MG	false
4108007	Florestópolis	-22.8623	-51.3882	41	PR	false
2203909	Floriano	-6.77182	-43.0241	22	PI	false
4308250	Floriano Peixoto	-27.8614	-52.0838	43	RS	false
4205407	Florianópolis	-27.5945	-48.5477	42	SC	true 
4108106	Flórida	-23.0847	-51.9546	41	PR	false
3516002	Flórida Paulista	-21.6127	-51.1724	35	SP	false
3516101	Florínia	-22.868	-50.6814	35	SP	false
1301605	Fonte Boa	-2.52342	-66.0942	13	AM	false
4308300	Fontoura Xavier	-28.9817	-52.3445	43	RS	false
3126109	Formiga	-20.4618	-45.4268	31	MG	false
4308409	Formigueiro	-30.0035	-53.4959	43	RS	false
5208004	Formosa	-15.54	-47.337	52	GO	false
2104099	Formosa da Serra Negra	-6.44017	-46.1916	21	MA	false
4108205	Formosa do Oeste	-24.2951	-53.3114	41	PR	false
2911105	Formosa do Rio Preto	-11.0328	-45.193	29	BA	false
4205431	Formosa do Sul	-26.6453	-52.7946	42	SC	false
5208103	Formoso	-13.6499	-48.8775	52	GO	false
3126208	Formoso	-14.9446	-46.2371	31	MG	false
1708205	Formoso do Araguaia	-11.7976	-49.5316	17	TO	false
4308433	Forquetinha	-29.3828	-52.0981	43	RS	false
2304350	Forquilha	-3.79945	-40.2634	23	CE	false
4205456	Forquilhinha	-28.7454	-49.4785	42	SC	false
2304400	Fortaleza	-3.71664	-38.5423	23	CE	true 
3126307	Fortaleza de Minas	-20.8508	-46.712	31	MG	false
1708254	Fortaleza do Tabocão	-9.05611	-48.5206	17	TO	false
2104107	Fortaleza dos Nogueiras	-6.95983	-46.1749	21	MA	false
4308458	Fortaleza dos Valos	-28.7986	-53.2249	43	RS	false
2304459	Fortim	-4.45126	-37.7981	23	CE	false
2104206	Fortuna	-5.72792	-44.1565	21	MA	false
3126406	Fortuna de Minas	-19.5578	-44.4472	31	MG	false
4108304	Foz do Iguaçu	-25.5427	-54.5827	41	PR	false
4108452	Foz do Jordão	-25.7371	-52.1188	41	PR	false
4205506	Fraiburgo	-27.0233	-50.92	42	SC	false
3516200	Franca	-20.5352	-47.4039	35	SP	false
2204006	Francinópolis	-6.39334	-42.2591	22	PI	false
4108320	Francisco Alves	-24.0667	-53.8461	41	PR	false
2204105	Francisco Ayres	-6.62606	-42.6881	22	PI	false
3126505	Francisco Badaró	-16.9883	-42.3568	31	MG	false
4108403	Francisco Beltrão	-26.0817	-53.0535	41	PR	false
2403905	Francisco Dantas	-6.07234	-38.1212	24	RN	false
3126604	Francisco Dumont	-17.3107	-44.2317	31	MG	false
2204154	Francisco Macedo	-7.331	-40.788	22	PI	false
3516309	Francisco Morato	-23.2792	-46.7448	35	SP	false
3126703	Francisco Sá	-16.4827	-43.4896	31	MG	false
2204204	Francisco Santos	-6.99491	-41.1288	22	PI	false
3126752	Franciscópolis	-17.9578	-42.0094	31	MG	false
3516408	Franco da Rocha	-23.3229	-46.729	35	SP	false
2304509	Frecheirinha	-3.75557	-40.818	23	CE	false
4308508	Frederico Westphalen	-27.3586	-53.3958	43	RS	false
3126802	Frei Gaspar	-18.0709	-41.4325	31	MG	false
3126901	Frei Inocêncio	-18.5556	-41.9121	31	MG	false
3126950	Frei Lagonegro	-18.1751	-42.7617	31	MG	false
2506202	Frei Martinho	-6.39759	-36.4526	25	PB	false
2605806	Frei Miguelinho	-7.93918	-35.9113	26	PE	false
2802304	Frei Paulo	-10.5513	-37.5279	28	SE	false
4205555	Frei Rogério	-27.175	-50.8076	42	SC	false
3127008	Fronteira	-20.2748	-49.1984	31	MG	false
3127057	Fronteira dos Vales	-16.8898	-40.923	31	MG	false
2204303	Fronteiras	-7.08173	-40.6146	22	PI	false
3127073	Fruta de Leite	-16.1225	-42.5288	31	MG	false
3127107	Frutal	-20.0259	-48.9355	31	MG	false
2404002	Frutuoso Gomes	-6.15669	-37.8375	24	RN	false
3202207	Fundão	-19.937	-40.4078	32	ES	false
3127206	Funilândia	-19.3661	-44.061	31	MG	false
3516507	Gabriel Monteiro	-21.5294	-50.5573	35	SP	false
2506251	Gado Bravo	-7.58279	-35.7899	25	PB	false
3516606	Gália	-22.2918	-49.5504	35	SP	false
3127305	Galiléia	-19.0005	-41.5387	31	MG	false
2404101	Galinhos	-5.0909	-36.2754	24	RN	false
4205605	Galvão	-26.4549	-52.6875	42	SC	false
2605905	Gameleira	-8.5798	-35.3846	26	PE	false
5208152	Gameleira de Goiás	-16.4854	-48.6454	52	GO	false
3127339	Gameleiras	-15.0829	-43.125	31	MG	false
2911204	Gandu	-13.7441	-39.4747	29	BA	false
2606002	Garanhuns	-8.88243	-36.4966	26	PE	false
2802403	Gararu	-9.9722	-37.0869	28	SE	false
3516705	Garça	-22.2125	-49.6546	35	SP	false
4308607	Garibaldi	-29.259	-51.5352	43	RS	false
4205704	Garopaba	-28.0275	-48.6192	42	SC	false
1503077	Garrafão do Norte	-1.92986	-47.0505	15	PA	false
4308656	Garruchos	-28.1944	-55.6383	43	RS	false
4205803	Garuva	-26.0292	-48.852	42	SC	false
4205902	Gaspar	-26.9336	-48.9534	42	SC	false
3516804	Gastão Vidigal	-20.7948	-50.1912	35	SP	false
5103858	Gaúcha do Norte	-13.2443	-53.0809	51	MT	false
4308706	Gaurama	-27.5856	-52.0915	43	RS	false
2911253	Gavião	-11.4688	-39.7757	29	BA	false
3516853	Gavião Peixoto	-21.8367	-48.4957	35	SP	false
2204352	Geminiano	-7.15476	-41.3409	22	PI	false
4308805	General Câmara	-29.9032	-51.7612	43	RS	false
5103908	General Carneiro	-15.7094	-52.7574	51	MT	false
4108502	General Carneiro	-26.425	-51.3172	41	PR	false
2802502	General Maynard	-10.6835	-36.9838	28	SE	false
3516903	General Salgado	-20.6485	-50.364	35	SP	false
2304608	General Sampaio	-4.04351	-39.454	23	CE	false
4308854	Gentil	-28.4316	-52.0337	43	RS	false
2911303	Gentio do Ouro	-11.4342	-42.5077	29	BA	false
3517000	Getulina	-21.7961	-49.9312	35	SP	false
4308904	Getúlio Vargas	-27.8911	-52.2294	43	RS	false
2204402	Gilbués	-9.83001	-45.3423	22	PI	false
2702900	Girau do Ponciano	-9.88404	-36.8316	27	AL	false
4309001	Giruá	-28.0297	-54.3517	43	RS	false
3127354	Glaucilândia	-16.8481	-43.692	31	MG	false
3517109	Glicério	-21.3812	-50.2123	35	SP	false
2911402	Glória	-9.34382	-38.2544	29	BA	false
5103957	Glória D'Oeste	-15.768	-58.3108	51	MT	false
5004007	Glória de Dourados	-22.4136	-54.2335	50	MS	false
2606101	Glória do Goitá	-8.00568	-35.2904	26	PE	false
4309050	Glorinha	-29.8798	-50.7734	43	RS	false
2104305	Godofredo Viana	-1.40259	-45.7795	21	MA	false
4108551	Godoy Moreira	-24.173	-51.9246	41	PR	false
3127370	Goiabeira	-18.9807	-41.2235	31	MG	false
3127388	Goianá	-21.536	-43.1957	31	MG	false
2606200	Goiana	-7.5606	-34.9959	26	PE	false
5208400	Goianápolis	-16.5098	-49.0234	52	GO	false
5208509	Goiandira	-18.1352	-48.0875	52	GO	false
5208608	Goianésia	-15.3118	-49.1162	52	GO	false
1503093	Goianésia do Pará	-3.84338	-49.0974	15	PA	false
5208707	Goiânia	-16.6864	-49.2643	52	GO	true 
2404200	Goianinha	-6.26486	-35.1943	24	RN	false
5208806	Goianira	-16.4947	-49.427	52	GO	false
1708304	Goianorte	-8.77413	-48.9313	17	TO	false
5208905	Goiás	-15.9333	-50.14	52	GO	false
1709005	Goiatins	-7.71478	-47.3252	17	TO	false
5209101	Goiatuba	-18.0105	-49.3658	52	GO	false
4108601	Goioerê	-24.1835	-53.0248	41	PR	false
4108650	Goioxim	-25.1927	-51.9911	41	PR	false
3127404	Gonçalves	-22.6545	-45.8556	31	MG	false
2104404	Gonçalves Dias	-5.1475	-44.3013	21	MA	false
2911501	Gongogi	-14.3195	-39.469	29	BA	false
3127503	Gonzaga	-18.8196	-42.4769	31	MG	false
3127602	Gouveia	-18.4519	-43.7423	31	MG	false
5209150	Gouvelândia	-18.6238	-50.0805	52	GO	false
2104503	Governador Archer	-5.02078	-44.2754	21	MA	false
4206009	Governador Celso Ramos	-27.3172	-48.5576	42	SC	false
2404309	Governador Dix-Sept Rosado	-5.44887	-37.5183	24	RN	false
2104552	Governador Edison Lobão	-5.74973	-47.3646	21	MA	false
2104602	Governador Eugênio Barros	-5.31897	-44.2469	21	MA	false
1101005	Governador Jorge Teixeira	-10.61	-62.7371	11	RO	false
3202256	Governador Lindenberg	-19.1864	-40.4473	32	ES	false
2104628	Governador Luiz Rocha	-5.47835	-44.0774	21	MA	false
2911600	Governador Mangabeira	-12.5994	-39.0412	29	BA	false
2104651	Governador Newton Bello	-3.43245	-45.6619	21	MA	false
2104677	Governador Nunes Freire	-2.12899	-45.8777	21	MA	false
3127701	Governador Valadares	-18.8545	-41.9555	31	MG	false
2304657	Graça	-4.04422	-40.749	23	CE	false
2104701	Graça Aranha	-5.40547	-44.3358	21	MA	false
2802601	Gracho Cardoso	-10.2252	-37.2006	28	SE	false
2104800	Grajaú	-5.81367	-46.1462	21	MA	false
4309100	Gramado	-29.3734	-50.8762	43	RS	false
4309126	Gramado dos Loureiros	-27.4429	-52.9149	43	RS	false
4309159	Gramado Xavier	-29.2706	-52.5795	43	RS	false
4108700	Grandes Rios	-24.1466	-51.5094	41	PR	false
2606309	Granito	-7.70711	-39.615	26	PE	false
2304707	Granja	-3.12788	-40.8372	23	CE	false
2304806	Granjeiro	-6.88134	-39.2144	23	CE	false
3127800	Grão Mogol	-16.5662	-42.8923	31	MG	false
4206108	Grão Pará	-28.1809	-49.2252	42	SC	false
2606408	Gravatá	-8.21118	-35.5675	26	PE	false
4309209	Gravataí	-29.9413	-50.9869	43	RS	false
4206207	Gravatal	-28.3208	-49.0427	42	SC	false
2304905	Groaíras	-3.91787	-40.3852	23	CE	false
2404408	Grossos	-4.98068	-37.1621	24	RN	false
3127909	Grupiara	-18.5003	-47.7318	31	MG	false
4309258	Guabiju	-28.5421	-51.6948	43	RS	false
4206306	Guabiruba	-27.0808	-48.9804	42	SC	false
3202306	Guaçuí	-20.7668	-41.6734	32	ES	false
2204501	Guadalupe	-6.78285	-43.5594	22	PI	false
4309308	Guaíba	-30.1086	-51.3233	43	RS	false
3517208	Guaiçara	-21.6195	-49.8013	35	SP	false
3517307	Guaimbê	-21.9091	-49.8986	35	SP	false
3517406	Guaíra	-20.3196	-48.312	35	SP	false
4108809	Guaíra	-24.085	-54.2573	41	PR	false
4108908	Guairaçá	-22.932	-52.6906	41	PR	false
2304954	Guaiúba	-4.04057	-38.6404	23	CE	false
1301654	Guajará	-7.53797	-72.5907	13	AM	false
1100106	Guajará-Mirim	-10.7889	-65.3296	11	RO	false
2911659	Guajeru	-14.5467	-41.9381	29	BA	false
2404507	Guamaré	-5.10619	-36.3222	24	RN	false
4108957	Guamiranga	-25.1912	-50.8021	41	PR	false
2911709	Guanambi	-14.2231	-42.7799	29	BA	false
3128006	Guanhães	-18.7713	-42.9312	31	MG	false
3128105	Guapé	-20.7631	-45.9152	31	MG	false
3517505	Guapiaçu	-20.7959	-49.2172	35	SP	false
3517604	Guapiara	-24.1892	-48.5295	35	SP	false
3301850	Guapimirim	-22.5347	-42.9895	33	RJ	false
4109005	Guapirama	-23.5203	-50.0407	41	PR	false
5209200	Guapó	-16.8297	-49.5345	52	GO	false
4309407	Guaporé	-28.8399	-51.8895	43	RS	false
4109104	Guaporema	-23.3402	-52.7786	41	PR	false
3517703	Guará	-20.4302	-47.8236	35	SP	false
2506301	Guarabira	-6.85064	-35.485	25	PB	false
3517802	Guaraçaí	-21.0292	-51.2119	35	SP	false
3517901	Guaraci	-20.4977	-48.9391	35	SP	false
4109203	Guaraci	-22.9694	-51.6504	41	PR	false
3128204	Guaraciaba	-20.5716	-43.0094	31	MG	false
4206405	Guaraciaba	-26.6042	-53.5243	42	SC	false
2305001	Guaraciaba do Norte	-4.15814	-40.7476	23	CE	false
3128253	Guaraciama	-17.0142	-43.6675	31	MG	false
1709302	Guaraí	-8.83543	-48.5114	17	TO	false
5209291	Guaraíta	-15.6121	-50.0265	52	GO	false
2305100	Guaramiranga	-4.26248	-38.932	23	CE	false
4206504	Guaramirim	-26.4688	-49.0026	42	SC	false
3128303	Guaranésia	-21.3009	-46.7964	31	MG	false
3128402	Guarani	-21.3563	-43.0328	31	MG	false
3518008	Guarani d'Oeste	-20.0746	-50.3411	35	SP	false
4309506	Guarani das Missões	-28.1491	-54.5629	43	RS	false
5209408	Guarani de Goiás	-13.9421	-46.4868	52	GO	false
4109302	Guaraniaçu	-25.0968	-52.8755	41	PR	false
3518107	Guarantã	-21.8942	-49.5914	35	SP	false
5104104	Guarantã do Norte	-9.96218	-54.9121	51	MT	false
3202405	Guarapari	-20.6772	-40.5093	32	ES	false
4109401	Guarapuava	-25.3902	-51.4623	41	PR	false
4109500	Guaraqueçaba	-25.3071	-48.3204	41	PR	false
3128501	Guarará	-21.7304	-43.0334	31	MG	false
3518206	Guararapes	-21.2544	-50.6453	35	SP	false
3518305	Guararema	-23.4112	-46.0369	35	SP	false
2911808	Guaratinga	-16.5833	-39.7847	29	BA	false
3518404	Guaratinguetá	-22.8075	-45.1938	35	SP	false
4109609	Guaratuba	-25.8817	-48.5752	41	PR	false
3128600	Guarda-Mor	-17.7673	-47.0998	31	MG	false
3518503	Guareí	-23.3714	-48.1837	35	SP	false
3518602	Guariba	-21.3594	-48.2316	35	SP	false
2204550	Guaribas	-9.38647	-43.6943	22	PI	false
5209457	Guarinos	-14.7292	-49.7006	52	GO	false
3518701	Guarujá	-23.9888	-46.258	35	SP	false
4206603	Guarujá do Sul	-26.3858	-53.5296	42	SC	false
3518800	Guarulhos	-23.4538	-46.5333	35	SP	false
4206652	Guatambú	-27.1341	-52.7887	42	SC	false
3518859	Guatapará	-21.4944	-48.0356	35	SP	false
3128709	Guaxupé	-21.305	-46.7081	31	MG	false
5004106	Guia Lopes da Laguna	-21.4583	-56.1117	50	MS	false
3128808	Guidoval	-21.155	-42.7887	31	MG	false
2104909	Guimarães	-2.12755	-44.602	21	MA	false
3128907	Guimarânia	-18.8425	-46.7901	31	MG	false
5104203	Guiratinga	-16.346	-53.7575	51	MT	false
3129004	Guiricema	-21.0098	-42.7207	31	MG	false
3129103	Gurinhatã	-19.2143	-49.7876	31	MG	false
2506400	Gurinhém	-7.1233	-35.4222	25	PB	false
2506509	Gurjão	-7.24833	-36.4923	25	PB	false
1503101	Gurupá	-1.41412	-51.6338	15	PA	false
1709500	Gurupi	-11.7279	-49.068	17	TO	false
3518909	Guzolândia	-20.6467	-50.6645	35	SP	false
4309555	Harmonia	-29.5456	-51.4185	43	RS	false
5209606	Heitoraí	-15.719	-49.8268	52	GO	false
3129202	Heliodora	-22.0644	-45.5453	31	MG	false
2911857	Heliópolis	-10.6825	-38.2907	29	BA	false
3519006	Herculândia	-22.0038	-50.3907	35	SP	false
4307104	Herval	-32.024	-53.3944	43	RS	false
4206702	Herval d'Oeste	-27.1903	-51.4917	42	SC	false
4309571	Herveiras	-29.4552	-52.6553	43	RS	false
5209705	Hidrolândia	-16.9626	-49.2265	52	GO	false
2305209	Hidrolândia	-4.40958	-40.4056	23	CE	false
5209804	Hidrolina	-14.7261	-49.4634	52	GO	false
3519055	Holambra	-22.6405	-47.0487	35	SP	false
4109658	Honório Serpa	-26.139	-52.3848	41	PR	false
2305233	Horizonte	-4.1209	-38.4707	23	CE	false
4309605	Horizontina	-27.6282	-54.3053	43	RS	false
3519071	Hortolândia	-22.8529	-47.2143	35	SP	false
2204600	Hugo Napoleão	-5.9886	-42.5598	22	PI	false
4309654	Hulha Negra	-31.4067	-53.8667	43	RS	false
4309704	Humaitá	-27.5691	-53.9695	43	RS	false
1301704	Humaitá	-7.51171	-63.0327	13	AM	false
2105005	Humberto de Campos	-2.59828	-43.4649	21	MA	false
3519105	Iacanga	-21.8896	-49.031	35	SP	false
5209903	Iaciara	-14.1011	-46.6335	52	GO	false
3519204	Iacri	-21.8572	-50.6932	35	SP	false
2911907	Iaçu	-12.7666	-40.2056	29	BA	false
3129301	Iapu	-19.4387	-42.2147	31	MG	false
3519253	Iaras	-22.8682	-49.1634	35	SP	false
2606507	Iati	-9.04559	-36.8498	26	PE	false
4109708	Ibaiti	-23.8478	-50.1932	41	PR	false
4309753	Ibarama	-29.4203	-53.1295	43	RS	false
2305266	Ibaretama	-4.80376	-38.7501	23	CE	false
3519303	Ibaté	-21.9584	-47.9882	35	SP	false
2703007	Ibateguara	-8.97823	-35.9373	27	AL	false
3202454	Ibatiba	-20.2347	-41.5087	32	ES	false
4109757	Ibema	-25.1193	-53.0072	41	PR	false
3129400	Ibertioga	-21.433	-43.9639	31	MG	false
3129509	Ibiá	-19.4749	-46.5474	31	MG	false
4309803	Ibiaçá	-28.0566	-51.8599	43	RS	false
3129608	Ibiaí	-16.8591	-44.9046	31	MG	false
4206751	Ibiam	-27.1847	-51.2352	42	SC	false
2305308	Ibiapina	-3.92403	-40.8911	23	CE	false
2506608	Ibiara	-7.47957	-38.4059	25	PB	false
2912004	Ibiassucê	-14.2711	-42.257	29	BA	false
2912103	Ibicaraí	-14.8579	-39.5914	29	BA	false
4206801	Ibicaré	-27.0881	-51.3681	42	SC	false
2912202	Ibicoara	-13.4059	-41.284	29	BA	false
2912301	Ibicuí	-14.845	-39.9879	29	BA	false
2305332	Ibicuitinga	-4.96999	-38.6362	23	CE	false
2606606	Ibimirim	-8.54026	-37.7032	26	PE	false
2912400	Ibipeba	-11.6438	-42.0195	29	BA	false
2912509	Ibipitanga	-12.8804	-42.4856	29	BA	false
4109807	Ibiporã	-23.2659	-51.0522	41	PR	false
2912608	Ibiquera	-12.6444	-40.9338	29	BA	false
3519402	Ibirá	-21.083	-49.2448	35	SP	false
3129657	Ibiracatu	-15.6605	-44.1667	31	MG	false
3129707	Ibiraci	-20.4611	-47.1222	31	MG	false
3202504	Ibiraçu	-19.8366	-40.3732	32	ES	false
4309902	Ibiraiaras	-28.3741	-51.6377	43	RS	false
2606705	Ibirajuba	-8.57633	-36.1812	26	PE	false
4206900	Ibirama	-27.0547	-49.5193	42	SC	false
2912707	Ibirapitanga	-14.1649	-39.3787	29	BA	false
2912806	Ibirapuã	-17.6832	-40.1129	29	BA	false
4309951	Ibirapuitã	-28.6247	-52.5158	43	RS	false
3519501	Ibirarema	-22.8185	-50.0739	35	SP	false
2912905	Ibirataia	-14.0643	-39.6459	29	BA	false
3129806	Ibirité	-20.0252	-44.0569	31	MG	false
4310009	Ibirubá	-28.6302	-53.0961	43	RS	false
2913002	Ibitiara	-12.6502	-42.2179	29	BA	false
3519600	Ibitinga	-21.7562	-48.8319	35	SP	false
3202553	Ibitirama	-20.5466	-41.6667	32	ES	false
2913101	Ibititá	-11.5414	-41.9748	29	BA	false
3129905	Ibitiúra de Minas	-22.0604	-46.4368	31	MG	false
3130002	Ibituruna	-21.1541	-44.7479	31	MG	false
3519709	Ibiúna	-23.6596	-47.223	35	SP	false
2913200	Ibotirama	-12.1779	-43.2167	29	BA	false
2305357	Icapuí	-4.71206	-37.3531	23	CE	false
4207007	Içara	-28.7132	-49.3087	42	SC	false
3130051	Icaraí de Minas	-16.214	-44.9034	31	MG	false
4109906	Icaraíma	-23.3944	-53.615	41	PR	false
2105104	Icatu	-2.77206	-44.0501	21	MA	false
3519808	Icém	-20.3391	-49.1915	35	SP	false
2913309	Ichu	-11.7431	-39.1905	29	BA	false
3202603	Iconha	-20.7913	-40.8132	32	ES	false
2404606	Ielmo Marinho	-5.82447	-35.55	24	RN	false
3519907	Iepê	-22.6602	-51.0779	35	SP	false
2703106	Igaci	-9.53768	-36.6372	27	AL	false
2913408	Igaporã	-13.774	-42.7155	29	BA	false
3520004	Igaraçu do Tietê	-22.509	-48.5597	35	SP	false
2502607	Igaracy	-7.17184	-38.1478	25	PB	false
3520103	Igarapava	-20.0407	-47.7466	35	SP	false
3130101	Igarapé	-20.0707	-44.2994	31	MG	false
2105153	Igarapé do Meio	-3.65771	-45.2114	21	MA	false
2105203	Igarapé Grande	-4.6625	-44.8558	21	MA	false
1503200	Igarapé-Açu	-1.12539	-47.626	15	PA	false
1503309	Igarapé-Miri	-1.97533	-48.9575	15	PA	false
2606804	Igarassu	-7.82881	-34.9013	26	PE	false
3520202	Igaratá	-23.2037	-46.157	35	SP	false
3130200	Igaratinga	-19.9476	-44.7063	31	MG	false
2913457	Igrapiúna	-13.8295	-39.1361	29	BA	false
2703205	Igreja Nova	-10.1235	-36.6597	27	AL	false
4310108	Igrejinha	-29.5693	-50.7919	43	RS	false
3301876	Iguaba Grande	-22.8495	-42.2299	33	RJ	false
2913507	Iguaí	-14.7528	-40.0894	29	BA	false
3520301	Iguape	-24.699	-47.5537	35	SP	false
4110003	Iguaraçu	-23.1949	-51.8256	41	PR	false
2606903	Iguaracy	-7.83222	-37.5082	26	PE	false
3130309	Iguatama	-20.1776	-45.7111	31	MG	false
5004304	Iguatemi	-23.6736	-54.5637	50	MS	false
2305506	Iguatu	-6.36281	-39.2892	23	CE	false
4110052	Iguatu	-24.7153	-53.0827	41	PR	false
3130408	Ijaci	-21.1738	-44.9233	31	MG	false
4310207	Ijuí	-28.388	-53.92	43	RS	false
3520426	Ilha Comprida	-24.7307	-47.5383	35	SP	false
2802700	Ilha das Flores	-10.4425	-36.5479	28	SE	false
2607604	Ilha de Itamaracá	-7.74766	-34.8303	26	PE	false
2204659	Ilha Grande	-2.85774	-41.8186	22	PI	false
3520442	Ilha Solteira	-20.4326	-51.3426	35	SP	false
3520400	Ilhabela	-23.7785	-45.3552	35	SP	false
2913606	Ilhéus	-14.793	-39.046	29	BA	false
4207106	Ilhota	-26.9023	-48.8251	42	SC	false
3130507	Ilicínea	-20.9402	-45.8308	31	MG	false
4310306	Ilópolis	-28.9282	-52.1258	43	RS	false
2506707	Imaculada	-7.3889	-37.5079	25	PB	false
4207205	Imaruí	-28.3339	-48.817	42	SC	false
4110078	Imbaú	-24.448	-50.7533	41	PR	false
4310330	Imbé	-29.9753	-50.1281	43	RS	false
3130556	Imbé de Minas	-19.6017	-41.9695	31	MG	false
4207304	Imbituba	-28.2284	-48.6659	42	SC	false
4110102	Imbituva	-25.2285	-50.5989	41	PR	false
4207403	Imbuia	-27.4908	-49.4218	42	SC	false
4310363	Imigrante	-29.3508	-51.7748	43	RS	false
2105302	Imperatriz	-5.51847	-47.4777	21	MA	false
4110201	Inácio Martins	-25.5704	-51.0769	41	PR	false
5209937	Inaciolândia	-18.4869	-49.9888	52	GO	false
2607000	Inajá	-8.90206	-37.8351	26	PE	false
4110300	Inajá	-22.7509	-52.1995	41	PR	false
3130606	Inconfidentes	-22.3136	-46.3264	31	MG	false
3130655	Indaiabira	-15.4911	-42.2005	31	MG	false
4207502	Indaial	-26.8992	-49.2354	42	SC	false
3520509	Indaiatuba	-23.0816	-47.2101	35	SP	false
4310405	Independência	-27.8354	-54.1886	43	RS	false
2305605	Independência	-5.38789	-40.3085	23	CE	false
3520608	Indiana	-22.1738	-51.2555	35	SP	false
4110409	Indianópolis	-23.4762	-52.6989	41	PR	false
3130705	Indianópolis	-19.0341	-47.9155	31	MG	false
3520707	Indiaporã	-19.979	-50.2909	35	SP	false
5209952	Indiara	-17.1387	-49.9862	52	GO	false
2802809	Indiaroba	-11.5157	-37.515	28	SE	false
5104500	Indiavaí	-15.4921	-58.5802	51	MT	false
2506806	Ingá	-7.28144	-35.605	25	PB	false
3130804	Ingaí	-21.4024	-44.9152	31	MG	false
2607109	Ingazeira	-7.66909	-37.4576	26	PE	false
4310413	Inhacorá	-27.8752	-54.015	43	RS	false
2913705	Inhambupe	-11.781	-38.355	29	BA	false
1503408	Inhangapi	-1.4349	-47.9114	15	PA	false
2703304	Inhapi	-9.22594	-37.7509	27	AL	false
3130903	Inhapim	-19.5476	-42.1147	31	MG	false
3131000	Inhaúma	-19.4898	-44.3934	31	MG	false
2204709	Inhuma	-6.665	-41.7041	22	PI	false
5210000	Inhumas	-16.3611	-49.5001	52	GO	false
3131109	Inimutaba	-18.7271	-44.3584	31	MG	false
5004403	Inocência	-19.7277	-51.9281	50	MS	false
3520806	Inúbia Paulista	-21.7695	-50.9633	35	SP	false
4207577	Iomerê	-27.0019	-51.2442	42	SC	false
3131158	Ipaba	-19.4158	-42.4139	31	MG	false
5210109	Ipameri	-17.7215	-48.1581	52	GO	false
3131208	Ipanema	-19.7992	-41.7164	31	MG	false
2404705	Ipanguaçu	-5.48984	-36.8501	24	RN	false
2305654	Ipaporanga	-4.89764	-40.7537	23	CE	false
3131307	Ipatinga	-19.4703	-42.5476	31	MG	false
2305704	Ipaumirim	-6.78265	-38.7179	23	CE	false
3520905	Ipaussu	-23.0575	-49.6279	35	SP	false
4310439	Ipê	-28.8171	-51.2859	43	RS	false
2913804	Ipecaetá	-12.3028	-39.3069	29	BA	false
3521002	Iperó	-23.3513	-47.6927	35	SP	false
3521101	Ipeúna	-22.4355	-47.7151	35	SP	false
3131406	Ipiaçu	-18.6927	-49.9436	31	MG	false
2913903	Ipiaú	-14.1226	-39.7353	29	BA	false
3521150	Ipiguá	-20.6557	-49.3842	35	SP	false
2914000	Ipirá	-12.1561	-39.7359	29	BA	false
4207601	Ipira	-27.4038	-51.7758	42	SC	false
4110508	Ipiranga	-25.0238	-50.5794	41	PR	false
5210158	Ipiranga de Goiás	-15.1689	-49.6695	52	GO	false
5104526	Ipiranga do Norte	-12.2408	-56.1531	51	MT	false
2204808	Ipiranga do Piauí	-6.82421	-41.7381	22	PI	false
4310462	Ipiranga do Sul	-27.9404	-52.4271	43	RS	false
1301803	Ipixuna	-7.04791	-71.6934	13	AM	false
1503457	Ipixuna do Pará	-2.55992	-47.5059	15	PA	false
2607208	Ipojuca	-8.39303	-35.0609	26	PE	false
4110607	Iporã	-24.0083	-53.706	41	PR	false
5210208	Iporá	-16.4398	-51.118	52	GO	false
4207650	Iporã do Oeste	-26.9854	-53.5355	42	SC	false
3521200	Iporanga	-24.5847	-48.5971	35	SP	false
2305803	Ipu	-4.31748	-40.7059	23	CE	false
3521309	Ipuã	-20.4438	-48.0129	35	SP	false
4207684	Ipuaçu	-26.635	-52.4556	42	SC	false
2607307	Ipubi	-7.64505	-40.1476	26	PE	false
2404804	Ipueira	-6.80596	-37.2045	24	RN	false
1709807	Ipueiras	-11.2329	-48.46	17	TO	false
2305902	Ipueiras	-4.53802	-40.7118	23	CE	false
3131505	Ipuiúna	-22.1013	-46.1915	31	MG	false
4207700	Ipumirim	-27.0772	-52.1289	42	SC	false
2914109	Ipupiara	-11.8219	-42.6179	29	BA	false
1400282	Iracema	2.18305	-61.0415	14	RR	false
2306009	Iracema	-5.8124	-38.2919	23	CE	false
4110656	Iracema do Oeste	-24.4262	-53.3528	41	PR	false
3521408	Iracemápolis	-22.5832	-47.523	35	SP	false
4207759	Iraceminha	-26.8215	-53.2767	42	SC	false
4310504	Iraí	-27.1951	-53.2543	43	RS	false
3131604	Iraí de Minas	-18.9819	-47.461	31	MG	false
2914208	Irajuba	-13.2563	-40.0848	29	BA	false
2914307	Iramaia	-13.2902	-40.9595	29	BA	false
1301852	Iranduba	-3.27479	-60.19	13	AM	false
4207809	Irani	-27.0287	-51.9012	42	SC	false
3521507	Irapuã	-21.2768	-49.4164	35	SP	false
3521606	Irapuru	-21.5684	-51.3472	35	SP	false
2914406	Iraquara	-12.2429	-41.6155	29	BA	false
2914505	Irará	-12.0504	-38.7631	29	BA	false
4110706	Irati	-25.4697	-50.6493	41	PR	false
4207858	Irati	-26.6539	-52.8955	42	SC	false
2306108	Irauçuba	-3.74737	-39.7843	23	CE	false
2914604	Irecê	-11.3033	-41.8535	29	BA	false
4110805	Iretama	-24.4253	-52.1012	41	PR	false
4207908	Irineópolis	-26.242	-50.7957	42	SC	false
1503507	Irituia	-1.76984	-47.446	15	PA	false
3202652	Irupi	-20.3501	-41.6444	32	ES	false
2204907	Isaías Coelho	-7.73597	-41.6735	22	PI	false
5210307	Israelândia	-16.3144	-50.9087	52	GO	false
4208005	Itá	-27.2907	-52.3212	42	SC	false
4310538	Itaara	-29.6013	-53.7725	43	RS	false
2506905	Itabaiana	-7.33167	-35.3317	25	PB	false
2802908	Itabaiana	-10.6826	-37.4273	28	SE	false
2803005	Itabaianinha	-11.2693	-37.7875	28	SE	false
2914653	Itabela	-16.5732	-39.5593	29	BA	false
3521705	Itaberá	-23.8638	-49.14	35	SP	false
2914703	Itaberaba	-12.5242	-40.3059	29	BA	false
5210406	Itaberaí	-16.0206	-49.806	52	GO	false
2803104	Itabi	-10.1248	-37.1056	28	SE	false
3131703	Itabira	-19.6239	-43.2312	31	MG	false
3131802	Itabirinha	-18.5712	-41.234	31	MG	false
3131901	Itabirito	-20.2501	-43.8038	31	MG	false
3301900	Itaboraí	-22.7565	-42.8639	33	RJ	false
2914802	Itabuna	-14.7876	-39.2781	29	BA	false
1710508	Itacajá	-8.39293	-47.7726	17	TO	false
3132008	Itacambira	-17.0625	-43.3069	31	MG	false
3132107	Itacarambi	-15.089	-44.095	31	MG	false
2914901	Itacaré	-14.2784	-38.9959	29	BA	false
1301902	Itacoatiara	-3.13861	-58.4449	13	AM	false
2607406	Itacuruba	-8.82231	-38.6975	26	PE	false
4310553	Itacurubi	-28.7913	-55.2447	43	RS	false
2915007	Itaeté	-12.9831	-40.9677	29	BA	false
2915106	Itagi	-14.1615	-40.0131	29	BA	false
2915205	Itagibá	-14.2782	-39.8449	29	BA	false
2915304	Itagimirim	-16.0819	-39.6133	29	BA	false
3202702	Itaguaçu	-19.8018	-40.8601	32	ES	false
2915353	Itaguaçu da Bahia	-11.0147	-42.3997	29	BA	false
3302007	Itaguaí	-22.8636	-43.7798	33	RJ	false
4110904	Itaguajé	-22.6183	-51.9674	41	PR	false
3132206	Itaguara	-20.3947	-44.4875	31	MG	false
5210562	Itaguari	-15.918	-49.6071	52	GO	false
5210604	Itaguaru	-15.7565	-49.6354	52	GO	false
1710706	Itaguatins	-5.77267	-47.4864	17	TO	false
3521804	Itaí	-23.4213	-49.092	35	SP	false
2607505	Itaíba	-8.94569	-37.4173	26	PE	false
2306207	Itaiçaba	-4.67146	-37.833	23	CE	false
2205003	Itainópolis	-7.44336	-41.4687	22	PI	false
4208104	Itaiópolis	-26.339	-49.9092	42	SC	false
2105351	Itaipava do Grajaú	-5.14252	-45.7877	21	MA	false
3132305	Itaipé	-17.4014	-41.6697	31	MG	false
4110953	Itaipulândia	-25.1366	-54.3001	41	PR	false
2306256	Itaitinga	-3.96577	-38.5298	23	CE	false
1503606	Itaituba	-4.2667	-55.9926	15	PA	false
2404853	Itajá	-5.63894	-36.8712	24	RN	false
5210802	Itajá	-19.0673	-51.5495	52	GO	false
4208203	Itajaí	-26.9101	-48.6705	42	SC	false
3521903	Itajobi	-21.3123	-49.0629	35	SP	false
3522000	Itaju	-21.9857	-48.8116	35	SP	false
2915403	Itaju do Colônia	-15.1366	-39.7283	29	BA	false
3132404	Itajubá	-22.4225	-45.4598	31	MG	false
2915502	Itajuípe	-14.6788	-39.3698	29	BA	false
3302056	Italva	-21.4296	-41.7014	33	RJ	false
2915601	Itamaraju	-17.0378	-39.5386	29	BA	false
3132503	Itamarandiba	-17.8552	-42.8561	31	MG	false
1301951	Itamarati	-6.43852	-68.2437	13	AM	false
3132602	Itamarati de Minas	-21.4179	-42.813	31	MG	false
2915700	Itamari	-13.7782	-39.683	29	BA	false
3132701	Itambacuri	-18.035	-41.683	31	MG	false
4111001	Itambaracá	-23.0181	-50.4097	41	PR	false
4111100	Itambé	-23.6601	-51.9912	41	PR	false
2607653	Itambé	-7.41403	-35.0963	26	PE	false
2915809	Itambé	-15.2429	-40.63	29	BA	false
3132800	Itambé do Mato Dentro	-19.4158	-43.3182	31	MG	false
3132909	Itamogi	-21.0758	-47.046	31	MG	false
3133006	Itamonte	-22.2859	-44.868	31	MG	false
2915908	Itanagra	-12.2614	-38.0436	29	BA	false
3522109	Itanhaém	-24.1736	-46.788	35	SP	false
3133105	Itanhandu	-22.2942	-44.9382	31	MG	false
5104542	Itanhangá	-12.2259	-56.6463	51	MT	false
2916005	Itanhém	-17.1642	-40.3321	29	BA	false
3133204	Itanhomi	-19.1736	-41.863	31	MG	false
3133303	Itaobim	-16.5571	-41.5017	31	MG	false
3522158	Itaóca	-24.6393	-48.8413	35	SP	false
3302106	Itaocara	-21.6748	-42.0758	33	RJ	false
5210901	Itapaci	-14.9522	-49.5511	52	GO	false
3133402	Itapagipe	-19.9062	-49.3781	31	MG	false
2306306	Itapajé	-3.68314	-39.5855	23	CE	false
2916104	Itaparica	-12.8932	-38.68	29	BA	false
2916203	Itapé	-14.8876	-39.4239	29	BA	false
2916302	Itapebi	-15.9551	-39.5329	29	BA	false
3133501	Itapecerica	-20.4704	-45.127	31	MG	false
3522208	Itapecerica da Serra	-23.7161	-46.8572	35	SP	false
2105401	Itapecuru Mirim	-3.40202	-44.3508	21	MA	false
4111209	Itapejara d'Oeste	-25.9619	-52.8152	41	PR	false
4208302	Itapema	-27.0861	-48.616	42	SC	false
3202801	Itapemirim	-21.0095	-40.8307	32	ES	false
4111258	Itaperuçu	-25.2193	-49.3454	41	PR	false
3302205	Itaperuna	-21.1997	-41.8799	33	RJ	false
2607703	Itapetim	-7.37178	-37.1863	26	PE	false
2916401	Itapetinga	-15.2475	-40.2482	29	BA	false
3522307	Itapetininga	-23.5886	-48.0483	35	SP	false
3522406	Itapeva	-23.9788	-48.8764	35	SP	false
3133600	Itapeva	-22.7665	-46.2241	31	MG	false
3522505	Itapevi	-23.5488	-46.9327	35	SP	false
2916500	Itapicuru	-11.3088	-38.2262	29	BA	false
2306405	Itapipoca	-3.49933	-39.5836	23	CE	false
3522604	Itapira	-22.4357	-46.8224	35	SP	false
1302009	Itapiranga	-2.74081	-58.0293	13	AM	false
4208401	Itapiranga	-27.1659	-53.7166	42	SC	false
5211008	Itapirapuã	-15.8205	-50.6094	52	GO	false
3522653	Itapirapuã Paulista	-24.572	-49.1661	35	SP	false
1710904	Itapiratins	-8.37982	-48.1072	17	TO	false
2607752	Itapissuma	-7.76798	-34.8971	26	PE	false
2916609	Itapitanga	-14.4139	-39.5657	29	BA	false
2306504	Itapiúna	-4.55516	-38.9281	23	CE	false
4208450	Itapoá	-26.1158	-48.6182	42	SC	false
3522703	Itápolis	-21.5942	-48.8149	35	SP	false
5004502	Itaporã	-22.08	-54.7934	50	MS	false
1711100	Itaporã do Tocantins	-8.57172	-48.6895	17	TO	false
3522802	Itaporanga	-23.7043	-49.4819	35	SP	false
2507002	Itaporanga	-7.30202	-38.1504	25	PB	false
2803203	Itaporanga d'Ajuda	-10.99	-37.3078	28	SE	false
2507101	Itapororoca	-6.82374	-35.2406	25	PB	false
1101104	Itapuã do Oeste	-9.19687	-63.1809	11	RO	false
4310579	Itapuca	-28.7768	-52.1693	43	RS	false
3522901	Itapuí	-22.2324	-48.7197	35	SP	false
3523008	Itapura	-20.6419	-51.5063	35	SP	false
5211206	Itapuranga	-15.5606	-49.949	52	GO	false
3523107	Itaquaquecetuba	-23.4835	-46.3457	35	SP	false
2916708	Itaquara	-13.4459	-39.9378	29	BA	false
4310603	Itaqui	-29.1311	-56.5515	43	RS	false
5004601	Itaquiraí	-23.4779	-54.187	50	MS	false
2607802	Itaquitinga	-7.66373	-35.1002	26	PE	false
3202900	Itarana	-19.875	-40.8753	32	ES	false
2916807	Itarantim	-15.6528	-40.065	29	BA	false
3523206	Itararé	-24.1085	-49.3352	35	SP	false
2306553	Itarema	-2.9248	-39.9167	23	CE	false
3523305	Itariri	-24.2834	-47.1736	35	SP	false
5211305	Itarumã	-18.7646	-51.3485	52	GO	false
4310652	Itati	-29.4974	-50.1016	43	RS	false
3302254	Itatiaia	-22.4897	-44.5675	33	RJ	false
3133709	Itatiaiuçu	-20.1983	-44.4211	31	MG	false
3523404	Itatiba	-23.0035	-46.8464	35	SP	false
4310702	Itatiba do Sul	-27.3846	-52.4538	43	RS	false
2916856	Itatim	-12.7099	-39.6952	29	BA	false
3523503	Itatinga	-23.1047	-48.6157	35	SP	false
2306603	Itatira	-4.52608	-39.6202	23	CE	false
2507200	Itatuba	-7.38115	-35.638	25	PB	false
2404903	Itaú	-5.8363	-37.9912	24	RN	false
3133758	Itaú de Minas	-20.7375	-46.7525	31	MG	false
5104559	Itaúba	-11.0614	-55.2766	51	MT	false
1600253	Itaubal	0.602185	-50.6996	16	AP	false
5211404	Itauçu	-16.2029	-49.6109	52	GO	false
2205102	Itaueira	-7.59989	-43.0249	22	PI	false
3133808	Itaúna	-20.0818	-44.5801	31	MG	false
4111308	Itaúna do Sul	-22.7289	-52.8874	41	PR	false
3133907	Itaverava	-20.6769	-43.6141	31	MG	false
3134004	Itinga	-16.61	-41.7672	31	MG	false
2105427	Itinga do Maranhão	-4.45293	-47.5235	21	MA	false
5104609	Itiquira	-17.2147	-54.1422	51	MT	false
3523602	Itirapina	-22.2562	-47.8166	35	SP	false
3523701	Itirapuã	-20.6416	-47.2194	35	SP	false
2916906	Itiruçu	-13.529	-40.1472	29	BA	false
2917003	Itiúba	-10.6948	-39.8446	29	BA	false
3523800	Itobi	-21.7309	-46.9743	35	SP	false
2917102	Itororó	-15.11	-40.0684	29	BA	false
3523909	Itu	-23.2544	-47.2927	35	SP	false
2917201	Ituaçu	-13.8107	-41.3003	29	BA	false
2917300	Ituberá	-13.7249	-39.1481	29	BA	false
3134103	Itueta	-19.3999	-41.1746	31	MG	false
3134202	Ituiutaba	-18.9772	-49.4639	31	MG	false
5211503	Itumbiara	-18.4093	-49.2158	52	GO	false
3134301	Itumirim	-21.3171	-44.8724	31	MG	false
3524006	Itupeva	-23.1526	-47.0593	35	SP	false
1503705	Itupiranga	-5.13272	-49.3358	15	PA	false
4208500	Ituporanga	-27.4101	-49.5963	42	SC	false
3134400	Iturama	-19.7276	-50.1966	31	MG	false
3134509	Itutinga	-21.3	-44.6567	31	MG	false
3524105	Ituverava	-20.3355	-47.7902	35	SP	false
2917334	Iuiú	-14.4054	-43.5595	29	BA	false
3203007	Iúna	-20.3531	-41.5334	32	ES	false
4111407	Ivaí	-25.0067	-50.857	41	PR	false
4111506	Ivaiporã	-24.2485	-51.6754	41	PR	false
4111555	Ivaté	-23.4072	-53.3687	41	PR	false
4111605	Ivatuba	-23.6187	-52.2203	41	PR	false
5004700	Ivinhema	-22.3046	-53.8184	50	MS	false
5211602	Ivolândia	-16.5995	-50.7921	52	GO	false
4310751	Ivorá	-29.5232	-53.5842	43	RS	false
4310801	Ivoti	-29.5995	-51.1533	43	RS	false
2607901	Jaboatão dos Guararapes	-8.11298	-35.015	26	PE	false
4208609	Jaborá	-27.1782	-51.7279	42	SC	false
2917359	Jaborandi	-13.6071	-44.4255	29	BA	false
3524204	Jaborandi	-20.6884	-48.4112	35	SP	false
4111704	Jaboti	-23.7435	-50.0729	41	PR	false
4310850	Jaboticaba	-27.6347	-53.2762	43	RS	false
3524303	Jaboticabal	-21.252	-48.3252	35	SP	false
3134608	Jaboticatubas	-19.5119	-43.7373	31	MG	false
2405009	Jaçanã	-6.41856	-36.2031	24	RN	false
2917409	Jacaraci	-14.8541	-42.4329	29	BA	false
2507309	Jacaraú	-6.61453	-35.289	25	PB	false
2703403	Jacaré dos Homens	-9.63545	-37.2076	27	AL	false
1503754	Jacareacanga	-6.21469	-57.7544	15	PA	false
3524402	Jacareí	-23.2983	-45.9658	35	SP	false
4111803	Jacarezinho	-23.1591	-49.9739	41	PR	false
3524501	Jaci	-20.8805	-49.5797	35	SP	false
5104807	Jaciara	-15.9548	-54.9733	51	MT	false
3134707	Jacinto	-16.1428	-40.295	31	MG	false
4208708	Jacinto Machado	-28.9961	-49.7623	42	SC	false
2917508	Jacobina	-11.1812	-40.5117	29	BA	false
2205151	Jacobina do Piauí	-7.93063	-41.2075	22	PI	false
3134806	Jacuí	-21.0137	-46.7359	31	MG	false
2703502	Jacuípe	-8.83951	-35.4591	27	AL	false
4310876	Jacuizinho	-29.0401	-53.0657	43	RS	false
1503804	Jacundá	-4.44617	-49.1153	15	PA	false
3524600	Jacupiranga	-24.6963	-48.0064	35	SP	false
4310900	Jacutinga	-27.7291	-52.5372	43	RS	false
3134905	Jacutinga	-22.286	-46.6166	31	MG	false
4111902	Jaguapitã	-23.1104	-51.5342	41	PR	false
2917607	Jaguaquara	-13.5248	-39.964	29	BA	false
3135001	Jaguaraçu	-19.647	-42.7498	31	MG	false
4311007	Jaguarão	-32.5604	-53.377	43	RS	false
2917706	Jaguarari	-10.2569	-40.1999	29	BA	false
3203056	Jaguaré	-18.907	-40.0759	32	ES	false
2306702	Jaguaretama	-5.6051	-38.7639	23	CE	false
4311106	Jaguari	-29.4936	-54.703	43	RS	false
4112009	Jaguariaíva	-24.2439	-49.7066	41	PR	false
2306801	Jaguaribara	-5.67765	-38.5359	23	CE	false
2306900	Jaguaribe	-5.90213	-38.6227	23	CE	false
2917805	Jaguaripe	-13.1109	-38.8939	29	BA	false
3524709	Jaguariúna	-22.7037	-46.9851	35	SP	false
2307007	Jaguaruana	-4.83151	-37.781	23	CE	false
4208807	Jaguaruna	-28.6146	-49.0296	42	SC	false
3135050	Jaíba	-15.3432	-43.6688	31	MG	false
2205201	Jaicós	-7.36229	-41.1371	22	PI	false
3524808	Jales	-20.2672	-50.5494	35	SP	false
3524907	Jambeiro	-23.2522	-45.6942	35	SP	false
3135076	Jampruca	-18.461	-41.809	31	MG	false
3135100	Janaúba	-15.8022	-43.3132	31	MG	false
5211701	Jandaia	-17.0481	-50.1453	52	GO	false
4112108	Jandaia do Sul	-23.6011	-51.6448	41	PR	false
2405108	Jandaíra	-5.35211	-36.1278	24	RN	false
2917904	Jandaíra	-11.5616	-37.7853	29	BA	false
3525003	Jandira	-23.5275	-46.9023	35	SP	false
2405207	Janduís	-6.01474	-37.4048	24	RN	false
5104906	Jangada	-15.235	-56.4917	51	MT	false
4112207	Janiópolis	-24.1401	-52.7784	41	PR	false
3135209	Januária	-15.4802	-44.3639	31	MG	false
2405306	Januário Cicco (Boa Saúde)	-6.16566	-35.6219	24	RN	false
3135308	Japaraíba	-20.1442	-45.5015	31	MG	false
2703601	Japaratinga	-9.08746	-35.2634	27	AL	false
2803302	Japaratuba	-10.5849	-36.9418	28	SE	false
3302270	Japeri	-22.6435	-43.6602	33	RJ	false
2405405	Japi	-6.46544	-35.9346	24	RN	false
4112306	Japira	-23.8142	-50.1422	41	PR	false
2803401	Japoatã	-10.3477	-36.8045	28	SE	false
3135357	Japonvar	-15.9891	-44.2758	31	MG	false
5004809	Japorã	-23.8903	-54.4059	50	MS	false
4112405	Japurá	-23.4693	-52.5557	41	PR	false
1302108	Japurá	-1.88237	-66.9291	13	AM	false
2607950	Jaqueira	-8.72618	-35.7942	26	PE	false
4311122	Jaquirana	-28.8811	-50.3637	43	RS	false
5211800	Jaraguá	-15.7529	-49.3344	52	GO	false
4208906	Jaraguá do Sul	-26.4851	-49.0713	42	SC	false
5004908	Jaraguari	-20.1386	-54.3996	50	MS	false
2703700	Jaramataia	-9.66224	-37.0046	27	AL	false
2307106	Jardim	-7.57599	-39.2826	23	CE	false
5005004	Jardim	-21.4799	-56.1489	50	MS	false
4112504	Jardim Alegre	-24.1809	-51.6902	41	PR	false
2405504	Jardim de Angicos	-5.64999	-35.9713	24	RN	false
2405603	Jardim de Piranhas	-6.37665	-37.3496	24	RN	false
2205250	Jardim do Mulato	-6.099	-42.63	22	PI	false
2405702	Jardim do Seridó	-6.58047	-36.7736	24	RN	false
4112603	Jardim Olinda	-22.5523	-52.0503	41	PR	false
3525102	Jardinópolis	-21.0176	-47.7606	35	SP	false
4208955	Jardinópolis	-26.7191	-52.8625	42	SC	false
4311130	Jari	-29.2922	-54.2237	43	RS	false
3525201	Jarinu	-23.1039	-46.728	35	SP	false
1100114	Jaru	-10.4318	-62.4788	11	RO	false
5211909	Jataí	-17.8784	-51.7204	52	GO	false
4112702	Jataizinho	-23.2578	-50.9777	41	PR	false
2608008	Jataúba	-7.97668	-36.4943	26	PE	false
5005103	Jateí	-22.4806	-54.3079	50	MS	false
2307205	Jati	-7.6797	-39.0029	23	CE	false
2105450	Jatobá	-5.82282	-44.2153	21	MA	false
2608057	Jatobá	-9.17476	-38.2607	26	PE	false
2205276	Jatobá do Piauí	-4.77025	-41.817	22	PI	false
3525300	Jaú	-22.2936	-48.5592	35	SP	false
1711506	Jaú do Tocantins	-12.6509	-48.589	17	TO	false
5212006	Jaupaci	-16.1773	-50.9508	52	GO	false
5105002	Jauru	-15.3342	-58.8723	51	MT	false
3135407	Jeceaba	-20.5339	-43.9894	31	MG	false
3135456	Jenipapo de Minas	-17.0831	-42.2589	31	MG	false
2105476	Jenipapo dos Vieiras	-5.36237	-45.6356	21	MA	false
3135506	Jequeri	-20.4542	-42.6651	31	MG	false
2703759	Jequiá da Praia	-10.0133	-36.0142	27	AL	false
2918001	Jequié	-13.8509	-40.0877	29	BA	false
3135605	Jequitaí	-17.229	-44.4376	31	MG	false
3135704	Jequitibá	-19.2345	-44.0304	31	MG	false
3135803	Jequitinhonha	-16.4375	-41.0117	31	MG	false
2918100	Jeremoabo	-10.0685	-38.3471	29	BA	false
2507408	Jericó	-6.54577	-37.8036	25	PB	false
3525409	Jeriquara	-20.3116	-47.5918	35	SP	false
3203106	Jerônimo Monteiro	-20.7994	-41.3948	32	ES	false
2205300	Jerumenha	-7.09128	-43.5033	22	PI	false
3135902	Jesuânia	-21.9887	-45.2911	31	MG	false
4112751	Jesuítas	-24.3839	-53.3849	41	PR	false
5212055	Jesúpolis	-15.9484	-49.3739	52	GO	false
1100122	Ji-Paraná	-10.8777	-61.9322	11	RO	false
2307254	Jijoca de Jericoacoara	-2.79331	-40.5127	23	CE	false
2918209	Jiquiriçá	-13.2621	-39.5737	29	BA	false
2918308	Jitaúna	-14.0131	-39.8969	29	BA	false
4209003	Joaçaba	-27.1721	-51.5108	42	SC	false
3136009	Joaíma	-16.6522	-41.0229	31	MG	false
3136108	Joanésia	-19.1729	-42.6775	31	MG	false
3525508	Joanópolis	-22.927	-46.2741	35	SP	false
2608107	João Alfredo	-7.86565	-35.5787	26	PE	false
2405801	João Câmara	-5.54094	-35.8122	24	RN	false
2205359	João Costa	-8.50736	-42.4264	22	PI	false
2405900	João Dias	-6.27215	-37.7885	24	RN	false
2918357	João Dourado	-11.3486	-41.6548	29	BA	false
2105500	João Lisboa	-5.44363	-47.4064	21	MA	false
3136207	João Monlevade	-19.8126	-43.1735	31	MG	false
3203130	João Neiva	-19.7577	-40.386	32	ES	false
2507507	João Pessoa	-7.11509	-34.8641	25	PB	true 
3136306	João Pinheiro	-17.7398	-46.1715	31	MG	false
3525607	João Ramalho	-22.2473	-50.7694	35	SP	false
3136405	Joaquim Felício	-17.758	-44.1643	31	MG	false
2703809	Joaquim Gomes	-9.1328	-35.7474	27	AL	false
2608206	Joaquim Nabuco	-8.62281	-35.5288	26	PE	false
2205409	Joaquim Pires	-3.50164	-42.1865	22	PI	false
4112801	Joaquim Távora	-23.4987	-49.909	41	PR	false
2513653	Joca Claudino	-6.48362	-38.4764	25	PB	false
2205458	Joca Marques	-3.4804	-42.4255	22	PI	false
4311155	Jóia	-28.6435	-54.1141	43	RS	false
4209102	Joinville	-26.3045	-48.8487	42	SC	false
3136504	Jordânia	-15.9009	-40.1841	31	MG	false
1200328	Jordão	-9.43091	-71.8974	12	AC	false
4209151	José Boiteux	-26.9566	-49.6286	42	SC	false
3525706	José Bonifácio	-21.0551	-49.6892	35	SP	false
2406007	José da Penha	-6.31095	-38.2823	24	RN	false
2205508	José de Freitas	-4.75146	-42.5746	22	PI	false
3136520	José Gonçalves de Minas	-16.9053	-42.6014	31	MG	false
3136553	José Raydan	-18.2195	-42.4946	31	MG	false
2105609	Joselândia	-4.98611	-44.6958	21	MA	false
3136579	Josenópolis	-16.5417	-42.5151	31	MG	false
5212105	Joviânia	-17.802	-49.6197	52	GO	false
5105101	Juara	-11.2639	-57.5244	51	MT	false
2507606	Juarez Távora	-7.1713	-35.5686	25	PB	false
1711803	Juarina	-8.11951	-49.0643	17	TO	false
3136652	Juatuba	-19.9448	-44.3451	31	MG	false
2507705	Juazeirinho	-7.06092	-36.5793	25	PB	false
2918407	Juazeiro	-9.41622	-40.5033	29	BA	false
2307304	Juazeiro do Norte	-7.19621	-39.3076	23	CE	false
2205516	Juazeiro do Piauí	-5.17459	-41.6976	22	PI	false
2307403	Jucás	-6.51523	-39.5187	23	CE	false
2608255	Jucati	-8.70195	-36.4871	26	PE	false
2918456	Jucuruçu	-16.8488	-40.1641	29	BA	false
2406106	Jucurutu	-6.0306	-37.009	24	RN	false
5105150	Juína	-11.3728	-58.7483	51	MT	false
3136702	Juiz de Fora	-21.7595	-43.3398	31	MG	false
2205524	Júlio Borges	-10.3225	-44.2381	22	PI	false
4311205	Júlio de Castilhos	-29.2299	-53.6772	43	RS	false
3525805	Júlio Mesquita	-22.0112	-49.7873	35	SP	false
3525854	Jumirim	-23.0884	-47.7868	35	SP	false
2105658	Junco do Maranhão	-1.83888	-46.09	21	MA	false
2507804	Junco do Seridó	-6.99269	-36.7166	25	PB	false
2406155	Jundiá	-6.26866	-35.3495	24	RN	false
2703908	Jundiá	-8.93297	-35.5669	27	AL	false
3525904	Jundiaí	-23.1852	-46.8974	35	SP	false
4112900	Jundiaí do Sul	-23.4357	-50.2496	41	PR	false
2704005	Junqueiro	-9.90696	-36.4803	27	AL	false
3526001	Junqueirópolis	-21.5103	-51.4342	35	SP	false
2608305	Jupi	-8.70904	-36.4126	26	PE	false
4209177	Jupiá	-26.395	-52.7298	42	SC	false
3526100	Juquiá	-24.3101	-47.6426	35	SP	false
3526209	Juquitiba	-23.9244	-47.0653	35	SP	false
3136801	Juramento	-16.8473	-43.5865	31	MG	false
4112959	Juranda	-24.4209	-52.8413	41	PR	false
2608404	Jurema	-8.70714	-36.1347	26	PE	false
2205532	Jurema	-9.21992	-43.1337	22	PI	false
2507903	Juripiranga	-7.36176	-35.2321	25	PB	false
2508000	Juru	-7.52983	-37.815	25	PB	false
1302207	Juruá	-3.48438	-66.0718	13	AM	false
3136900	Juruaia	-21.2493	-46.5735	31	MG	false
5105176	Juruena	-10.3178	-58.3592	51	MT	false
1503903	Juruti	-2.16347	-56.0889	15	PA	false
5105200	Juscimeira	-16.0633	-54.8859	51	MT	false
2918506	Jussara	-11.0431	-41.9702	29	BA	false
5212204	Jussara	-15.8659	-50.8668	52	GO	false
4113007	Jussara	-23.6219	-52.4693	41	PR	false
2918555	Jussari	-15.192	-39.491	29	BA	false
2918605	Jussiape	-13.5155	-41.5882	29	BA	false
1302306	Jutaí	-2.75814	-66.7595	13	AM	false
5005152	Juti	-22.8596	-54.6061	50	MS	false
3136959	Juvenília	-14.2662	-44.1597	31	MG	false
4113106	Kaloré	-23.8188	-51.6687	41	PR	false
1302405	Lábrea	-7.26413	-64.7948	13	AM	false
4209201	Lacerdópolis	-27.2579	-51.5577	42	SC	false
3137007	Ladainha	-17.6279	-41.7488	31	MG	false
5005202	Ladário	-19.0089	-57.5973	50	MS	false
2918704	Lafaiete Coutinho	-13.6541	-40.2119	29	BA	false
3137106	Lagamar	-18.1759	-46.8063	31	MG	false
2803500	Lagarto	-10.9136	-37.6689	28	SE	false
4209300	Lages	-27.815	-50.3259	42	SC	false
2105708	Lago da Pedra	-4.56974	-45.1319	21	MA	false
2105807	Lago do Junco	-4.609	-45.049	21	MA	false
2105948	Lago dos Rodrigues	-4.61173	-44.9798	21	MA	false
2105906	Lago Verde	-3.94661	-44.826	21	MA	false
2508109	Lagoa	-6.58572	-37.9127	25	PB	false
2205557	Lagoa Alegre	-4.51539	-42.6309	22	PI	false
4311239	Lagoa Bonita do Sul	-29.4939	-53.017	43	RS	false
2406205	Lagoa d'Anta	-6.39493	-35.5949	24	RN	false
2704104	Lagoa da Canoa	-9.83291	-36.7413	27	AL	false
1711902	Lagoa da Confusão	-10.7906	-49.6199	17	TO	false
3137205	Lagoa da Prata	-20.0237	-45.5401	31	MG	false
2508208	Lagoa de Dentro	-6.67213	-35.3706	25	PB	false
2608503	Lagoa de Itaenga	-7.93005	-35.2874	26	PE	false
2406304	Lagoa de Pedras	-6.15082	-35.4299	24	RN	false
2205573	Lagoa de São Francisco	-4.38505	-41.5969	22	PI	false
2406403	Lagoa de Velhos	-6.0119	-35.8729	24	RN	false
2205565	Lagoa do Barro do Piauí	-8.47673	-41.5342	22	PI	false
2608453	Lagoa do Carro	-7.84383	-35.3108	26	PE	false
2105922	Lagoa do Mato	-6.05023	-43.5333	21	MA	false
2608602	Lagoa do Ouro	-9.12567	-36.4584	26	PE	false
2205581	Lagoa do Piauí	-5.41864	-42.6437	22	PI	false
2205599	Lagoa do Sítio	-6.50766	-41.5653	22	PI	false
1711951	Lagoa do Tocantins	-10.368	-47.538	17	TO	false
2608701	Lagoa dos Gatos	-8.6602	-35.904	26	PE	false
3137304	Lagoa dos Patos	-16.978	-44.5754	31	MG	false
4311270	Lagoa dos Três Cantos	-28.5676	-52.8618	43	RS	false
3137403	Lagoa Dourada	-20.9139	-44.0797	31	MG	false
3137502	Lagoa Formosa	-18.7715	-46.4012	31	MG	false
3137536	Lagoa Grande	-17.8323	-46.5165	31	MG	false
2608750	Lagoa Grande	-8.99452	-40.2767	26	PE	false
2105963	Lagoa Grande do Maranhão	-4.98893	-45.3816	21	MA	false
2406502	Lagoa Nova	-6.09339	-36.4703	24	RN	false
2918753	Lagoa Real	-14.0334	-42.1328	29	BA	false
2406601	Lagoa Salgada	-6.12295	-35.4724	24	RN	false
5212253	Lagoa Santa	-19.1832	-51.3998	52	GO	false
3137601	Lagoa Santa	-19.6397	-43.8932	31	MG	false
2508307	Lagoa Seca	-7.15535	-35.8491	25	PB	false
4311304	Lagoa Vermelha	-28.2093	-51.5248	43	RS	false
4311254	Lagoão	-29.2348	-52.7997	43	RS	false
3526308	Lagoinha	-23.0846	-45.1944	35	SP	false
2205540	Lagoinha do Piauí	-5.83074	-42.6223	22	PI	false
4209409	Laguna	-28.4843	-48.7772	42	SC	false
5005251	Laguna Carapã	-22.5448	-55.1502	50	MS	false
2918803	Laje	-13.1673	-39.4213	29	BA	false
3302304	Laje do Muriaé	-21.2091	-42.1271	33	RJ	false
1712009	Lajeado	-9.74996	-48.3565	17	TO	false
4311403	Lajeado	-29.4591	-51.9644	43	RS	false
4311429	Lajeado do Bugre	-27.6913	-53.1818	43	RS	false
4209458	Lajeado Grande	-26.8576	-52.5648	42	SC	false
2105989	Lajeado Novo	-6.18539	-47.0293	21	MA	false
2918902	Lajedão	-17.6056	-40.3383	29	BA	false
2919009	Lajedinho	-12.3529	-40.9048	29	BA	false
2608800	Lajedo	-8.65791	-36.3293	26	PE	false
2919058	Lajedo do Tabocal	-13.4663	-40.2204	29	BA	false
2406700	Lajes	-5.69322	-36.247	24	RN	false
2406809	Lajes Pintadas	-6.14943	-36.1171	24	RN	false
3137700	Lajinha	-20.1539	-41.6228	31	MG	false
2919108	Lamarão	-11.773	-38.887	29	BA	false
3137809	Lambari	-21.9671	-45.3498	31	MG	false
5105234	Lambari D'Oeste	-15.3188	-58.0046	51	MT	false
3137908	Lamim	-20.79	-43.4706	31	MG	false
2205607	Landri Sales	-7.25922	-43.9364	22	PI	false
4113205	Lapa	-25.7671	-49.7168	41	PR	false
2919157	Lapão	-11.3851	-41.8286	29	BA	false
3203163	Laranja da Terra	-19.8994	-41.0621	32	ES	false
3138005	Laranjal	-21.3715	-42.4732	31	MG	false
4113254	Laranjal	-24.8862	-52.47	41	PR	false
1600279	Laranjal do Jari	-0.804911	-52.453	16	AP	false
3526407	Laranjal Paulista	-23.0506	-47.8375	35	SP	false
2803609	Laranjeiras	-10.7981	-37.1731	28	SE	false
4113304	Laranjeiras do Sul	-25.4077	-52.4109	41	PR	false
3138104	Lassance	-17.887	-44.5735	31	MG	false
2508406	Lastro	-6.50603	-38.1742	25	PB	false
4209508	Laurentino	-27.2173	-49.7331	42	SC	false
2919207	Lauro de Freitas	-12.8978	-38.321	29	BA	false
4209607	Lauro Muller	-28.3859	-49.4035	42	SC	false
1712157	Lavandeira	-12.7847	-46.5099	17	TO	false
3526506	Lavínia	-21.1639	-51.0412	35	SP	false
3138203	Lavras	-21.248	-45.0009	31	MG	false
2307502	Lavras da Mangabeira	-6.7448	-38.9706	23	CE	false
4311502	Lavras do Sul	-30.8071	-53.8931	43	RS	false
3526605	Lavrinhas	-22.57	-44.9024	35	SP	false
3138302	Leandro Ferreira	-19.7193	-45.0279	31	MG	false
4209706	Lebon Régis	-26.928	-50.6921	42	SC	false
3526704	Leme	-22.1809	-47.3841	35	SP	false
3138351	Leme do Prado	-17.0793	-42.6936	31	MG	false
2919306	Lençóis	-12.5616	-41.3928	29	BA	false
3526803	Lençóis Paulista	-22.6027	-48.8037	35	SP	false
4209805	Leoberto Leal	-27.5081	-49.2789	42	SC	false
3138401	Leopoldina	-21.5296	-42.6421	31	MG	false
5212303	Leopoldo de Bulhões	-16.619	-48.7428	52	GO	false
4113403	Leópolis	-23.0818	-50.7511	41	PR	false
4311601	Liberato Salzano	-27.601	-53.0753	43	RS	false
3138500	Liberdade	-22.0275	-44.3208	31	MG	false
2919405	Licínio de Almeida	-14.6842	-42.5095	29	BA	false
4113429	Lidianópolis	-24.11	-51.6506	41	PR	false
2106003	Lima Campos	-4.51837	-44.4646	21	MA	false
3138609	Lima Duarte	-21.8386	-43.7934	31	MG	false
3526902	Limeira	-22.566	-47.397	35	SP	false
3138625	Limeira do Oeste	-19.5512	-50.5815	31	MG	false
2608909	Limoeiro	-7.8726	-35.4402	26	PE	false
2704203	Limoeiro de Anadia	-9.74098	-36.5121	27	AL	false
1504000	Limoeiro do Ajuru	-1.8985	-49.3903	15	PA	false
2307601	Limoeiro do Norte	-5.14392	-38.0847	23	CE	false
4113452	Lindoeste	-25.2596	-53.5733	41	PR	false
3527009	Lindóia	-22.5226	-46.65	35	SP	false
4209854	Lindóia do Sul	-27.0545	-52.069	42	SC	false
4311627	Lindolfo Collor	-29.5859	-51.2141	43	RS	false
4311643	Linha Nova	-29.4679	-51.2003	43	RS	false
3203205	Linhares	-19.3946	-40.0643	32	ES	false
3527108	Lins	-21.6718	-49.7526	35	SP	false
2508505	Livramento	-7.37113	-36.9491	25	PB	false
2919504	Livramento de Nossa Senhora	-13.6369	-41.8432	29	BA	false
1712405	Lizarda	-9.59002	-46.6738	17	TO	false
4113502	Loanda	-22.9232	-53.1362	41	PR	false
4113601	Lobato	-23.0058	-51.9524	41	PR	false
2508554	Logradouro	-6.61191	-35.4384	25	PB	false
4113700	Londrina	-23.304	-51.1691	41	PR	false
3138658	Lontra	-15.9013	-44.306	31	MG	false
4209904	Lontras	-27.1684	-49.535	42	SC	false
3527207	Lorena	-22.7334	-45.1197	35	SP	false
2106102	Loreto	-7.08111	-45.1451	21	MA	false
3527256	Lourdes	-20.966	-50.2263	35	SP	false
3527306	Louveira	-23.0856	-46.9484	35	SP	false
5105259	Lucas do Rio Verde	-13.0588	-55.9042	51	MT	false
3527405	Lucélia	-21.7182	-51.0215	35	SP	false
2508604	Lucena	-6.90258	-34.8748	25	PB	false
3527504	Lucianópolis	-22.4294	-49.522	35	SP	false
5105309	Luciara	-11.2219	-50.6676	51	MT	false
2406908	Lucrécia	-6.10525	-37.8134	24	RN	false
3527603	Luís Antônio	-21.55	-47.7801	35	SP	false
2205706	Luís Correia	-2.88438	-41.6641	22	PI	false
2106201	Luís Domingues	-1.27492	-45.867	21	MA	false
2919553	Luís Eduardo Magalhães	-12.0956	-45.7866	29	BA	false
2407005	Luís Gomes	-6.40588	-38.3899	24	RN	false
3138674	Luisburgo	-20.4468	-42.0976	31	MG	false
3138682	Luislândia	-16.1095	-44.5886	31	MG	false
4210001	Luiz Alves	-26.7151	-48.9322	42	SC	false
4113734	Luiziana	-24.2853	-52.269	41	PR	false
3527702	Luiziânia	-21.6737	-50.3294	35	SP	false
3138708	Luminárias	-21.5145	-44.9034	31	MG	false
4113759	Lunardelli	-24.0821	-51.7368	41	PR	false
3527801	Lupércio	-22.4146	-49.818	35	SP	false
4113809	Lupionópolis	-22.755	-51.6601	41	PR	false
3527900	Lutécia	-22.3384	-50.394	35	SP	false
3138807	Luz	-19.7911	-45.6794	31	MG	false
4210035	Luzerna	-27.1304	-51.4682	42	SC	false
5212501	Luziânia	-16.253	-47.95	52	GO	false
2205805	Luzilândia	-3.4683	-42.3718	22	PI	false
1712454	Luzinópolis	-6.17794	-47.8582	17	TO	false
3302403	Macaé	-22.3768	-41.7848	33	RJ	false
2407104	Macaíba	-5.85229	-35.3552	24	RN	false
2919603	Macajuba	-12.1326	-40.3571	29	BA	false
4311718	Maçambará	-29.1445	-56.0674	43	RS	false
2803708	Macambira	-10.6619	-37.5413	28	SE	false
1600303	Macapá	0.034934	-51.0694	16	AP	true 
2609006	Macaparana	-7.55564	-35.4425	26	PE	false
2919702	Macarani	-15.5646	-40.4209	29	BA	false
3528007	Macatuba	-22.5002	-48.7102	35	SP	false
2407203	Macau	-5.10795	-36.6318	24	RN	false
3528106	Macaubal	-20.8022	-49.9687	35	SP	false
2919801	Macaúbas	-13.0186	-42.6945	29	BA	false
3528205	Macedônia	-20.1444	-50.1973	35	SP	false
2704302	Maceió	-9.66599	-35.735	27	AL	true 
3138906	Machacalis	-17.0723	-40.7245	31	MG	false
4311700	Machadinho	-27.5667	-51.6668	43	RS	false
1100130	Machadinho D'Oeste	-9.44363	-61.9818	11	RO	false
3139003	Machado	-21.6778	-45.9219	31	MG	false
2609105	Machados	-7.68827	-35.5114	26	PE	false
4210050	Macieira	-26.8552	-51.3705	42	SC	false
3302452	Macuco	-21.9813	-42.2533	33	RJ	false
2919900	Macururé	-9.16226	-39.0518	29	BA	false
2307635	Madalena	-4.84601	-39.5725	23	CE	false
2205854	Madeiro	-3.48624	-42.4981	22	PI	false
2919926	Madre de Deus	-12.7446	-38.6153	29	BA	false
3139102	Madre de Deus de Minas	-21.483	-44.3287	31	MG	false
2508703	Mãe d'Água	-7.25201	-37.4322	25	PB	false
1504059	Mãe do Rio	-2.05683	-47.5601	15	PA	false
2919959	Maetinga	-14.6623	-41.4915	29	BA	false
4210100	Mafra	-26.1159	-49.8086	42	SC	false
1504109	Magalhães Barata	-0.803391	-47.6014	15	PA	false
2106300	Magalhães de Almeida	-3.39232	-42.2117	21	MA	false
3528304	Magda	-20.6445	-50.2305	35	SP	false
3302502	Magé	-22.6632	-43.0315	33	RJ	false
2920007	Maiquinique	-15.624	-40.2587	29	BA	false
2920106	Mairi	-11.7107	-40.1437	29	BA	false
3528403	Mairinque	-23.5398	-47.185	35	SP	false
3528502	Mairiporã	-23.3171	-46.5897	35	SP	false
5212600	Mairipotaba	-17.2975	-49.4898	52	GO	false
4210209	Major Gercino	-27.4192	-48.9488	42	SC	false
2704401	Major Isidoro	-9.53009	-36.992	27	AL	false
2407252	Major Sales	-6.39949	-38.324	24	RN	false
4210308	Major Vieira	-26.3709	-50.3266	42	SC	false
3139201	Malacacheta	-17.8456	-42.0769	31	MG	false
2920205	Malhada	-14.3371	-43.7686	29	BA	false
2920304	Malhada de Pedras	-14.3847	-41.8842	29	BA	false
2803807	Malhada dos Bois	-10.3418	-36.9252	28	SE	false
2803906	Malhador	-10.6649	-37.3004	28	SE	false
4113908	Mallet	-25.8806	-50.8173	41	PR	false
2508802	Malta	-6.89719	-37.5221	25	PB	false
2508901	Mamanguape	-6.8337	-35.1213	25	PB	false
5212709	Mambaí	-14.4823	-46.1165	52	GO	false
4114005	Mamborê	-24.317	-52.5271	41	PR	false
3139250	Mamonas	-15.0479	-42.9469	31	MG	false
4311734	Mampituba	-29.2136	-49.9311	43	RS	false
1302504	Manacapuru	-3.29066	-60.6216	13	AM	false
2509008	Manaíra	-7.70331	-38.1523	25	PB	false
1302553	Manaquiri	-3.44078	-60.4612	13	AM	false
2609154	Manari	-8.9649	-37.6313	26	PE	false
1302603	Manaus	-3.11866	-60.0212	13	AM	true 
1200336	Mâncio Lima	-7.61657	-72.8997	12	AC	false
4114104	Mandaguaçu	-23.3458	-52.0944	41	PR	false
4114203	Mandaguari	-23.5446	-51.671	41	PR	false
4114302	Mandirituba	-25.777	-49.3282	41	PR	false
3528601	Manduri	-23.0056	-49.3202	35	SP	false
4114351	Manfrinópolis	-26.1441	-53.3113	41	PR	false
3139300	Manga	-14.7529	-43.9391	31	MG	false
3302601	Mangaratiba	-22.9594	-44.0409	33	RJ	false
4114401	Mangueirinha	-25.9421	-52.1743	41	PR	false
3139409	Manhuaçu	-20.2572	-42.028	31	MG	false
3139508	Manhumirim	-20.3591	-41.9589	31	MG	false
1302702	Manicoré	-5.80462	-61.2895	13	AM	false
2205904	Manoel Emídio	-8.01234	-43.8755	22	PI	false
4114500	Manoel Ribas	-24.5144	-51.6658	41	PR	false
1200344	Manoel Urbano	-8.83291	-69.2679	12	AC	false
4311759	Manoel Viana	-29.5859	-55.4841	43	RS	false
2920403	Manoel Vitorino	-14.1476	-40.2399	29	BA	false
2920452	Mansidão	-10.7227	-44.0428	29	BA	false
3139607	Mantena	-18.7761	-40.9874	31	MG	false
3203304	Mantenópolis	-18.8594	-41.124	32	ES	false
4311775	Maquiné	-29.6798	-50.2079	43	RS	false
3139805	Mar de Espanha	-21.8707	-43.0062	31	MG	false
2704906	Mar Vermelho	-9.44739	-36.3881	27	AL	false
5212808	Mara Rosa	-14.0148	-49.1777	52	GO	false
1302801	Maraã	-1.85313	-65.573	13	AM	false
1504208	Marabá	-5.38075	-49.1327	15	PA	false
3528700	Marabá Paulista	-22.1068	-51.9617	35	SP	false
2106326	Maracaçumé	-2.04918	-45.9587	21	MA	false
3528809	Maracaí	-22.6149	-50.6713	35	SP	false
4210407	Maracajá	-28.8463	-49.4605	42	SC	false
5005400	Maracaju	-21.6105	-55.1678	50	MS	false
1504307	Maracanã	-0.778899	-47.452	15	PA	false
2307650	Maracanaú	-3.86699	-38.6259	23	CE	false
2920502	Maracás	-13.4355	-40.4323	29	BA	false
2704500	Maragogi	-9.00744	-35.2267	27	AL	false
2920601	Maragogipe	-12.776	-38.9175	29	BA	false
2609204	Maraial	-8.79062	-35.8266	26	PE	false
2106359	Marajá do Sena	-4.62806	-45.4531	21	MA	false
2307700	Maranguape	-3.89143	-38.6829	23	CE	false
2106375	Maranhãozinho	-2.24078	-45.8507	21	MA	false
1504406	Marapanim	-0.714702	-47.7034	15	PA	false
3528858	Marapoama	-21.2587	-49.13	35	SP	false
4311791	Maratá	-29.5457	-51.5573	43	RS	false
3203320	Marataízes	-21.0398	-40.8384	32	ES	false
4311809	Marau	-28.4498	-52.1986	43	RS	false
2920700	Maraú	-14.1035	-39.0137	29	BA	false
2704609	Maravilha	-9.23045	-37.3524	27	AL	false
4210506	Maravilha	-26.7665	-53.1737	42	SC	false
3139706	Maravilhas	-19.5076	-44.6779	31	MG	false
2509057	Marcação	-6.76535	-35.0087	25	PB	false
5105580	Marcelândia	-11.0463	-54.4377	51	MT	false
4311908	Marcelino Ramos	-27.4676	-51.9095	43	RS	false
2407302	Marcelino Vieira	-6.2846	-38.1642	24	RN	false
2920809	Marcionílio Souza	-13.0064	-40.5295	29	BA	false
2307809	Marco	-3.1285	-40.1582	23	CE	false
2205953	Marcolândia	-7.44169	-40.6602	22	PI	false
2206001	Marcos Parente	-7.11565	-43.8926	22	PI	false
4114609	Marechal Cândido Rondon	-24.557	-54.0571	41	PR	false
2704708	Marechal Deodoro	-9.70971	-35.8967	27	AL	false
3203346	Marechal Floriano	-20.4159	-40.67	32	ES	false
1200351	Marechal Thaumaturgo	-8.93898	-72.7997	12	AC	false
4210555	Marema	-26.8024	-52.6264	42	SC	false
2509107	Mari	-7.05942	-35.318	25	PB	false
3139904	Maria da Fé	-22.3044	-45.3773	31	MG	false
4114708	Maria Helena	-23.6158	-53.2053	41	PR	false
4114807	Marialva	-23.4843	-51.7928	41	PR	false
3140001	Mariana	-20.3765	-43.414	31	MG	false
4311981	Mariana Pimentel	-30.353	-51.5803	43	RS	false
4312005	Mariano Moro	-27.3568	-52.1467	43	RS	false
1712504	Marianópolis do Tocantins	-9.79377	-49.6553	17	TO	false
3528908	Mariápolis	-21.7959	-51.1824	35	SP	false
2704807	Maribondo	-9.58353	-36.3045	27	AL	false
3302700	Maricá	-22.9354	-42.8246	33	RJ	false
3140100	Marilac	-18.5079	-42.0822	31	MG	false
3203353	Marilândia	-19.4114	-40.5456	32	ES	false
4114906	Marilândia do Sul	-23.7425	-51.3137	41	PR	false
4115002	Marilena	-22.7336	-53.0402	41	PR	false
3529005	Marília	-22.2171	-49.9501	35	SP	false
4115101	Mariluz	-24.0089	-53.1432	41	PR	false
4115200	Maringá	-23.4205	-51.9333	41	PR	false
3529104	Marinópolis	-20.4389	-50.8254	35	SP	false
3140159	Mário Campos	-20.0582	-44.1883	31	MG	false
4115309	Mariópolis	-26.355	-52.5532	41	PR	false
4115358	Maripá	-24.42	-53.8286	41	PR	false
3140209	Maripá de Minas	-21.6979	-42.9546	31	MG	false
1504422	Marituba	-1.36002	-48.3421	15	PA	false
2509156	Marizópolis	-6.82748	-38.3528	25	PB	false
3140308	Marliéria	-19.7096	-42.7327	31	MG	false
4115408	Marmeleiro	-26.1472	-53.0267	41	PR	false
3140407	Marmelópolis	-22.447	-45.1645	31	MG	false
4312054	Marques de Souza	-29.3311	-52.0973	43	RS	false
4115457	Marquinho	-25.112	-52.2497	41	PR	false
3140506	Martinho Campos	-19.3306	-45.2434	31	MG	false
2307908	Martinópole	-3.2252	-40.6896	23	CE	false
3529203	Martinópolis	-22.1462	-51.1709	35	SP	false
2407401	Martins	-6.08279	-37.908	24	RN	false
3140530	Martins Soares	-20.2546	-41.8786	31	MG	false
2804003	Maruim	-10.7308	-37.0856	28	SE	false
4115507	Marumbi	-23.7058	-51.6404	41	PR	false
5212907	Marzagão	-17.983	-48.6415	52	GO	false
2920908	Mascote	-15.5542	-39.3016	29	BA	false
2308005	Massapê	-3.52364	-40.3423	23	CE	false
2206050	Massapê do Piauí	-7.47469	-41.1103	22	PI	false
2509206	Massaranduba	-7.18995	-35.7848	25	PB	false
4210605	Massaranduba	-26.6109	-49.0054	42	SC	false
4312104	Mata	-29.5649	-54.4641	43	RS	false
2921005	Mata de São João	-12.5307	-38.3009	29	BA	false
2705002	Mata Grande	-9.11824	-37.7323	27	AL	false
2106409	Mata Roma	-3.62035	-43.1112	21	MA	false
3140555	Mata Verde	-15.6869	-40.7366	31	MG	false
3529302	Matão	-21.6025	-48.364	35	SP	false
2509305	Mataraca	-6.59673	-35.0531	25	PB	false
1712702	Mateiros	-10.5464	-46.4168	17	TO	false
4115606	Matelândia	-25.2496	-53.9935	41	PR	false
3140605	Materlândia	-18.4699	-43.0579	31	MG	false
3140704	Mateus Leme	-19.9794	-44.4318	31	MG	false
3171501	Mathias Lobato	-18.59	-41.9166	31	MG	false
3140803	Matias Barbosa	-21.869	-43.3135	31	MG	false
3140852	Matias Cardoso	-14.8563	-43.9146	31	MG	false
2206100	Matias Olímpio	-3.71492	-42.5507	22	PI	false
2921054	Matina	-13.9109	-42.8439	29	BA	false
2106508	Matinha	-3.09849	-45.035	21	MA	false
2509339	Matinhas	-7.12486	-35.7669	25	PB	false
4115705	Matinhos	-25.8237	-48.549	41	PR	false
3140902	Matipó	-20.2873	-42.3401	31	MG	false
4312138	Mato Castelhano	-28.28	-52.1932	43	RS	false
2509370	Mato Grosso	-6.54018	-37.7279	25	PB	false
4312153	Mato Leitão	-29.5285	-52.1278	43	RS	false
4312179	Mato Queimado	-28.252	-54.6159	43	RS	false
4115739	Mato Rico	-24.6995	-52.1454	41	PR	false
3141009	Mato Verde	-15.3944	-42.86	31	MG	false
2106607	Matões	-5.51359	-43.2018	21	MA	false
2106631	Matões do Norte	-3.6244	-44.5468	21	MA	false
4210704	Matos Costa	-26.4709	-51.1501	42	SC	false
3141108	Matozinhos	-19.5543	-44.0868	31	MG	false
5212956	Matrinchã	-15.4342	-50.7456	52	GO	false
2705101	Matriz de Camaragibe	-9.15437	-35.5243	27	AL	false
5105606	Matupá	-10.1821	-54.9467	51	MT	false
2509396	Maturéia	-7.26188	-37.351	25	PB	false
3141207	Matutina	-19.2179	-45.9664	31	MG	false
3529401	Mauá	-23.6677	-46.4613	35	SP	false
4115754	Mauá da Serra	-23.8988	-51.2277	41	PR	false
1302900	Maués	-3.39289	-57.7067	13	AM	false
5213004	Maurilândia	-17.9719	-50.3388	52	GO	false
1712801	Maurilândia do Tocantins	-5.95169	-47.5125	17	TO	false
2308104	Mauriti	-7.38597	-38.7708	23	CE	false
2407500	Maxaranguape	-5.52181	-35.2631	24	RN	false
4312203	Maximiliano de Almeida	-27.6325	-51.802	43	RS	false
1600402	Mazagão	-0.11336	-51.2891	16	AP	false
3141306	Medeiros	-19.9865	-46.2181	31	MG	false
2921104	Medeiros Neto	-17.3707	-40.2238	29	BA	false
4115804	Medianeira	-25.2977	-54.0943	41	PR	false
1504455	Medicilândia	-3.44637	-52.8875	15	PA	false
3141405	Medina	-16.2245	-41.4728	31	MG	false
4210803	Meleiro	-28.8244	-49.6378	42	SC	false
1504505	Melgaço	-1.8032	-50.7149	15	PA	false
3302809	Mendes	-22.5245	-43.7312	33	RJ	false
3141504	Mendes Pimentel	-18.6631	-41.4052	31	MG	false
3529500	Mendonça	-21.1757	-49.5791	35	SP	false
4115853	Mercedes	-24.4538	-54.1618	41	PR	false
3141603	Mercês	-21.1976	-43.3337	31	MG	false
3529609	Meridiano	-20.3579	-50.1811	35	SP	false
2308203	Meruoca	-3.53974	-40.4531	23	CE	false
3529658	Mesópolis	-19.9684	-50.6326	35	SP	false
3302858	Mesquita	-22.8028	-43.4601	33	RJ	false
3141702	Mesquita	-19.224	-42.6079	31	MG	false
2705200	Messias	-9.39384	-35.8392	27	AL	false
2407609	Messias Targino	-6.07194	-37.5158	24	RN	false
2206209	Miguel Alves	-4.16857	-42.8963	22	PI	false
2921203	Miguel Calmon	-11.4299	-40.6031	29	BA	false
2206308	Miguel Leão	-5.68077	-42.7436	22	PI	false
3302908	Miguel Pereira	-22.4572	-43.4803	33	RJ	false
3529708	Miguelópolis	-20.1796	-48.031	35	SP	false
2308302	Milagres	-7.29749	-38.9378	23	CE	false
2921302	Milagres	-12.8646	-39.8611	29	BA	false
2106672	Milagres do Maranhão	-3.57443	-42.6131	21	MA	false
2308351	Milhã	-5.67252	-39.1875	23	CE	false
2206357	Milton Brandão	-4.68295	-41.4173	22	PI	false
5213053	Mimoso de Goiás	-15.0515	-48.1611	52	GO	false
3203403	Mimoso do Sul	-21.0628	-41.3615	32	ES	false
5213087	Minaçu	-13.5304	-48.2206	52	GO	false
2705309	Minador do Negrão	-9.31236	-36.8696	27	AL	false
4312252	Minas do Leão	-30.1346	-52.0423	43	RS	false
3141801	Minas Novas	-17.2156	-42.5884	31	MG	false
3141900	Minduri	-21.6797	-44.6051	31	MG	false
5213103	Mineiros	-17.5654	-52.5537	52	GO	false
3529807	Mineiros do Tietê	-22.412	-48.451	35	SP	false
1101203	Ministro Andreazza	-11.196	-61.5174	11	RO	false
3530003	Mira Estrela	-19.9789	-50.139	35	SP	false
3142007	Mirabela	-16.256	-44.1602	31	MG	false
3529906	Miracatu	-24.2766	-47.4625	35	SP	false
3303005	Miracema	-21.4148	-42.1938	33	RJ	false
1713205	Miracema do Tocantins	-9.56556	-48.393	17	TO	false
2106706	Mirador	-6.37454	-44.3683	21	MA	false
4115903	Mirador	-23.255	-52.7761	41	PR	false
3142106	Miradouro	-20.8899	-42.3458	31	MG	false
4312302	Miraguaí	-27.497	-53.6891	43	RS	false
3142205	Miraí	-21.2021	-42.6122	31	MG	false
2308377	Miraíma	-3.56867	-39.9663	23	CE	false
5005608	Miranda	-20.2355	-56.3746	50	MS	false
2106755	Miranda do Norte	-3.56313	-44.5814	21	MA	false
2609303	Mirandiba	-8.12113	-38.7388	26	PE	false
3530102	Mirandópolis	-21.1313	-51.1035	35	SP	false
2921401	Mirangaba	-10.961	-40.574	29	BA	false
1713304	Miranorte	-9.52907	-48.5922	17	TO	false
2921450	Mirante	-14.2385	-40.7718	29	BA	false
1101302	Mirante da Serra	-11.029	-62.6696	11	RO	false
3530201	Mirante do Paranapanema	-22.2904	-51.9084	35	SP	false
4116000	Miraselva	-22.9657	-51.4846	41	PR	false
3530300	Mirassol	-20.8169	-49.5206	35	SP	false
5105622	Mirassol d'Oeste	-15.6759	-58.0951	51	MT	false
3530409	Mirassolândia	-20.6179	-49.4617	35	SP	false
3142254	Miravânia	-14.7348	-44.4092	31	MG	false
4210852	Mirim Doce	-27.197	-50.0786	42	SC	false
2106805	Mirinzal	-2.07094	-44.7787	21	MA	false
4116059	Missal	-25.0919	-54.2477	41	PR	false
2308401	Missão Velha	-7.23522	-39.143	23	CE	false
1504604	Mocajuba	-2.5831	-49.5042	15	PA	false
3530508	Mococa	-21.4647	-47.0024	35	SP	false
4210902	Modelo	-26.7729	-53.04	42	SC	false
3142304	Moeda	-20.3399	-44.0509	31	MG	false
3142403	Moema	-19.8387	-45.4127	31	MG	false
2509404	Mogeiro	-7.28517	-35.4832	25	PB	false
3530607	Mogi das Cruzes	-23.5208	-46.1854	35	SP	false
3530706	Mogi Guaçu	-22.3675	-46.9428	35	SP	false
3530805	Mogi Mirim	-22.4332	-46.9532	35	SP	false
5213400	Moiporá	-16.5434	-50.739	52	GO	false
2804102	Moita Bonita	-10.5769	-37.3512	28	SE	false
1504703	Moju	-1.88993	-48.7668	15	PA	false
1504752	Mojuí dos Campos	-2.6822	-54.6425	15	PA	false
2308500	Mombaça	-5.73844	-39.63	23	CE	false
3530904	Mombuca	-22.9285	-47.559	35	SP	false
2106904	Monção	-3.48125	-45.2496	21	MA	false
3531001	Monções	-20.8509	-50.0975	35	SP	false
4211009	Mondaí	-27.1008	-53.4032	42	SC	false
3531100	Mongaguá	-24.0809	-46.6265	35	SP	false
3142502	Monjolos	-18.3245	-44.118	31	MG	false
2206407	Monsenhor Gil	-5.562	-42.6075	22	PI	false
2206506	Monsenhor Hipólito	-6.99275	-41.026	22	PI	false
3142601	Monsenhor Paulo	-21.7579	-45.5391	31	MG	false
2308609	Monsenhor Tabosa	-4.79102	-40.0646	23	CE	false
2509503	Montadas	-7.08848	-35.9592	25	PB	false
3142700	Montalvânia	-14.4197	-44.3719	31	MG	false
3203502	Montanha	-18.1303	-40.3668	32	ES	false
2407708	Montanhas	-6.48522	-35.2842	24	RN	false
4312351	Montauri	-28.6462	-52.0767	43	RS	false
1504802	Monte Alegre	-1.99768	-54.0724	15	PA	false
2407807	Monte Alegre	-6.07063	-35.3253	24	RN	false
5213509	Monte Alegre de Goiás	-13.2552	-46.8928	52	GO	false
3142809	Monte Alegre de Minas	-18.869	-48.881	31	MG	false
2804201	Monte Alegre de Sergipe	-10.0256	-37.5616	28	SE	false
2206605	Monte Alegre do Piauí	-9.75364	-45.3037	22	PI	false
3531209	Monte Alegre do Sul	-22.6817	-46.681	35	SP	false
4312377	Monte Alegre dos Campos	-28.6805	-50.7834	43	RS	false
3531308	Monte Alto	-21.2655	-48.4971	35	SP	false
3531407	Monte Aprazível	-20.768	-49.7184	35	SP	false
3142908	Monte Azul	-15.1514	-42.8718	31	MG	false
3531506	Monte Azul Paulista	-20.9065	-48.6387	35	SP	false
3143005	Monte Belo	-21.3271	-46.3635	31	MG	false
4312385	Monte Belo do Sul	-29.1607	-51.6333	43	RS	false
4211058	Monte Carlo	-27.2239	-50.9808	42	SC	false
3143104	Monte Carmelo	-18.7302	-47.4912	31	MG	false
4211108	Monte Castelo	-26.461	-50.2327	42	SC	false
3531605	Monte Castelo	-21.2981	-51.5679	35	SP	false
2407906	Monte das Gameleiras	-6.43698	-35.7831	24	RN	false
1713601	Monte do Carmo	-10.7611	-48.1114	17	TO	false
3143153	Monte Formoso	-16.8691	-41.2473	31	MG	false
2509602	Monte Horebe	-7.20402	-38.5838	25	PB	false
3531803	Monte Mor	-22.945	-47.3122	35	SP	false
1101401	Monte Negro	-10.2458	-63.29	11	RO	false
2921500	Monte Santo	-10.4374	-39.3321	29	BA	false
3143203	Monte Santo de Minas	-21.1873	-46.9753	31	MG	false
1713700	Monte Santo do Tocantins	-10.0075	-48.9941	17	TO	false
3143401	Monte Sião	-22.4335	-46.573	31	MG	false
2509701	Monteiro	-7.88363	-37.1184	25	PB	false
3531704	Monteiro Lobato	-22.9544	-45.8407	35	SP	false
2705408	Monteirópolis	-9.60357	-37.2505	27	AL	false
4312401	Montenegro	-29.6824	-51.4679	43	RS	false
2107001	Montes Altos	-5.83067	-47.0673	21	MA	false
3143302	Montes Claros	-16.7282	-43.8578	31	MG	false
5213707	Montes Claros de Goiás	-16.0059	-51.3979	52	GO	false
3143450	Montezuma	-15.1702	-42.4941	31	MG	false
5213756	Montividiu	-17.4439	-51.1728	52	GO	false
5213772	Montividiu do Norte	-13.3485	-48.6853	52	GO	false
2308708	Morada Nova	-5.09736	-38.3702	23	CE	false
3143500	Morada Nova de Minas	-18.5998	-45.3584	31	MG	false
2308807	Moraújo	-3.46311	-40.6776	23	CE	false
2614303	Moreilândia	-7.61931	-39.546	26	PE	false
4116109	Moreira Sales	-24.0509	-53.0102	41	PR	false
2609402	Moreno	-8.10871	-35.0835	26	PE	false
4312427	Mormaço	-28.6968	-52.6999	43	RS	false
2921609	Morpará	-11.5569	-43.2766	29	BA	false
4116208	Morretes	-25.4744	-48.8345	41	PR	false
5213806	Morrinhos	-17.7334	-49.1059	52	GO	false
2308906	Morrinhos	-3.23426	-40.1233	23	CE	false
4312443	Morrinhos do Sul	-29.3578	-49.9328	43	RS	false
3531902	Morro Agudo	-20.7288	-48.0581	35	SP	false
5213855	Morro Agudo de Goiás	-15.3184	-50.0553	52	GO	false
2206654	Morro Cabeça no Tempo	-9.71891	-43.9072	22	PI	false
4211207	Morro da Fumaça	-28.6511	-49.2169	42	SC	false
3143609	Morro da Garça	-18.5356	-44.601	31	MG	false
2921708	Morro do Chapéu	-11.5488	-41.1565	29	BA	false
2206670	Morro do Chapéu do Piauí	-3.73337	-42.3024	22	PI	false
3143708	Morro do Pilar	-19.2236	-43.3795	31	MG	false
4211256	Morro Grande	-28.8006	-49.7214	42	SC	false
4312450	Morro Redondo	-31.5887	-52.6261	43	RS	false
4312476	Morro Reuter	-29.5379	-51.0811	43	RS	false
2107100	Morros	-2.85379	-44.0357	21	MA	false
2921807	Mortugaba	-15.0225	-42.3727	29	BA	false
3532009	Morungaba	-22.8811	-46.7896	35	SP	false
5213905	Mossâmedes	-16.124	-50.2136	52	GO	false
2408003	Mossoró	-5.18374	-37.3474	24	RN	false
4312500	Mostardas	-31.1054	-50.9167	43	RS	false
3532058	Motuca	-21.5103	-48.1538	35	SP	false
5214002	Mozarlândia	-14.7457	-50.5713	52	GO	false
1504901	Muaná	-1.53936	-49.2224	15	PA	false
1400308	Mucajaí	2.43998	-60.9096	14	RR	false
2309003	Mucambo	-3.90271	-40.7452	23	CE	false
2921906	Mucugê	-13.0053	-41.3703	29	BA	false
4312609	Muçum	-29.163	-51.8714	43	RS	false
2922003	Mucuri	-18.0754	-39.5565	29	BA	false
3203601	Mucurici	-18.0965	-40.52	32	ES	false
4312617	Muitos Capões	-28.3132	-51.1836	43	RS	false
4312625	Muliterno	-28.3253	-51.7697	43	RS	false
2509800	Mulungu	-7.02525	-35.46	25	PB	false
2309102	Mulungu	-4.30294	-38.9951	23	CE	false
2922052	Mulungu do Morro	-11.9648	-41.6374	29	BA	false
2922102	Mundo Novo	-11.8541	-40.4714	29	BA	false
5005681	Mundo Novo	-23.9355	-54.281	50	MS	false
5214051	Mundo Novo	-13.7729	-50.2814	52	GO	false
3143807	Munhoz	-22.6092	-46.362	31	MG	false
4116307	Munhoz de Melo	-23.1487	-51.7737	41	PR	false
2922201	Muniz Ferreira	-13.0092	-39.1092	29	BA	false
3203700	Muniz Freire	-20.4652	-41.4156	32	ES	false
2922250	Muquém de São Francisco	-12.065	-43.5497	29	BA	false
3203809	Muqui	-20.9509	-41.346	32	ES	false
3143906	Muriaé	-21.13	-42.3693	31	MG	false
2804300	Muribeca	-10.4271	-36.9588	28	SE	false
2705507	Murici	-9.30682	-35.9428	27	AL	false
2206696	Murici dos Portelas	-3.319	-42.094	22	PI	false
1713957	Muricilândia	-7.14669	-48.6091	17	TO	false
2922300	Muritiba	-12.6329	-38.9921	29	BA	false
3532108	Murutinga do Sul	-20.9908	-51.2774	35	SP	false
2922409	Mutuípe	-13.2284	-39.5044	29	BA	false
3144003	Mutum	-19.8121	-41.4407	31	MG	false
5214101	Mutunópolis	-13.7303	-49.2745	52	GO	false
3144102	Muzambinho	-21.3692	-46.5213	31	MG	false
3144201	Nacip Raydan	-18.4544	-42.2481	31	MG	false
3532157	Nantes	-22.6156	-51.24	35	SP	false
3144300	Nanuque	-17.8481	-40.3533	31	MG	false
4312658	Não-Me-Toque	-28.4548	-52.8182	43	RS	false
3144359	Naque	-19.2291	-42.3312	31	MG	false
3532207	Narandiba	-22.4057	-51.5274	35	SP	false
2408102	Natal	-5.79357	-35.1986	24	RN	true 
3144375	Natalândia	-16.5021	-46.4874	31	MG	false
3144409	Natércia	-22.1158	-45.5123	31	MG	false
1714203	Natividade	-11.7034	-47.7223	17	TO	false
3303104	Natividade	-21.039	-41.9697	33	RJ	false
3532306	Natividade da Serra	-23.3707	-45.4468	35	SP	false
2509909	Natuba	-7.63514	-35.5586	25	PB	false
4211306	Navegantes	-26.8943	-48.6546	42	SC	false
5005707	Naviraí	-23.0618	-54.1995	50	MS	false
2922508	Nazaré	-13.0235	-39.0108	29	BA	false
1714302	Nazaré	-6.37496	-47.6643	17	TO	false
2609501	Nazaré da Mata	-7.74149	-35.2193	26	PE	false
2206704	Nazaré do Piauí	-6.97023	-42.6773	22	PI	false
3532405	Nazaré Paulista	-23.1747	-46.3983	35	SP	false
3144508	Nazareno	-21.2168	-44.6138	31	MG	false
2510006	Nazarezinho	-6.9114	-38.322	25	PB	false
2206720	Nazária	-5.35128	-42.8153	22	PI	false
5214408	Nazário	-16.5808	-49.8817	52	GO	false
2804409	Neópolis	-10.3215	-36.585	28	SE	false
3144607	Nepomuceno	-21.2324	-45.235	31	MG	false
5214507	Nerópolis	-16.4047	-49.2227	52	GO	false
3532504	Neves Paulista	-20.843	-49.6358	35	SP	false
1303007	Nhamundá	-2.20793	-56.7112	13	AM	false
3532603	Nhandeara	-20.6945	-50.0436	35	SP	false
4312674	Nicolau Vergueiro	-28.5298	-52.4676	43	RS	false
2922607	Nilo Peçanha	-13.604	-39.1091	29	BA	false
3303203	Nilópolis	-22.8057	-43.4233	33	RJ	false
2107209	Nina Rodrigues	-3.46788	-43.9134	21	MA	false
3144656	Ninheira	-15.3148	-41.7564	31	MG	false
5005806	Nioaque	-21.1419	-55.8296	50	MS	false
3532702	Nipoã	-20.9114	-49.7833	35	SP	false
5214606	Niquelândia	-14.4662	-48.4599	52	GO	false
2408201	Nísia Floresta	-6.09329	-35.1991	24	RN	false
3303302	Niterói	-22.8832	-43.1034	33	RJ	false
5105903	Nobres	-14.7192	-56.3284	51	MT	false
4312708	Nonoai	-27.3689	-52.7756	43	RS	false
2922656	Nordestina	-10.8192	-39.4297	29	BA	false
1400407	Normandia	3.8853	-59.6204	14	RR	false
5106000	Nortelândia	-14.454	-56.7945	51	MT	false
2804458	Nossa Senhora Aparecida	-10.3944	-37.4517	28	SE	false
2804508	Nossa Senhora da Glória	-10.2158	-37.4211	28	SE	false
2804607	Nossa Senhora das Dores	-10.4854	-37.1963	28	SE	false
4116406	Nossa Senhora das Graças	-22.9129	-51.7978	41	PR	false
2804706	Nossa Senhora de Lourdes	-10.0772	-37.0615	28	SE	false
2206753	Nossa Senhora de Nazaré	-4.63019	-42.173	22	PI	false
5106109	Nossa Senhora do Livramento	-15.772	-56.3432	51	MT	false
2804805	Nossa Senhora do Socorro	-10.8468	-37.1231	28	SE	false
2206803	Nossa Senhora dos Remédios	-3.97574	-42.6184	22	PI	false
3532801	Nova Aliança	-21.0156	-49.4986	35	SP	false
4116505	Nova Aliança do Ivaí	-23.1763	-52.6032	41	PR	false
4312757	Nova Alvorada	-28.6822	-52.1631	43	RS	false
5006002	Nova Alvorada do Sul	-21.4657	-54.3825	50	MS	false
5214705	Nova América	-15.0206	-49.8953	52	GO	false
4116604	Nova América da Colina	-23.3308	-50.7168	41	PR	false
5006200	Nova Andradina	-22.238	-53.3437	50	MS	false
4312807	Nova Araçá	-28.6537	-51.7458	43	RS	false
4116703	Nova Aurora	-24.5289	-53.2575	41	PR	false
5214804	Nova Aurora	-18.0597	-48.2552	52	GO	false
5106158	Nova Bandeirantes	-9.84977	-57.8139	51	MT	false
4312906	Nova Bassano	-28.7291	-51.7072	43	RS	false
3144672	Nova Belém	-18.4925	-41.1107	31	MG	false
4312955	Nova Boa Vista	-27.9926	-52.9784	43	RS	false
5106208	Nova Brasilândia	-14.9612	-54.9685	51	MT	false
1100148	Nova Brasilândia D'Oeste	-11.7247	-62.3127	11	RO	false
4313003	Nova Bréscia	-29.2182	-52.0319	43	RS	false
3532827	Nova Campina	-24.1224	-48.9022	35	SP	false
2922706	Nova Canaã	-14.7912	-40.1458	29	BA	false
5106216	Nova Canaã do Norte	-10.558	-55.953	51	MT	false
3532843	Nova Canaã Paulista	-20.3836	-50.9483	35	SP	false
4313011	Nova Candelária	-27.6137	-54.1074	43	RS	false
4116802	Nova Cantu	-24.6723	-52.5661	41	PR	false
3532868	Nova Castilho	-20.7615	-50.3477	35	SP	false
2107258	Nova Colinas	-7.12263	-46.2607	21	MA	false
5214838	Nova Crixás	-14.0957	-50.33	52	GO	false
2408300	Nova Cruz	-6.47511	-35.4286	24	RN	false
3144706	Nova Era	-19.7577	-43.0333	31	MG	false
4211405	Nova Erechim	-26.8982	-52.9066	42	SC	false
4116901	Nova Esperança	-23.182	-52.2031	41	PR	false
1504950	Nova Esperança do Piriá	-2.26693	-46.9731	15	PA	false
4116950	Nova Esperança do Sudoeste	-25.9004	-53.2618	41	PR	false
4313037	Nova Esperança do Sul	-29.4066	-54.8293	43	RS	false
3532900	Nova Europa	-21.7765	-48.5705	35	SP	false
4117008	Nova Fátima	-23.4324	-50.5665	41	PR	false
2922730	Nova Fátima	-11.6031	-39.6302	29	BA	false
2510105	Nova Floresta	-6.45056	-36.2057	25	PB	false
3303401	Nova Friburgo	-22.2932	-42.5377	33	RJ	false
5214861	Nova Glória	-15.145	-49.5737	52	GO	false
3533007	Nova Granada	-20.5321	-49.3123	35	SP	false
5108808	Nova Guarita	-10.312	-55.4061	51	MT	false
3533106	Nova Guataporanga	-21.332	-51.6447	35	SP	false
4313060	Nova Hartz	-29.5808	-50.9051	43	RS	false
2922755	Nova Ibiá	-13.812	-39.6182	29	BA	false
3303500	Nova Iguaçu	-22.7556	-43.4603	33	RJ	false
5214879	Nova Iguaçu de Goiás	-14.2868	-49.3872	52	GO	false
3533205	Nova Independência	-21.1026	-51.4905	35	SP	false
2107308	Nova Iorque	-6.73047	-44.0471	21	MA	false
1504976	Nova Ipixuna	-4.91622	-49.0822	15	PA	false
4211454	Nova Itaberaba	-26.9428	-52.8141	42	SC	false
2922805	Nova Itarana	-13.0241	-40.0653	29	BA	false
5106182	Nova Lacerda	-14.4727	-59.6001	51	MT	false
4117057	Nova Laranjeiras	-25.3054	-52.5447	41	PR	false
3144805	Nova Lima	-19.9758	-43.8509	31	MG	false
4117107	Nova Londrina	-22.7639	-52.9868	41	PR	false
3533304	Nova Luzitânia	-20.856	-50.2617	35	SP	false
1100338	Nova Mamoré	-10.4077	-65.3346	11	RO	false
5108857	Nova Marilândia	-14.3568	-56.9696	51	MT	false
5108907	Nova Maringá	-13.0136	-57.0908	51	MT	false
3144904	Nova Módica	-18.4417	-41.4984	31	MG	false
5108956	Nova Monte Verde	-9.99998	-57.5261	51	MT	false
5106224	Nova Mutum	-13.8374	-56.0743	51	MT	false
5106174	Nova Nazaré	-13.9486	-51.8002	51	MT	false
3533403	Nova Odessa	-22.7832	-47.2941	35	SP	false
4117206	Nova Olímpia	-23.4703	-53.0898	41	PR	false
5106232	Nova Olímpia	-14.7889	-57.2886	51	MT	false
1714880	Nova Olinda	-7.63171	-48.4252	17	TO	false
2309201	Nova Olinda	-7.08415	-39.6713	23	CE	false
2510204	Nova Olinda	-7.47232	-38.0382	25	PB	false
2107357	Nova Olinda do Maranhão	-2.84227	-45.6953	21	MA	false
1303106	Nova Olinda do Norte	-3.90037	-59.094	13	AM	false
4313086	Nova Pádua	-29.0275	-51.3098	43	RS	false
4313102	Nova Palma	-29.471	-53.4689	43	RS	false
2510303	Nova Palmeira	-6.67122	-36.422	25	PB	false
4313201	Nova Petrópolis	-29.3741	-51.1136	43	RS	false
3145000	Nova Ponte	-19.1461	-47.6779	31	MG	false
3145059	Nova Porteirinha	-15.7993	-43.2941	31	MG	false
4313300	Nova Prata	-28.7799	-51.6113	43	RS	false
4117255	Nova Prata do Iguaçu	-25.6309	-53.3469	41	PR	false
4313334	Nova Ramada	-28.0667	-53.6992	43	RS	false
2922854	Nova Redenção	-12.815	-41.0748	29	BA	false
3145109	Nova Resende	-21.1286	-46.4157	31	MG	false
5214903	Nova Roma	-13.7388	-46.8734	52	GO	false
4313359	Nova Roma do Sul	-28.9882	-51.4095	43	RS	false
1715002	Nova Rosalândia	-10.5651	-48.9125	17	TO	false
2309300	Nova Russas	-4.70581	-40.5621	23	CE	false
4117214	Nova Santa Bárbara	-23.5865	-50.7598	41	PR	false
5106190	Nova Santa Helena	-10.8651	-55.1872	51	MT	false
4313375	Nova Santa Rita	-29.8525	-51.2837	43	RS	false
2207959	Nova Santa Rita	-8.09707	-42.0471	22	PI	false
4117222	Nova Santa Rosa	-24.4693	-53.9552	41	PR	false
3145208	Nova Serrana	-19.8713	-44.9847	31	MG	false
2922904	Nova Soure	-11.2329	-38.4871	29	BA	false
4117271	Nova Tebas	-24.438	-51.9454	41	PR	false
1505007	Nova Timboteua	-1.20874	-47.3921	15	PA	false
4211504	Nova Trento	-27.278	-48.9298	42	SC	false
5106240	Nova Ubiratã	-12.9834	-55.2556	51	MT	false
3136603	Nova União	-19.6876	-43.583	31	MG	false
1101435	Nova União	-10.9068	-62.5564	11	RO	false
3203908	Nova Venécia	-18.715	-40.4053	32	ES	false
4211603	Nova Veneza	-28.6338	-49.5055	42	SC	false
5215009	Nova Veneza	-16.3695	-49.3168	52	GO	false
2923001	Nova Viçosa	-17.8926	-39.3743	29	BA	false
5106257	Nova Xavantina	-14.6771	-52.3502	51	MT	false
3533254	Novais	-20.9893	-48.9141	35	SP	false
1715101	Novo Acordo	-9.97063	-47.6785	17	TO	false
1303205	Novo Airão	-2.63637	-60.9434	13	AM	false
1715150	Novo Alegre	-12.9217	-46.5713	17	TO	false
1303304	Novo Aripuanã	-5.12593	-60.3732	13	AM	false
4313490	Novo Barreiro	-27.9077	-53.1103	43	RS	false
5215207	Novo Brasil	-16.0313	-50.7113	52	GO	false
4313391	Novo Cabrais	-29.7338	-52.9489	43	RS	false
3145307	Novo Cruzeiro	-17.4654	-41.8826	31	MG	false
5215231	Novo Gama	-16.0592	-48.0417	52	GO	false
4313409	Novo Hamburgo	-29.6875	-51.1328	43	RS	false
4211652	Novo Horizonte	-26.4442	-52.8281	42	SC	false
3533502	Novo Horizonte	-21.4651	-49.2234	35	SP	false
2923035	Novo Horizonte	-12.8083	-42.1682	29	BA	false
5106273	Novo Horizonte do Norte	-11.4089	-57.3488	51	MT	false
1100502	Novo Horizonte do Oeste	-11.6961	-61.9951	11	RO	false
5006259	Novo Horizonte do Sul	-22.6693	-53.8601	50	MS	false
4117297	Novo Itacolomi	-23.7631	-51.5079	41	PR	false
1715259	Novo Jardim	-11.826	-46.6325	17	TO	false
2705606	Novo Lino	-8.94191	-35.664	27	AL	false
4313425	Novo Machado	-27.5765	-54.5036	43	RS	false
5106265	Novo Mundo	-9.95616	-55.2029	51	MT	false
2309409	Novo Oriente	-5.52552	-40.7713	23	CE	false
3145356	Novo Oriente de Minas	-17.4089	-41.2194	31	MG	false
2206902	Novo Oriente do Piauí	-6.44901	-41.9261	22	PI	false
5215256	Novo Planalto	-13.2424	-49.506	52	GO	false
1505031	Novo Progresso	-7.14347	-55.3786	15	PA	false
1505064	Novo Repartimento	-4.24749	-49.9499	15	PA	false
2206951	Novo Santo Antônio	-5.28749	-41.9325	22	PI	false
5106315	Novo Santo Antônio	-12.2875	-50.9686	51	MT	false
5106281	Novo São Joaquim	-14.9054	-53.0194	51	MT	false
4313441	Novo Tiradentes	-27.5649	-53.1837	43	RS	false
2923050	Novo Triunfo	-10.3182	-38.4014	29	BA	false
4313466	Novo Xingu	-27.749	-53.0639	43	RS	false
3145372	Novorizonte	-16.0162	-42.4044	31	MG	false
3533601	Nuporanga	-20.7296	-47.7429	35	SP	false
1505106	Óbidos	-1.90107	-55.5208	15	PA	false
2309458	Ocara	-4.48523	-38.5933	23	CE	false
3533700	Ocauçu	-22.438	-49.922	35	SP	false
2207009	Oeiras	-7.01915	-42.1283	22	PI	false
1505205	Oeiras do Pará	-2.00358	-49.8628	15	PA	false
1600501	Oiapoque	3.84074	-51.8331	16	AP	false
3145406	Olaria	-21.8598	-43.9356	31	MG	false
3533809	Óleo	-22.9435	-49.3419	35	SP	false
2510402	Olho d'Água	-7.22118	-37.7406	25	PB	false
2107407	Olho d'Água das Cunhãs	-4.13417	-45.1163	21	MA	false
2705705	Olho d'Água das Flores	-9.53686	-37.2971	27	AL	false
2705804	Olho d'Água do Casado	-9.50357	-37.8301	27	AL	false
2207108	Olho D'Água do Piauí	-5.84125	-42.5594	22	PI	false
2705903	Olho d'Água Grande	-10.0572	-36.8101	27	AL	false
2408409	Olho-d'Água do Borges	-5.9486	-37.7047	24	RN	false
3145455	Olhos d'Água	-17.3982	-43.5719	31	MG	false
3533908	Olímpia	-20.7366	-48.9106	35	SP	false
3145505	Olímpio Noronha	-22.0685	-45.2657	31	MG	false
2609600	Olinda	-8.01017	-34.8545	26	PE	false
2107456	Olinda Nova do Maranhão	-2.99295	-44.9897	21	MA	false
2923100	Olindina	-11.3497	-38.3379	29	BA	false
2510501	Olivedos	-6.98434	-36.241	25	PB	false
3145604	Oliveira	-20.6982	-44.829	31	MG	false
1715507	Oliveira de Fátima	-10.707	-48.9086	17	TO	false
2923209	Oliveira dos Brejinhos	-12.3132	-42.8969	29	BA	false
3145703	Oliveira Fortes	-21.3401	-43.4499	31	MG	false
2706000	Olivença	-9.51954	-37.1954	27	AL	false
3145802	Onça de Pitangui	-19.7276	-44.8058	31	MG	false
3534005	Onda Verde	-20.6042	-49.2929	35	SP	false
3145851	Oratórios	-20.4298	-42.7977	31	MG	false
3534104	Oriente	-22.1549	-50.0971	35	SP	false
3534203	Orindiúva	-20.1861	-49.3464	35	SP	false
1505304	Oriximiná	-1.75989	-55.8579	15	PA	false
3145877	Orizânia	-20.5142	-42.1991	31	MG	false
5215306	Orizona	-17.0334	-48.2964	52	GO	false
3534302	Orlândia	-20.7169	-47.8852	35	SP	false
4211702	Orleans	-28.3487	-49.2986	42	SC	false
2609709	Orobó	-7.74553	-35.5956	26	PE	false
2609808	Orocó	-8.61026	-39.6026	26	PE	false
2309508	Orós	-6.25182	-38.9053	23	CE	false
4117305	Ortigueira	-24.2058	-50.9185	41	PR	false
3534401	Osasco	-23.5324	-46.7916	35	SP	false
3534500	Oscar Bressane	-22.3149	-50.2811	35	SP	false
4313508	Osório	-29.8881	-50.2667	43	RS	false
3534609	Osvaldo Cruz	-21.7968	-50.8793	35	SP	false
4211751	Otacílio Costa	-27.4789	-50.1231	42	SC	false
1505403	Ourém	-1.54168	-47.1126	15	PA	false
2923308	Ouriçangas	-12.0175	-38.6166	29	BA	false
2609907	Ouricuri	-7.87918	-40.08	26	PE	false
1505437	Ourilândia do Norte	-6.7529	-51.0858	15	PA	false
3534708	Ourinhos	-22.9797	-49.8697	35	SP	false
4117404	Ourizona	-23.4053	-52.1964	41	PR	false
4211801	Ouro	-27.3379	-51.6194	42	SC	false
3145901	Ouro Branco	-20.5263	-43.6962	31	MG	false
2408508	Ouro Branco	-6.6958	-36.9428	24	RN	false
2706109	Ouro Branco	-9.15884	-37.3556	27	AL	false
3146008	Ouro Fino	-22.2779	-46.3716	31	MG	false
3146107	Ouro Preto	-20.3796	-43.512	31	MG	false
1100155	Ouro Preto do Oeste	-10.7167	-62.2565	11	RO	false
2510600	Ouro Velho	-7.61604	-37.1519	25	PB	false
4211850	Ouro Verde	-26.692	-52.3108	42	SC	false
3534807	Ouro Verde	-21.4872	-51.7024	35	SP	false
5215405	Ouro Verde de Goiás	-16.2181	-49.1942	52	GO	false
3146206	Ouro Verde de Minas	-18.0719	-41.2734	31	MG	false
4117453	Ouro Verde do Oeste	-24.7933	-53.9043	41	PR	false
3534757	Ouroeste	-20.0061	-50.3768	35	SP	false
2923357	Ourolândia	-10.9578	-41.0756	29	BA	false
5215504	Ouvidor	-18.2277	-47.8355	52	GO	false
3534906	Pacaembu	-21.5627	-51.2654	35	SP	false
1505486	Pacajá	-3.83542	-50.6399	15	PA	false
2309607	Pacajus	-4.17107	-38.465	23	CE	false
1400456	Pacaraima	4.4799	-61.1477	14	RR	false
2309706	Pacatuba	-3.9784	-38.6183	23	CE	false
2804904	Pacatuba	-10.4538	-36.6531	28	SE	false
2107506	Paço do Lumiar	-2.51657	-44.1019	21	MA	false
2309805	Pacoti	-4.22492	-38.922	23	CE	false
2309904	Pacujá	-3.98327	-40.6989	23	CE	false
5215603	Padre Bernardo	-15.1605	-48.2833	52	GO	false
3146255	Padre Carvalho	-16.3646	-42.5088	31	MG	false
2207207	Padre Marcos	-7.35101	-40.8997	22	PI	false
3146305	Padre Paraíso	-17.0758	-41.4821	31	MG	false
2207306	Paes Landim	-7.77375	-42.2474	22	PI	false
3146552	Pai Pedro	-15.5271	-43.07	31	MG	false
4211876	Paial	-27.2541	-52.4975	42	SC	false
4117503	Paiçandu	-23.4555	-52.046	41	PR	false
4313607	Paim Filho	-27.7075	-51.763	43	RS	false
3146404	Paineiras	-18.8993	-45.5321	31	MG	false
4211892	Painel	-27.9234	-50.0972	42	SC	false
3146503	Pains	-20.3705	-45.6627	31	MG	false
3146602	Paiva	-21.2913	-43.4088	31	MG	false
2207355	Pajeú do Piauí	-7.85508	-42.8248	22	PI	false
2706208	Palestina	-9.67493	-37.339	27	AL	false
3535002	Palestina	-20.39	-49.4309	35	SP	false
5215652	Palestina de Goiás	-16.7392	-51.5309	52	GO	false
1505494	Palestina do Pará	-5.74027	-48.3181	15	PA	false
2310001	Palhano	-4.73672	-37.9655	23	CE	false
4211900	Palhoça	-27.6455	-48.6697	42	SC	false
3146701	Palma	-21.3748	-42.3123	31	MG	false
4212007	Palma Sola	-26.3471	-53.2771	42	SC	false
2310100	Palmácia	-4.13831	-38.8446	23	CE	false
2610004	Palmares	-8.68423	-35.589	26	PE	false
4313656	Palmares do Sul	-30.2535	-50.5103	43	RS	false
3535101	Palmares Paulista	-21.0854	-48.8037	35	SP	false
4117602	Palmas	-26.4839	-51.9888	41	PR	false
1721000	Palmas	-10.24	-48.3558	17	TO	true 
2923407	Palmas de Monte Alto	-14.2676	-43.1609	29	BA	false
4117701	Palmeira	-25.4257	-50.007	41	PR	false
4212056	Palmeira	-27.583	-50.1577	42	SC	false
3535200	Palmeira d'Oeste	-20.4148	-50.7632	35	SP	false
4313706	Palmeira das Missões	-27.9007	-53.3134	43	RS	false
2207405	Palmeira do Piauí	-8.73076	-44.2466	22	PI	false
2706307	Palmeira dos Índios	-9.40568	-36.6328	27	AL	false
2207504	Palmeirais	-5.97086	-43.056	22	PI	false
2107605	Palmeirândia	-2.64433	-44.8933	21	MA	false
1715705	Palmeirante	-7.84786	-47.9242	17	TO	false
2923506	Palmeiras	-12.5059	-41.5809	29	BA	false
5215702	Palmeiras de Goiás	-16.8044	-49.924	52	GO	false
1713809	Palmeiras do Tocantins	-6.61658	-47.5464	17	TO	false
2610103	Palmeirina	-9.0109	-36.3242	26	PE	false
1715754	Palmeirópolis	-13.0447	-48.4026	17	TO	false
5215801	Palmelo	-17.3258	-48.426	52	GO	false
5215900	Palminópolis	-16.7924	-50.1652	52	GO	false
3535309	Palmital	-22.7858	-50.218	35	SP	false
4117800	Palmital	-24.8853	-52.2029	41	PR	false
4313805	Palmitinho	-27.3596	-53.558	43	RS	false
4212106	Palmitos	-27.0702	-53.1586	42	SC	false
3146750	Palmópolis	-16.7364	-40.4296	31	MG	false
4117909	Palotina	-24.2868	-53.8404	41	PR	false
5216007	Panamá	-18.1783	-49.355	52	GO	false
4313904	Panambi	-28.2833	-53.5023	43	RS	false
3204005	Pancas	-19.2229	-40.8534	32	ES	false
2610202	Panelas	-8.66121	-36.0125	26	PE	false
3535408	Panorama	-21.354	-51.8562	35	SP	false
4313953	Pantano Grande	-30.1902	-52.3729	43	RS	false
2706406	Pão de Açúcar	-9.74032	-37.4403	27	AL	false
3146909	Papagaios	-19.4419	-44.7468	31	MG	false
4212205	Papanduva	-26.3777	-50.1419	42	SC	false
2207553	Paquetá	-7.10303	-41.7	22	PI	false
3147105	Pará de Minas	-19.8534	-44.6114	31	MG	false
3303609	Paracambi	-22.6078	-43.7108	33	RJ	false
3147006	Paracatu	-17.2252	-46.8711	31	MG	false
2310209	Paracuru	-3.41436	-39.03	23	CE	false
1505502	Paragominas	-3.00212	-47.3527	15	PA	false
3147204	Paraguaçu	-21.5465	-45.7374	31	MG	false
3535507	Paraguaçu Paulista	-22.4114	-50.5732	35	SP	false
4314001	Paraí	-28.5964	-51.7896	43	RS	false
3303708	Paraíba do Sul	-22.1585	-43.304	33	RJ	false
2107704	Paraibano	-6.4264	-43.9792	21	MA	false
3535606	Paraibuna	-23.3872	-45.6639	35	SP	false
2310258	Paraipaba	-3.43799	-39.1479	23	CE	false
3535705	Paraíso	-21.0159	-48.7761	35	SP	false
4212239	Paraíso	-26.62	-53.6716	42	SC	false
5006275	Paraíso das Águas	-19.0216	-53.0116	50	MS	false
4118006	Paraíso do Norte	-23.2824	-52.6054	41	PR	false
4314027	Paraíso do Sul	-29.6717	-53.144	43	RS	false
1716109	Paraíso do Tocantins	-10.175	-48.8823	17	TO	false
3147303	Paraisópolis	-22.5539	-45.7803	31	MG	false
2310308	Parambu	-6.20768	-40.6905	23	CE	false
2923605	Paramirim	-13.4388	-42.2395	29	BA	false
2310407	Paramoti	-4.08815	-39.2417	23	CE	false
1716208	Paranã	-12.6167	-47.8734	17	TO	false
2408607	Paraná	-6.47565	-38.3057	24	RN	false
4118105	Paranacity	-22.9297	-52.1549	41	PR	false
4118204	Paranaguá	-25.5161	-48.5225	41	PR	false
5006309	Paranaíba	-19.6746	-51.1909	50	MS	false
5216304	Paranaiguara	-18.9141	-50.6539	52	GO	false
5106299	Paranaíta	-9.65835	-56.4786	51	MT	false
3535804	Paranapanema	-23.3862	-48.7214	35	SP	false
4118303	Paranapoema	-22.6412	-52.0905	41	PR	false
3535903	Paranapuã	-20.1048	-50.5886	35	SP	false
2610301	Paranatama	-8.91875	-36.6549	26	PE	false
5106307	Paranatinga	-14.4265	-54.0524	51	MT	false
4118402	Paranavaí	-23.0816	-52.4617	41	PR	false
5006358	Paranhos	-23.8911	-55.429	50	MS	false
3147402	Paraopeba	-19.2732	-44.4044	31	MG	false
3536000	Parapuã	-21.7792	-50.7949	35	SP	false
2510659	Parari	-7.30975	-36.6522	25	PB	false
2923704	Paratinga	-12.687	-43.1798	29	BA	false
3303807	Paraty	-23.2221	-44.7175	33	RJ	false
2408706	Paraú	-5.76893	-37.1032	24	RN	false
1505536	Parauapebas	-6.06781	-49.9037	15	PA	false
5216403	Paraúna	-16.9463	-50.4484	52	GO	false
2408805	Parazinho	-5.22276	-35.8398	24	RN	false
3536109	Pardinho	-23.0841	-48.3679	35	SP	false
4314035	Pareci Novo	-29.6365	-51.3974	43	RS	false
1101450	Parecis	-12.1754	-61.6032	11	RO	false
2408904	Parelhas	-6.68491	-36.6566	24	RN	false
2706422	Pariconha	-9.25634	-37.9988	27	AL	false
1303403	Parintins	-2.63741	-56.729	13	AM	false
2923803	Paripiranga	-10.6859	-37.8626	29	BA	false
2706448	Paripueira	-9.46313	-35.552	27	AL	false
3536208	Pariquera-Açu	-24.7147	-47.8742	35	SP	false
3536257	Parisi	-20.3034	-50.0163	35	SP	false
2207603	Parnaguá	-10.2166	-44.63	22	PI	false
2207702	Parnaíba	-2.90585	-41.7754	22	PI	false
2403251	Parnamirim	-5.91116	-35.271	24	RN	false
2610400	Parnamirim	-8.08729	-39.5795	26	PE	false
2107803	Parnarama	-5.67365	-43.1011	21	MA	false
4314050	Parobé	-29.6243	-50.8312	43	RS	false
2409100	Passa e Fica	-6.43018	-35.6442	24	RN	false
3147600	Passa Quatro	-22.3871	-44.9709	31	MG	false
4314068	Passa Sete	-29.4577	-52.9599	43	RS	false
3147709	Passa Tempo	-20.6539	-44.4926	31	MG	false
3147808	Passa-Vinte	-22.2097	-44.2344	31	MG	false
3147501	Passabém	-19.3509	-43.1383	31	MG	false
2409209	Passagem	-6.27268	-35.37	24	RN	false
2510709	Passagem	-7.13467	-37.0433	25	PB	false
2107902	Passagem Franca	-6.17745	-43.7755	21	MA	false
2207751	Passagem Franca do Piauí	-5.86036	-42.4436	22	PI	false
2610509	Passira	-7.9971	-35.5813	26	PE	false
2706505	Passo de Camaragibe	-9.24511	-35.4745	27	AL	false
4212254	Passo de Torres	-29.3099	-49.722	42	SC	false
4314076	Passo do Sobrado	-29.748	-52.2748	43	RS	false
4314100	Passo Fundo	-28.2576	-52.4091	43	RS	false
3147907	Passos	-20.7193	-46.609	31	MG	false
4212270	Passos Maia	-26.7829	-52.0568	42	SC	false
2108009	Pastos Bons	-6.60296	-44.0745	21	MA	false
3147956	Patis	-16.0773	-44.0787	31	MG	false
4118451	Pato Bragado	-24.6271	-54.2265	41	PR	false
4118501	Pato Branco	-26.2292	-52.6706	41	PR	false
2510808	Patos	-7.01743	-37.2747	25	PB	false
3148004	Patos de Minas	-18.5699	-46.5013	31	MG	false
2207777	Patos do Piauí	-7.67231	-41.2408	22	PI	false
3148103	Patrocínio	-18.9379	-46.9934	31	MG	false
3148202	Patrocínio do Muriaé	-21.1544	-42.2125	31	MG	false
3536307	Patrocínio Paulista	-20.6384	-47.2801	35	SP	false
2409308	Patu	-6.10656	-37.6356	24	RN	false
3303856	Paty do Alferes	-22.4309	-43.4285	33	RJ	false
2923902	Pau Brasil	-15.4572	-39.6458	29	BA	false
1505551	Pau d'Arco	-1.59772	-46.9268	15	PA	false
1716307	Pau D'Arco	-7.53919	-49.367	17	TO	false
2207793	Pau D'Arco do Piauí	-5.26072	-42.3908	22	PI	false
2409407	Pau dos Ferros	-6.10498	-38.2077	24	RN	false
2610608	Paudalho	-7.90287	-35.1716	26	PE	false
1303502	Pauini	-7.71311	-66.992	13	AM	false
3148301	Paula Cândido	-20.8754	-42.9752	31	MG	false
4118600	Paula Freitas	-26.2105	-50.931	41	PR	false
3536406	Paulicéia	-21.3153	-51.8321	35	SP	false
3536505	Paulínia	-22.7542	-47.1488	35	SP	false
2108058	Paulino Neves	-2.72094	-42.5258	21	MA	false
2510907	Paulista	-6.59138	-37.6185	25	PB	false
2610707	Paulista	-7.93401	-34.8684	26	PE	false
2207801	Paulistana	-8.13436	-41.1431	22	PI	false
3536570	Paulistânia	-22.5768	-49.4008	35	SP	false
3148400	Paulistas	-18.4276	-42.8628	31	MG	false
2924009	Paulo Afonso	-9.3983	-38.2216	29	BA	false
4314134	Paulo Bento	-27.7051	-52.4169	43	RS	false
3536604	Paulo de Faria	-20.0296	-49.4	35	SP	false
4118709	Paulo Frontin	-26.0466	-50.8304	41	PR	false
2706604	Paulo Jacinto	-9.36792	-36.3672	27	AL	false
4212304	Paulo Lopes	-27.9607	-48.6864	42	SC	false
2108108	Paulo Ramos	-4.44485	-45.2398	21	MA	false
3148509	Pavão	-17.4267	-41.0035	31	MG	false
4314159	Paverama	-29.5486	-51.7339	43	RS	false
2207850	Pavussu	-7.96059	-43.2284	22	PI	false
2924058	Pé de Serra	-11.8313	-39.611	29	BA	false
4118808	Peabiru	-23.914	-52.3431	41	PR	false
3148608	Peçanha	-18.5441	-42.5583	31	MG	false
3536703	Pederneiras	-22.3511	-48.7781	35	SP	false
2610806	Pedra	-8.49641	-36.94	26	PE	false
3148707	Pedra Azul	-16.0086	-41.2909	31	MG	false
3536802	Pedra Bela	-22.7902	-46.4455	35	SP	false
3148756	Pedra Bonita	-20.5219	-42.3304	31	MG	false
2511004	Pedra Branca	-7.42169	-38.0689	25	PB	false
2310506	Pedra Branca	-5.45341	-39.7078	23	CE	false
1600154	Pedra Branca do Amapari	0.777424	-51.9503	16	AP	false
3148806	Pedra do Anta	-20.5968	-42.7123	31	MG	false
3148905	Pedra do Indaiá	-20.2563	-45.2107	31	MG	false
3149002	Pedra Dourada	-20.8266	-42.1515	31	MG	false
2409506	Pedra Grande	-5.14988	-35.876	24	RN	false
2511103	Pedra Lavrada	-6.74997	-36.4758	25	PB	false
2805000	Pedra Mole	-10.6134	-37.6922	28	SE	false
2409605	Pedra Preta	-5.57352	-36.1084	24	RN	false
5106372	Pedra Preta	-16.6245	-54.4722	51	MT	false
3149101	Pedralva	-22.2386	-45.4654	31	MG	false
3536901	Pedranópolis	-20.2474	-50.1129	35	SP	false
2924108	Pedrão	-12.1491	-38.6487	29	BA	false
4314175	Pedras Altas	-31.7365	-53.5814	43	RS	false
2511202	Pedras de Fogo	-7.39107	-35.1065	25	PB	false
3149150	Pedras de Maria da Cruz	-15.6032	-44.391	31	MG	false
4212403	Pedras Grandes	-28.4339	-49.1949	42	SC	false
3537008	Pedregulho	-20.2535	-47.4775	35	SP	false
3537107	Pedreira	-22.7413	-46.8948	35	SP	false
2108207	Pedreiras	-4.56482	-44.6006	21	MA	false
2805109	Pedrinhas	-11.1902	-37.6775	28	SE	false
3537156	Pedrinhas Paulista	-22.8174	-50.7933	35	SP	false
3149200	Pedrinópolis	-19.2241	-47.4579	31	MG	false
1716505	Pedro Afonso	-8.97034	-48.1729	17	TO	false
2924207	Pedro Alexandre	-10.012	-37.8932	29	BA	false
2409704	Pedro Avelino	-5.5161	-36.3867	24	RN	false
3204054	Pedro Canário	-18.3004	-39.9574	32	ES	false
3537206	Pedro de Toledo	-24.2764	-47.2354	35	SP	false
2108256	Pedro do Rosário	-2.97272	-45.3493	21	MA	false
5006408	Pedro Gomes	-18.0996	-54.5507	50	MS	false
2207900	Pedro II	-4.42585	-41.4482	22	PI	false
2207934	Pedro Laurentino	-8.06807	-42.2847	22	PI	false
3149309	Pedro Leopoldo	-19.6308	-44.0383	31	MG	false
4314209	Pedro Osório	-31.8642	-52.8184	43	RS	false
2512721	Pedro Régis	-6.63323	-35.2966	25	PB	false
3149408	Pedro Teixeira	-21.7076	-43.743	31	MG	false
2409803	Pedro Velho	-6.4356	-35.2195	24	RN	false
1716604	Peixe	-12.0254	-48.5395	17	TO	false
1505601	Peixe-Boi	-1.19382	-47.324	15	PA	false
5106422	Peixoto de Azevedo	-10.2262	-54.9794	51	MT	false
4314308	Pejuçara	-28.4283	-53.6579	43	RS	false
4314407	Pelotas	-31.7649	-52.3371	43	RS	false
2310605	Penaforte	-7.82163	-39.0707	23	CE	false
2108306	Penalva	-3.27674	-45.1768	21	MA	false
3537305	Penápolis	-21.4148	-50.0769	35	SP	false
2409902	Pendências	-5.2564	-36.7095	24	RN	false
2706703	Penedo	-10.2874	-36.5819	27	AL	false
4212502	Penha	-26.7754	-48.6465	42	SC	false
2310704	Pentecoste	-3.79274	-39.2692	23	CE	false
3149507	Pequeri	-21.8341	-43.1145	31	MG	false
3149606	Pequi	-19.6284	-44.6604	31	MG	false
1716653	Pequizeiro	-8.5932	-48.9327	17	TO	false
3149705	Perdigão	-19.9411	-45.078	31	MG	false
3149804	Perdizes	-19.3434	-47.2963	31	MG	false
3149903	Perdões	-21.0932	-45.0896	31	MG	false
3537404	Pereira Barreto	-20.6368	-51.1123	35	SP	false
3537503	Pereiras	-23.0804	-47.972	35	SP	false
2310803	Pereiro	-6.03576	-38.4624	23	CE	false
2108405	Peri Mirim	-2.57676	-44.8504	21	MA	false
3149952	Periquito	-19.1573	-42.2333	31	MG	false
4212601	Peritiba	-27.3754	-51.9018	42	SC	false
2108454	Peritoró	-4.37459	-44.3369	21	MA	false
4118857	Perobal	-23.8949	-53.4098	41	PR	false
4118907	Pérola	-23.8039	-53.6834	41	PR	false
4119004	Pérola d'Oeste	-25.8278	-53.7433	41	PR	false
5216452	Perolândia	-17.5258	-52.065	52	GO	false
3537602	Peruíbe	-24.312	-47.0012	35	SP	false
3150000	Pescador	-18.357	-41.6006	31	MG	false
4212650	Pescaria Brava	-28.3966	-48.8864	42	SC	false
2610905	Pesqueira	-8.35797	-36.6978	26	PE	false
2611002	Petrolândia	-9.06863	-38.3027	26	PE	false
4212700	Petrolândia	-27.5346	-49.6937	42	SC	false
2611101	Petrolina	-9.38866	-40.5027	26	PE	false
5216809	Petrolina de Goiás	-16.0968	-49.3364	52	GO	false
3303906	Petrópolis	-22.52	-43.1926	33	RJ	false
2706802	Piaçabuçu	-10.406	-36.434	27	AL	false
3537701	Piacatu	-21.5921	-50.6003	35	SP	false
2511301	Piancó	-7.19282	-37.9289	25	PB	false
2924306	Piatã	-13.1465	-41.7702	29	BA	false
3150109	Piau	-21.5096	-43.313	31	MG	false
4314423	Picada Café	-29.4464	-51.1367	43	RS	false
1505635	Piçarra	-6.43778	-48.8716	15	PA	false
2208007	Picos	-7.07721	-41.467	22	PI	false
2511400	Picuí	-6.50845	-36.3497	25	PB	false
3537800	Piedade	-23.7139	-47.4256	35	SP	false
3150158	Piedade de Caratinga	-19.7593	-42.0756	31	MG	false
3150208	Piedade de Ponte Nova	-20.2438	-42.7379	31	MG	false
3150307	Piedade do Rio Grande	-21.469	-44.1938	31	MG	false
3150406	Piedade dos Gerais	-20.4715	-44.2243	31	MG	false
4119103	Piên	-26.0965	-49.4336	41	PR	false
2924405	Pilão Arcado	-10.0051	-42.4936	29	BA	false
2511509	Pilar	-7.26403	-35.2523	25	PB	false
2706901	Pilar	-9.60135	-35.9543	27	AL	false
5216908	Pilar de Goiás	-14.7608	-49.5784	52	GO	false
3537909	Pilar do Sul	-23.8077	-47.7222	35	SP	false
2410009	Pilões	-6.26364	-38.0461	24	RN	false
2511608	Pilões	-6.86827	-35.613	25	PB	false
2511707	Pilõezinhos	-6.84277	-35.531	25	PB	false
3150505	Pimenta	-20.4827	-45.8049	31	MG	false
1100189	Pimenta Bueno	-11.672	-61.198	11	RO	false
2208106	Pimenteiras	-6.23839	-41.4113	22	PI	false
1101468	Pimenteiras do Oeste	-13.4823	-61.0471	11	RO	false
2924504	Pindaí	-14.4921	-42.686	29	BA	false
3538006	Pindamonhangaba	-22.9246	-45.4613	35	SP	false
2108504	Pindaré-Mirim	-3.60985	-45.342	21	MA	false
2707008	Pindoba	-9.47382	-36.2918	27	AL	false
2924603	Pindobaçu	-10.7433	-40.3675	29	BA	false
3538105	Pindorama	-21.1853	-48.9086	35	SP	false
1717008	Pindorama do Tocantins	-11.1311	-47.5726	17	TO	false
2310852	Pindoretama	-4.01584	-38.3061	23	CE	false
3150539	Pingo-d'Água	-19.7287	-42.4095	31	MG	false
4119152	Pinhais	-25.4429	-49.1927	41	PR	false
4314456	Pinhal	-27.508	-53.2082	43	RS	false
4314464	Pinhal da Serra	-27.8751	-51.1673	43	RS	false
4119251	Pinhal de São Bento	-26.0324	-53.482	41	PR	false
4314472	Pinhal Grande	-29.345	-53.3206	43	RS	false
4119202	Pinhalão	-23.7982	-50.0536	41	PR	false
3538204	Pinhalzinho	-22.7811	-46.5897	35	SP	false
4212908	Pinhalzinho	-26.8495	-52.9913	42	SC	false
2805208	Pinhão	-10.5677	-37.7242	28	SE	false
4119301	Pinhão	-25.6944	-51.6536	41	PR	false
3303955	Pinheiral	-22.5172	-44.0022	33	RJ	false
4314498	Pinheirinho do Vale	-27.2109	-53.608	43	RS	false
2108603	Pinheiro	-2.52224	-45.0788	21	MA	false
4314506	Pinheiro Machado	-31.5794	-53.3798	43	RS	false
4213005	Pinheiro Preto	-27.0483	-51.2243	42	SC	false
3204104	Pinheiros	-18.4141	-40.2171	32	ES	false
2924652	Pintadas	-11.8117	-39.9009	29	BA	false
4314548	Pinto Bandeira	-29.0975	-51.4503	43	RS	false
3150570	Pintópolis	-16.0572	-45.1402	31	MG	false
2208205	Pio IX	-6.83002	-40.6083	22	PI	false
2108702	Pio XII	-3.89315	-45.1759	21	MA	false
3538303	Piquerobi	-21.8747	-51.7282	35	SP	false
2310902	Piquet Carneiro	-5.80025	-39.417	23	CE	false
3538501	Piquete	-22.6069	-45.1869	35	SP	false
3538600	Piracaia	-23.0525	-46.3594	35	SP	false
5217104	Piracanjuba	-17.302	-49.017	52	GO	false
3150604	Piracema	-20.5089	-44.4783	31	MG	false
3538709	Piracicaba	-22.7338	-47.6476	35	SP	false
2208304	Piracuruca	-3.93335	-41.7088	22	PI	false
3304003	Piraí	-22.6215	-43.9081	33	RJ	false
2924678	Piraí do Norte	-13.759	-39.3836	29	BA	false
4119400	Piraí do Sul	-24.5306	-49.9433	41	PR	false
3538808	Piraju	-23.1981	-49.3803	35	SP	false
3150703	Pirajuba	-19.9092	-48.7027	31	MG	false
3538907	Pirajuí	-21.999	-49.4608	35	SP	false
2805307	Pirambu	-10.7215	-36.8544	28	SE	false
3150802	Piranga	-20.6834	-43.2967	31	MG	false
3539004	Pirangi	-21.0886	-48.6607	35	SP	false
3150901	Piranguçu	-22.5249	-45.4945	31	MG	false
3151008	Piranguinho	-22.395	-45.5324	31	MG	false
2707107	Piranhas	-9.624	-37.757	27	AL	false
5217203	Piranhas	-16.4258	-51.8235	52	GO	false
2108801	Pirapemas	-3.72041	-44.2216	21	MA	false
3151107	Pirapetinga	-21.6554	-42.3434	31	MG	false
4314555	Pirapó	-28.0439	-55.2001	43	RS	false
3151206	Pirapora	-17.3392	-44.934	31	MG	false
3539103	Pirapora do Bom Jesus	-23.3965	-46.9991	35	SP	false
3539202	Pirapozinho	-22.2711	-51.4976	35	SP	false
4119509	Piraquara	-25.4422	-49.0624	41	PR	false
1717206	Piraquê	-6.77302	-48.2958	17	TO	false
3539301	Pirassununga	-21.996	-47.4257	35	SP	false
4314605	Piratini	-31.4473	-53.0973	43	RS	false
3539400	Piratininga	-22.4142	-49.1339	35	SP	false
4213104	Piratuba	-27.4242	-51.7668	42	SC	false
3151305	Piraúba	-21.2825	-43.0172	31	MG	false
5217302	Pirenópolis	-15.8507	-48.9584	52	GO	false
5217401	Pires do Rio	-17.3019	-48.2768	52	GO	false
2310951	Pires Ferreira	-4.23922	-40.6442	23	CE	false
2924702	Piripá	-14.9444	-41.7168	29	BA	false
2208403	Piripiri	-4.27157	-41.7716	22	PI	false
2924801	Piritiba	-11.73	-40.5587	29	BA	false
2511806	Pirpirituba	-6.77922	-35.4906	25	PB	false
4119608	Pitanga	-24.7588	-51.7596	41	PR	false
3539509	Pitangueiras	-21.0132	-48.221	35	SP	false
4119657	Pitangueiras	-23.2281	-51.5873	41	PR	false
3151404	Pitangui	-19.6741	-44.8964	31	MG	false
2511905	Pitimbu	-7.4664	-34.8151	25	PB	false
1717503	Pium	-10.442	-49.1876	17	TO	false
3204203	Piúma	-20.8334	-40.7268	32	ES	false
3151503	Piumhi	-20.4762	-45.9589	31	MG	false
1505650	Placas	-3.86813	-54.2124	15	PA	false
1200385	Plácido de Castro	-10.2806	-67.1371	12	AC	false
5217609	Planaltina	-15.452	-47.6089	52	GO	false
4119707	Planaltina do Paraná	-23.0101	-52.9162	41	PR	false
2924900	Planaltino	-13.2618	-40.3695	29	BA	false
2925006	Planalto	-14.6654	-40.4718	29	BA	false
4314704	Planalto	-27.3297	-53.0575	43	RS	false
3539608	Planalto	-21.0342	-49.933	35	SP	false
4119806	Planalto	-25.7211	-53.7642	41	PR	false
4213153	Planalto Alegre	-27.0704	-52.867	42	SC	false
5106455	Planalto da Serra	-14.6518	-54.7819	51	MT	false
3151602	Planura	-20.1376	-48.7	31	MG	false
3539707	Platina	-22.6371	-50.2104	35	SP	false
3539806	Poá	-23.5333	-46.3473	35	SP	false
2611200	Poção	-8.18726	-36.7111	26	PE	false
2108900	Poção de Pedras	-4.74626	-44.9432	21	MA	false
2512002	Pocinhos	-7.06658	-36.0668	25	PB	false
2410108	Poço Branco	-5.62233	-35.6635	24	RN	false
2512036	Poço Dantas	-6.39876	-38.4909	25	PB	false
4314753	Poço das Antas	-29.4481	-51.6719	43	RS	false
2707206	Poço das Trincheiras	-9.30742	-37.2889	27	AL	false
2512077	Poço de José de Moura	-6.56401	-38.5111	25	PB	false
3151701	Poço Fundo	-21.78	-45.9658	31	MG	false
2805406	Poço Redondo	-9.80616	-37.6833	28	SE	false
2805505	Poço Verde	-10.7151	-38.1813	28	SE	false
2925105	Poções	-14.5234	-40.3634	29	BA	false
5106505	Poconé	-16.266	-56.6261	51	MT	false
3151800	Poços de Caldas	-21.78	-46.5692	31	MG	false
3151909	Pocrane	-19.6208	-41.6334	31	MG	false
2925204	Pojuca	-12.4303	-38.3374	29	BA	false
3539905	Poloni	-20.7829	-49.8258	35	SP	false
2512101	Pombal	-6.76606	-37.8003	25	PB	false
2611309	Pombos	-8.13982	-35.3967	26	PE	false
4213203	Pomerode	-26.7384	-49.1785	42	SC	false
3540002	Pompéia	-22.107	-50.176	35	SP	false
3152006	Pompéu	-19.2257	-45.0141	31	MG	false
3540101	Pongaí	-21.7396	-49.3604	35	SP	false
1505700	Ponta de Pedras	-1.39587	-48.8661	15	PA	false
4119905	Ponta Grossa	-25.0916	-50.1668	41	PR	false
5006606	Ponta Porã	-22.5296	-55.7203	50	MS	false
3540200	Pontal	-21.0216	-48.0423	35	SP	false
5106653	Pontal do Araguaia	-15.9274	-52.3273	51	MT	false
4119954	Pontal do Paraná	-25.6735	-48.5111	41	PR	false
5217708	Pontalina	-17.5225	-49.4489	52	GO	false
3540259	Pontalinda	-20.4396	-50.5258	35	SP	false
4314779	Pontão	-28.0585	-52.6791	43	RS	false
4213302	Ponte Alta	-27.4835	-50.3764	42	SC	false
1717800	Ponte Alta do Bom Jesus	-12.0853	-46.4825	17	TO	false
4213351	Ponte Alta do Norte	-27.1591	-50.4659	42	SC	false
1717909	Ponte Alta do Tocantins	-10.7481	-47.5276	17	TO	false
5106703	Ponte Branca	-16.7584	-52.8369	51	MT	false
3152105	Ponte Nova	-20.4111	-42.8978	31	MG	false
4314787	Ponte Preta	-27.6587	-52.4848	43	RS	false
4213401	Ponte Serrada	-26.8733	-52.0112	42	SC	false
5106752	Pontes e Lacerda	-15.2219	-59.3435	51	MT	false
3540309	Pontes Gestal	-20.1727	-49.7064	35	SP	false
3204252	Ponto Belo	-18.1253	-40.5458	32	ES	false
3152131	Ponto Chique	-16.6282	-45.0588	31	MG	false
3152170	Ponto dos Volantes	-16.7473	-41.5025	31	MG	false
2925253	Ponto Novo	-10.8653	-40.1311	29	BA	false
3540408	Populina	-19.9453	-50.538	35	SP	false
2311009	Poranga	-4.74672	-40.9205	23	CE	false
3540507	Porangaba	-23.1761	-48.1195	35	SP	false
5218003	Porangatu	-13.4391	-49.1503	52	GO	false
3304102	Porciúncula	-20.9632	-42.0465	33	RJ	false
4120002	Porecatu	-22.7537	-51.3795	41	PR	false
2410207	Portalegre	-6.02064	-37.9865	24	RN	false
4314803	Portão	-29.7015	-51.2429	43	RS	false
5218052	Porteirão	-17.8143	-50.1653	52	GO	false
2311108	Porteiras	-7.52265	-39.114	23	CE	false
3152204	Porteirinha	-15.7404	-43.0281	31	MG	false
1505809	Portel	-1.93639	-50.8194	15	PA	false
5218102	Portelândia	-17.3554	-52.6799	52	GO	false
2208502	Porto	-3.88815	-42.6998	22	PI	false
1200807	Porto Acre	-9.58138	-67.5478	12	AC	false
4314902	Porto Alegre	-30.0318	-51.2065	43	RS	true 
5106778	Porto Alegre do Norte	-10.8761	-51.6357	51	MT	false
2208551	Porto Alegre do Piauí	-6.96423	-44.1837	22	PI	false
1718006	Porto Alegre do Tocantins	-11.618	-47.0621	17	TO	false
4120101	Porto Amazonas	-25.54	-49.8946	41	PR	false
4120150	Porto Barreiro	-25.5477	-52.4067	41	PR	false
4213500	Porto Belo	-27.1586	-48.5469	42	SC	false
2707305	Porto Calvo	-9.05195	-35.3987	27	AL	false
2805604	Porto da Folha	-9.91626	-37.2842	28	SE	false
1505908	Porto de Moz	-1.74691	-52.2361	15	PA	false
2707404	Porto de Pedras	-9.16006	-35.3049	27	AL	false
2410256	Porto do Mangue	-5.05441	-36.7887	24	RN	false
5106802	Porto dos Gaúchos	-11.533	-57.4132	51	MT	false
5106828	Porto Esperidião	-15.857	-58.4619	51	MT	false
5106851	Porto Estrela	-15.3235	-57.2204	51	MT	false
3540606	Porto Feliz	-23.2093	-47.5251	35	SP	false
3540705	Porto Ferreira	-21.8498	-47.487	35	SP	false
3152303	Porto Firme	-20.6642	-43.0834	31	MG	false
2109007	Porto Franco	-6.34149	-47.3962	21	MA	false
1600535	Porto Grande	0.71243	-51.4155	16	AP	false
4315008	Porto Lucena	-27.8569	-55.01	43	RS	false
4315057	Porto Mauá	-27.5796	-54.6657	43	RS	false
5006903	Porto Murtinho	-21.6981	-57.8836	50	MS	false
1718204	Porto Nacional	-10.7027	-48.408	17	TO	false
3304110	Porto Real	-22.4175	-44.2952	33	RJ	false
2707503	Porto Real do Colégio	-10.1849	-36.8376	27	AL	false
4120200	Porto Rico	-22.7747	-53.2677	41	PR	false
2109056	Porto Rico do Maranhão	-1.85925	-44.5842	21	MA	false
2925303	Porto Seguro	-16.4435	-39.0643	29	BA	false
4213609	Porto União	-26.2451	-51.0759	42	SC	false
1100205	Porto Velho	-8.76077	-63.8999	11	RO	true 
4315073	Porto Vera Cruz	-27.7405	-54.8994	43	RS	false
4120309	Porto Vitória	-26.1674	-51.231	41	PR	false
1200393	Porto Walter	-8.26323	-72.7537	12	AC	false
4315107	Porto Xavier	-27.9082	-55.1379	43	RS	false
5218300	Posse	-14.0859	-46.3704	52	GO	false
3152402	Poté	-17.8077	-41.786	31	MG	false
2311207	Potengi	-7.09154	-40.0233	23	CE	false
3540754	Potim	-22.8343	-45.2552	35	SP	false
2925402	Potiraguá	-15.5943	-39.8638	29	BA	false
3540804	Potirendaba	-21.0428	-49.3815	35	SP	false
2311231	Potiretama	-5.71287	-38.1578	23	CE	false
3152501	Pouso Alegre	-22.2266	-45.9389	31	MG	false
3152600	Pouso Alto	-22.1964	-44.9748	31	MG	false
4315131	Pouso Novo	-29.1738	-52.2136	43	RS	false
4213708	Pouso Redondo	-27.2567	-49.9301	42	SC	false
5107008	Poxoréu	-15.8299	-54.4208	51	MT	false
3540853	Pracinha	-21.8496	-51.0868	35	SP	false
1600550	Pracuúba	1.74543	-50.7892	16	AP	false
2925501	Prado	-17.3364	-39.2227	29	BA	false
4120333	Prado Ferreira	-23.0357	-51.4429	41	PR	false
3540903	Pradópolis	-21.3626	-48.0679	35	SP	false
3152709	Prados	-21.0597	-44.0778	31	MG	false
3541000	Praia Grande	-24.0084	-46.4121	35	SP	false
4213807	Praia Grande	-29.1918	-49.9525	42	SC	false
1718303	Praia Norte	-5.39281	-47.8111	17	TO	false
1506005	Prainha	-1.798	-53.4779	15	PA	false
4120358	Pranchita	-26.0209	-53.7397	41	PR	false
3152808	Prata	-19.3086	-48.9276	31	MG	false
2512200	Prata	-7.68826	-37.0801	25	PB	false
2208601	Prata do Piauí	-5.67265	-42.2046	22	PI	false
3541059	Pratânia	-22.8112	-48.6636	35	SP	false
3152907	Pratápolis	-20.7411	-46.8624	31	MG	false
3153004	Pratinha	-19.739	-46.3755	31	MG	false
3541109	Presidente Alves	-22.0999	-49.4381	35	SP	false
3541208	Presidente Bernardes	-22.0082	-51.5565	35	SP	false
3153103	Presidente Bernardes	-20.7656	-43.1895	31	MG	false
4213906	Presidente Castello Branco	-27.2218	-51.8089	42	SC	false
4120408	Presidente Castelo Branco	-23.2782	-52.1536	41	PR	false
2925600	Presidente Dutra	-11.2923	-41.9843	29	BA	false
2109106	Presidente Dutra	-5.2898	-44.495	21	MA	false
3541307	Presidente Epitácio	-21.7651	-52.1111	35	SP	false
1303536	Presidente Figueiredo	-2.02981	-60.0234	13	AM	false
4214003	Presidente Getúlio	-27.0474	-49.6246	42	SC	false
2925709	Presidente Jânio Quadros	-14.6885	-41.6798	29	BA	false
3153202	Presidente Juscelino	-18.6401	-44.06	31	MG	false
2109205	Presidente Juscelino	-2.91872	-44.0715	21	MA	false
1718402	Presidente Kennedy	-8.5406	-48.5062	17	TO	false
3204302	Presidente Kennedy	-21.0964	-41.0468	32	ES	false
3153301	Presidente Kubitschek	-18.6193	-43.5628	31	MG	false
4315149	Presidente Lucena	-29.5175	-51.1798	43	RS	false
1100254	Presidente Médici	-11.169	-61.8986	11	RO	false
2109239	Presidente Médici	-2.38991	-45.82	21	MA	false
4214102	Presidente Nereu	-27.2768	-49.3889	42	SC	false
3153400	Presidente Olegário	-18.4096	-46.4165	31	MG	false
3541406	Presidente Prudente	-22.1207	-51.3925	35	SP	false
2109270	Presidente Sarney	-2.58799	-45.3595	21	MA	false
2925758	Presidente Tancredo Neves	-13.4471	-39.4203	29	BA	false
2109304	Presidente Vargas	-3.40787	-44.0234	21	MA	false
3541505	Presidente Venceslau	-21.8732	-51.8447	35	SP	false
2611408	Primavera	-8.32999	-35.3544	26	PE	false
1506104	Primavera	-0.945439	-47.1253	15	PA	false
1101476	Primavera de Rondônia	-11.8295	-61.3153	11	RO	false
5107040	Primavera do Leste	-15.544	-54.2811	51	MT	false
2109403	Primeira Cruz	-2.50568	-43.4232	21	MA	false
4120507	Primeiro de Maio	-22.8517	-51.0293	41	PR	false
4214151	Princesa	-26.4441	-53.5994	42	SC	false
2512309	Princesa Isabel	-7.73175	-37.9886	25	PB	false
5218391	Professor Jamil	-17.2497	-49.244	52	GO	false
4315156	Progresso	-29.2441	-52.3197	43	RS	false
3541604	Promissão	-21.5356	-49.8599	35	SP	false
2805703	Propriá	-10.2138	-36.8442	28	SE	false
4315172	Protásio Alves	-28.7572	-51.4757	43	RS	false
3153608	Prudente de Morais	-19.4742	-44.1591	31	MG	false
4120606	Prudentópolis	-25.2111	-50.9754	41	PR	false
1718451	Pugmil	-10.424	-48.8957	17	TO	false
2410405	Pureza	-5.46393	-35.5554	24	RN	false
4315206	Putinga	-29.0045	-52.1569	43	RS	false
2512408	Puxinanã	-7.15479	-35.9543	25	PB	false
3541653	Quadra	-23.2993	-48.0547	35	SP	false
4315305	Quaraí	-30.384	-56.4483	43	RS	false
3153707	Quartel Geral	-19.2703	-45.5569	31	MG	false
4120655	Quarto Centenário	-24.2775	-53.0759	41	PR	false
3541703	Quatá	-22.2456	-50.6966	35	SP	false
4120705	Quatiguá	-23.5671	-49.916	41	PR	false
1506112	Quatipuru	-0.899604	-47.0134	15	PA	false
3304128	Quatis	-22.4045	-44.2597	33	RJ	false
4120804	Quatro Barras	-25.3673	-49.0763	41	PR	false
4315313	Quatro Irmãos	-27.8257	-52.4424	43	RS	false
4120853	Quatro Pontes	-24.5752	-53.9759	41	PR	false
2707602	Quebrangulo	-9.32001	-36.4692	27	AL	false
4120903	Quedas do Iguaçu	-25.4492	-52.9102	41	PR	false
2208650	Queimada Nova	-8.57064	-41.4106	22	PI	false
2512507	Queimadas	-7.35029	-35.9031	25	PB	false
2925808	Queimadas	-10.9736	-39.6293	29	BA	false
3304144	Queimados	-22.7102	-43.5518	33	RJ	false
3541802	Queiroz	-21.7969	-50.2415	35	SP	false
3541901	Queluz	-22.5312	-44.7781	35	SP	false
3153806	Queluzito	-20.7416	-43.8851	31	MG	false
5107065	Querência	-12.6093	-52.1821	51	MT	false
4121000	Querência do Norte	-23.0838	-53.483	41	PR	false
4315321	Quevedos	-29.3504	-54.0789	43	RS	false
2925907	Quijingue	-10.7505	-39.2137	29	BA	false
4214201	Quilombo	-26.7264	-52.724	42	SC	false
4121109	Quinta do Sol	-23.8533	-52.1309	41	PR	false
3542008	Quintana	-22.0692	-50.307	35	SP	false
4315354	Quinze de Novembro	-28.7466	-53.1011	43	RS	false
2611507	Quipapá	-8.81175	-36.0137	26	PE	false
5218508	Quirinópolis	-18.4472	-50.4547	52	GO	false
3304151	Quissamã	-22.1031	-41.4693	33	RJ	false
4121208	Quitandinha	-25.8734	-49.4973	41	PR	false
2311264	Quiterianópolis	-5.8425	-40.7002	23	CE	false
2512606	Quixabá	-7.0224	-37.1458	25	PB	false
2611533	Quixaba	-7.70734	-37.8446	26	PE	false
2925931	Quixabeira	-11.4031	-40.12	29	BA	false
2311306	Quixadá	-4.9663	-39.0155	23	CE	false
2311355	Quixelô	-6.24637	-39.2011	23	CE	false
2311405	Quixeramobim	-5.19067	-39.2889	23	CE	false
2311504	Quixeré	-5.07148	-37.9802	23	CE	false
2410504	Rafael Fernandes	-6.18987	-38.2211	24	RN	false
2410603	Rafael Godeiro	-6.07244	-37.716	24	RN	false
2925956	Rafael Jambeiro	-12.4053	-39.5007	29	BA	false
3542107	Rafard	-23.0105	-47.5318	35	SP	false
4121257	Ramilândia	-25.1195	-54.023	41	PR	false
3542206	Rancharia	-22.2269	-50.893	35	SP	false
4121307	Rancho Alegre	-23.0676	-50.9145	41	PR	false
4121356	Rancho Alegre D'Oeste	-24.3065	-52.9552	41	PR	false
4214300	Rancho Queimado	-27.6727	-49.0191	42	SC	false
2109452	Raposa	-2.4254	-44.0973	21	MA	false
3153905	Raposos	-19.9636	-43.8079	31	MG	false
3154002	Raul Soares	-20.1061	-42.4502	31	MG	false
4121406	Realeza	-25.7711	-53.526	41	PR	false
4121505	Rebouças	-25.6232	-50.6877	41	PR	false
2611606	Recife	-8.04666	-34.8771	26	PE	true 
3154101	Recreio	-21.5289	-42.4676	31	MG	false
1718501	Recursolândia	-8.7227	-47.2421	17	TO	false
1506138	Redenção	-8.02529	-50.0317	15	PA	false
2311603	Redenção	-4.21587	-38.7277	23	CE	false
3542305	Redenção da Serra	-23.2638	-45.5422	35	SP	false
2208700	Redenção do Gurguéia	-9.47937	-44.5811	22	PI	false
4315404	Redentora	-27.664	-53.6407	43	RS	false
3154150	Reduto	-20.2401	-41.9848	31	MG	false
2208809	Regeneração	-6.23115	-42.6842	22	PI	false
3542404	Regente Feijó	-22.2181	-51.3055	35	SP	false
3542503	Reginópolis	-21.8914	-49.2268	35	SP	false
3542602	Registro	-24.4979	-47.8449	35	SP	false
4315453	Relvado	-29.1164	-52.0778	43	RS	false
2926004	Remanso	-9.61944	-42.0848	29	BA	false
2512705	Remígio	-6.94992	-35.8011	25	PB	false
4121604	Renascença	-26.1588	-52.9703	41	PR	false
2311702	Reriutaba	-4.14191	-40.5759	23	CE	false
3304201	Resende	-22.4705	-44.4509	33	RJ	false
3154200	Resende Costa	-20.9171	-44.2407	31	MG	false
4121703	Reserva	-24.6492	-50.8466	41	PR	false
5107156	Reserva do Cabaçal	-15.0743	-58.4585	51	MT	false
4121752	Reserva do Iguaçu	-25.8319	-52.0272	41	PR	false
3154309	Resplendor	-19.3194	-41.2462	31	MG	false
3154408	Ressaquinha	-21.0642	-43.7598	31	MG	false
3542701	Restinga	-20.6056	-47.4833	35	SP	false
4315503	Restinga Sêca	-29.8188	-53.3807	43	RS	false
2926103	Retirolândia	-11.4832	-39.4234	29	BA	false
2512747	Riachão	-6.54269	-35.661	25	PB	false
2109502	Riachão	-7.35819	-46.6225	21	MA	false
2926202	Riachão das Neves	-11.7508	-44.9143	29	BA	false
2512754	Riachão do Bacamarte	-7.25347	-35.6693	25	PB	false
2805802	Riachão do Dantas	-11.0729	-37.731	28	SE	false
2926301	Riachão do Jacuípe	-11.8067	-39.3818	29	BA	false
2512762	Riachão do Poço	-7.14173	-35.2914	25	PB	false
1718550	Riachinho	-6.44005	-48.1371	17	TO	false
3154457	Riachinho	-16.2258	-45.9888	31	MG	false
2410702	Riacho da Cruz	-5.92654	-37.949	24	RN	false
2611705	Riacho das Almas	-8.13742	-35.8648	26	PE	false
2410801	Riacho de Santana	-6.25139	-38.3116	24	RN	false
2926400	Riacho de Santana	-13.6059	-42.9397	29	BA	false
2512788	Riacho de Santo Antônio	-7.68023	-36.157	25	PB	false
2512804	Riacho dos Cavalos	-6.44067	-37.6483	25	PB	false
3154507	Riacho dos Machados	-16.0091	-43.0488	31	MG	false
2208858	Riacho Frio	-10.1244	-44.9503	22	PI	false
2410900	Riachuelo	-5.82156	-35.8215	24	RN	false
2805901	Riachuelo	-10.735	-37.1966	28	SE	false
5218607	Rialma	-15.3145	-49.5814	52	GO	false
5218706	Rianápolis	-15.4456	-49.5114	52	GO	false
2109551	Ribamar Fiquene	-5.93067	-47.3888	21	MA	false
5007109	Ribas do Rio Pardo	-20.4445	-53.7588	50	MS	false
3542800	Ribeira	-24.6517	-49.0044	35	SP	false
2926509	Ribeira do Amparo	-11.0421	-38.4242	29	BA	false
2208874	Ribeira do Piauí	-7.69028	-42.7128	22	PI	false
2926608	Ribeira do Pombal	-10.8373	-38.5382	29	BA	false
2611804	Ribeirão	-8.50957	-35.3698	26	PE	false
3542909	Ribeirão Bonito	-22.0685	-48.182	35	SP	false
3543006	Ribeirão Branco	-24.2206	-48.7635	35	SP	false
5107180	Ribeirão Cascalheira	-12.9367	-51.8244	51	MT	false
4121802	Ribeirão Claro	-23.1941	-49.7597	41	PR	false
3543105	Ribeirão Corrente	-20.4579	-47.5904	35	SP	false
3154606	Ribeirão das Neves	-19.7621	-44.0844	31	MG	false
2926657	Ribeirão do Largo	-15.4508	-40.7441	29	BA	false
4121901	Ribeirão do Pinhal	-23.4091	-50.3601	41	PR	false
3543204	Ribeirão do Sul	-22.789	-49.933	35	SP	false
3543238	Ribeirão dos Índios	-21.8382	-51.6103	35	SP	false
3543253	Ribeirão Grande	-24.1011	-48.3679	35	SP	false
3543303	Ribeirão Pires	-23.7067	-46.4058	35	SP	false
3543402	Ribeirão Preto	-21.1699	-47.8099	35	SP	false
3154705	Ribeirão Vermelho	-21.1879	-45.0637	31	MG	false
5107198	Ribeirãozinho	-16.4856	-52.6924	51	MT	false
2208908	Ribeiro Gonçalves	-7.55651	-45.2447	22	PI	false
2806008	Ribeirópolis	-10.5357	-37.438	28	SE	false
3543600	Rifaina	-20.0803	-47.4291	35	SP	false
3543709	Rincão	-21.5894	-48.0728	35	SP	false
3543808	Rinópolis	-21.7284	-50.7239	35	SP	false
3154804	Rio Acima	-20.0876	-43.7878	31	MG	false
4122008	Rio Azul	-25.7306	-50.7985	41	PR	false
3204351	Rio Bananal	-19.2719	-40.3366	32	ES	false
4122107	Rio Bom	-23.7606	-51.4122	41	PR	false
3304300	Rio Bonito	-22.7181	-42.6276	33	RJ	false
4122156	Rio Bonito do Iguaçu	-25.4874	-52.5292	41	PR	false
5107206	Rio Branco	-15.2483	-58.1259	51	MT	false
1200401	Rio Branco	-9.97499	-67.8243	12	AC	true 
4122172	Rio Branco do Ivaí	-24.3244	-51.3187	41	PR	false
4122206	Rio Branco do Sul	-25.1892	-49.3115	41	PR	false
5007208	Rio Brilhante	-21.8033	-54.5427	50	MS	false
3154903	Rio Casca	-20.2285	-42.6462	31	MG	false
3304409	Rio Claro	-22.72	-44.1419	33	RJ	false
3543907	Rio Claro	-22.3984	-47.5546	35	SP	false
1100262	Rio Crespo	-9.69965	-62.9011	11	RO	false
1718659	Rio da Conceição	-11.3949	-46.8847	17	TO	false
4214409	Rio das Antas	-26.8946	-51.0674	42	SC	false
3304508	Rio das Flores	-22.1692	-43.5856	33	RJ	false
3304524	Rio das Ostras	-22.5174	-41.9475	33	RJ	false
3544004	Rio das Pedras	-22.8417	-47.6047	35	SP	false
2926707	Rio de Contas	-13.5852	-41.8048	29	BA	false
3304557	Rio de Janeiro	-22.9129	-43.2003	33	RJ	true 
2926806	Rio do Antônio	-14.4071	-42.0721	29	BA	false
4214508	Rio do Campo	-26.9452	-50.136	42	SC	false
2408953	Rio do Fogo	-5.2765	-35.3794	24	RN	false
4214607	Rio do Oeste	-27.1952	-49.7989	42	SC	false
2926905	Rio do Pires	-13.1185	-42.2902	29	BA	false
3155108	Rio do Prado	-16.6056	-40.5714	31	MG	false
4214805	Rio do Sul	-27.2156	-49.643	42	SC	false
3155009	Rio Doce	-20.2412	-42.8995	31	MG	false
1718709	Rio dos Bois	-9.34425	-48.5245	17	TO	false
4214706	Rio dos Cedros	-26.7398	-49.2718	42	SC	false
4315552	Rio dos Índios	-27.2973	-52.8417	43	RS	false
3155207	Rio Espera	-20.855	-43.4721	31	MG	false
2611903	Rio Formoso	-8.6592	-35.1532	26	PE	false
4214904	Rio Fortuna	-28.1244	-49.1068	42	SC	false
4315602	Rio Grande	-32.0349	-52.1071	43	RS	false
3544103	Rio Grande da Serra	-23.7437	-46.3971	35	SP	false
2209005	Rio Grande do Piauí	-7.78029	-43.1369	22	PI	false
2707701	Rio Largo	-9.47783	-35.8394	27	AL	false
3155306	Rio Manso	-20.2666	-44.3069	31	MG	false
1506161	Rio Maria	-7.31236	-50.0379	15	PA	false
4215000	Rio Negrinho	-26.2591	-49.5177	42	SC	false
5007307	Rio Negro	-19.447	-54.9859	50	MS	false
4122305	Rio Negro	-26.095	-49.7982	41	PR	false
3155405	Rio Novo	-21.4649	-43.1168	31	MG	false
3204401	Rio Novo do Sul	-20.8556	-40.9388	32	ES	false
3155504	Rio Paranaíba	-19.1861	-46.2455	31	MG	false
4315701	Rio Pardo	-29.988	-52.3711	43	RS	false
3155603	Rio Pardo de Minas	-15.616	-42.5405	31	MG	false
3155702	Rio Piracicaba	-19.9284	-43.1829	31	MG	false
3155801	Rio Pomba	-21.2712	-43.1696	31	MG	false
3155900	Rio Preto	-22.0861	-43.8293	31	MG	false
1303569	Rio Preto da Eva	-2.7045	-59.6858	13	AM	false
5218789	Rio Quente	-17.774	-48.7725	52	GO	false
2927002	Rio Real	-11.4814	-37.9332	29	BA	false
4215059	Rio Rufino	-27.8592	-49.7754	42	SC	false
1718758	Rio Sono	-9.35002	-47.888	17	TO	false
2512903	Rio Tinto	-6.80383	-35.0776	25	PB	false
5218805	Rio Verde	-17.7923	-50.9192	52	GO	false
5007406	Rio Verde de Mato Grosso	-18.9249	-54.8434	50	MS	false
3156007	Rio Vermelho	-18.2922	-43.0018	31	MG	false
3544202	Riolândia	-19.9868	-49.6836	35	SP	false
4315750	Riozinho	-29.639	-50.4488	43	RS	false
4215075	Riqueza	-27.0653	-53.3265	42	SC	false
3156106	Ritápolis	-21.0276	-44.3204	31	MG	false
3543501	Riversul	-23.829	-49.429	35	SP	false
4315800	Roca Sales	-29.2884	-51.8658	43	RS	false
5007505	Rochedo	-19.9565	-54.8848	50	MS	false
3156205	Rochedo de Minas	-21.6284	-43.0165	31	MG	false
4215109	Rodeio	-26.9243	-49.3649	42	SC	false
4315909	Rodeio Bonito	-27.4742	-53.1706	43	RS	false
3156304	Rodeiro	-21.2035	-42.8586	31	MG	false
2927101	Rodelas	-8.85021	-38.78	29	BA	false
2411007	Rodolfo Fernandes	-5.78393	-38.0579	24	RN	false
1200427	Rodrigues Alves	-7.73864	-72.661	12	AC	false
4315958	Rolador	-28.2566	-54.8186	43	RS	false
4122404	Rolândia	-23.3101	-51.3659	41	PR	false
4316006	Rolante	-29.6462	-50.5819	43	RS	false
1100288	Rolim de Moura	-11.7271	-61.7714	11	RO	false
3156403	Romaria	-18.8838	-47.5782	31	MG	false
4215208	Romelândia	-26.6809	-53.3172	42	SC	false
4122503	Roncador	-24.5958	-52.2716	41	PR	false
4316105	Ronda Alta	-27.7758	-52.8056	43	RS	false
4316204	Rondinha	-27.8315	-52.9081	43	RS	false
5107578	Rondolândia	-10.8376	-61.4697	51	MT	false
4122602	Rondon	-23.412	-52.7659	41	PR	false
1506187	Rondon do Pará	-4.77793	-48.067	15	PA	false
5107602	Rondonópolis	-16.4673	-54.6372	51	MT	false
4316303	Roque Gonzales	-28.1297	-55.0266	43	RS	false
1400472	Rorainópolis	0.939956	-60.4389	14	RR	false
3544251	Rosana	-22.5782	-53.0603	35	SP	false
2109601	Rosário	-2.93444	-44.2531	21	MA	false
3156452	Rosário da Limeira	-20.9812	-42.5112	31	MG	false
2806107	Rosário do Catete	-10.6904	-37.0357	28	SE	false
4122651	Rosário do Ivaí	-24.2682	-51.272	41	PR	false
4316402	Rosário do Sul	-30.2515	-54.9221	43	RS	false
5107701	Rosário Oeste	-14.8259	-56.4236	51	MT	false
3544301	Roseira	-22.8938	-45.307	35	SP	false
2707800	Roteiro	-9.83503	-35.9782	27	AL	false
3156502	Rubelita	-16.4053	-42.261	31	MG	false
3544400	Rubiácea	-21.3006	-50.7296	35	SP	false
5218904	Rubiataba	-15.1617	-49.8048	52	GO	false
3156601	Rubim	-16.3775	-40.5397	31	MG	false
3544509	Rubinéia	-20.1759	-51.007	35	SP	false
1506195	Rurópolis	-4.10028	-54.9092	15	PA	false
2311801	Russas	-4.92673	-37.9721	23	CE	false
2411106	Ruy Barbosa	-5.88745	-35.933	24	RN	false
2927200	Ruy Barbosa	-12.2816	-40.4931	29	BA	false
3156700	Sabará	-19.884	-43.8263	31	MG	false
4122701	Sabáudia	-23.3155	-51.555	41	PR	false
3544608	Sabino	-21.4593	-49.5755	35	SP	false
3156809	Sabinópolis	-18.6653	-43.0752	31	MG	false
2311900	Saboeiro	-6.5346	-39.9017	23	CE	false
3156908	Sacramento	-19.8622	-47.4508	31	MG	false
4316428	Sagrada Família	-27.7085	-53.1351	43	RS	false
3544707	Sagres	-21.8823	-50.9594	35	SP	false
2612000	Sairé	-8.32864	-35.6967	26	PE	false
4316436	Saldanha Marinho	-28.3941	-53.097	43	RS	false
3544806	Sales	-21.3427	-49.4897	35	SP	false
3544905	Sales Oliveira	-20.7696	-47.8369	35	SP	false
3545001	Salesópolis	-23.5288	-45.8465	35	SP	false
4215307	Salete	-26.9798	-49.9988	42	SC	false
2513000	Salgadinho	-7.10098	-36.8458	25	PB	false
2612109	Salgadinho	-7.9269	-35.6503	26	PE	false
2806206	Salgado	-11.0288	-37.4804	28	SE	false
2513109	Salgado de São Félix	-7.35337	-35.4305	25	PB	false
4122800	Salgado Filho	-26.1777	-53.3631	41	PR	false
2612208	Salgueiro	-8.07373	-39.1247	26	PE	false
3157005	Salinas	-16.1753	-42.2964	31	MG	false
2927309	Salinas da Margarida	-12.873	-38.7562	29	BA	false
1506203	Salinópolis	-0.630815	-47.3465	15	PA	false
2311959	Salitre	-7.28398	-40.45	23	CE	false
3545100	Salmourão	-21.6267	-50.8614	35	SP	false
2612307	Saloá	-8.9723	-36.691	26	PE	false
4215356	Saltinho	-26.6049	-53.0578	42	SC	false
3545159	Saltinho	-22.8442	-47.6754	35	SP	false
3545209	Salto	-23.1996	-47.2931	35	SP	false
3157104	Salto da Divisa	-16.0063	-39.9391	31	MG	false
3545308	Salto de Pirapora	-23.6474	-47.5743	35	SP	false
5107750	Salto do Céu	-15.1303	-58.1317	51	MT	false
4122909	Salto do Itararé	-23.6074	-49.6354	41	PR	false
4316451	Salto do Jacuí	-29.0951	-53.2133	43	RS	false
4123006	Salto do Lontra	-25.7813	-53.3135	41	PR	false
3545407	Salto Grande	-22.8894	-49.9831	35	SP	false
4215406	Salto Veloso	-26.903	-51.4043	42	SC	false
2927408	Salvador	-12.9718	-38.5011	29	BA	true 
4316477	Salvador das Missões	-28.1233	-54.8373	43	RS	false
4316501	Salvador do Sul	-29.4386	-51.5077	43	RS	false
1506302	Salvaterra	-0.758444	-48.5139	15	PA	false
2109700	Sambaíba	-7.13447	-45.3515	21	MA	false
1718808	Sampaio	-5.35423	-47.8782	17	TO	false
4316600	Sananduva	-27.947	-51.8079	43	RS	false
5219001	Sanclerlândia	-16.197	-50.3124	52	GO	false
1718840	Sandolândia	-12.538	-49.9242	17	TO	false
3545506	Sandovalina	-22.4551	-51.7648	35	SP	false
4215455	Sangão	-28.6326	-49.1322	42	SC	false
2612406	Sanharó	-8.36097	-36.5696	26	PE	false
4317103	Sant'Ana do Livramento	-30.8773	-55.5392	43	RS	false
3545605	Santa Adélia	-21.2427	-48.8063	35	SP	false
3545704	Santa Albertina	-20.0311	-50.7297	35	SP	false
4123105	Santa Amélia	-23.2654	-50.4288	41	PR	false
2927507	Santa Bárbara	-11.9515	-38.9681	29	BA	false
3157203	Santa Bárbara	-19.9604	-43.4101	31	MG	false
3545803	Santa Bárbara d'Oeste	-22.7553	-47.4143	35	SP	false
5219100	Santa Bárbara de Goiás	-16.5714	-49.6954	52	GO	false
3157252	Santa Bárbara do Leste	-19.9753	-42.1457	31	MG	false
3157278	Santa Bárbara do Monte Verde	-21.9592	-43.7027	31	MG	false
1506351	Santa Bárbara do Pará	-1.19219	-48.238	15	PA	false
4316709	Santa Bárbara do Sul	-28.3653	-53.251	43	RS	false
3157302	Santa Bárbara do Tugúrio	-21.2431	-43.5607	31	MG	false
3546009	Santa Branca	-23.3933	-45.8875	35	SP	false
2927606	Santa Brígida	-9.73227	-38.1209	29	BA	false
5107248	Santa Carmem	-11.9125	-55.2263	51	MT	false
4215505	Santa Cecília	-26.9592	-50.4252	42	SC	false
2513158	Santa Cecília	-7.7389	-35.8764	25	PB	false
4123204	Santa Cecília do Pavão	-23.5201	-50.7835	41	PR	false
4316733	Santa Cecília do Sul	-28.1609	-51.9279	43	RS	false
3546108	Santa Clara d'Oeste	-20.09	-50.9491	35	SP	false
4316758	Santa Clara do Sul	-29.4747	-52.0843	43	RS	false
2411205	Santa Cruz	-6.22475	-36.0193	24	RN	false
2513208	Santa Cruz	-6.5237	-38.0617	25	PB	false
2612455	Santa Cruz	-8.24153	-40.3434	26	PE	false
2927705	Santa Cruz Cabrália	-16.2825	-39.0295	29	BA	false
2612471	Santa Cruz da Baixa Verde	-7.81339	-38.1476	26	PE	false
3546207	Santa Cruz da Conceição	-22.1405	-47.4512	35	SP	false
3546256	Santa Cruz da Esperança	-21.2951	-47.4304	35	SP	false
2927804	Santa Cruz da Vitória	-14.964	-39.8115	29	BA	false
3546306	Santa Cruz das Palmeiras	-21.8235	-47.248	35	SP	false
5219209	Santa Cruz de Goiás	-17.3155	-48.4809	52	GO	false
3157336	Santa Cruz de Minas	-21.1241	-44.2202	31	MG	false
4123303	Santa Cruz de Monte Castelo	-22.9582	-53.2949	41	PR	false
3157377	Santa Cruz de Salinas	-16.0967	-41.7418	31	MG	false
1506401	Santa Cruz do Arari	-0.661019	-49.1771	15	PA	false
2612505	Santa Cruz do Capibaribe	-7.94802	-36.2061	26	PE	false
3157401	Santa Cruz do Escalvado	-20.2372	-42.8169	31	MG	false
2209104	Santa Cruz do Piauí	-7.1785	-41.7609	22	PI	false
3546405	Santa Cruz do Rio Pardo	-22.8988	-49.6354	35	SP	false
4316808	Santa Cruz do Sul	-29.722	-52.4343	43	RS	false
5107743	Santa Cruz do Xingu	-10.1532	-52.3953	51	MT	false
2209153	Santa Cruz dos Milagres	-5.80581	-41.9506	22	PI	false
3157500	Santa Efigênia de Minas	-18.8235	-42.4388	31	MG	false
3546504	Santa Ernestina	-21.4618	-48.3953	35	SP	false
4123402	Santa Fé	-23.04	-51.808	41	PR	false
5219258	Santa Fé de Goiás	-15.7664	-51.1037	52	GO	false
3157609	Santa Fé de Minas	-16.6859	-45.4102	31	MG	false
1718865	Santa Fé do Araguaia	-7.15803	-48.7165	17	TO	false
3546603	Santa Fé do Sul	-20.2083	-50.932	35	SP	false
2209203	Santa Filomena	-9.11228	-45.9116	22	PI	false
2612554	Santa Filomena	-8.16688	-40.6079	26	PE	false
2109759	Santa Filomena do Maranhão	-5.49671	-44.5638	21	MA	false
3546702	Santa Gertrudes	-22.4572	-47.5272	35	SP	false
4123501	Santa Helena	-24.8585	-54.336	41	PR	false
4215554	Santa Helena	-26.937	-53.6214	42	SC	false
2109809	Santa Helena	-2.24426	-45.29	21	MA	false
2513307	Santa Helena	-6.7176	-38.6427	25	PB	false
5219308	Santa Helena de Goiás	-17.8115	-50.5977	52	GO	false
3157658	Santa Helena de Minas	-16.9707	-40.6727	31	MG	false
2927903	Santa Inês	-13.2793	-39.814	29	BA	false
4123600	Santa Inês	-22.6376	-51.9024	41	PR	false
2513356	Santa Inês	-7.621	-38.554	25	PB	false
2109908	Santa Inês	-3.65112	-45.3774	21	MA	false
3546801	Santa Isabel	-23.3172	-46.2237	35	SP	false
5219357	Santa Isabel	-15.2958	-49.4259	52	GO	false
4123709	Santa Isabel do Ivaí	-23.0025	-53.1989	41	PR	false
1303601	Santa Isabel do Rio Negro	-0.410824	-65.0092	13	AM	false
4123808	Santa Izabel do Oeste	-25.8217	-53.4801	41	PR	false
1506500	Santa Izabel do Pará	-1.29686	-48.1606	15	PA	false
3157708	Santa Juliana	-19.3108	-47.5322	31	MG	false
3204500	Santa Leopoldina	-20.0999	-40.527	32	ES	false
3546900	Santa Lúcia	-21.685	-48.0885	35	SP	false
4123824	Santa Lúcia	-25.4104	-53.5638	41	PR	false
2209302	Santa Luz	-8.9488	-44.1296	22	PI	false
2110005	Santa Luzia	-4.06873	-45.69	21	MA	false
2928059	Santa Luzia	-15.4342	-39.3287	29	BA	false
3157807	Santa Luzia	-19.7548	-43.8497	31	MG	false
2513406	Santa Luzia	-6.86092	-36.9178	25	PB	false
1100296	Santa Luzia D'Oeste	-11.9074	-61.7777	11	RO	false
2806305	Santa Luzia do Itanhy	-11.3536	-37.4586	28	SE	false
2707909	Santa Luzia do Norte	-9.6037	-35.8232	27	AL	false
1506559	Santa Luzia do Pará	-1.52147	-46.9008	15	PA	false
2110039	Santa Luzia do Paruá	-2.51123	-45.7801	21	MA	false
3157906	Santa Margarida	-20.3839	-42.2519	31	MG	false
4316972	Santa Margarida do Sul	-30.3393	-54.0817	43	RS	false
4316907	Santa Maria	-29.6868	-53.8149	43	RS	false
2409332	Santa Maria	-5.83802	-35.6914	24	RN	false
2612604	Santa Maria da Boa Vista	-8.79766	-39.8241	26	PE	false
3547007	Santa Maria da Serra	-22.5661	-48.1593	35	SP	false
2928109	Santa Maria da Vitória	-13.3859	-44.2011	29	BA	false
1506583	Santa Maria das Barreiras	-8.85784	-49.7215	15	PA	false
3158003	Santa Maria de Itabira	-19.4431	-43.1064	31	MG	false
3204559	Santa Maria de Jetibá	-20.0253	-40.7439	32	ES	false
2612703	Santa Maria do Cambucá	-7.83676	-35.8941	26	PE	false
4316956	Santa Maria do Herval	-29.4902	-50.9919	43	RS	false
4123857	Santa Maria do Oeste	-24.9377	-51.8696	41	PR	false
1506609	Santa Maria do Pará	-1.35392	-47.5712	15	PA	false
3158102	Santa Maria do Salto	-16.2479	-40.1512	31	MG	false
3158201	Santa Maria do Suaçuí	-18.1896	-42.4139	31	MG	false
1718881	Santa Maria do Tocantins	-8.8046	-47.7887	17	TO	false
3304607	Santa Maria Madalena	-21.9547	-42.0098	33	RJ	false
4123907	Santa Mariana	-23.1465	-50.5167	41	PR	false
3547106	Santa Mercedes	-21.3495	-51.7564	35	SP	false
4123956	Santa Mônica	-23.108	-53.1103	41	PR	false
2312205	Santa Quitéria	-4.32608	-40.1523	23	CE	false
2110104	Santa Quitéria do Maranhão	-3.49308	-42.5688	21	MA	false
2110203	Santa Rita	-3.14241	-44.3211	21	MA	false
2513703	Santa Rita	-7.11724	-34.9753	25	PB	false
3547403	Santa Rita d'Oeste	-20.1414	-50.8358	35	SP	false
3159209	Santa Rita de Caldas	-22.0292	-46.3385	31	MG	false
2928406	Santa Rita de Cássia	-11.0063	-44.5255	29	BA	false
3159407	Santa Rita de Ibitipoca	-21.5658	-43.9163	31	MG	false
3159308	Santa Rita de Jacutinga	-22.1474	-44.0977	31	MG	false
3159357	Santa Rita de Minas	-19.876	-42.1363	31	MG	false
5219407	Santa Rita do Araguaia	-17.3269	-53.2012	52	GO	false
3159506	Santa Rita do Itueto	-19.3576	-41.3821	31	MG	false
5219456	Santa Rita do Novo Destino	-15.1351	-49.1203	52	GO	false
5007554	Santa Rita do Pardo	-21.3016	-52.8333	50	MS	false
3547502	Santa Rita do Passa Quatro	-21.7083	-47.478	35	SP	false
3159605	Santa Rita do Sapucaí	-22.2461	-45.7034	31	MG	false
1718899	Santa Rita do Tocantins	-10.8617	-48.9161	17	TO	false
5107768	Santa Rita do Trivelato	-13.8146	-55.2706	51	MT	false
4317202	Santa Rosa	-27.8702	-54.4796	43	RS	false
3159704	Santa Rosa da Serra	-19.5186	-45.9611	31	MG	false
5219506	Santa Rosa de Goiás	-16.084	-49.4953	52	GO	false
4215604	Santa Rosa de Lima	-28.0331	-49.133	42	SC	false
2806503	Santa Rosa de Lima	-10.6434	-37.1931	28	SE	false
3547601	Santa Rosa de Viterbo	-21.4776	-47.3622	35	SP	false
2209377	Santa Rosa do Piauí	-6.79581	-42.2814	22	PI	false
1200435	Santa Rosa do Purus	-9.44652	-70.4902	12	AC	false
4215653	Santa Rosa do Sul	-29.1313	-49.7109	42	SC	false
1718907	Santa Rosa do Tocantins	-11.4474	-48.1216	17	TO	false
3547650	Santa Salete	-20.2429	-50.6887	35	SP	false
3204609	Santa Teresa	-19.9363	-40.5979	32	ES	false
2928505	Santa Teresinha	-12.7697	-39.5215	29	BA	false
2513802	Santa Teresinha	-7.07964	-37.4435	25	PB	false
4317251	Santa Tereza	-29.1655	-51.7351	43	RS	false
5219605	Santa Tereza de Goiás	-13.7138	-49.0144	52	GO	false
4124020	Santa Tereza do Oeste	-25.0543	-53.6274	41	PR	false
1719004	Santa Tereza do Tocantins	-10.2746	-47.8033	17	TO	false
4215679	Santa Terezinha	-26.7813	-50.009	42	SC	false
5107776	Santa Terezinha	-10.4704	-50.514	51	MT	false
2612802	Santa Terezinha	-7.37696	-37.4787	26	PE	false
5219704	Santa Terezinha de Goiás	-14.4326	-49.7091	52	GO	false
4124053	Santa Terezinha de Itaipu	-25.4391	-54.402	41	PR	false
4215687	Santa Terezinha do Progresso	-26.624	-53.1997	42	SC	false
1720002	Santa Terezinha do Tocantins	-6.44438	-47.6684	17	TO	false
3159803	Santa Vitória	-18.8414	-50.1208	31	MG	false
4317301	Santa Vitória do Palmar	-33.525	-53.3717	43	RS	false
2928000	Santaluz	-11.2508	-39.375	29	BA	false
2928208	Santana	-12.9792	-44.0506	29	BA	false
1600600	Santana	-0.045434	-51.1729	16	AP	false
4317004	Santana da Boa Vista	-30.8697	-53.11	43	RS	false
3547205	Santana da Ponte Pensa	-20.2523	-50.8014	35	SP	false
3158300	Santana da Vargem	-21.2449	-45.5005	31	MG	false
3158409	Santana de Cataguases	-21.2893	-42.5524	31	MG	false
2513505	Santana de Mangueira	-7.54705	-38.3236	25	PB	false
3547304	Santana de Parnaíba	-23.4439	-46.9178	35	SP	false
3158508	Santana de Pirapama	-18.9962	-44.0409	31	MG	false
2312007	Santana do Acaraú	-3.46144	-40.2118	23	CE	false
1506708	Santana do Araguaia	-9.3281	-50.35	15	PA	false
2312106	Santana do Cariri	-7.17613	-39.7302	23	CE	false
3158607	Santana do Deserto	-21.9512	-43.1583	31	MG	false
3158706	Santana do Garambéu	-21.5983	-44.105	31	MG	false
2708006	Santana do Ipanema	-9.36999	-37.248	27	AL	false
4124004	Santana do Itararé	-23.7587	-49.6293	41	PR	false
3158805	Santana do Jacaré	-20.9007	-45.1285	31	MG	false
3158904	Santana do Manhuaçu	-20.1031	-41.9278	31	MG	false
2110237	Santana do Maranhão	-3.109	-42.4064	21	MA	false
2411403	Santana do Matos	-5.94605	-36.6578	24	RN	false
2708105	Santana do Mundaú	-9.17141	-36.2176	27	AL	false
3158953	Santana do Paraíso	-19.3661	-42.5446	31	MG	false
2209351	Santana do Piauí	-6.94696	-41.5178	22	PI	false
3159001	Santana do Riacho	-19.1662	-43.722	31	MG	false
2806404	Santana do São Francisco	-10.2922	-36.6105	28	SE	false
2411429	Santana do Seridó	-6.76643	-36.7312	24	RN	false
2513604	Santana dos Garrotes	-7.38162	-37.9819	25	PB	false
3159100	Santana dos Montes	-20.7868	-43.6949	31	MG	false
2928307	Santanópolis	-12.0311	-38.8694	29	BA	false
1506807	Santarém	-2.43849	-54.6996	15	PA	false
1506906	Santarém Novo	-0.93097	-47.3855	15	PA	false
4317400	Santiago	-29.1897	-54.8666	43	RS	false
4215695	Santiago do Sul	-26.6388	-52.6799	42	SC	false
5107263	Santo Afonso	-14.4945	-57.0091	51	MT	false
2928604	Santo Amaro	-12.5472	-38.7137	29	BA	false
4215703	Santo Amaro da Imperatriz	-27.6852	-48.7813	42	SC	false
2806602	Santo Amaro das Brotas	-10.7892	-37.0564	28	SE	false
2110278	Santo Amaro do Maranhão	-2.50068	-43.238	21	MA	false
3547700	Santo Anastácio	-21.9747	-51.6527	35	SP	false
3547809	Santo André	-23.6737	-46.5432	35	SP	false
2513851	Santo André	-7.22016	-36.6213	25	PB	false
4317509	Santo Ângelo	-28.3001	-54.2668	43	RS	false
2411502	Santo Antônio	-6.31195	-35.4739	24	RN	false
3547908	Santo Antônio da Alegria	-21.0864	-47.1464	35	SP	false
5219712	Santo Antônio da Barra	-17.5585	-50.6345	52	GO	false
4317608	Santo Antônio da Patrulha	-29.8268	-50.5175	43	RS	false
4124103	Santo Antônio da Platina	-23.2959	-50.0815	41	PR	false
4317707	Santo Antônio das Missões	-28.514	-55.2251	43	RS	false
5219738	Santo Antônio de Goiás	-16.4815	-49.3096	52	GO	false
2928703	Santo Antônio de Jesus	-12.9614	-39.2584	29	BA	false
2209401	Santo Antônio de Lisboa	-6.98676	-41.2252	22	PI	false
3304706	Santo Antônio de Pádua	-21.541	-42.1832	33	RJ	false
3548005	Santo Antônio de Posse	-22.6029	-46.9192	35	SP	false
3159902	Santo Antônio do Amparo	-20.943	-44.9176	31	MG	false
3548054	Santo Antônio do Aracanguá	-20.9331	-50.498	35	SP	false
3160009	Santo Antônio do Aventureiro	-21.7606	-42.8115	31	MG	false
4124202	Santo Antônio do Caiuá	-22.7351	-52.344	41	PR	false
5219753	Santo Antônio do Descoberto	-15.9412	-48.2578	52	GO	false
3160108	Santo Antônio do Grama	-20.3185	-42.6047	31	MG	false
1303700	Santo Antônio do Içá	-3.09544	-67.9463	13	AM	false
3160207	Santo Antônio do Itambé	-18.4609	-43.3006	31	MG	false
3160306	Santo Antônio do Jacinto	-16.5332	-40.1817	31	MG	false
3548104	Santo Antônio do Jardim	-22.1121	-46.6845	35	SP	false
5107792	Santo Antônio do Leste	-14.805	-53.6075	51	MT	false
5107800	Santo Antônio do Leverger	-15.8632	-56.0788	51	MT	false
3160405	Santo Antônio do Monte	-20.085	-45.2947	31	MG	false
4317558	Santo Antônio do Palma	-28.4956	-52.0267	43	RS	false
4124301	Santo Antônio do Paraíso	-23.4969	-50.6455	41	PR	false
3548203	Santo Antônio do Pinhal	-22.827	-45.663	35	SP	false
4317756	Santo Antônio do Planalto	-28.403	-52.6992	43	RS	false
3160454	Santo Antônio do Retiro	-15.3393	-42.6171	31	MG	false
3160504	Santo Antônio do Rio Abaixo	-19.2374	-43.2604	31	MG	false
4124400	Santo Antônio do Sudoeste	-26.0737	-53.7251	41	PR	false
1507003	Santo Antônio do Tauá	-1.1522	-48.1314	15	PA	false
2110302	Santo Antônio dos Lopes	-4.86613	-44.3653	21	MA	false
2209450	Santo Antônio dos Milagres	-6.04647	-42.7123	22	PI	false
4317806	Santo Augusto	-27.8526	-53.7776	43	RS	false
4317905	Santo Cristo	-27.8263	-54.662	43	RS	false
2928802	Santo Estêvão	-12.428	-39.2505	29	BA	false
3548302	Santo Expedito	-21.8467	-51.3929	35	SP	false
4317954	Santo Expedito do Sul	-27.9074	-51.6434	43	RS	false
3160603	Santo Hipólito	-18.2968	-44.2229	31	MG	false
4124509	Santo Inácio	-22.6957	-51.7969	41	PR	false
2209500	Santo Inácio do Piauí	-7.42072	-41.9063	22	PI	false
3548401	Santópolis do Aguapeí	-21.6376	-50.5044	35	SP	false
3548500	Santos	-23.9535	-46.335	35	SP	false
3160702	Santos Dumont	-21.4634	-43.5499	31	MG	false
2312304	São Benedito	-4.04713	-40.8596	23	CE	false
2110401	São Benedito do Rio Preto	-3.33515	-43.5287	21	MA	false
2612901	São Benedito do Sul	-8.8166	-35.9453	26	PE	false
2513927	São Bentinho	-6.88596	-37.7243	25	PB	false
2513901	São Bento	-6.48529	-37.4488	25	PB	false
2110500	São Bento	-2.69781	-44.8289	21	MA	false
3160801	São Bento Abade	-21.5839	-45.0699	31	MG	false
2411601	São Bento do Norte	-5.09259	-35.9587	24	RN	false
3548609	São Bento do Sapucaí	-22.6837	-45.7287	35	SP	false
4215802	São Bento do Sul	-26.2495	-49.3831	42	SC	false
1720101	São Bento do Tocantins	-6.0258	-47.9012	17	TO	false
2411700	São Bento do Trairí	-6.33798	-36.0863	24	RN	false
2613008	São Bento do Una	-8.52637	-36.4465	26	PE	false
4215752	São Bernardino	-26.4739	-52.9687	42	SC	false
2110609	São Bernardo	-3.37223	-42.4191	21	MA	false
3548708	São Bernardo do Campo	-23.6914	-46.5646	35	SP	false
4215901	São Bonifácio	-27.9009	-48.9326	42	SC	false
4318002	São Borja	-28.6578	-56.0036	43	RS	false
2708204	São Brás	-10.1141	-36.8522	27	AL	false
3160900	São Brás do Suaçuí	-20.6242	-43.9515	31	MG	false
2209559	São Braz do Piauí	-9.05797	-43.0076	22	PI	false
2613107	São Caetano	-8.33763	-36.2869	26	PE	false
1507102	São Caetano de Odivelas	-0.747293	-48.0246	15	PA	false
3548807	São Caetano do Sul	-23.6229	-46.5548	35	SP	false
3548906	São Carlos	-22.0174	-47.886	35	SP	false
4216008	São Carlos	-27.0798	-53.0037	42	SC	false
4124608	São Carlos do Ivaí	-23.3158	-52.4761	41	PR	false
2806701	São Cristóvão	-11.0084	-37.2044	28	SE	false
4216057	São Cristovão do Sul	-27.2666	-50.4388	42	SC	false
2928901	São Desidério	-12.3572	-44.9769	29	BA	false
2928950	São Domingos	-11.4649	-39.5268	29	BA	false
4216107	São Domingos	-26.5548	-52.5313	42	SC	false
2513968	São Domingos	-6.80313	-37.9488	25	PB	false
2806800	São Domingos	-10.7916	-37.5685	28	SE	false
5219803	São Domingos	-13.621	-46.7415	52	GO	false
3160959	São Domingos das Dores	-19.5246	-42.0106	31	MG	false
1507151	São Domingos do Araguaia	-5.53732	-48.7366	15	PA	false
2110658	São Domingos do Azeitão	-6.81471	-44.6509	21	MA	false
1507201	São Domingos do Capim	-1.68768	-47.7665	15	PA	false
2513943	São Domingos do Cariri	-7.63273	-36.4374	25	PB	false
2110708	São Domingos do Maranhão	-5.58095	-44.3822	21	MA	false
3204658	São Domingos do Norte	-19.1452	-40.6281	32	ES	false
3161007	São Domingos do Prata	-19.8678	-42.971	31	MG	false
4318051	São Domingos do Sul	-28.5312	-51.886	43	RS	false
2929107	São Felipe	-12.8394	-39.0893	29	BA	false
1101484	São Felipe D'Oeste	-11.9023	-61.5026	11	RO	false
2929008	São Félix	-12.6104	-38.9727	29	BA	false
2110807	São Félix de Balsas	-7.07535	-44.8092	21	MA	false
3161056	São Félix de Minas	-18.5959	-41.4889	31	MG	false
5107859	São Félix do Araguaia	-11.615	-50.6706	51	MT	false
2929057	São Félix do Coribe	-13.4019	-44.1837	29	BA	false
2209609	São Félix do Piauí	-5.93485	-42.1172	22	PI	false
1720150	São Félix do Tocantins	-10.1615	-46.6618	17	TO	false
1507300	São Félix do Xingu	-6.64254	-51.9904	15	PA	false
2411809	São Fernando	-6.37975	-37.1864	24	RN	false
3304805	São Fidélis	-21.6551	-41.756	33	RJ	false
3549003	São Francisco	-20.3623	-50.6952	35	SP	false
2513984	São Francisco	-6.60773	-38.0968	25	PB	false
2806909	São Francisco	-10.3442	-36.8869	28	SE	false
3161106	São Francisco	-15.9514	-44.8593	31	MG	false
4318101	São Francisco de Assis	-29.5547	-55.1253	43	RS	false
2209658	São Francisco de Assis do Piauí	-8.23599	-41.6873	22	PI	false
5219902	São Francisco de Goiás	-15.9256	-49.2605	52	GO	false
3304755	São Francisco de Itabapoana	-21.4702	-41.1091	33	RJ	false
4318200	São Francisco de Paula	-29.4404	-50.5828	43	RS	false
3161205	São Francisco de Paula	-20.7036	-44.9838	31	MG	false
3161304	São Francisco de Sales	-19.8611	-49.7727	31	MG	false
2110856	São Francisco do Brejão	-5.12584	-47.389	21	MA	false
2929206	São Francisco do Conde	-12.6183	-38.6786	29	BA	false
3161403	São Francisco do Glória	-20.7923	-42.2673	31	MG	false
1101492	São Francisco do Guaporé	-12.052	-63.568	11	RO	false
2110906	São Francisco do Maranhão	-6.25159	-42.8668	21	MA	false
2411908	São Francisco do Oeste	-5.97472	-38.1519	24	RN	false
1507409	São Francisco do Pará	-1.16963	-47.7917	15	PA	false
2209708	São Francisco do Piauí	-7.2463	-42.541	22	PI	false
4216206	São Francisco do Sul	-26.2579	-48.6344	42	SC	false
4318309	São Gabriel	-30.3337	-54.3217	43	RS	false
2929255	São Gabriel	-11.2175	-41.8843	29	BA	false
1303809	São Gabriel da Cachoeira	-0.11909	-67.084	13	AM	false
3204708	São Gabriel da Palha	-19.0182	-40.5365	32	ES	false
5007695	São Gabriel do Oeste	-19.3889	-54.5507	50	MS	false
3161502	São Geraldo	-20.9252	-42.8364	31	MG	false
3161601	São Geraldo da Piedade	-18.8411	-42.2867	31	MG	false
1507458	São Geraldo do Araguaia	-6.39471	-48.5592	15	PA	false
3161650	São Geraldo do Baixio	-18.9097	-41.363	31	MG	false
3304904	São Gonçalo	-22.8268	-43.0634	33	RJ	false
3161700	São Gonçalo do Abaeté	-18.3315	-45.8265	31	MG	false
2412005	São Gonçalo do Amarante	-5.79068	-35.3257	24	RN	false
2312403	São Gonçalo do Amarante	-3.60515	-38.9726	23	CE	false
2209757	São Gonçalo do Gurguéia	-10.0319	-45.3092	22	PI	false
3161809	São Gonçalo do Pará	-19.9822	-44.8593	31	MG	false
2209807	São Gonçalo do Piauí	-5.99393	-42.7095	22	PI	false
3161908	São Gonçalo do Rio Abaixo	-19.8221	-43.366	31	MG	false
3125507	São Gonçalo do Rio Preto	-18.0025	-43.3854	31	MG	false
3162005	São Gonçalo do Sapucaí	-21.8932	-45.5893	31	MG	false
2929305	São Gonçalo dos Campos	-12.4331	-38.9663	29	BA	false
3162104	São Gotardo	-19.3087	-46.0465	31	MG	false
4318408	São Jerônimo	-29.9716	-51.7251	43	RS	false
4124707	São Jerônimo da Serra	-23.7218	-50.7475	41	PR	false
4124806	São João	-25.8214	-52.7252	41	PR	false
2613206	São João	-8.87576	-36.3653	26	PE	false
2111003	São João Batista	-2.95398	-44.7953	21	MA	false
4216305	São João Batista	-27.2772	-48.8474	42	SC	false
3162203	São João Batista do Glória	-20.635	-46.508	31	MG	false
5220009	São João d'Aliança	-14.7048	-47.5228	52	GO	false
1400506	São João da Baliza	0.951659	-59.9133	14	RR	false
3305000	São João da Barra	-21.638	-41.0446	33	RJ	false
3549102	São João da Boa Vista	-21.9707	-46.7944	35	SP	false
2209856	São João da Canabrava	-6.81203	-41.3415	22	PI	false
2209872	São João da Fronteira	-3.95497	-41.2569	22	PI	false
3162252	São João da Lagoa	-16.8455	-44.3507	31	MG	false
3162302	São João da Mata	-21.928	-45.9297	31	MG	false
5220058	São João da Paraúna	-16.8126	-50.4092	52	GO	false
1507466	São João da Ponta	-0.857885	-47.918	15	PA	false
3162401	São João da Ponte	-15.9271	-44.0096	31	MG	false
2209906	São João da Serra	-5.51081	-41.8923	22	PI	false
4318424	São João da Urtiga	-27.8195	-51.8257	43	RS	false
2209955	São João da Varjota	-6.94082	-41.8889	22	PI	false
3549201	São João das Duas Pontes	-20.3879	-50.3792	35	SP	false
3162450	São João das Missões	-14.8859	-44.0922	31	MG	false
3549250	São João de Iracema	-20.5111	-50.3561	35	SP	false
3305109	São João de Meriti	-22.8058	-43.3729	33	RJ	false
1507474	São João de Pirabas	-0.780222	-47.181	15	PA	false
3162500	São João del Rei	-21.1311	-44.2526	31	MG	false
1507508	São João do Araguaia	-5.36334	-48.7926	15	PA	false
2209971	São João do Arraial	-3.8186	-42.4459	22	PI	false
4124905	São João do Caiuá	-22.8535	-52.3411	41	PR	false
2514008	São João do Cariri	-7.38168	-36.5345	25	PB	false
2111029	São João do Carú	-3.5503	-46.2507	21	MA	false
4216354	São João do Itaperiú	-26.6213	-48.7683	42	SC	false
4125001	São João do Ivaí	-23.9833	-51.8215	41	PR	false
2312502	São João do Jaguaribe	-5.27516	-38.2694	23	CE	false
3162559	São João do Manhuaçu	-20.3933	-42.1533	31	MG	false
3162575	São João do Manteninha	-18.723	-41.1628	31	MG	false
4216255	São João do Oeste	-27.0984	-53.5977	42	SC	false
3162609	São João do Oriente	-19.3384	-42.1575	31	MG	false
3162658	São João do Pacuí	-16.5373	-44.5134	31	MG	false
3162708	São João do Paraíso	-15.3168	-42.0213	31	MG	false
2111052	São João do Paraíso	-6.45634	-47.0594	21	MA	false
3549300	São João do Pau d'Alho	-21.2662	-51.6672	35	SP	false
2210003	São João do Piauí	-8.35466	-42.2559	22	PI	false
4318432	São João do Polêsine	-29.6194	-53.4439	43	RS	false
2500700	São João do Rio do Peixe	-6.72195	-38.4468	25	PB	false
2412104	São João do Sabugi	-6.71387	-37.2027	24	RN	false
2111078	São João do Soter	-5.10821	-43.8163	21	MA	false
4216404	São João do Sul	-29.2154	-49.8094	42	SC	false
2514107	São João do Tigre	-8.07703	-36.8547	25	PB	false
4125100	São João do Triunfo	-25.683	-50.2949	41	PR	false
2111102	São João dos Patos	-6.4934	-43.7036	21	MA	false
3162807	São João Evangelista	-18.548	-42.7655	31	MG	false
3162906	São João Nepomuceno	-21.5381	-43.0069	31	MG	false
4216503	São Joaquim	-28.2887	-49.9457	42	SC	false
3549409	São Joaquim da Barra	-20.5812	-47.8593	35	SP	false
3162922	São Joaquim de Bicas	-20.048	-44.2749	31	MG	false
2613305	São Joaquim do Monte	-8.43196	-35.8035	26	PE	false
4318440	São Jorge	-28.4984	-51.7064	43	RS	false
4125209	São Jorge d'Oeste	-25.7085	-52.9204	41	PR	false
4125308	São Jorge do Ivaí	-23.4336	-52.2929	41	PR	false
4125357	São Jorge do Patrocínio	-23.7647	-53.8823	41	PR	false
4216602	São José	-27.6136	-48.6366	42	SC	false
3162948	São José da Barra	-20.7178	-46.313	31	MG	false
3549508	São José da Bela Vista	-20.5935	-47.6424	35	SP	false
4125407	São José da Boa Vista	-23.9122	-49.6577	41	PR	false
2613404	São José da Coroa Grande	-8.88937	-35.1515	26	PE	false
2514206	São José da Lagoa Tapada	-6.93646	-38.1622	25	PB	false
2708303	São José da Laje	-9.01278	-36.0515	27	AL	false
3162955	São José da Lapa	-19.6971	-43.9586	31	MG	false
3163003	São José da Safira	-18.3243	-42.1431	31	MG	false
2708402	São José da Tapera	-9.55768	-37.3831	27	AL	false
3163102	São José da Varginha	-19.7006	-44.556	31	MG	false
2929354	São José da Vitória	-15.0787	-39.3437	29	BA	false
4318457	São José das Missões	-27.7789	-53.1226	43	RS	false
4125456	São José das Palmeiras	-24.8369	-54.0572	41	PR	false
2514305	São José de Caiana	-7.24636	-38.2989	25	PB	false
2514404	São José de Espinharas	-6.83974	-37.3214	25	PB	false
2412203	São José de Mipibu	-6.0773	-35.2417	24	RN	false
2514503	São José de Piranhas	-7.1187	-38.502	25	PB	false
2514552	São José de Princesa	-7.73633	-38.0894	25	PB	false
2111201	São José de Ribamar	-2.54704	-44.0597	21	MA	false
3305133	São José de Ubá	-21.3661	-41.9511	33	RJ	false
3163201	São José do Alegre	-22.3243	-45.5258	31	MG	false
3549607	São José do Barreiro	-22.6414	-44.5774	35	SP	false
2613503	São José do Belmonte	-7.85723	-38.7577	26	PE	false
2514602	São José do Bonfim	-7.1607	-37.3036	25	PB	false
2514651	São José do Brejo do Cruz	-6.21054	-37.3601	25	PB	false
3204807	São José do Calçado	-21.0274	-41.6636	32	ES	false
2412302	São José do Campestre	-6.31087	-35.7067	24	RN	false
4216701	São José do Cedro	-26.4561	-53.4955	42	SC	false
4216800	São José do Cerrito	-27.6602	-50.5733	42	SC	false
2210052	São José do Divino	-3.81411	-41.8308	22	PI	false
3163300	São José do Divino	-18.4793	-41.3907	31	MG	false
2613602	São José do Egito	-7.46945	-37.274	26	PE	false
3163409	São José do Goiabal	-19.9214	-42.7035	31	MG	false
4318465	São José do Herval	-29.052	-52.295	43	RS	false
4318481	São José do Hortêncio	-29.528	-51.245	43	RS	false
4318499	São José do Inhacorá	-27.7251	-54.1275	43	RS	false
2929370	São José do Jacuípe	-11.4137	-39.8669	29	BA	false
3163508	São José do Jacuri	-18.281	-42.6729	31	MG	false
3163607	São José do Mantimento	-20.0058	-41.7486	31	MG	false
4318507	São José do Norte	-32.0151	-52.0331	43	RS	false
4318606	São José do Ouro	-27.7707	-51.5966	43	RS	false
2210102	São José do Peixe	-7.48554	-42.5672	22	PI	false
2210201	São José do Piauí	-6.87194	-41.4731	22	PI	false
5107297	São José do Povo	-16.4549	-54.2487	51	MT	false
5107305	São José do Rio Claro	-13.4398	-56.7218	51	MT	false
3549706	São José do Rio Pardo	-21.5953	-46.8873	35	SP	false
3549805	São José do Rio Preto	-20.8113	-49.3758	35	SP	false
2514701	São José do Sabugi	-6.76295	-36.7972	25	PB	false
2412401	São José do Seridó	-6.44002	-36.8746	24	RN	false
4318614	São José do Sul	-29.5448	-51.4821	43	RS	false
3305158	São José do Vale do Rio Preto	-22.1525	-42.9327	33	RJ	false
5107354	São José do Xingu	-10.7982	-52.7486	51	MT	false
4318622	São José dos Ausentes	-28.7476	-50.0677	43	RS	false
2111250	São José dos Basílios	-5.05493	-44.5809	21	MA	false
3549904	São José dos Campos	-23.1896	-45.8841	35	SP	false
2514800	São José dos Cordeiros	-7.38775	-36.8085	25	PB	false
4125506	São José dos Pinhais	-25.5313	-49.2031	41	PR	false
5107107	São José dos Quatro Marcos	-15.6276	-58.1772	51	MT	false
2514453	São José dos Ramos	-7.25238	-35.3725	25	PB	false
2210300	São Julião	-7.08391	-40.8246	22	PI	false
4318705	São Leopoldo	-29.7545	-51.1498	43	RS	false
3163706	São Lourenço	-22.1166	-45.0506	31	MG	false
2613701	São Lourenço da Mata	-8.00684	-35.0124	26	PE	false
3549953	São Lourenço da Serra	-23.8491	-46.9432	35	SP	false
4216909	São Lourenço do Oeste	-26.3557	-52.8498	42	SC	false
2210359	São Lourenço do Piauí	-9.16463	-42.5496	22	PI	false
4318804	São Lourenço do Sul	-31.3564	-51.9715	43	RS	false
4217006	São Ludgero	-28.3144	-49.1806	42	SC	false
2111300	São Luís	-2.53874	-44.2825	21	MA	true 
5220108	São Luís de Montes Belos	-16.5211	-50.3726	52	GO	false
2312601	São Luís do Curu	-3.66976	-39.2391	23	CE	false
2210375	São Luis do Piauí	-6.81936	-41.3175	22	PI	false
2708501	São Luís do Quitunde	-9.31816	-35.5606	27	AL	false
2111409	São Luís Gonzaga do Maranhão	-4.38541	-44.6654	21	MA	false
1400605	São Luiz	1.01019	-60.0419	14	RR	false
5220157	São Luiz do Norte	-14.8608	-49.3285	52	GO	false
3550001	São Luiz do Paraitinga	-23.222	-45.3109	35	SP	false
4318903	São Luiz Gonzaga	-28.412	-54.9559	43	RS	false
2514909	São Mamede	-6.92386	-37.0954	25	PB	false
4125555	São Manoel do Paraná	-23.3941	-52.6454	41	PR	false
3550100	São Manuel	-22.7321	-48.5723	35	SP	false
4319000	São Marcos	-28.9677	-51.0696	43	RS	false
4217105	São Martinho	-28.1609	-48.9867	42	SC	false
4319109	São Martinho	-27.7112	-53.9699	43	RS	false
4319125	São Martinho da Serra	-29.5397	-53.859	43	RS	false
3204906	São Mateus	-18.7214	-39.8579	32	ES	false
2111508	São Mateus do Maranhão	-4.03736	-44.4707	21	MA	false
4125605	São Mateus do Sul	-25.8677	-50.384	41	PR	false
2412500	São Miguel	-6.20283	-38.4947	24	RN	false
3550209	São Miguel Arcanjo	-23.8782	-47.9935	35	SP	false
2210383	São Miguel da Baixa Grande	-5.85646	-42.1934	22	PI	false
4217154	São Miguel da Boa Vista	-26.687	-53.2511	42	SC	false
2929404	São Miguel das Matas	-13.0434	-39.4578	29	BA	false
4319158	São Miguel das Missões	-28.556	-54.5559	43	RS	false
2515005	São Miguel de Taipu	-7.24764	-35.2016	25	PB	false
2807006	São Miguel do Aleixo	-10.3847	-37.3836	28	SE	false
3163805	São Miguel do Anta	-20.7067	-42.7174	31	MG	false
5220207	São Miguel do Araguaia	-13.2731	-50.1634	52	GO	false
2210391	São Miguel do Fidalgo	-7.59713	-42.3676	22	PI	false
2412559	São Miguel do Gostoso	-5.12302	-35.6354	24	RN	false
1507607	São Miguel do Guamá	-1.61307	-47.4784	15	PA	false
1100320	São Miguel do Guaporé	-11.6953	-62.7192	11	RO	false
4125704	São Miguel do Iguaçu	-25.3492	-54.2405	41	PR	false
4217204	São Miguel do Oeste	-26.7242	-53.5163	42	SC	false
5220264	São Miguel do Passa Quatro	-17.0582	-48.662	52	GO	false
2210409	São Miguel do Tapuio	-5.49729	-41.3165	22	PI	false
1720200	São Miguel do Tocantins	-5.56305	-47.5743	17	TO	false
2708600	São Miguel dos Campos	-9.78301	-36.0971	27	AL	false
2708709	São Miguel dos Milagres	-9.26493	-35.3763	27	AL	false
4319208	São Nicolau	-28.1834	-55.2654	43	RS	false
5220280	São Patrício	-15.35	-49.818	52	GO	false
3550308	São Paulo	-23.5329	-46.6395	35	SP	true 
4319307	São Paulo das Missões	-28.0195	-54.9404	43	RS	false
1303908	São Paulo de Olivença	-3.47292	-68.9646	13	AM	false
2412609	São Paulo do Potengi	-5.8994	-35.7642	24	RN	false
2412708	São Pedro	-5.90559	-35.6317	24	RN	false
3550407	São Pedro	-22.5483	-47.9096	35	SP	false
2111532	São Pedro da Água Branca	-5.08472	-48.4291	21	MA	false
3305208	São Pedro da Aldeia	-22.8429	-42.1026	33	RJ	false
5107404	São Pedro da Cipa	-16.0109	-54.9176	51	MT	false
4319356	São Pedro da Serra	-29.4193	-51.5134	43	RS	false
3163904	São Pedro da União	-21.131	-46.6123	31	MG	false
4319364	São Pedro das Missões	-27.7706	-53.2513	43	RS	false
4217253	São Pedro de Alcântara	-27.5665	-48.8048	42	SC	false
4319372	São Pedro do Butiá	-28.1243	-54.8926	43	RS	false
4125753	São Pedro do Iguaçu	-24.9373	-53.8521	41	PR	false
4125803	São Pedro do Ivaí	-23.8634	-51.8568	41	PR	false
4125902	São Pedro do Paraná	-22.8239	-53.2241	41	PR	false
2210508	São Pedro do Piauí	-5.92078	-42.7192	22	PI	false
3164100	São Pedro do Suaçuí	-18.3609	-42.5981	31	MG	false
4319406	São Pedro do Sul	-29.6202	-54.1855	43	RS	false
3550506	São Pedro do Turvo	-22.7453	-49.7428	35	SP	false
2111573	São Pedro dos Crentes	-6.82389	-46.5319	21	MA	false
3164001	São Pedro dos Ferros	-20.1732	-42.5251	31	MG	false
2412807	São Rafael	-5.79791	-36.8778	24	RN	false
2111607	São Raimundo das Mangabeiras	-7.02183	-45.4809	21	MA	false
2111631	São Raimundo do Doca Bezerra	-5.11053	-45.0696	21	MA	false
2210607	São Raimundo Nonato	-9.01241	-42.6987	22	PI	false
2111672	São Roberto	-5.0231	-45.001	21	MA	false
3164209	São Romão	-16.3641	-45.0749	31	MG	false
3550605	São Roque	-23.5226	-47.1357	35	SP	false
3164308	São Roque de Minas	-20.249	-46.3639	31	MG	false
3204955	São Roque do Canaã	-19.7411	-40.6526	32	ES	false
1720259	São Salvador do Tocantins	-12.7458	-48.2352	17	TO	false
3550704	São Sebastião	-23.7951	-45.4143	35	SP	false
2708808	São Sebastião	-9.93043	-36.559	27	AL	false
4126009	São Sebastião da Amoreira	-23.4656	-50.7625	41	PR	false
3164407	São Sebastião da Bela Vista	-22.1583	-45.7546	31	MG	false
1507706	São Sebastião da Boa Vista	-1.71597	-49.5249	15	PA	false
3550803	São Sebastião da Grama	-21.7041	-46.8208	35	SP	false
3164431	São Sebastião da Vargem Alegre	-19.7477	-43.3679	31	MG	false
2515104	São Sebastião de Lagoa de Roça	-7.11034	-35.8678	25	PB	false
3305307	São Sebastião do Alto	-21.9578	-42.1328	33	RJ	false
3164472	São Sebastião do Anta	-19.5064	-41.985	31	MG	false
4319505	São Sebastião do Caí	-29.5885	-51.3749	43	RS	false
3164506	São Sebastião do Maranhão	-18.0873	-42.5659	31	MG	false
3164605	São Sebastião do Oeste	-20.2758	-45.0063	31	MG	false
3164704	São Sebastião do Paraíso	-20.9167	-46.9837	31	MG	false
2929503	São Sebastião do Passé	-12.5123	-38.4905	29	BA	false
3164803	São Sebastião do Rio Preto	-19.2959	-43.1757	31	MG	false
3164902	São Sebastião do Rio Verde	-22.2183	-44.9761	31	MG	false
1720309	São Sebastião do Tocantins	-5.26131	-48.2021	17	TO	false
1303957	São Sebastião do Uatumã	-2.55915	-57.8731	13	AM	false
2515203	São Sebastião do Umbuzeiro	-8.15289	-37.0138	25	PB	false
4319604	São Sepé	-30.1643	-53.5603	43	RS	false
3550902	São Simão	-21.4732	-47.5518	35	SP	false
5220405	São Simão	-18.996	-50.547	52	GO	false
3165206	São Thomé das Letras	-21.7218	-44.9849	31	MG	false
3165008	São Tiago	-20.9075	-44.5098	31	MG	false
3165107	São Tomás de Aquino	-20.7791	-47.0962	31	MG	false
4126108	São Tomé	-23.5349	-52.5901	41	PR	false
2412906	São Tomé	-5.96404	-36.0798	24	RN	false
4319703	São Valentim	-27.5583	-52.5237	43	RS	false
4319711	São Valentim do Sul	-29.0451	-51.7684	43	RS	false
1720499	São Valério	-11.9743	-48.2353	17	TO	false
4319737	São Valério do Sul	-27.7906	-53.9368	43	RS	false
4319752	São Vendelino	-29.3729	-51.3675	43	RS	false
3551009	São Vicente	-23.9574	-46.3883	35	SP	false
2413003	São Vicente	-6.21893	-36.6827	24	RN	false
3165305	São Vicente de Minas	-21.7042	-44.4431	31	MG	false
2515401	São Vicente do Seridó	-6.85426	-36.4122	25	PB	false
4319802	São Vicente do Sul	-29.6882	-54.6826	43	RS	false
2613800	São Vicente Ferrer	-7.58969	-35.4808	26	PE	false
2111706	São Vicente Ferrer	-2.89487	-44.8681	21	MA	false
2515302	Sapé	-7.09359	-35.228	25	PB	false
2929602	Sapeaçu	-12.7208	-39.1824	29	BA	false
5107875	Sapezal	-12.9892	-58.7645	51	MT	false
4319901	Sapiranga	-29.6349	-51.0064	43	RS	false
4126207	Sapopema	-23.9078	-50.5801	41	PR	false
3165404	Sapucaí-Mirim	-22.7409	-45.738	31	MG	false
1507755	Sapucaia	-6.94018	-49.6834	15	PA	false
3305406	Sapucaia	-21.9949	-42.9142	33	RJ	false
4320008	Sapucaia do Sul	-29.8276	-51.145	43	RS	false
3305505	Saquarema	-22.9292	-42.5099	33	RJ	false
4126256	Sarandi	-23.4441	-51.876	41	PR	false
4320107	Sarandi	-27.942	-52.9231	43	RS	false
3551108	Sarapuí	-23.6397	-47.8249	35	SP	false
3165503	Sardoá	-18.7828	-42.3629	31	MG	false
3551207	Sarutaiá	-23.2721	-49.4763	35	SP	false
3165537	Sarzedo	-20.0367	-44.1446	31	MG	false
2929701	Sátiro Dias	-11.5929	-38.5938	29	BA	false
2708907	Satuba	-9.56911	-35.8227	27	AL	false
2111722	Satubinha	-4.04913	-45.2457	21	MA	false
2929750	Saubara	-12.7387	-38.7625	29	BA	false
4126272	Saudade do Iguaçu	-25.6917	-52.6184	41	PR	false
4217303	Saudades	-26.9317	-53.0021	42	SC	false
2929800	Saúde	-10.9428	-40.4155	29	BA	false
4217402	Schroeder	-26.4116	-49.074	42	SC	false
2929909	Seabra	-12.4169	-41.7722	29	BA	false
4217501	Seara	-27.1564	-52.299	42	SC	false
3551306	Sebastianópolis do Sul	-20.6523	-49.925	35	SP	false
2210623	Sebastião Barros	-10.817	-44.8337	22	PI	false
2930006	Sebastião Laranjeiras	-14.571	-42.9434	29	BA	false
2210631	Sebastião Leal	-7.56803	-44.06	22	PI	false
4320206	Seberi	-27.4829	-53.4026	43	RS	false
4320230	Sede Nova	-27.6367	-53.9493	43	RS	false
4320263	Segredo	-29.3523	-52.9767	43	RS	false
4320305	Selbach	-28.6294	-52.9498	43	RS	false
5007802	Selvíria	-20.3637	-51.4192	50	MS	false
3165560	Sem-Peixe	-20.1008	-42.8483	31	MG	false
1200500	Sena Madureira	-9.06596	-68.6571	12	AC	false
2111748	Senador Alexandre Costa	-5.25096	-44.0533	21	MA	false
3165578	Senador Amaral	-22.5869	-46.1763	31	MG	false
5220454	Senador Canedo	-16.7084	-49.0914	52	GO	false
3165602	Senador Cortes	-21.7986	-42.9424	31	MG	false
2413102	Senador Elói de Souza	-6.03334	-35.6978	24	RN	false
3165701	Senador Firmino	-20.9158	-43.0904	31	MG	false
2413201	Senador Georgino Avelino	-6.1576	-35.1299	24	RN	false
1200450	Senador Guiomard	-10.1497	-67.7362	12	AC	false
3165800	Senador José Bento	-22.1633	-46.1792	31	MG	false
1507805	Senador José Porfírio	-4.31242	-51.5764	15	PA	false
2111763	Senador La Rocque	-5.4461	-47.2959	21	MA	false
3165909	Senador Modestino Gonçalves	-17.9465	-43.2172	31	MG	false
2312700	Senador Pompeu	-5.58244	-39.3704	23	CE	false
2708956	Senador Rui Palmeira	-9.46986	-37.4576	27	AL	false
2312809	Senador Sá	-3.35305	-40.4662	23	CE	false
4320321	Senador Salgado Filho	-28.025	-54.5507	43	RS	false
4126306	Sengés	-24.1129	-49.4616	41	PR	false
2930105	Senhor do Bonfim	-10.4594	-40.1865	29	BA	false
3166006	Senhora de Oliveira	-20.7972	-43.3394	31	MG	false
3166105	Senhora do Porto	-18.8909	-43.0799	31	MG	false
3166204	Senhora dos Remédios	-21.0351	-43.5812	31	MG	false
4320354	Sentinela do Sul	-30.6107	-51.5862	43	RS	false
2930204	Sento Sé	-9.74138	-41.8786	29	BA	false
4320404	Serafina Corrêa	-28.7126	-51.9352	43	RS	false
3166303	Sericita	-20.4748	-42.4828	31	MG	false
1101500	Seringueiras	-11.8055	-63.0182	11	RO	false
4320453	Sério	-29.3904	-52.2685	43	RS	false
3166402	Seritinga	-21.9134	-44.518	31	MG	false
3305554	Seropédica	-22.7526	-43.7155	33	RJ	false
3205002	Serra	-20.121	-40.3074	32	ES	false
4217550	Serra Alta	-26.7229	-53.0409	42	SC	false
3551405	Serra Azul	-21.3074	-47.5602	35	SP	false
3166501	Serra Azul de Minas	-18.3602	-43.1675	31	MG	false
2515500	Serra Branca	-7.48034	-36.666	25	PB	false
2410306	Serra Caiada	-6.10478	-35.7113	24	RN	false
2515609	Serra da Raiz	-6.68527	-35.4379	25	PB	false
3166600	Serra da Saudade	-19.4447	-45.795	31	MG	false
2413300	Serra de São Bento	-6.41762	-35.7033	24	RN	false
2413359	Serra do Mel	-5.17725	-37.0242	24	RN	false
1600055	Serra do Navio	0.901357	-52.0036	16	AP	false
2930154	Serra do Ramalho	-13.5659	-43.5929	29	BA	false
3166808	Serra do Salitre	-19.1083	-46.6961	31	MG	false
3166709	Serra dos Aimorés	-17.7872	-40.2453	31	MG	false
2930303	Serra Dourada	-12.759	-43.9504	29	BA	false
2515708	Serra Grande	-7.20957	-38.3647	25	PB	false
3551603	Serra Negra	-22.6139	-46.7033	35	SP	false
2413409	Serra Negra do Norte	-6.66031	-37.3996	24	RN	false
5107883	Serra Nova Dourada	-12.0896	-51.4025	51	MT	false
2930402	Serra Preta	-12.156	-39.3305	29	BA	false
2515807	Serra Redonda	-7.18622	-35.6842	25	PB	false
2613909	Serra Talhada	-7.98178	-38.289	26	PE	false
3551504	Serrana	-21.2043	-47.5952	35	SP	false
3166907	Serrania	-21.5441	-46.0417	31	MG	false
2111789	Serrano do Maranhão	-1.85229	-45.1207	21	MA	false
5220504	Serranópolis	-18.3067	-51.9586	52	GO	false
3166956	Serranópolis de Minas	-15.8176	-42.8732	31	MG	false
4126355	Serranópolis do Iguaçu	-25.3799	-54.0518	41	PR	false
3167004	Serranos	-21.8857	-44.5125	31	MG	false
2515906	Serraria	-6.81569	-35.6282	25	PB	false
2413508	Serrinha	-6.28181	-35.5	24	RN	false
2930501	Serrinha	-11.6584	-39.01	29	BA	false
2413557	Serrinha dos Pintos	-6.11087	-37.9548	24	RN	false
2614006	Serrita	-7.94041	-39.2951	26	PE	false
3167103	Serro	-18.5991	-43.3744	31	MG	false
2930600	Serrolândia	-11.4085	-40.2983	29	BA	false
4126405	Sertaneja	-23.0361	-50.8317	41	PR	false
2614105	Sertânia	-8.06847	-37.2684	26	PE	false
4126504	Sertanópolis	-23.0571	-51.0399	41	PR	false
4320503	Sertão	-27.9798	-52.2588	43	RS	false
4320552	Sertão Santana	-30.4562	-51.6017	43	RS	false
3551702	Sertãozinho	-21.1316	-47.9875	35	SP	false
2515930	Sertãozinho	-6.75127	-35.4372	25	PB	false
3551801	Sete Barras	-24.382	-47.9279	35	SP	false
4320578	Sete de Setembro	-28.1362	-54.4637	43	RS	false
3167202	Sete Lagoas	-19.4569	-44.2413	31	MG	false
5007703	Sete Quedas	-23.9705	-55.0398	50	MS	false
3165552	Setubinha	-17.6002	-42.1587	31	MG	false
4320602	Severiano de Almeida	-27.4362	-52.1217	43	RS	false
2413607	Severiano Melo	-5.77666	-37.957	24	RN	false
3551900	Severínia	-20.8108	-48.8054	35	SP	false
4217600	Siderópolis	-28.5955	-49.4314	42	SC	false
5007901	Sidrolândia	-20.9302	-54.9692	50	MS	false
2210656	Sigefredo Pacheco	-4.91665	-41.7311	22	PI	false
3305604	Silva Jardim	-22.6574	-42.3961	33	RJ	false
5220603	Silvânia	-16.66	-48.6083	52	GO	false
1720655	Silvanópolis	-11.1471	-48.1694	17	TO	false
4320651	Silveira Martins	-29.6467	-53.591	43	RS	false
3167301	Silveirânia	-21.1615	-43.2128	31	MG	false
3552007	Silveiras	-22.6638	-44.8522	35	SP	false
1304005	Silves	-2.81748	-58.248	13	AM	false
3167400	Silvianópolis	-22.0274	-45.8385	31	MG	false
2807105	Simão Dias	-10.7387	-37.8097	28	SE	false
3167509	Simão Pereira	-21.964	-43.3088	31	MG	false
2210706	Simões	-7.59109	-40.8137	22	PI	false
2930709	Simões Filho	-12.7866	-38.4029	29	BA	false
5220686	Simolândia	-14.4644	-46.4847	52	GO	false
3167608	Simonésia	-20.1341	-42.0091	31	MG	false
2210805	Simplício Mendes	-7.85294	-41.9075	22	PI	false
4320677	Sinimbu	-29.5357	-52.5304	43	RS	false
5107909	Sinop	-11.8604	-55.5091	51	MT	false
4126603	Siqueira Campos	-23.6875	-49.8304	41	PR	false
2614204	Sirinhaém	-8.58778	-35.1126	26	PE	false
2807204	Siriri	-10.5965	-37.1131	28	SE	false
5220702	Sítio d'Abadia	-14.7992	-46.2506	52	GO	false
2930758	Sítio do Mato	-13.0801	-43.4689	29	BA	false
2930766	Sítio do Quinto	-10.3545	-38.2213	29	BA	false
2111805	Sítio Novo	-5.87601	-46.7033	21	MA	false
2413706	Sítio Novo	-6.11132	-35.909	24	RN	false
1720804	Sítio Novo do Tocantins	-5.6012	-47.6381	17	TO	false
2930774	Sobradinho	-9.45024	-40.8145	29	BA	false
4320701	Sobradinho	-29.4194	-53.0326	43	RS	false
2515971	Sobrado	-7.14429	-35.2357	25	PB	false
2312908	Sobral	-3.68913	-40.3482	23	CE	false
3167707	Sobrália	-19.2345	-42.0998	31	MG	false
3552106	Socorro	-22.5903	-46.5251	35	SP	false
2210904	Socorro do Piauí	-7.86773	-42.4922	22	PI	false
2516003	Solânea	-6.75161	-35.6636	25	PB	false
2516102	Soledade	-7.05829	-36.3668	25	PB	false
4320800	Soledade	-28.8306	-52.5131	43	RS	false
3167806	Soledade de Minas	-22.0554	-45.0464	31	MG	false
2614402	Solidão	-7.59472	-37.6445	26	PE	false
2313005	Solonópole	-5.71894	-39.0107	23	CE	false
4217709	Sombrio	-29.108	-49.6328	42	SC	false
5007935	Sonora	-17.5698	-54.7551	50	MS	false
3205010	Sooretama	-19.1897	-40.0974	32	ES	false
3552205	Sorocaba	-23.4969	-47.4451	35	SP	false
5107925	Sorriso	-12.5425	-55.7211	51	MT	false
2516151	Sossêgo	-6.77067	-36.2538	25	PB	false
1507904	Soure	-0.73032	-48.5015	15	PA	false
2516201	Sousa	-6.75148	-38.2311	25	PB	false
2930808	Souto Soares	-12.088	-41.6427	29	BA	false
1720853	Sucupira	-11.993	-48.9685	17	TO	false
2111904	Sucupira do Norte	-6.47839	-44.1919	21	MA	false
2111953	Sucupira do Riachão	-6.40858	-43.5455	21	MA	false
3552304	Sud Mennucci	-20.6872	-50.9238	35	SP	false
4217758	Sul Brasil	-26.7351	-52.964	42	SC	false
4126652	Sulina	-25.7066	-52.7299	41	PR	false
3552403	Sumaré	-22.8204	-47.2728	35	SP	false
2516300	Sumé	-7.66206	-36.884	25	PB	false
3305703	Sumidouro	-22.0485	-42.6761	33	RJ	false
2614501	Surubim	-7.84746	-35.7481	26	PE	false
2210938	Sussuapara	-7.03687	-41.3767	22	PI	false
3552551	Suzanápolis	-20.4981	-51.0268	35	SP	false
3552502	Suzano	-23.5448	-46.3112	35	SP	false
4320859	Tabaí	-29.643	-51.6823	43	RS	false
5107941	Tabaporã	-11.3007	-56.8312	51	MT	false
3552601	Tabapuã	-20.9602	-49.0307	35	SP	false
3552700	Tabatinga	-21.7239	-48.6896	35	SP	false
1304062	Tabatinga	-4.2416	-69.9383	13	AM	false
2614600	Tabira	-7.58366	-37.5377	26	PE	false
3552809	Taboão da Serra	-23.6019	-46.7526	35	SP	false
2930907	Tabocas do Brejo Velho	-12.7026	-44.0075	29	BA	false
2413805	Taboleiro Grande	-5.91948	-38.0367	24	RN	false
3167905	Tabuleiro	-21.3632	-43.2381	31	MG	false
2313104	Tabuleiro do Norte	-5.24353	-38.1282	23	CE	false
2614709	Tacaimbó	-8.30867	-36.3	26	PE	false
2614808	Tacaratu	-9.09798	-38.1504	26	PE	false
3552908	Taciba	-22.3866	-51.2882	35	SP	false
2516409	Tacima	-6.48759	-35.6367	25	PB	false
5007950	Tacuru	-23.636	-55.0141	50	MS	false
3553005	Taguaí	-23.4452	-49.4024	35	SP	false
1720903	Taguatinga	-12.4026	-46.437	17	TO	false
3553104	Taiaçu	-21.1431	-48.5112	35	SP	false
1507953	Tailândia	-2.94584	-48.9489	15	PA	false
4217808	Taió	-27.121	-49.9942	42	SC	false
3168002	Taiobeiras	-15.8106	-42.2259	31	MG	false
1720937	Taipas do Tocantins	-12.1873	-46.9797	17	TO	false
2413904	Taipu	-5.63058	-35.5918	24	RN	false
3553203	Taiúva	-21.1223	-48.4528	35	SP	false
1720978	Talismã	-12.7949	-49.0896	17	TO	false
2614857	Tamandaré	-8.75665	-35.1033	26	PE	false
4126678	Tamarana	-23.7204	-51.0991	41	PR	false
3553302	Tambaú	-21.7029	-47.2703	35	SP	false
4126702	Tamboara	-23.2036	-52.4743	41	PR	false
2313203	Tamboril	-4.83136	-40.3196	23	CE	false
2210953	Tamboril do Piauí	-8.40937	-42.9211	22	PI	false
3553401	Tanabi	-20.6228	-49.6563	35	SP	false
2414001	Tangará	-6.19649	-35.7989	24	RN	false
4217907	Tangará	-27.0996	-51.2473	42	SC	false
5107958	Tangará da Serra	-14.6229	-57.4933	51	MT	false
3305752	Tanguá	-22.7423	-42.7202	33	RJ	false
2931004	Tanhaçu	-14.0197	-41.2473	29	BA	false
2709004	Tanque d'Arca	-9.53379	-36.4366	27	AL	false
2210979	Tanque do Piauí	-6.59787	-42.2795	22	PI	false
2931053	Tanque Novo	-13.5485	-42.4934	29	BA	false
2931103	Tanquinho	-11.968	-39.1033	29	BA	false
3168051	Taparuba	-19.7621	-41.608	31	MG	false
1304104	Tapauá	-5.62085	-63.1808	13	AM	false
4126801	Tapejara	-23.7315	-52.8735	41	PR	false
4320909	Tapejara	-28.0652	-52.0097	43	RS	false
4321006	Tapera	-28.6277	-52.8613	43	RS	false
2931202	Taperoá	-13.5321	-39.1009	29	BA	false
2516508	Taperoá	-7.20629	-36.8245	25	PB	false
4321105	Tapes	-30.6683	-51.3991	43	RS	false
4126900	Tapira	-23.3193	-53.0684	41	PR	false
3168101	Tapira	-19.9166	-46.8264	31	MG	false
3168200	Tapiraí	-19.8936	-46.0221	31	MG	false
3553500	Tapiraí	-23.9612	-47.5062	35	SP	false
2931301	Tapiramutá	-11.8475	-40.7927	29	BA	false
3553609	Tapiratiba	-21.4713	-46.7448	35	SP	false
5108006	Tapurah	-12.695	-56.5178	51	MT	false
4321204	Taquara	-29.6505	-50.7753	43	RS	false
3168309	Taquaraçu de Minas	-19.6652	-43.6922	31	MG	false
3553658	Taquaral	-21.0737	-48.4126	35	SP	false
5221007	Taquaral de Goiás	-16.0521	-49.6039	52	GO	false
2709103	Taquarana	-9.64529	-36.4928	27	AL	false
4321303	Taquari	-29.7943	-51.8653	43	RS	false
3553708	Taquaritinga	-21.4049	-48.5103	35	SP	false
2615003	Taquaritinga do Norte	-7.89446	-36.0423	26	PE	false
3553807	Taquarituba	-23.5307	-49.241	35	SP	false
3553856	Taquarivaí	-23.9211	-48.6948	35	SP	false
4321329	Taquaruçu do Sul	-27.4005	-53.4702	43	RS	false
5007976	Taquarussu	-22.4898	-53.3519	50	MS	false
3553906	Tarabai	-22.3016	-51.5621	35	SP	false
1200609	Tarauacá	-8.15697	-70.7722	12	AC	false
2313252	Tarrafas	-6.67838	-39.753	23	CE	false
1600709	Tartarugalzinho	1.50652	-50.9087	16	AP	false
3553955	Tarumã	-22.7429	-50.5786	35	SP	false
3168408	Tarumirim	-19.2835	-42.0097	31	MG	false
2112001	Tasso Fragoso	-8.4662	-45.7536	21	MA	false
3554003	Tatuí	-23.3487	-47.8461	35	SP	false
2313302	Tauá	-5.98585	-40.2968	23	CE	false
3554102	Taubaté	-23.0104	-45.5593	35	SP	false
4321352	Tavares	-31.2843	-51.088	43	RS	false
2516607	Tavares	-7.62697	-37.8712	25	PB	false
1304203	Tefé	-3.36822	-64.7193	13	AM	false
2516706	Teixeira	-7.22104	-37.2525	25	PB	false
2931350	Teixeira de Freitas	-17.5399	-39.74	29	BA	false
4127007	Teixeira Soares	-25.3701	-50.4571	41	PR	false
3168507	Teixeiras	-20.6561	-42.8564	31	MG	false
1101559	Teixeirópolis	-10.9056	-62.242	11	RO	false
2313351	Tejuçuoca	-3.98831	-39.5799	23	CE	false
3554201	Tejupá	-23.3425	-49.3722	35	SP	false
4127106	Telêmaco Borba	-24.3245	-50.6176	41	PR	false
2807303	Telha	-10.2064	-36.8818	28	SE	false
2414100	Tenente Ananias	-6.45823	-38.182	24	RN	false
2414159	Tenente Laurentino Cruz	-6.1378	-36.7135	24	RN	false
4321402	Tenente Portela	-27.3711	-53.7585	43	RS	false
2516755	Tenório	-6.93855	-36.6273	25	PB	false
2931400	Teodoro Sampaio	-12.295	-38.6347	29	BA	false
3554300	Teodoro Sampaio	-22.5299	-52.1682	35	SP	false
2931509	Teofilândia	-11.4827	-38.9913	29	BA	false
3168606	Teófilo Otoni	-17.8595	-41.5087	31	MG	false
2931608	Teolândia	-13.5896	-39.484	29	BA	false
2709152	Teotônio Vilela	-9.91656	-36.3492	27	AL	false
5008008	Terenos	-20.4378	-54.8647	50	MS	false
2211001	Teresina	-5.09194	-42.8034	22	PI	true 
5221080	Teresina de Goiás	-13.7801	-47.2659	52	GO	false
3305802	Teresópolis	-22.4165	-42.9752	33	RJ	false
2615102	Terezinha	-9.05621	-36.6272	26	PE	false
5221197	Terezópolis de Goiás	-16.3945	-49.0797	52	GO	false
1507961	Terra Alta	-1.02963	-47.9004	15	PA	false
4127205	Terra Boa	-23.7683	-52.447	41	PR	false
4321436	Terra de Areia	-29.5782	-50.0644	43	RS	false
2931707	Terra Nova	-12.3888	-38.6238	29	BA	false
2615201	Terra Nova	-8.22244	-39.3825	26	PE	false
5108055	Terra Nova do Norte	-10.517	-55.231	51	MT	false
4127304	Terra Rica	-22.7111	-52.6188	41	PR	false
4127403	Terra Roxa	-24.1575	-54.0988	41	PR	false
3554409	Terra Roxa	-20.787	-48.3314	35	SP	false
1507979	Terra Santa	-2.10443	-56.4877	15	PA	false
5108105	Tesouro	-16.0809	-53.559	51	MT	false
4321451	Teutônia	-29.4482	-51.8044	43	RS	false
1101609	Theobroma	-10.2483	-62.3538	11	RO	false
2313401	Tianguá	-3.72965	-40.9923	23	CE	false
4127502	Tibagi	-24.5153	-50.4176	41	PR	false
2411056	Tibau	-4.83729	-37.2554	24	RN	false
2414209	Tibau do Sul	-6.19176	-35.0866	24	RN	false
3554508	Tietê	-23.1101	-47.7164	35	SP	false
4217956	Tigrinhos	-26.6876	-53.1545	42	SC	false
4218004	Tijucas	-27.2354	-48.6322	42	SC	false
4127601	Tijucas do Sul	-25.9311	-49.195	41	PR	false
2615300	Timbaúba	-7.50484	-35.3119	26	PE	false
2414308	Timbaúba dos Batistas	-6.45768	-37.2745	24	RN	false
4218103	Timbé do Sul	-28.8287	-49.842	42	SC	false
2112100	Timbiras	-4.25597	-43.932	21	MA	false
4218202	Timbó	-26.8246	-49.269	42	SC	false
4218251	Timbó Grande	-26.6127	-50.6607	42	SC	false
3554607	Timburi	-23.2057	-49.6096	35	SP	false
2112209	Timon	-5.09769	-42.8329	21	MA	false
3168705	Timóteo	-19.5811	-42.6471	31	MG	false
4321469	Tio Hugo	-28.5712	-52.5955	43	RS	false
3168804	Tiradentes	-21.1102	-44.1744	31	MG	false
4321477	Tiradentes do Sul	-27.4022	-54.0814	43	RS	false
3168903	Tiros	-19.0037	-45.9626	31	MG	false
2807402	Tobias Barreto	-11.1798	-37.9995	28	SE	false
1721109	Tocantínia	-9.5632	-48.3741	17	TO	false
1721208	Tocantinópolis	-6.32447	-47.4224	17	TO	false
3169000	Tocantins	-21.1774	-43.0127	31	MG	false
3169059	Tocos do Moji	-22.3698	-46.0971	31	MG	false
3169109	Toledo	-22.7421	-46.3728	31	MG	false
4127700	Toledo	-24.7246	-53.7412	41	PR	false
2807501	Tomar do Geru	-11.3694	-37.8433	28	SE	false
4127809	Tomazina	-23.7796	-49.9499	41	PR	false
3169208	Tombos	-20.9086	-42.0228	31	MG	false
1508001	Tomé-Açu	-2.41302	-48.1415	15	PA	false
1304237	Tonantins	-2.86582	-67.7919	13	AM	false
2615409	Toritama	-8.00955	-36.0637	26	PE	false
5108204	Torixoréu	-16.2006	-52.5571	51	MT	false
4321493	Toropi	-29.4782	-54.2244	43	RS	false
3554656	Torre de Pedra	-23.2462	-48.1955	35	SP	false
4321501	Torres	-29.3334	-49.7333	43	RS	false
3554706	Torrinha	-22.4237	-48.1731	35	SP	false
2414407	Touros	-5.20182	-35.4621	24	RN	false
3554755	Trabiju	-22.0388	-48.3342	35	SP	false
1508035	Tracuateua	-1.07653	-46.9031	15	PA	false
2615508	Tracunhaém	-7.80228	-35.2314	26	PE	false
2709202	Traipu	-9.96262	-37.0071	27	AL	false
1508050	Trairão	-4.57347	-55.9429	15	PA	false
2313500	Trairi	-3.26932	-39.2681	23	CE	false
3305901	Trajano de Moraes	-22.0638	-42.0643	33	RJ	false
4321600	Tramandaí	-29.9841	-50.1322	43	RS	false
4321626	Travesseiro	-29.2977	-52.0532	43	RS	false
2931806	Tremedal	-14.9736	-41.4142	29	BA	false
3554805	Tremembé	-22.9571	-45.5475	35	SP	false
4321634	Três Arroios	-27.5003	-52.1448	43	RS	false
4218301	Três Barras	-26.1056	-50.3197	42	SC	false
4127858	Três Barras do Paraná	-25.4185	-53.1833	41	PR	false
4321667	Três Cachoeiras	-29.4487	-49.9275	43	RS	false
3169307	Três Corações	-21.6921	-45.2511	31	MG	false
4321709	Três Coroas	-29.5137	-50.7739	43	RS	false
4321808	Três de Maio	-27.78	-54.2357	43	RS	false
4321832	Três Forquilhas	-29.5384	-50.0708	43	RS	false
3554904	Três Fronteiras	-20.2344	-50.8905	35	SP	false
5008305	Três Lagoas	-20.7849	-51.7007	50	MS	false
3169356	Três Marias	-18.2048	-45.2473	31	MG	false
4321857	Três Palmeiras	-27.6139	-52.8437	43	RS	false
4321907	Três Passos	-27.4555	-53.9296	43	RS	false
3169406	Três Pontas	-21.3694	-45.5109	31	MG	false
5221304	Três Ranchos	-18.3539	-47.776	52	GO	false
3306008	Três Rios	-22.1165	-43.2185	33	RJ	false
4218350	Treviso	-28.5097	-49.4634	42	SC	false
4218400	Treze de Maio	-28.5537	-49.1565	42	SC	false
4218509	Treze Tílias	-27.0026	-51.4084	42	SC	false
5221403	Trindade	-16.6517	-49.4927	52	GO	false
2615607	Trindade	-7.759	-40.2647	26	PE	false
4321956	Trindade do Sul	-27.5239	-52.8956	43	RS	false
4322004	Triunfo	-29.9291	-51.7075	43	RS	false
2516805	Triunfo	-6.5713	-38.5986	25	PB	false
2615706	Triunfo	-7.83272	-38.0978	26	PE	false
2414456	Triunfo Potiguar	-5.85408	-37.1786	24	RN	false
2112233	Trizidela do Vale	-4.538	-44.628	21	MA	false
5221452	Trombas	-13.5079	-48.7417	52	GO	false
4218608	Trombudo Central	-27.3033	-49.793	42	SC	false
4218707	Tubarão	-28.4713	-49.0144	42	SC	false
2931905	Tucano	-10.9584	-38.7894	29	BA	false
1508084	Tucumã	-6.74687	-51.1626	15	PA	false
4322103	Tucunduva	-27.6573	-54.4439	43	RS	false
1508100	Tucuruí	-3.7657	-49.6773	15	PA	false
2112274	Tufilândia	-3.67355	-45.6238	21	MA	false
3554953	Tuiuti	-22.8193	-46.6937	35	SP	false
3169505	Tumiritinga	-18.9844	-41.6527	31	MG	false
4218756	Tunápolis	-26.9681	-53.6417	42	SC	false
4322152	Tunas	-29.1039	-52.9538	43	RS	false
4127882	Tunas do Paraná	-24.9731	-49.0879	41	PR	false
4127908	Tuneiras do Oeste	-23.8648	-52.8769	41	PR	false
2112308	Tuntum	-5.25476	-44.6444	21	MA	false
3555000	Tupã	-21.9335	-50.5191	35	SP	false
3169604	Tupaciguara	-18.5866	-48.6985	31	MG	false
2615805	Tupanatinga	-8.74798	-37.3445	26	PE	false
4322186	Tupanci do Sul	-27.9241	-51.5383	43	RS	false
4322202	Tupanciretã	-29.0858	-53.8445	43	RS	false
4322251	Tupandi	-29.4772	-51.4174	43	RS	false
4322301	Tuparendi	-27.7598	-54.4814	43	RS	false
2615904	Tuparetama	-7.6003	-37.3165	26	PE	false
4127957	Tupãssi	-24.5879	-53.5105	41	PR	false
3555109	Tupi Paulista	-21.3825	-51.575	35	SP	false
1721257	Tupirama	-8.97168	-48.1883	17	TO	false
1721307	Tupiratins	-8.39388	-48.1277	17	TO	false
2112407	Turiaçu	-1.65893	-45.3798	21	MA	false
2112456	Turilândia	-2.21638	-45.3044	21	MA	false
3555208	Turiúba	-20.9428	-50.1135	35	SP	false
3555307	Turmalina	-20.0486	-50.4792	35	SP	false
3169703	Turmalina	-17.2828	-42.7285	31	MG	false
4322327	Turuçu	-31.4173	-52.1706	43	RS	false
2313559	Tururu	-3.58413	-39.4297	23	CE	false
5221502	Turvânia	-16.6125	-50.1369	52	GO	false
5221551	Turvelândia	-17.8502	-50.3024	52	GO	false
4127965	Turvo	-25.0437	-51.5282	41	PR	false
4218806	Turvo	-28.9272	-49.6831	42	SC	false
3169802	Turvolândia	-21.8733	-45.7859	31	MG	false
2112506	Tutóia	-2.76141	-42.2755	21	MA	false
1304260	Uarini	-2.99609	-65.1133	13	AM	false
2932002	Uauá	-9.83325	-39.4794	29	BA	false
3169901	Ubá	-21.1204	-42.9359	31	MG	false
3170008	Ubaí	-16.2885	-44.7783	31	MG	false
2932101	Ubaíra	-13.2714	-39.666	29	BA	false
2932200	Ubaitaba	-14.303	-39.3222	29	BA	false
2313609	Ubajara	-3.85448	-40.9204	23	CE	false
3170057	Ubaporanga	-19.6351	-42.1059	31	MG	false
3555356	Ubarana	-21.165	-49.7198	35	SP	false
2932309	Ubatã	-14.2063	-39.5207	29	BA	false
3555406	Ubatuba	-23.4332	-45.0834	35	SP	false
3170107	Uberaba	-19.7472	-47.9381	31	MG	false
3170206	Uberlândia	-18.9141	-48.2749	31	MG	false
3555505	Ubirajara	-22.5272	-49.6613	35	SP	false
4128005	Ubiratã	-24.5393	-52.9865	41	PR	false
4322343	Ubiretama	-28.0404	-54.686	43	RS	false
3555604	Uchoa	-20.9511	-49.1713	35	SP	false
2932408	Uibaí	-11.3394	-42.1354	29	BA	false
1400704	Uiramutã	4.60314	-60.1815	14	RR	false
5221577	Uirapuru	-14.2835	-49.9201	52	GO	false
2516904	Uiraúna	-6.51504	-38.4128	25	PB	false
1508126	Ulianópolis	-3.75007	-47.4892	15	PA	false
2313708	Umari	-6.63893	-38.7008	23	CE	false
2414506	Umarizal	-5.98238	-37.818	24	RN	false
2807600	Umbaúba	-11.3809	-37.6623	28	SE	false
2932457	Umburanas	-10.7339	-41.3234	29	BA	false
3170305	Umburatiba	-17.2548	-40.5779	31	MG	false
2517001	Umbuzeiro	-7.69199	-35.6582	25	PB	false
2313757	Umirim	-3.67654	-39.3465	23	CE	false
4128104	Umuarama	-23.7656	-53.3201	41	PR	false
2932507	Una	-15.2791	-39.0765	29	BA	false
3170404	Unaí	-16.3592	-46.9022	31	MG	false
2211100	União	-4.58571	-42.8583	22	PI	false
4322350	União da Serra	-28.7833	-52.0238	43	RS	false
4128203	União da Vitória	-26.2273	-51.0873	41	PR	false
3170438	União de Minas	-19.5299	-50.338	31	MG	false
4218855	União do Oeste	-26.762	-52.8541	42	SC	false
5108303	União do Sul	-11.5308	-54.3616	51	MT	false
2709301	União dos Palmares	-9.15921	-36.0223	27	AL	false
3555703	União Paulista	-20.8862	-49.9025	35	SP	false
4128302	Uniflor	-23.0868	-52.1573	41	PR	false
4322376	Unistalda	-29.04	-55.1517	43	RS	false
2414605	Upanema	-5.63761	-37.2635	24	RN	false
4128401	Uraí	-23.2	-50.7939	41	PR	false
2932606	Urandi	-14.7678	-42.6498	29	BA	false
3555802	Urânia	-20.2455	-50.6455	35	SP	false
2112605	Urbano Santos	-3.20642	-43.3878	21	MA	false
3555901	Uru	-21.7866	-49.2848	35	SP	false
5221601	Uruaçu	-14.5238	-49.1396	52	GO	false
5221700	Uruana	-15.4993	-49.6861	52	GO	false
3170479	Uruana de Minas	-16.0634	-46.2443	31	MG	false
1508159	Uruará	-3.71519	-53.7396	15	PA	false
4218905	Urubici	-28.0157	-49.5925	42	SC	false
2313807	Uruburetama	-3.62316	-39.5107	23	CE	false
3170503	Urucânia	-20.3521	-42.737	31	MG	false
1304302	Urucará	-2.52936	-57.7538	13	AM	false
2932705	Uruçuca	-14.5963	-39.2851	29	BA	false
2211209	Uruçuí	-7.23944	-44.5577	22	PI	false
3170529	Urucuia	-16.1244	-45.7352	31	MG	false
1304401	Urucurituba	-3.12841	-58.1496	13	AM	false
4322400	Uruguaiana	-29.7614	-57.0853	43	RS	false
2313906	Uruoca	-3.30819	-40.5628	23	CE	false
1101708	Urupá	-11.1261	-62.3639	11	RO	false
4218954	Urupema	-27.9557	-49.8729	42	SC	false
3556008	Urupês	-21.2032	-49.2931	35	SP	false
4219002	Urussanga	-28.518	-49.3238	42	SC	false
5221809	Urutaí	-17.4651	-48.2015	52	GO	false
2932804	Utinga	-12.0783	-41.0954	29	BA	false
4322509	Vacaria	-28.5079	-50.9418	43	RS	false
5108352	Vale de São Domingos	-15.286	-59.0683	51	MT	false
1101757	Vale do Anari	-9.86215	-62.1876	11	RO	false
1101807	Vale do Paraíso	-10.4465	-62.1352	11	RO	false
4322533	Vale do Sol	-29.5967	-52.6839	43	RS	false
4322541	Vale Real	-29.3919	-51.2559	43	RS	false
4322525	Vale Verde	-29.7864	-52.1857	43	RS	false
2932903	Valença	-13.3669	-39.073	29	BA	false
3306107	Valença	-22.2445	-43.7129	33	RJ	false
2211308	Valença do Piauí	-6.40301	-41.7375	22	PI	false
2933000	Valente	-11.4062	-39.457	29	BA	false
3556107	Valentim Gentil	-20.4217	-50.0889	35	SP	false
3556206	Valinhos	-22.9698	-46.9974	35	SP	false
3556305	Valparaíso	-21.2229	-50.8699	35	SP	false
5221858	Valparaíso de Goiás	-16.0651	-47.9757	52	GO	false
4322558	Vanini	-28.4758	-51.8447	43	RS	false
4219101	Vargeão	-26.8621	-52.1549	42	SC	false
4219150	Vargem	-27.4867	-50.9724	42	SC	false
3556354	Vargem	-22.887	-46.4124	35	SP	false
3170578	Vargem Alegre	-19.5988	-42.2949	31	MG	false
3205036	Vargem Alta	-20.669	-41.0179	32	ES	false
3170602	Vargem Bonita	-20.3333	-46.3688	31	MG	false
4219176	Vargem Bonita	-27.0055	-51.7402	42	SC	false
2112704	Vargem Grande	-3.53639	-43.917	21	MA	false
3170651	Vargem Grande do Rio Pardo	-15.3987	-42.3085	31	MG	false
3556404	Vargem Grande do Sul	-21.8322	-46.8913	35	SP	false
3556453	Vargem Grande Paulista	-23.5993	-47.022	35	SP	false
3170701	Varginha	-21.5556	-45.4364	31	MG	false
5221908	Varjão	-17.0471	-49.6312	52	GO	false
3170750	Varjão de Minas	-18.3741	-46.0313	31	MG	false
2313955	Varjota	-4.19387	-40.4741	23	CE	false
3306156	Varre-Sai	-20.9276	-41.8701	33	RJ	false
2414704	Várzea	-6.34641	-35.3732	24	RN	false
2517100	Várzea	-6.76189	-36.9913	25	PB	false
2314003	Várzea Alegre	-6.78264	-39.2942	23	CE	false
2211357	Várzea Branca	-9.238	-42.9692	22	PI	false
3170800	Várzea da Palma	-17.5944	-44.7226	31	MG	false
2933059	Várzea da Roça	-11.6005	-40.1328	29	BA	false
2933109	Várzea do Poço	-11.5273	-40.3149	29	BA	false
2211407	Várzea Grande	-6.54899	-42.248	22	PI	false
5108402	Várzea Grande	-15.6458	-56.1322	51	MT	false
2933158	Várzea Nova	-11.2557	-40.9432	29	BA	false
3556503	Várzea Paulista	-23.2136	-46.8234	35	SP	false
2933174	Varzedo	-12.9672	-39.3919	29	BA	false
3170909	Varzelândia	-15.6992	-44.0278	31	MG	false
3306206	Vassouras	-22.4059	-43.6686	33	RJ	false
3171006	Vazante	-17.9827	-46.9088	31	MG	false
4322608	Venâncio Aires	-29.6143	-52.1932	43	RS	false
3205069	Venda Nova do Imigrante	-20.327	-41.1355	32	ES	false
2414753	Venha-Ver	-6.32016	-38.4896	24	RN	false
4128534	Ventania	-24.2458	-50.2376	41	PR	false
2616001	Venturosa	-8.57885	-36.8742	26	PE	false
5108501	Vera	-12.3017	-55.3045	51	MT	false
2414803	Vera Cruz	-6.04399	-35.428	24	RN	false
2933208	Vera Cruz	-12.9568	-38.6153	29	BA	false
4322707	Vera Cruz	-29.7184	-52.5152	43	RS	false
3556602	Vera Cruz	-22.2183	-49.8207	35	SP	false
4128559	Vera Cruz do Oeste	-25.0577	-53.8771	41	PR	false
2211506	Vera Mendes	-7.59748	-41.4673	22	PI	false
4322806	Veranópolis	-28.9312	-51.5516	43	RS	false
2616100	Verdejante	-7.92235	-38.9701	26	PE	false
3171030	Verdelândia	-15.5845	-43.6121	31	MG	false
4128609	Verê	-25.8772	-52.9051	41	PR	false
2933257	Vereda	-17.2183	-40.0974	29	BA	false
3171071	Veredinha	-17.3974	-42.7307	31	MG	false
3171105	Veríssimo	-19.6657	-48.3118	31	MG	false
3171154	Vermelho Novo	-20.0406	-42.2688	31	MG	false
2616183	Vertente do Lério	-7.77084	-35.8491	26	PE	false
2616209	Vertentes	-7.90158	-35.9681	26	PE	false
3171204	Vespasiano	-19.6883	-43.9239	31	MG	false
4322855	Vespasiano Corrêa	-29.0655	-51.8625	43	RS	false
4322905	Viadutos	-27.5716	-52.0211	43	RS	false
4323002	Viamão	-30.0819	-51.0194	43	RS	false
3205101	Viana	-20.3825	-40.4933	32	ES	false
2112803	Viana	-3.20451	-44.9912	21	MA	false
5222005	Vianópolis	-16.7405	-48.5159	52	GO	false
2616308	Vicência	-7.65655	-35.3139	26	PE	false
4323101	Vicente Dutra	-27.1607	-53.4022	43	RS	false
5008404	Vicentina	-22.4098	-54.4415	50	MS	false
5222054	Vicentinópolis	-17.7322	-49.8047	52	GO	false
2414902	Viçosa	-5.98253	-37.9462	24	RN	false
2709400	Viçosa	-9.36763	-36.2431	27	AL	false
3171303	Viçosa	-20.7559	-42.8742	31	MG	false
2314102	Viçosa do Ceará	-3.5667	-41.0916	23	CE	false
4323200	Victor Graeff	-28.5632	-52.7495	43	RS	false
4219200	Vidal Ramos	-27.3886	-49.3593	42	SC	false
4219309	Videira	-27.0086	-51.1543	42	SC	false
3171402	Vieiras	-20.867	-42.2401	31	MG	false
2517209	Vieirópolis	-6.50684	-38.2567	25	PB	false
1508209	Vigia	-0.861194	-48.1386	15	PA	false
5105507	Vila Bela da Santíssima Trindade	-15.0068	-59.9504	51	MT	false
5222203	Vila Boa	-15.0387	-47.052	52	GO	false
2415008	Vila Flor	-6.31287	-35.067	24	RN	false
4323309	Vila Flores	-28.8598	-51.5504	43	RS	false
4323358	Vila Lângaro	-28.1062	-52.1438	43	RS	false
4323408	Vila Maria	-28.5359	-52.1486	43	RS	false
2211605	Vila Nova do Piauí	-7.13272	-40.9345	22	PI	false
4323457	Vila Nova do Sul	-30.3461	-53.876	43	RS	false
2112852	Vila Nova dos Martírios	-5.18889	-48.1336	21	MA	false
3205150	Vila Pavão	-18.6091	-40.609	32	ES	false
5222302	Vila Propício	-15.4542	-48.8819	52	GO	false
5108600	Vila Rica	-10.0137	-51.1186	51	MT	false
3205176	Vila Valério	-18.9958	-40.3849	32	ES	false
3205200	Vila Velha	-20.3417	-40.2875	32	ES	false
1100304	Vilhena	-12.7502	-60.1488	11	RO	false
3556701	Vinhedo	-23.0302	-46.9833	35	SP	false
3556800	Viradouro	-20.8734	-48.293	35	SP	false
3171600	Virgem da Lapa	-16.807	-42.3431	31	MG	false
3171709	Virgínia	-22.3264	-45.0965	31	MG	false
3171808	Virginópolis	-18.8154	-42.7015	31	MG	false
3171907	Virgolândia	-18.4738	-42.3067	31	MG	false
4128658	Virmond	-25.3829	-52.1987	41	PR	false
3172004	Visconde do Rio Branco	-21.0127	-42.8361	31	MG	false
1508308	Viseu	-1.19124	-46.1399	15	PA	false
4323507	Vista Alegre	-27.3686	-53.4919	43	RS	false
3556909	Vista Alegre do Alto	-21.1692	-48.6284	35	SP	false
4323606	Vista Alegre do Prata	-28.8052	-51.7947	43	RS	false
4323705	Vista Gaúcha	-27.2902	-53.6974	43	RS	false
2505501	Vista Serrana	-6.7303	-37.5704	25	PB	false
4219358	Vitor Meireles	-26.8782	-49.8328	42	SC	false
3205309	Vitória	-20.3155	-40.3128	32	ES	true 
3556958	Vitória Brasil	-20.1956	-50.4875	35	SP	false
2933307	Vitória da Conquista	-14.8615	-40.8442	29	BA	false
4323754	Vitória das Missões	-28.3516	-54.504	43	RS	false
2616407	Vitória de Santo Antão	-8.12819	-35.2976	26	PE	false
1600808	Vitória do Jari	-0.938	-52.424	16	AP	false
2112902	Vitória do Mearim	-3.45125	-44.8643	21	MA	false
1508357	Vitória do Xingu	-2.87922	-52.0088	15	PA	false
4128708	Vitorino	-26.2683	-52.7843	41	PR	false
2113009	Vitorino Freire	-4.28184	-45.2505	21	MA	false
3172103	Volta Grande	-21.7671	-42.5375	31	MG	false
3306305	Volta Redonda	-22.5202	-44.0996	33	RJ	false
3557006	Votorantim	-23.5446	-47.4388	35	SP	false
3557105	Votuporanga	-20.4237	-49.9781	35	SP	false
2933406	Wagner	-12.2819	-41.1715	29	BA	false
2211704	Wall Ferraz	-7.23151	-41.905	22	PI	false
1722081	Wanderlândia	-6.85274	-47.9601	17	TO	false
2933455	Wanderley	-12.1144	-43.8958	29	BA	false
3172202	Wenceslau Braz	-22.5368	-45.3626	31	MG	false
4128500	Wenceslau Braz	-23.8742	-49.8032	41	PR	false
2933505	Wenceslau Guimarães	-13.6908	-39.4762	29	BA	false
4323770	Westfália	-29.4263	-51.7645	43	RS	false
4219408	Witmarsum	-26.9275	-49.7947	42	SC	false
1722107	Xambioá	-6.4141	-48.532	17	TO	false
4128807	Xambrê	-23.7364	-53.4884	41	PR	false
4323804	Xangri-lá	-29.8065	-50.0519	43	RS	false
4219507	Xanxerê	-26.8747	-52.4036	42	SC	false
1200708	Xapuri	-10.6516	-68.4969	12	AC	false
4219606	Xavantina	-27.0667	-52.343	42	SC	false
4219705	Xaxim	-26.9596	-52.5374	42	SC	false
2616506	Xexéu	-8.8046	-35.6212	26	PE	false
1508407	Xinguara	-7.0983	-49.9437	15	PA	false
2933604	Xique-Xique	-10.823	-42.7245	29	BA	false
2517407	Zabelê	-8.07901	-37.1057	25	PB	false
3557154	Zacarias	-21.0506	-50.0552	35	SP	false
2114007	Zé Doca	-3.27014	-45.6553	21	MA	false
4219853	Zortéa	-27.4521	-51.552	42	SC	false
\.


--
-- Data for Name: queue; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.queue (id, status, email, subject, msg) FROM stdin;
1	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
2	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
3	sent	alexandremasbr@gmail.com	Candidato cadastrado	\n                <p class="text-center"> Olá Alexandre Miguel de Andrade Souza, você acaba de se candidatar ao cargo de Conselheiro Representante do Segmento Quilombola para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG é .<br>\n                Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n                Fique atento ao sistema e seu email.<p>\n                <br><br>\n                <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n                <p class="text-danger">Não responda este email</p>\n                
4	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
5	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
6	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
7	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
8	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
9	sent	wallison.francisco@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n            <p class="text-center"> Você acaba de se habilitar para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para eleger o cargo Conselheiro Representante do Segmento x.<br>\n            Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n            Fique atento ao sistema e seu email.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
10	sent	wallison.francisco@social.mg.gov.br	Candidato cadastrado	\n                <p class="text-center"> Olá Wallison Francisco, você acaba de se candidatar ao cargo de Conselheiro Representante do Segmento x para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG é .<br>\n                Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n                Fique atento ao sistema e seu email.<p>\n                <br><br>\n                <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n                <p class="text-danger">Não responda este email</p>\n                
11	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
12	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
13	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para eleger o cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por O documento não representa uma identidade. </li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
14	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição Conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
15	sent	alexandremasbr@gmail.com	Candidato cadastrado	\n                <p class="text-center"> Olá Alexandre Miguel de Andrade Souza, você acaba de se candidatar ao cargo de Conselheiro Representante do Segmento Quilombola para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG é .<br>\n                Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n                Fique atento ao sistema e seu email.<p>\n                <br><br>\n                <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n                <p class="text-danger">Não responda este email</p>\n                
16	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
17	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
18	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
19	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para eleger o cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por O documento não representa uma identidade. </li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
20	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para eleger o cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por O documento não representa uma identidade. </li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
21	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Moreira Vertelo precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para eleger o cargo Conselheiro Representante do Segmento Quilombola.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
22	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por O documento não representa uma identidade. </li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
23	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
24	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
25	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
26	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/29">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
27	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Jovens Desempregados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
49	sent	chrisbh2014@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns christiane machado<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
50	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
28	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Jovens Desempregados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
29	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
30	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por dadfaddsfas</li><li>Comprovantes de funcionamento da entidade há pelo menos 2 (dois) anos Rejeitado por None</li><li>Comprovantes de atuação no segmento em pelo menos 03 (três) municípios em Minas Gerais Rejeitado por None</li><li>Certidão Negativa de Débito  de ordem jurídica, fiscal, trabalhista e previdenciária Rejeitado por None</li><li>Consulta ao CAGEC – Demonstrando que entidade não se encontra impedida de contratar/licitar  Rejeitado por documento não confere</li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
31	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por dadfaddsfas</li><li>Comprovantes de funcionamento da entidade há pelo menos 2 (dois) anos Rejeitado por None</li><li>Comprovantes de atuação no segmento em pelo menos 03 (três) municípios em Minas Gerais Rejeitado por None</li><li>Certidão Negativa de Débito  de ordem jurídica, fiscal, trabalhista e previdenciária Rejeitado por None</li><li>Consulta ao CAGEC – Demonstrando que entidade não se encontra impedida de contratar/licitar  Rejeitado por documento não confere</li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
33	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
32	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
34	sent	alexandre.vertelo@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        <li>Documento de Identidade Rejeitado por arquivo não enviado</li><li>Documento de Identidade Rejeitado por arquivo não enviado</li><li>CPF do Presidente da entidade (ou função equivalente) Rejeitado por O documento não é o cpf</li>\n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
35	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
36	sent	alexandre.vertelo@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
37	sent	alexandre.vertelo@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento x.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
38	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Jovens Desempregados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
39	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Jovens Desempregados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
40	sent	wallison.francisco@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de ASILO DE VELHOS JESUS NAZARENO para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
41	sent	dilene.lopes@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Conselho Estadual de Defesa dos Direitos da Pessoa com Deficiência - CONPED para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
42	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
43	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Wallison Francisco precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento X.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
44	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Dirlene Ribeiro Lopes precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
45	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Dirlene Ribeiro Lopes precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento das Mulheres Negras.<br>\n        Agora você precisa enviar os documentos e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
47	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
48	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
46	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Moreira Vertelo precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
51	sent	jcnoivas@hotmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Juliana de Melo Cordeiro<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
52	sent	vanialuciadeoliveiraguimaraes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Vânia Lúcia de Oliveira Guimarães<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
53	sent	wallison.francisco@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Wallison Francisco<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
54	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
55	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
56	sent	chrisbh2014@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns christiane machado<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
57	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
58	sent	jcnoivas@hotmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Juliana de Melo Cordeiro<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
59	sent	vanialuciadeoliveiraguimaraes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Vânia Lúcia de Oliveira Guimarães<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
60	sent	wallison.francisco@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Wallison Francisco<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
61	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
62	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
63	sent	chrisbh2014@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns christiane machado<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
64	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
65	sent	jcnoivas@hotmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Juliana de Melo Cordeiro<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
66	sent	vanialuciadeoliveiraguimaraes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Vânia Lúcia de Oliveira Guimarães<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
67	sent	wallison.francisco@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Wallison Francisco<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
68	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
69	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
70	sent	jcnoivas@hotmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Juliana de Melo Cordeiro<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
71	sent	vanialuciadeoliveiraguimaraes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Vânia Lúcia de Oliveira Guimarães<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
72	sent	wallison.francisco@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Wallison Francisco<strong>, você foi selecionado para compor a comissão eleitoral na eleição Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/31">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
73	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Jovens Desempregados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
74	sent	alexandre.vertelo@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza precisa checar os documentos abaixo para prosseguir no processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n        Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>\n        Documentos:\n        <uL>\n        \n        <ul>\n        <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
75	sent	alexandre.vertelo@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
76	sent	alexandre.vertelo@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
77	sent	alexandre.vertelo@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A inscrição  no  processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG para se candidatar ao cargo Conselheiro Representante do Segmento X foi aprovada\n            pela comissão eleitoral. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            Fique atento ao sistema e seu email.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
78	sent	alexandre.vertelo@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
79	sent	alexandre.vertelo@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
80	sent	alexandremasbr@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
81	sent	alexandremasbr@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
82	sent	alexandremasbr@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
83	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
84	sent	wymored@outlook.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Eleição das organizações da sociedade civil para compor as 11 (onze) cadeiras da representação do CONEPIR/MG.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-02 07:00:00 a 2021-12-28 18:00:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
85	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição Conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
86	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição Conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
87	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição Conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
88	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição Conselho Estadual da mulher <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
145	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
146	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
89	sent	dilene.lopes@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Conselho Estadual da mulher.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-02 15:48:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
90	sent	dilene.lopes@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Conselho Estadual de Defesa dos Direitos da Pessoa com Deficiência - CONPED para o processo eleitoral Conselho Estadual da mulher<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
91	sent	dilene.lopes@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A inscrição  no  processo eleitoral Conselho Estadual da mulher para se candidatar ao cargo Setor Rural foi aprovada\n            pela comissão eleitoral. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            Fique atento ao sistema e seu email.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
92	sent	dilene.lopes@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Conselho Estadual da mulher.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-02 15:48:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
93	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
94	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
95	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
96	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
97	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
98	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
99	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
100	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
101	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
106	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
102	sent	alexandremasbr@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-01 08:00:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
103	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
104	sent	wymored@outlook.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação dos Pré-Aposentados para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
105	sent	wymored@outlook.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
116	sent	alexandremasbr@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação Cigana de Minas Gerais para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
117	sent	alexandremasbr@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
107	sent	wymored@outlook.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
108	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
109	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
110	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
111	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
112	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
113	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n        Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
114	sent	wymored@outlook.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-03 08:00:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
115	sent	wymored@outlook.com	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Mães foi aprovada\n            pela comissão eleitoral. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            Fique atento ao sistema e seu email.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
118	sent	wymored@outlook.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-03 08:00:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
132	sent	claudia.ribeiro@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
133	sent	talles.freitas@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de 0EntidadeTesteUH"!@#$%¨&*()_+ para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
119	sent	alexandremasbr@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-03 08:00:00 a 2021-12-15 15:48:00 para votar. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
120	sent	alexandremasbr@gmail.com	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Alexandre Miguel de Andrade Souza. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Estudantes foi aprovada\n            pela comissão eleitoral. <br>\n            Documentos:\n            <uL>\n            \n            <ul>\n            <br>\n            Fique atento ao sistema e seu email.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
121	sent	dilene.lopes@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral Teste 2 - Eleição Conselheiros e Conselheiras da Sociedade Civil Compor CONEPIR.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-20 08:00:00 a 2021-12-21 18:00:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
122	sent	paulo.martins@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação das Mulheres Negras de Varginha para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
123	sent	paulo.martins@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
124	sent	beatrizetrindade@gmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação das Mulheres Negras de Belo Horizonte para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
125	sent	beatrizetrindade@gmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
126	sent	paulo.martins@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Paulo Henrique Martins. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-07 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
127	sent	paulo.martins@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação das Mulheres Negras de Varginha para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
128	sent	paulo.martins@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
129	sent	paulo.martins@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação das Mulheres Negras de Varginha para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
130	sent	paulo.martins@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
131	sent	claudia.ribeiro@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação das Feministas de Minas Gerais para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
134	sent	talles.freitas@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
135	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
136	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
137	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
138	sent	beatrizetrindade@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Beatriz Trindade<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
139	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
140	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
141	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
142	sent	wymored@outlook.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
143	sent	alexandremasbr@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Miguel de Andrade Souza<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
144	sent	alexandre.vertelo@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Alexandre Moreira Vertelo<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
147	sent	elenirriossantos31@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Elenir Rios dos Santos<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
148	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição CONSELHO ESTADUAL DA MULHER - 2021 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/30">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
149	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/32">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
150	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/32">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
151	sent	teste@teste.com	Seleção para compor comissão eleitoral	<strong>Parabéns Testudo Testudão<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/32">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
152	sent	teste@teste.com	Seleção para compor comissão eleitoral	<strong>Parabéns Testudo Testudão<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/32">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
153	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE002 - ELEIÇÃO 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/33">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
154	sent	beatrizetrindade@gmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Beatriz Trindade. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-07 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
155	sent	paulo.martins@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Paulo Henrique Martins. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-07 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
156	sent	claudia.ribeiro@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Cláudia. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-07 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
157	sent	beatrizetrindade@gmail.com	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Beatriz Trindade. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Mulheres Negras foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
158	sent	paulo.martins@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Paulo Henrique Martins. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Empreendedoras foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
159	sent	claudia.ribeiro@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Cláudia. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Feminista foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
160	sent	wallison.francisco@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de ASILO DE VELHOS JESUS NAZARENO para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
161	sent	wallison.francisco@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
162	sent	wallison.francisco@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Wallison Francisco. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-08 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
163	sent	wallison.francisco@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Wallison Francisco. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Mães foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
164	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/34">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
165	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/34">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
166	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/34">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
167	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO 002 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/35">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
168	sent	dilene.lopes@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Conselho Estadual de Defesa dos Direitos da Pessoa com Deficiência - CONPED para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
169	sent	dilene.lopes@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
170	sent	dilene.lopes@social.mg.gov.br	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-08 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
171	sent	dilene.lopes@social.mg.gov.br	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Dirlene Ribeiro Lopes. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Setor Rural foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
172	sent	talles.freitas@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de 0EntidadeTesteUH"!@#$%¨&*()_+ para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
173	sent	talles.freitas@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
174	sent	chrisbh2014@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns christiane machado<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/36">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
175	sent	claudia.ribeiro@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Cláudia<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/36">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
176	sent	claudia.damc123456@gmail.com	Seleção para compor comissão eleitoral	<strong>Parabéns Claudia Rodrigues César<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/36">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
177	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO003 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/36">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
178	sent	talles.freitas@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de 0EntidadeTesteUH"!@#$%¨&*()_+ para o processo eleitoral COLEGIADOTESTE001 - ELEIÇÃO003<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
179	sent	talles.freitas@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral COLEGIADOTESTE001 - ELEIÇÃO003<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
180	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO004 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/37">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
181	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO004 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/37">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
182	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO005 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/38">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
183	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO005 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/38">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
184	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição SEGMENTOTESTE 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/39">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
185	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição SEGMENTOTESTE 001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/39">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
186	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO006 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/40">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
187	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 - ELEIÇÃO006 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/40">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
188	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/41">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
189	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição COLEGIADOTESTE001 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/41">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
190	sent	dilene.lopes@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Dirlene Ribeiro Lopes<strong>, você foi selecionado para compor a comissão eleitoral na eleição 002 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/42">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
191	sent	talles.freitas@social.mg.gov.br	Seleção para compor comissão eleitoral	<strong>Parabéns Talles Roque de Freitas<strong>, você foi selecionado para compor a comissão eleitoral na eleição 002 <br><br>\n            Acompanhe o processo eleitoral <a href="/voto/comite/index/42">por aqui</a>.\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
192	sent	talles.freitas@social.mg.gov.br	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de ColegiadoTeste001 para o processo eleitoral 002<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
193	sent	talles.freitas@social.mg.gov.br	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral 002<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
194	sent	jcnoivas@hotmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação de Mulheres do Norte de Minas para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
195	sent	jcnoivas@hotmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
196	sent	jcnoivas@hotmail.com	Candidatura para processo eleitoral	\n        <p class="text-center"> Foi solicitida a inscrição de Associação de Mulheres do Norte de Minas para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
197	sent	jcnoivas@hotmail.com	Inscrição Solicitada para processo eleitoral	\n        <p class="text-center"> Você acaba de solicitar a habilitação  para  votar no processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021<br>\n        Agora você precisa aguardar a aprovação da comissão eleitoral. <br>\n        Fique atento ao sistema e seu email.<p>\n        <br><br>\n        <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n        <p class="text-danger">Não responda este email</p>\n        
198	sent	jcnoivas@hotmail.com	Aprovação de Inscrição para processo eleitoral	\n            <p class="text-center"> Olá, Juliana de Melo Cordeiro. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021.<br>\n            Agora você só precisa aguardar o período eleitoral, de 2021-12-07 08:00:00 a 2021-12-12 10:48:00 para votar. <br>\n            <br>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
199	sent	jcnoivas@hotmail.com	Aprovação no processo eleitoral	\n            <p class="text-center"> Olá, Juliana de Melo Cordeiro. A inscrição  no  processo eleitoral CONSELHO ESTADUAL DA MULHER - 2021 para se candidatar ao cargo Setor Rural foi aprovada\n            pela comissão eleitoral. <br>\n            Aguarde o período de votação.<p>\n            <br><br>\n            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>\n            <p class="text-danger">Não responda este email</p>\n            
\.


--
-- Data for Name: recurso; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.recurso (id, titulo, tipo, eleicao, descricao, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
1	Impugnação de artigo do Edital	Edital	30	O art x contraria a legislação xxx, ao dispor de forma diversa. 	T	2021-12-09 20:30:57	76	2021-12-09 20:30:57	76
\.


--
-- Data for Name: resposta_recurso; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.resposta_recurso (id, resposta, is_active, created_on, created_by, modified_on, modified_by, recurso) FROM stdin;
1	apenas um teste	T	2021-12-09 22:41:04	76	2021-12-09 22:41:04	76	1
\.


--
-- Data for Name: tipo_documento; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_documento (id, tipo, is_active, created_on, created_by, modified_on, modified_by, relativo_a) FROM stdin;
9	Estatuto da entidade registrado em cartório e alterações posteriores, se houver	T	2021-11-09 19:51:45	69	2021-11-09 19:51:45	69	entidade
11	Comprovação de regularidade fiscal e trabalhista	T	2021-11-09 19:53:03	69	2021-11-09 19:53:03	69	entidade
12	Comprovantes de funcionamento da entidade há pelo menos 2 (dois) anos	T	2021-11-09 19:53:26	69	2021-11-09 19:53:26	69	entidade
15	Atestado  ou comprovante de ausência de registro no Cadastro de Entidades Privadas Sem Fins Lucrativos Impedidas – Cepim 	T	2021-11-09 19:54:27	69	2021-11-09 19:54:27	69	entidade
16	Certidão Negativa de Débito  de ordem jurídica, fiscal, trabalhista e previdenciária	T	2021-11-09 19:54:50	69	2021-11-09 19:54:50	69	entidade
17	Consulta ao CAGEC – Demonstrando que entidade não se encontra impedida de contratar/licitar 	T	2021-11-09 19:55:31	69	2021-11-09 19:55:31	69	entidade
18	CADIN-MG - Consulta ao Cadastro Informativo de Inadimplência em relação à Administração Pública do Estado de Minas Gerais 	T	2021-11-09 19:56:12	69	2021-11-09 19:56:12	69	entidade
19	Certidões Negativas ou Positivas com Efeito de Negativa da pessoa física, junto às Receitas Municipal, Estadual e Federal - Quando representante não estatutário	T	2021-11-09 19:57:20	69	2021-11-09 19:57:20	69	usuario
14	Declaração de veracidade das informações preenchido e assinado (Anexo I)	T	2021-11-09 19:54:02	69	2021-11-09 20:00:35	69	entidade
10	CPF do Presidente da entidade (ou função equivalente)	T	2021-11-09 19:52:39	69	2021-11-09 20:04:37	69	usuario
13	Comprovantes de atuação no segmento em pelo menos 03 (três) municípios em Minas Gerais	T	2021-11-09 19:53:45	69	2021-11-12 12:27:08	1	entidade
1	Documento de Identidade	T	2020-10-02 03:56:47	1	2021-11-12 15:09:20	76	usuario
21	CNPJ - Comprovante de Inscrição	T	2021-11-29 21:43:31	69	2021-11-29 21:43:31	69	entidade
\.


--
-- Data for Name: tipo_documento_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.tipo_documento_archive (current_record, id, tipo, relativo_a, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: voto; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.voto (id, entidade, eleicao, cargo, candidato, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
12	22	29	40	67	T	2021-11-30 19:38:15	86	2021-11-30 19:38:15	86
16	20	30	36	72	T	2021-12-02 15:50:29	82	2021-12-02 15:50:29	82
17	20	30	55	71	T	2021-12-02 15:50:33	82	2021-12-02 15:50:33	82
18	20	30	53	73	T	2021-12-02 15:50:37	82	2021-12-02 15:50:37	82
19	6	30	42	77	T	2021-12-07 18:21:23	71	2021-12-07 18:21:23	71
20	6	30	36	79	T	2021-12-07 18:25:32	71	2021-12-07 18:25:32	71
21	56	30	55	83	T	2021-12-07 18:28:05	72	2021-12-07 18:28:05	72
22	56	30	42	77	T	2021-12-07 18:28:11	72	2021-12-07 18:28:11	72
23	56	30	54	76	T	2021-12-07 18:28:15	72	2021-12-07 18:28:15	72
24	22	30	36	72	T	2021-12-07 18:29:34	86	2021-12-07 18:29:34	86
25	22	30	55	71	T	2021-12-07 18:29:38	86	2021-12-07 18:29:38	86
26	22	30	53	73	T	2021-12-07 18:29:43	86	2021-12-07 18:29:43	86
27	29	30	36	79	T	2021-12-09 18:25:04	1	2021-12-09 18:25:04	1
28	29	30	55	83	T	2021-12-09 18:25:17	1	2021-12-09 18:25:17	1
29	29	30	53	73	T	2021-12-09 18:25:29	1	2021-12-09 18:25:29	1
\.


--
-- Data for Name: voto_archive; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.voto_archive (current_record, id, entidade, eleicao, cargo, candidato, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: wiki_media; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.wiki_media (id, wiki_page, title, filename, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: wiki_page; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.wiki_page (id, slug, title, body, tags, can_read, can_edit, changelog, html, render, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Data for Name: wiki_tag; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.wiki_tag (id, name, wiki_page, is_active, created_on, created_by, modified_on, modified_by) FROM stdin;
\.


--
-- Name: auth_cas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_cas_id_seq', 1, false);


--
-- Name: auth_event_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_event_id_seq', 1028, true);


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 51, true);


--
-- Name: auth_membership_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_membership_id_seq', 203, true);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 1, true);


--
-- Name: auth_user_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_user_archive_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 94, true);


--
-- Name: candidato_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.candidato_archive_id_seq', 1, false);


--
-- Name: candidato_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.candidato_id_seq', 83, true);


--
-- Name: cargo_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.cargo_archive_id_seq', 1, false);


--
-- Name: cargo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.cargo_id_seq', 70, true);


--
-- Name: documento_recurso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.documento_recurso_id_seq', 1, true);


--
-- Name: documentos_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.documentos_archive_id_seq', 6, true);


--
-- Name: documentos_eleitor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.documentos_eleitor_id_seq', 26, true);


--
-- Name: documentos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.documentos_id_seq', 103, true);


--
-- Name: documentos_obrigatorios_eleicao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.documentos_obrigatorios_eleicao_id_seq', 4, true);


--
-- Name: eleicao_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.eleicao_archive_id_seq', 3, true);


--
-- Name: eleicao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.eleicao_id_seq', 42, true);


--
-- Name: eleitor_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.eleitor_archive_id_seq', 1, false);


--
-- Name: eleitor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.eleitor_id_seq', 117, true);


--
-- Name: entidade_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.entidade_id_seq', 56, true);


--
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.estado_id_seq', 1, false);


--
-- Name: habilitacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.habilitacao_id_seq', 93, true);


--
-- Name: municipio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.municipio_id_seq', 1, false);


--
-- Name: queue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.queue_id_seq', 199, true);


--
-- Name: recurso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.recurso_id_seq', 1, true);


--
-- Name: resposta_recurso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.resposta_recurso_id_seq', 1, true);


--
-- Name: tipo_documento_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.tipo_documento_archive_id_seq', 1, false);


--
-- Name: tipo_documento_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_documento_id_seq', 21, true);


--
-- Name: voto_archive_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.voto_archive_id_seq', 1, false);


--
-- Name: voto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.voto_id_seq', 29, true);


--
-- Name: wiki_media_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.wiki_media_id_seq', 1, false);


--
-- Name: wiki_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.wiki_page_id_seq', 1, false);


--
-- Name: wiki_tag_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.wiki_tag_id_seq', 1, false);


--
-- Name: auth_cas auth_cas_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_cas
    ADD CONSTRAINT auth_cas_pkey PRIMARY KEY (id);


--
-- Name: auth_event auth_event_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_event
    ADD CONSTRAINT auth_event_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_membership auth_membership_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_membership
    ADD CONSTRAINT auth_membership_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_archive auth_user_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user_archive
    ADD CONSTRAINT auth_user_archive_pkey PRIMARY KEY (id);


--
-- Name: auth_user auth_user_email_key; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_email_key UNIQUE (email);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: candidato_archive candidato_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_pkey PRIMARY KEY (id);


--
-- Name: candidato candidato_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_pkey PRIMARY KEY (id);


--
-- Name: cargo_archive cargo_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_pkey PRIMARY KEY (id);


--
-- Name: cargo cargo_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_pkey PRIMARY KEY (id);


--
-- Name: documento_recurso documento_recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documento_recurso
    ADD CONSTRAINT documento_recurso_pkey PRIMARY KEY (id);


--
-- Name: documentos_archive documentos_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_pkey PRIMARY KEY (id);


--
-- Name: documentos_eleitor documentos_eleitor_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_eleitor
    ADD CONSTRAINT documentos_eleitor_pkey PRIMARY KEY (id);


--
-- Name: documentos_obrigatorios_eleicao documentos_obrigatorios_eleicao_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_obrigatorios_eleicao
    ADD CONSTRAINT documentos_obrigatorios_eleicao_pkey PRIMARY KEY (id);


--
-- Name: documentos documentos_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos
    ADD CONSTRAINT documentos_pkey PRIMARY KEY (id);


--
-- Name: eleicao_archive eleicao_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive
    ADD CONSTRAINT eleicao_archive_pkey PRIMARY KEY (id);


--
-- Name: eleicao eleicao_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao
    ADD CONSTRAINT eleicao_pkey PRIMARY KEY (id);


--
-- Name: eleitor_archive eleitor_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_pkey PRIMARY KEY (id);


--
-- Name: eleitor eleitor_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_pkey PRIMARY KEY (id);


--
-- Name: entidade entidade_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.entidade
    ADD CONSTRAINT entidade_pkey PRIMARY KEY (id);


--
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- Name: habilitacao habilitacao_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao
    ADD CONSTRAINT habilitacao_pkey PRIMARY KEY (id);


--
-- Name: municipio municipio_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_pkey PRIMARY KEY (id);


--
-- Name: queue queue_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.queue
    ADD CONSTRAINT queue_pkey PRIMARY KEY (id);


--
-- Name: recurso recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.recurso
    ADD CONSTRAINT recurso_pkey PRIMARY KEY (id);


--
-- Name: resposta_recurso resposta_recurso_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.resposta_recurso
    ADD CONSTRAINT resposta_recurso_pkey PRIMARY KEY (id);


--
-- Name: tipo_documento_archive tipo_documento_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.tipo_documento_archive
    ADD CONSTRAINT tipo_documento_archive_pkey PRIMARY KEY (id);


--
-- Name: tipo_documento tipo_documento_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_pkey PRIMARY KEY (id);


--
-- Name: voto_archive voto_archive_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_pkey PRIMARY KEY (id);


--
-- Name: voto voto_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_pkey PRIMARY KEY (id);


--
-- Name: wiki_media wiki_media_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_media
    ADD CONSTRAINT wiki_media_pkey PRIMARY KEY (id);


--
-- Name: wiki_page wiki_page_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT wiki_page_pkey PRIMARY KEY (id);


--
-- Name: wiki_page wiki_page_title_key; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT wiki_page_title_key UNIQUE (title);


--
-- Name: wiki_tag wiki_tag_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_tag
    ADD CONSTRAINT wiki_tag_pkey PRIMARY KEY (id);


--
-- Name: auth_cas auth_cas_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_cas
    ADD CONSTRAINT auth_cas_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_event auth_event_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_event
    ADD CONSTRAINT auth_event_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_membership auth_membership_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_membership
    ADD CONSTRAINT auth_membership_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.auth_group(id) ON DELETE CASCADE;


--
-- Name: auth_membership auth_membership_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_membership
    ADD CONSTRAINT auth_membership_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_permission auth_permission_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.auth_group(id) ON DELETE CASCADE;


--
-- Name: auth_user_archive auth_user_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user_archive
    ADD CONSTRAINT auth_user_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_user_archive auth_user_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user_archive
    ADD CONSTRAINT auth_user_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_user_archive auth_user_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user_archive
    ADD CONSTRAINT auth_user_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_user auth_user_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_user auth_user_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: auth_user auth_user_municipio_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_municipio_fkey FOREIGN KEY (municipio) REFERENCES public.municipio(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_candidato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_candidato_fkey FOREIGN KEY (candidato) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_cargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_cargo_fkey FOREIGN KEY (cargo) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.candidato(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: candidato_archive candidato_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato_archive
    ADD CONSTRAINT candidato_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_candidato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_candidato_fkey FOREIGN KEY (candidato) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_cargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_cargo_fkey FOREIGN KEY (cargo) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: candidato candidato_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.candidato
    ADD CONSTRAINT candidato_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: cargo_archive cargo_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: cargo_archive cargo_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: cargo_archive cargo_archive_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: cargo_archive cargo_archive_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: cargo_archive cargo_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo_archive
    ADD CONSTRAINT cargo_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: cargo cargo_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: cargo cargo_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: cargo cargo_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: cargo cargo_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.cargo
    ADD CONSTRAINT cargo_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documento_recurso documento_recurso_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documento_recurso
    ADD CONSTRAINT documento_recurso_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documento_recurso documento_recurso_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documento_recurso
    ADD CONSTRAINT documento_recurso_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documento_recurso documento_recurso_recurso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documento_recurso
    ADD CONSTRAINT documento_recurso_recurso_fkey FOREIGN KEY (recurso) REFERENCES public.recurso(id) ON DELETE CASCADE;


--
-- Name: documentos_archive documentos_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos_archive documentos_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.documentos(id) ON DELETE CASCADE;


--
-- Name: documentos_archive documentos_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos_archive documentos_archive_tipo_arquivo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_tipo_arquivo_fkey FOREIGN KEY (tipo_arquivo) REFERENCES public.tipo_documento(id) ON DELETE CASCADE;


--
-- Name: documentos_archive documentos_archive_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_archive
    ADD CONSTRAINT documentos_archive_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos documentos_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos
    ADD CONSTRAINT documentos_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos_eleitor documentos_eleitor_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_eleitor
    ADD CONSTRAINT documentos_eleitor_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos_eleitor documentos_eleitor_eleitor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_eleitor
    ADD CONSTRAINT documentos_eleitor_eleitor_fkey FOREIGN KEY (eleitor) REFERENCES public.eleitor(id) ON DELETE CASCADE;


--
-- Name: documentos_eleitor documentos_eleitor_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_eleitor
    ADD CONSTRAINT documentos_eleitor_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos documentos_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos
    ADD CONSTRAINT documentos_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: documentos_obrigatorios_eleicao documentos_obrigatorios_eleicao_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_obrigatorios_eleicao
    ADD CONSTRAINT documentos_obrigatorios_eleicao_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: documentos_obrigatorios_eleicao documentos_obrigatorios_eleicao_tipo_documento_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos_obrigatorios_eleicao
    ADD CONSTRAINT documentos_obrigatorios_eleicao_tipo_documento_fkey FOREIGN KEY (tipo_documento) REFERENCES public.tipo_documento(id) ON DELETE CASCADE;


--
-- Name: documentos documentos_tipo_arquivo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos
    ADD CONSTRAINT documentos_tipo_arquivo_fkey FOREIGN KEY (tipo_arquivo) REFERENCES public.tipo_documento(id) ON DELETE CASCADE;


--
-- Name: documentos documentos_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.documentos
    ADD CONSTRAINT documentos_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleicao_archive eleicao_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive
    ADD CONSTRAINT eleicao_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleicao_archive eleicao_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive
    ADD CONSTRAINT eleicao_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: eleicao_archive eleicao_archive_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive
    ADD CONSTRAINT eleicao_archive_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: eleicao_archive eleicao_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao_archive
    ADD CONSTRAINT eleicao_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleicao eleicao_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao
    ADD CONSTRAINT eleicao_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleicao eleicao_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao
    ADD CONSTRAINT eleicao_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: eleicao eleicao_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleicao
    ADD CONSTRAINT eleicao_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor_archive eleitor_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor_archive eleitor_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.eleitor(id) ON DELETE CASCADE;


--
-- Name: eleitor_archive eleitor_archive_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: eleitor_archive eleitor_archive_eleitor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_eleitor_fkey FOREIGN KEY (eleitor) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor_archive eleitor_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor_archive
    ADD CONSTRAINT eleitor_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor eleitor_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor eleitor_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: eleitor eleitor_eleitor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_eleitor_fkey FOREIGN KEY (eleitor) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: eleitor eleitor_entidade_representada_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_entidade_representada_fkey FOREIGN KEY (entidade_representada) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: eleitor eleitor_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.eleitor
    ADD CONSTRAINT eleitor_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: habilitacao habilitacao_cargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao
    ADD CONSTRAINT habilitacao_cargo_fkey FOREIGN KEY (cargo) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: habilitacao habilitacao_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao
    ADD CONSTRAINT habilitacao_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: habilitacao habilitacao_eleitor_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao
    ADD CONSTRAINT habilitacao_eleitor_fkey FOREIGN KEY (eleitor) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: habilitacao habilitacao_entidade_representada_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.habilitacao
    ADD CONSTRAINT habilitacao_entidade_representada_fkey FOREIGN KEY (entidade_representada) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: municipio municipio_estado_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.municipio
    ADD CONSTRAINT municipio_estado_fkey FOREIGN KEY (estado) REFERENCES public.estado(id) ON DELETE CASCADE;


--
-- Name: recurso recurso_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.recurso
    ADD CONSTRAINT recurso_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: recurso recurso_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.recurso
    ADD CONSTRAINT recurso_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: recurso recurso_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.recurso
    ADD CONSTRAINT recurso_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: resposta_recurso resposta_recurso_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.resposta_recurso
    ADD CONSTRAINT resposta_recurso_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: resposta_recurso resposta_recurso_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.resposta_recurso
    ADD CONSTRAINT resposta_recurso_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: resposta_recurso resposta_recurso_recurso_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.resposta_recurso
    ADD CONSTRAINT resposta_recurso_recurso_fkey FOREIGN KEY (recurso) REFERENCES public.recurso(id) ON DELETE CASCADE;


--
-- Name: tipo_documento_archive tipo_documento_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.tipo_documento_archive
    ADD CONSTRAINT tipo_documento_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: tipo_documento_archive tipo_documento_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.tipo_documento_archive
    ADD CONSTRAINT tipo_documento_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.tipo_documento(id) ON DELETE CASCADE;


--
-- Name: tipo_documento_archive tipo_documento_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.tipo_documento_archive
    ADD CONSTRAINT tipo_documento_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: tipo_documento tipo_documento_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: tipo_documento tipo_documento_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_documento
    ADD CONSTRAINT tipo_documento_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_candidato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_candidato_fkey FOREIGN KEY (candidato) REFERENCES public.candidato(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_cargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_cargo_fkey FOREIGN KEY (cargo) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_current_record_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_current_record_fkey FOREIGN KEY (current_record) REFERENCES public.voto(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: voto_archive voto_archive_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto_archive
    ADD CONSTRAINT voto_archive_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: voto voto_candidato_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_candidato_fkey FOREIGN KEY (candidato) REFERENCES public.candidato(id) ON DELETE CASCADE;


--
-- Name: voto voto_cargo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_cargo_fkey FOREIGN KEY (cargo) REFERENCES public.cargo(id) ON DELETE CASCADE;


--
-- Name: voto voto_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: voto voto_eleicao_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_eleicao_fkey FOREIGN KEY (eleicao) REFERENCES public.eleicao(id) ON DELETE CASCADE;


--
-- Name: voto voto_entidade_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_entidade_fkey FOREIGN KEY (entidade) REFERENCES public.entidade(id) ON DELETE CASCADE;


--
-- Name: voto voto_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.voto
    ADD CONSTRAINT voto_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_media wiki_media_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_media
    ADD CONSTRAINT wiki_media_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_media wiki_media_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_media
    ADD CONSTRAINT wiki_media_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_media wiki_media_wiki_page_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_media
    ADD CONSTRAINT wiki_media_wiki_page_fkey FOREIGN KEY (wiki_page) REFERENCES public.wiki_page(id) ON DELETE CASCADE;


--
-- Name: wiki_page wiki_page_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT wiki_page_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_page wiki_page_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT wiki_page_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_tag wiki_tag_created_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_tag
    ADD CONSTRAINT wiki_tag_created_by_fkey FOREIGN KEY (created_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_tag wiki_tag_modified_by_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_tag
    ADD CONSTRAINT wiki_tag_modified_by_fkey FOREIGN KEY (modified_by) REFERENCES public.auth_user(id) ON DELETE CASCADE;


--
-- Name: wiki_tag wiki_tag_wiki_page_fkey; Type: FK CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.wiki_tag
    ADD CONSTRAINT wiki_tag_wiki_page_fkey FOREIGN KEY (wiki_page) REFERENCES public.wiki_page(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

