--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Ubuntu 12.2-4)
-- Dumped by pg_dump version 12.2 (Ubuntu 12.2-4)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: estado; Type: TABLE; Schema: public; Owner: x
--

CREATE TABLE public.estado (
    id integer NOT NULL,
    uf character varying(512),
    nome character varying(512),
    latitude double precision,
    longitude double precision
);


ALTER TABLE public.estado OWNER TO x;

--
-- Name: estado_id_seq; Type: SEQUENCE; Schema: public; Owner: x
--

CREATE SEQUENCE public.estado_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.estado_id_seq OWNER TO x;

--
-- Name: estado_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: x
--

ALTER SEQUENCE public.estado_id_seq OWNED BY public.estado.id;


--
-- Name: estado id; Type: DEFAULT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.estado ALTER COLUMN id SET DEFAULT nextval('public.estado_id_seq'::regclass);


--
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: x
--

COPY public.estado (id, uf, nome, latitude, longitude) FROM stdin;
11	RO	Rondônia	-10.83	-63.34
12	AC	Acre	-8.77	-70.55
13	AM	Amazonas	-3.47	-65.1
14	RR	Roraima	1.99	-61.33
15	PA	Pará	-3.79	-52.48
16	AP	Amapá	1.41	-51.77
17	TO	Tocantins	-9.46	-48.26
21	MA	Maranhão	-5.42	-45.44
22	PI	Piauí	-6.6	-42.28
23	CE	Ceará	-5.2	-39.53
24	RN	Rio Grande do Norte	-5.81	-36.59
25	PB	Paraíba	-7.28	-36.72
26	PE	Pernambuco	-8.38	-37.86
27	AL	Alagoas	-9.62	-36.82
28	SE	Sergipe	-10.57	-37.45
29	BA	Bahia	-13.29	-41.71
31	MG	Minas Gerais	-18.1	-44.38
32	ES	Espírito Santo	-19.19	-40.34
33	RJ	Rio de Janeiro	-22.25	-42.66
35	SP	São Paulo	-22.19	-48.79
41	PR	Paraná	-24.89	-51.55
42	SC	Santa Catarina	-27.45	-50.95
43	RS	Rio Grande do Sul	-30.17	-53.5
50	MS	Mato Grosso do Sul	-20.51	-54.54
51	MT	Mato Grosso	-12.64	-55.42
52	GO	Goiás	-15.98	-49.86
53	DF	Distrito Federal	-15.83	-47.86
\.


--
-- Name: estado_id_seq; Type: SEQUENCE SET; Schema: public; Owner: x
--

SELECT pg_catalog.setval('public.estado_id_seq', 1, false);


--
-- Name: estado estado_pkey; Type: CONSTRAINT; Schema: public; Owner: x
--

ALTER TABLE ONLY public.estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

