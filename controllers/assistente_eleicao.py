#-*- coding: utf-8 -*-

#===========================================================================================#
# Padrão de URL
# function/id?id=id&other=anyway
# Se args: mode==edit
#-------------------------------------------------------------------------------------------#

@auth.requires_membership("admin")
def index():
    
    return {}


@auth.requires_membership("admin")
def form_entidade():
    db.entidade.orgao_colegiado.writable=True
    db.entidade.cnpj.writable=False
    # db.entidade.comprovante_endereco.writable=False
    db.entidade.segmento_atuacao.writable=False
    db.entidade.texto_concordancia.writable=False
    db.entidade.texto_concordancia.readable=False
    db.entidade.orgao_colegiado.default=True
    db.entidade.representante_legal.writable=False
    form = SQLFORM(db.entidade)
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        
        redirect(URL('assistente_eleicao', 'entidades', vars=dict(entidade=form.vars.id)))
    elif form.errors:
        if form.errors.nome:
            form[0][0][1][0]['_style']='border:1px solid red'
        if form.errors.data_criacao_entidade:
            form[0][1][1][0]['_style']='border:1px solid red'
        if form.errors.municipio:
            form[0][3][1][0]['_style']='border:1px solid red'           
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'
    return {"form": form}

@auth.requires_membership("admin")
def entidades():
    query = db.entidade.orgao_colegiado == True
    if request.vars.q:
        query = ((db.entidade.orgao_colegiado == True)
                & (db.entidade.nome.contains(request.vars.q)))
        
    entidades = db(query).select(db.entidade.nome, db.entidade.id, orderby=db.entidade.nome)
    return { "entidades": entidades }

@auth.requires_membership("admin")
def dados_eleicao():

    if request.vars.eleicao:
        eleicao = db.eleicao(request.vars.eleicao)
        
    if not request.vars.entidade and not request.vars.eleicao:
        response.flash = "Não há entidades selecionadas!"
        redirect(URL('assistente_eleicao', "index", args=request.args, vars=request.vars))


    fields = ["nome","inicio_alistamento","fim_alistamento","inicio_eleicao","fim_eleicao", 
            'tipo_eleitor','tipo_candidato',
            'edital', "prazo_correcao_documentos","segmentos","segmentos_candidatura","publicar"]

    entidade = db.entidade(request.get_vars.entidade or eleicao.entidade)
    
    # set default
    db.eleicao.entidade.default = entidade.id
    form = SQLFORM(db.eleicao, 
        record=request.vars.eleicao, 
        fields=fields, 
        upload=URL(c='default',f='download'),
        submit_button="Prosseguir")
    def form_validation(form):
        # checa datas
        if form.vars.inicio_alistamento >= form.vars.fim_alistamento:

            form.errors.inicio_alistamento = "O inicio do alistamento não pode ser maior do que a data final de alistamento!"
            form.errors.fim_alistamento = "O inicio do alistamento não pode ser maior do que a data final de alistamento!"
        
        if form.vars.inicio_eleicao >= form.vars.fim_eleicao:
            form.errors.inicio_eleicao = "A data de inicio da eleição não pode ser maior do que a data de termino!"
            form.errors.fim_eleicao = "A data de inicio da eleição não pode ser maior do que a data de termino!"

        if form.vars.fim_alistamento >= form.vars.inicio_eleicao:
            form.errors.fim_alistamento = "O período de alistamento precisa ser antes da eleição"
            form.errors.fim_eleicao = form.errors.inicio_eleicao = "O período de alistamento precisa ser antes da eleição"


    if form.process(onvalidation=form_validation).accepted:
        cria_grupo(form)
        response.flash = 'Entidade Selecionada!'
        
        session.eleicao = form.vars.id
        redirect(URL(c='assistente_eleicao', f='documentos', vars=dict(entidade=entidade.id, eleicao=form.vars.id or request.get_vars.eleicao)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'
    # response.view='generic.html'
    return dict(form=form, entidade=entidade)

@auth.requires_membership("admin")
def documentos():
    request.vars.eleicao or redirect(URL('index'))

    eleicao = db.eleicao(request.vars.eleicao)
    entidade = eleicao.entidade
    fields = ['obrigatorio']
    
    form = SQLFORM(db.eleicao, record=request.vars.eleicao, fields=fields, submit_button="Prosseguir")
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(c='assistente_eleicao', f='docs_candidato', vars=dict(eleicao=eleicao.id)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'

        
    return {"eleicao":eleicao, "entidade": entidade, "form": form}


@auth.requires_membership("admin")
def docs_eleitor():
    request.vars.eleicao or redirect(URL('index'))

    eleicao = db.eleicao(request.vars.eleicao)
    entidade = eleicao.entidade
    fields = ['documentos_requeridos_eleitor']
    
    query = db(
        (~db.tipo_documento.id.belongs(eleicao.obrigatorio))
        & (db.eleicao.id==eleicao.id)
        # & (db.tipo_documento.relativo_a==cargo.tipo_eleitor)
        # & (db.cargo.id==cargo.id)
    )
    db.eleicao.documentos_requeridos_eleitor.requires=IS_IN_DB(query, "tipo_documento.id", "%(tipo)s (%(relativo_a)s)", multiple=True)



    form = SQLFORM(db.eleicao, record=request.vars.eleicao, fields=fields, submit_button="Prosseguir")
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(c='assistente_eleicao', f='docs_candidato', vars=dict(eleicao=eleicao.id)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'

        
    return {"eleicao":eleicao, "entidade": entidade, "form": form}


@auth.requires_membership("admin")
def docs_candidato():
    request.vars.eleicao or redirect(URL('index'))

    eleicao = db.eleicao(request.vars.eleicao)
    entidade = eleicao.entidade
    fields = ['documentos_requeridos_cargo']

    query = db(
        (~db.tipo_documento.id.belongs(eleicao.obrigatorio))
        # & (~db.tipo_documento.id.belongs(eleicao.documentos_requeridos_eleitor)
        
        # & (db.eleicao.id==eleicao.id)
        # & (db.tipo_documento.relativo_a==cargo.tipo_eleitor)
        # & (db.cargo.id==cargo.id)
    )
    db.eleicao.documentos_requeridos_cargo.requires=IS_IN_DB(query, "tipo_documento.id", "%(tipo)s (%(relativo_a)s)", multiple=True)

    form = SQLFORM(db.eleicao, record=request.vars.eleicao, fields=fields, submit_button="Prosseguir")
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(c='assistente_eleicao', f='cargos', vars=dict(eleicao=eleicao.id)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'

        
    return {"eleicao":eleicao, "entidade": entidade, "form": form}

@auth.requires_membership("admin")
def excluir_cargo():
    
    if request.vars.cargo:
        row = db(db.cargo.id==int(request.vars.cargo)).delete()
    redirect(URL('assistente_eleicao', 'cargos', vars=dict(mode='list', eleicao=request.vars.eleicao, entidade=request.vars.entidade)))

@auth.requires_membership("admin")
def cargos():
    if request.vars.eleicao:
        eleicao = db.eleicao(int(request.vars.eleicao))
        session.eleicao = eleicao
    else:
        eleicao = session.eleicao
    
    entidade = eleicao.entidade
    if request.vars.mode=="form":
        response.view = 'assistente_eleicao/form_cargo.html'

        fields = ["nome", "cadeiras"]

        db.cargo.entidade.default = entidade.id
        db.cargo.eleicao.default = eleicao.id

        form = SQLFORM(db.cargo, record=request.vars.cargo, fields=fields, submit_button="Salvar"
            , **{"_id":"frm-cargos", "_action": URL('assistente_eleicao', 'cargos', vars=request.get_vars, args=request.args ) })

        if form.process().accepted:
            response.flash = 'Entidade Selecionada'
            # session.entidade_id = form.vars.id
            # redirect(URL(c='assistente_eleicao', f='cargos', vars=dict(eleicao=request.vars.eleicao)))

        elif form.errors:
            response.flash = 'há erros no formulário'
        else:
            response.flash = 'Gentileza preencher o formulário'
        return {"eleicao":eleicao, "form": form, "entidade": entidade}
    
    elif request.vars.mode=='list':
        response.view = 'assistente_eleicao/cargos_list.html'
        query = db.cargo.eleicao==eleicao.id
        tbl = db.cargo
        # fields = []
        lista = db(query).select(tbl.id, tbl.eleicao, tbl.nome, tbl.cadeiras)

        return dict(lista=lista) 

    else: 
        return dict(entidade=entidade, eleicao=eleicao)   



@auth.requires_membership('admin')
def docs_cargo_eleitor():
    """
    Pegar somente documentos do tipo do eleitor
    """

    eleicao = db.eleicao(request.vars.eleicao)
    cargo = db.cargo(request.vars.cargo)
    fields = ["documentos_requeridos_eleitor"]

    #muda validador
    query = db(
        (~db.tipo_documento.id.belongs(eleicao.obrigatorio))
        & (db.eleicao.id==eleicao.id)
        & (db.tipo_documento.relativo_a==cargo.tipo_eleitor)
        & (db.cargo.id==cargo.id)
    )
    db.cargo.documentos_requeridos_eleitor.requires=IS_IN_DB(query, "tipo_documento.id", "%(tipo)s (%(relativo_a)s)", multiple=True)
    
    form = SQLFORM(db.cargo, record=request.vars.cargo, fields=fields, submit_button="Salvar"
    , **{"_id":"frm-cargos", "_action": 
        URL('assistente_eleicao', 'docs_cargo_eleitor', vars=request.get_vars, args=request.args ) }
    )

    if form.process().accepted:
        response.flash = 'Entidade Selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(c='assistente_eleicao', f='docs_cargo', vars=dict(
            cargo=request.vars.cargo, eleicao=request.vars.eleicao)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'
    
    return {"eleicao": eleicao, "form": form, "cargo":cargo}

@auth.requires_membership('admin')
def docs_cargo():
    eleicao = db.eleicao(request.vars.eleicao)
    fields = ["documentos_requeridos_cargo"]
    cargo = db.cargo(request.vars.cargo)
    #muda validador
    query = db(
        (~db.tipo_documento.id.belongs(eleicao.obrigatorio))
        & (db.eleicao.id==eleicao.id)
        & (~db.tipo_documento.id.belongs(cargo.documentos_requeridos_eleitor))
        & (db.tipo_documento.relativo_a==cargo.tipo_candidato)
        & (db.cargo.id==cargo.id)
    )

    db.cargo.documentos_requeridos_cargo.requires=IS_IN_DB(query, "tipo_documento.id", "%(tipo)s (%(relativo_a)s)", multiple=True)

    form = SQLFORM(db.cargo, record=request.vars.cargo, fields=fields, submit_button="Prosseguir"
    , **{"_id":"frm-cargos", "_action": URL('assistente_eleicao', 'docs_cargo', vars=request.get_vars, args=request.args ) })
    
    if form.process().accepted:
        response.flash = 'Entidade Selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(c='assistente_eleicao', f='cargos', vars=dict(
            cargo=request.vars.cargo, eleicao=request.vars.eleicao, mode='list')))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'
    return {"eleicao": eleicao, "form": form, "cargo": cargo}

@auth.requires_membership("admin")
def comite():
    eleicao = db.eleicao(request.vars.eleicao)
    grupo = db.auth_group(db.auth_group.role.contains("_%s"%request.vars.eleicao))
    
    def insert_or_update(h):
        record = db((db.auth_membership.user_id==int(h)) & (db.auth_membership.group_id==grupo.id)).select().first()
        if not record:
            record = db.auth_membership.insert(user_id=int(h), group_id=grupo.id)
            record = db.auth_membership(record)


            mensagem = f"""<strong>Parabéns {record.user_id.first_name}<strong>, você foi selecionado para compor a comissão eleitoral na eleição {eleicao.nome} <br><br>
            Acompanhe o processo eleitoral <a href="{URL('comite', 'index', args=eleicao.id)}">por aqui</a>.
            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
            <p class="text-danger">Não responda este email</p>
            """
            envia_emails(record.user_id.email, f"Seleção para compor comissão eleitoral", mensagem)
        else:
            record.user_id=int(h)
            record.group_id=int(h)
            record.update_record()

    entidade = eleicao.entidade
    if "habilitado" in request.post_vars:
        hab = request.post_vars.habilitado
        # é mais fácil apagar tudo e inserir tudo de novo
        db(db.auth_membership.group_id==grupo.id).delete()

        if isinstance(hab, list):
            for h in hab:
                insert_or_update(h)
        else:
            insert_or_update(hab)

        return response.render("assistente_eleicao/finalizar.html")

    lista = db(db.auth_user.is_active==True).select(orderby=db.auth_user.first_name)
    return dict(lista = lista, entidade=entidade, eleicao=eleicao, grupo=grupo.id)
