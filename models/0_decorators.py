# coding: utf8

from datetime import datetime,timedelta
import locale
# locale.setlocale(locale.LC_ALL, "pt_BR.utf8")
#del session.ano_base
if not session.ano_base:
    session.ano_base=2018


if not session.ano_certificacao:
    session.ano_certificacao=session.ano_base+1

ano_certificacao = session.ano_certificacao

dias_notificacao = 10

from decimal import *
getcontext().prec = 4

hoje = datetime.today().date() 

session.today = hoje
data_reanalise = '2018-01-09 00:00:00'




def check_edition_data():

  parametros = db(db.parametros.ano_base==session.ano_base).select().first()

  data_resultado= None
  data_preliminar = None

  limite_cadastro = None
  inicio_cadastro = None

  if parametros:
      data_resultado= parametros.data_resultado_definitivo
      data_preliminar = parametros.data_resultado_provisorio

      limite_cadastro = parametros.termino_cadastro
      inicio_cadastro = parametros.inicio_cadastro

  #limite_cadastro = datetime.strptime("31/03/{}".format(session.ano_base+1), "%d/%m/%Y").date()
  #print(limite_cadastro, inicio_cadastro)
  m = auth.user.municipio


  r = db((db.circuito_municipio.municipio == m) & (db.circuito_municipio.ano_base == session.ano_base)).select().first()
  lm = r.limite_edicao or limite_cadastro
  #print(lm)
#   print(request.function)
  if request.function in ['visualizar','visualizar2'] and request.args(0):
      mid = request.args(0)
  else:
      mid = auth.user.municipio

  if inicio_cadastro and hoje < inicio_cadastro:
      if request.function in ['editar','conselho']:
          redirect(URL(f='visualizar2',args=[mid]))


  #print(hoje,lm)
  elif data_resultado and hoje >= data_resultado:
      #print('hoje maior que data do resultado')
      if not request.function == 'visualizar2':
          redirect(URL(f='visualizar2',args=[mid]))
          session.acao = 'visualizar2'
  elif data_preliminar and hoje >= data_preliminar:
      #print('hoje maior que data do resultado')
      if not request.function == 'visualizar2':
          redirect(URL(f='visualizar2',args=[mid]))
          session.acao = 'visualizar2'
      """
  elif lm and (hoje <= lm):
      if check_habilitado(mid) and not request.function == 'visualizar2':
          redirect(URL(f='visualizar2', args=[mid]))
          session.acao = 'visualizar2'
      else:
          if mid != auth.user.municipio and not request.function == 'visualizar2':
              redirect(URL(f='visualizar2', args=[mid]))
            
  elif limite_cadastro and hoje > limite_cadastro:
      #print('hoje maior que limite cadastro')


      if r.limite_edicao and hoje > r.limite_edicao:
          #print('hoje maior que limite edição')
          if check_notificado(mid):
              #print('foi notificado')
              if not request.function == 'visualizar2':
                  redirect(URL(f='visualizar2',args=[mid]))
                  session.acao = 'visualizar2'
          elif not request.function == 'visualizar':
              redirect(URL(f='visualizar',args=[mid]))
              session.acao = 'visualizar'

  """
  elif not request.function == 'visualizar':
      redirect(URL(f='visualizar', args=[mid]))
  else:
    pass


