07/12/2021


- Filtrar as eleições às proprias do admin

- Implementar tela de recurso - Categorias Edital, Habilitação/Candidatura e Resultado
    - Preenche formulário, pode anexar documentos - envia email confirmando o recebimento  
    - A Comissão Eleitoral Preenche um formulário e ao finalizar envia um email 

    
Feito
- Caixa de confirmação ao escolher o candidato para votar
"Você confirma o seu voto em "Nome do Candidato"? 
- Na tela, inicial, quando estiver no período de votação, se o eleitor já em todos os segmentos, alterar o texto do botão para 'Confira seu voto"
- Forçar a caixa alta no nome da entidade

- Limitar os processos encerrados a 1 ano.

Fluxos 
0. Login/Cadastro
1. Administrador > Promove usuários a administrador
2. Cadastro/Gerenciamento de eleição - Administrador > Entidade, dados da eleição, documentação eleitor e candidato, segmentos e comissão 
3. Habilitação - Usuário/Entidade: Cadastro login do usuário, (opcional - cadastro da entidade), seleção de segmentos, documentação
4. Validação - Comissão > Analisar a documentação, envia email
5. Diligência - Usuário > Reeenvia a documentação rejeitada
6. Votação - Usuários 
7. Definição - Comissão valida os resultados e eleitos. 
8. Resultados - Publicação 


24/11/2021
- Alterar tela de habilitação para condicional para preenchimento do representante legal se for entidade




Fluxos 
0. Cadastro/Login Usuário
1. Administrador > Promove usuários a administrador
2. Cadastro/Gerenciamento de eleição - Administrador > Entidade, dados da eleição, documentação eleitor e candidato, segmentos e comissão 
3. Habilitação - Usuário/Entidade: Cadastro login do usuário, (opcional - cadastro da entidade), seleção de segmentos, documentação
4. Validação - Comissão > Analisar a documentação, envia email
5. Diligência - Usuário > Reeenvia a documentação rejeitada
6. Votação - Usuários 
7. Definição - Comissão valida os resultados e eleitos. 
8. Resultados - Divulgação
-------------
09/11/2021


- Corrigir envio formulário salvar segmento, redirecionr corretamente.
- incluir 'publicar' na eleição
- manter salva a entidade escolhida
- Corrigir erro cadastro de órgão colegiado 
- Deixar apenas um botão prosseguir da seleção dos segmentos na habilitação

- incluir vlibras




Feito
-----------------------

- Tela de Análise do Comitê

- Retirar o status "Não enviado"
- Incluir validação para exigir justificativa quando não aprovado


- em habilitar_eleitor/documentos/candidatar : 

- Após a inscrição, apresentar status na tela habilitar_eleitor/cargos
- Destacar o status e alterar a cor 
- Incluir opção para trocar Segmento. 
- Alterar o texto da dica para "Verifique abaixo se o Segmento para o qual você solicitou inscrição está correto. Caso queira trocar, selecione abaixo a opção correta"
- Texto dica inicial "Escolha abaixo o Segmento para o qual deseja habilitar-se para votar ou candidatar-se. Se sua candidatura for aprovada, você estará automaticamente habilitado para votar." 
- Melhorar a informação de status: "Inscrição para votar Solicitada" e "Canditatura Solicitada" 
- No envio de arquivos/documentos, informar os formatos aceitos e o tamanho máximo 



- a documentação de eleitor e candidato é organizada por eleição e não por cargo. 
- configurar o numero de segmentos que o eleitor pode habilitar (1,3, todos) e candidatar (1,2, todos)
- número de cadeiras por cargo


- Tela de Análise do Comitê
- Não diferencia se a análise foi concluída parcial ou totalmente
- Destacar o status

* Na tela de cadastro
- Retirar entidade ao cadastrar o usuário
- Retirar cor, genero, escolaridade, data_nascimento, nome da mãe e foto ao cadastrar

- na tela de habilitar o eleitor


- corrigir erro no final do cadastro da eleição
- em habilitar_eleitor/documentos/candidatar : 
- centralizar e aumentar fonte do texto "Cargo a se habilitar: Conselheiro Representante do Segmento Quilombola"
- substituir usuário por pessoa física na documentação
- Corrigir o texto da dica. 
- substituir [arquivo], por [consultar arquivo enviado]
- informar a entidade para participar do proceso no ínicio do formulário

- orgão, norma de criação
- destacar erro de não preenchimento de nome completo, cpf e email no cadastro, com a borda em vermelho
- incluir legenda informando que asterisco significa campo obrigatório. 
- colocar um asterisco em vermelho quando o campo for obrigatório
- Validar data (mostrar erro)
- Mostrar senha no login
- retirar campos obrigatórios para orgãos colegiados (cnpj, comprovante de endereço, representante legal, declaracao)
--------------
#08/05/2021#


---
# Front-End #

###    1) Tela Inicial  ###

- [x] Ajustar o Menu para ter um contraste maior (cinza para branco, de forma a contrastar com o fundo preto e 
- [x] retirar o widget search

# Back-End #

- [x] A tela inicial deverá ter seu layout alterado para ter uma lista das entidades que estão organizando os Processos Eleitorais. Isso visa facilitar para o usuário, que tem referência da Entidade: 

### 2 ) Ao clicar no link, será apresentado ao usuário, mesmo visitante, as informações de cada processo eletivo vinculado à entidade, do mais recente para o mais antigo. (Adaptando da tela Processos Eleitorais/Eleições) ###
- [x] A ultima coluna só deve estar disponível para o grupo da comissão Eleitoral daquela eleição (e não para todos os admins)

### 3) Em entidade Eleitora, ao clicar em “Habilitar-se” será direcionado para uma tela especifica que reunirá todas as informações para a Habilitação (basicamente reunindo um tela ou sequencia de telas) os dados de cadastro do Usuário/Responsável Legal, da Entidade, e os documentos específicos pelo edital. ###
- [x] reunirá todas as informações para a Habilitação: os dados de cadastro do Usuário/Responsável Legal, da Entidade, e os documentos específicos pelo edital


### 4)No cadastro da Eleição, o gestor deve ter acesso a uma tela para marcar quais serão os campos obrigatórios do Usuário, da Entidade, e os documentos obrigatórios a serem exigidos. Esses campos serão refletidos no fluxo de habilitação. ###

### 5) Incluir os seguintes campos no cadastro da Entidade: ###
- [x]    -a) Arquivo de cartão de CNPJ
- [x]    -b) Arquivo de comprovante de endereço da entidade atualizado
- [x]    -c) Município sede
- [ ]    -d) Exibir Declaração de Veracidade, conforme modelo abaixo e colocar as opções “Concordo” ou “Não Concordo”


### 6) mensagem para o usuário:“Deseja se candidatar ###
- [ ] Ao terminar de se cadastrar no fluxo de ‘habilitação’, deve ser apresentada uma mensagem para o usuário:“Deseja se candidatar?’, apresentando as opções de cargo disponíveis e os requisitos. 
* Provavelmente será necessário permitir o cadastro de um texto de apresentação do cargo e os requisitos

### 7) Fluxo/tela para se candidatar ###
- [ ] Ao escolher o cargo, o usuário será direcionado ao fluxo/tela para se candidatar, complementando as informações necessárias. 
- [ ] Da mesma forma que na habilitação, na candidatura o gestor da eleição deverá ter a opção de marcar os campos e documentos necessários. 

O objetivo dessas alterações é facilitar e orientar o usuário a identificar o processo eleitoral que se interessa, bem como facilitar o processo de habilitação. Os futuros ‘gestores’ também tiveram dificuldade em entender o fluxo para criar e gerenciar a eleição. 

### 8) Fluxo de análise da Habilitação e Candidatura. ###

* O sistema deve incluir a funcionalidade de envio de emails em pontos específicos do processo:


python web2py.py -S voto -M -R applications/voto/modules/mail_queue.py &

- [x] a) Cadastro da Habilitação 
- [x] b) Cadastro da Candidatura
- [ ] c) Resultado da Análise - Reprovado - com os detalhes da reprovação
- [ ] d) Resultado da Análise - Aprovação - listando a aceitação das informações e documentos apresentado#



### Tela de análise ###
- [ ] A comissão Eleitoral terá acesso a uma tela de análise em que, para cada informação e documento apresentado, A comissão Eleitoral marcará um checkbox para aprovado, ou descreverá em um campo texto o motivo da rejeição.  

- [ ] O detalhamento dessa análise é que servirá de conteúdo para a elaboração do email do resultado da  análise


### 9) Recursos: ###
- [ ] Os usuários devem ter a possibilidade de entrar com um ou mais “Recurso”s, quando discordarem de alguma decisão apresentada pelA comissão Eleitoral. 
Os recursos são uma comunicação entre o usuário e a comissão, iniciada sempre pelo usuário. 



Abaixo seguem os documentos originais recebidos. Algumas coisas foram alteradas em função de uma reunião, o que explica em parte as divergências com a descrição acima. 
Outros pontos podem demandar um alinhamento futuro.


---
#06/08/2020#

#1) Criar uma tela para aprovação dos perfis pendentes. ( [x] ) #

#2) Durante a criação do perfil, redirecionar o usuário para as eleições que ele vai pleitear participar. ( [x] ) #

#3) Durante a criação do perfil, permitir pleitear candidatura para entidade) -( [x] ) #

- [x] 4) Fazer uma tela de cadastro de conselho como entidade sem a exigência de: CNPJ, Segmento de Atuação 

#5) A tela de homologação deve permitir separar por eleição (eleitor e candidato) #

#6) Relatórios por eleição/conselho #


Fluxo
1) Cadastrar o Conselho
2) Cadastra a eleição (periodo de alistamento e candidatura)
3) Cadastra o cargo/área de atuação 


Fluxo
Habilitar Eleitor
