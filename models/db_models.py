#-*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# Status do eleitor
tipo_eleitor = {'usuario': "Pessoa Física" # vincular ao auth_user
                        , 'entidade': "Entidade" # vincular a entidade
                        }
db.define_table("tipo_documento",
                Field("tipo", label="Tipo do Documento",
                      requires=[IS_NOT_IN_DB(db, 'tipo_documento.tipo'),IS_NOT_EMPTY()]),
                Field('relativo_a', label="Será exigido de quem?"
                    , requires=IS_IN_SET(tipo_eleitor)
                ),
                auth.signature,
                format="%(tipo)s (%(relativo_a)s)"
                )
                # is_in_set ["cpf", "rg"...]

# Para votar no cargo do fulano ele tem que mostrar que está habilitado para votar no cargo.
db.define_table("eleicao",
                Field("entidade", "reference entidade"),
                Field("nome", label=ob("Nome/Ano da Eleição"), requires=IS_UPPER(),
                    comment="Digite o nome completo do conselho e ano de realização da eleição"
                ),
                Field("inicio_alistamento", "datetime",
                      label=ob("Inicio da Habilitação"),
                      requires=  IS_DATETIME(format=T('%d/%m/%Y %H:%M:%S'), error_message='preencha no formato  01/01/2021 08:00:00'), 
                      comment="preencha no formato  01/01/2021 08:00:00",
                      widget=SQLFORM.widgets.string.widget, 
                      ),
                Field("fim_alistamento", "datetime",
                      label=ob("Fim da Habilitação"),
                      requires=  IS_DATETIME(format=T('%d/%m/%Y %H:%M:%S'), error_message='preencha no formato  01/01/2021 08:00:00'), 
                      widget=SQLFORM.widgets.string.widget, 
                      ),
                Field("inicio_eleicao", "datetime", 
                    label="Inicio da Votação",
                      requires=  IS_DATETIME(format=T('%d/%m/%Y %H:%M:%S'), error_message='preencha no formato  01/01/2021 08:00:00'), 
                      widget=SQLFORM.widgets.string.widget,                     
                    ),
                Field("fim_eleicao", "datetime", 
                    label="Fim da Votação",
                      requires=  IS_DATETIME(format=T('%d/%m/%Y %H:%M:%S'), error_message='preencha no formato  01/01/2021 08:00:00'), 
                      widget=SQLFORM.widgets.string.widget,                      
                    ),
                Field('edital', 'upload', autodelete=True, separatefolder=True),
                Field("tipo_eleitor", label="Tipo de Eleitor"
                , requires=IS_IN_SET(tipo_eleitor) ),
                Field("tipo_candidato", label="Tipo de Candidato"
                , requires=IS_IN_SET(tipo_eleitor) ),

                Field('obrigatorio', "list:reference tipo_documento",  label="Documentos requeridos em geral",
                    widget=SQLFORM.widgets.checkboxes.widget),
                Field("prazo_correcao_documentos", "integer", label="Prazo correção documentos (dias)", default=5 ),
                Field('segmentos','integer',label='Máximo de segmentos votação', comment="Número de segmentos máximo em que o eleitor pode votar. Deixe em branco para o eleitor votar em todos"),
                Field('segmentos_candidatura','integer',label='Máximo de segmentos candidatura', 
                default=1,
                comment="Número de segmentos máximo em que o eleitor pode candidatar. Deixe em branco para o eleitor candidatar em todos"),
                Field("documentos_requeridos_eleitor", "list:reference tipo_documento",  label="Documentos requeridos ao eleitor",
                    widget=SQLFORM.widgets.checkboxes.widget),
                Field("documentos_requeridos_cargo", "list:reference tipo_documento",  label="Documentos adicionais requeridos ao candidato",
                    widget=SQLFORM.widgets.checkboxes.widget),
                Field('publicar','boolean'),

                auth.signature,
                format="%(nome)s",
                # migrate="eleicao.table"
        )


def representa_eleitor(row):
    return (
        db((db.eleitor.id == row.id) & (db.eleitor.eleitor == db.auth_user.id))
        .select(db.auth_user.first_name)
        .first()
    )

status_habilitacao_ico = {"nao_inscrito": "", "inscricao_solicitada":"text-info", "rejeitado":"text-danger", "aprovado":"text-success"}
status_habilitacao = {"nao_inscrito": "Habilitar para Votar", "inscricao_solicitada":"Habilitação Solicitada", "rejeitado":"Habilitação Rejeitada", "aprovado":"Habilitado"}
status_candidatura = {"nao_inscrito": "Candidatar-se", "inscricao_solicitada":"Candidatura Solicitada", "rejeitado":"Candidatura Rejeitada", "aprovado":"Candidatura Aprovada"}

db.define_table("eleitor",
                Field("eleicao", "reference eleicao", writable=False),
                Field("eleitor", "reference auth_user", writable=False),
                Field("status", requires=IS_IN_SET(status_habilitacao), default="nao_inscrito"),
                Field("entidade_representada", "reference entidade"
                    , requires=IS_EMPTY_OR(
                        IS_IN_DB(db(db.entidade.representante_legal==auth.user_id)
                            , "entidade.id", "%(nome)s"))),                
                auth.signature,
                format=lambda row: representa_eleitor(row)['first_name'],
                singular="Eleitor", plural="Eleitores"
                )
# Eu preciso fazer alguma forma de obrigar esse eleitor a usar aqueles documentos


db.define_table("cargo",
                Field("entidade", "reference entidade"),
                Field("eleicao", "reference eleicao"),
                # Field("tipo_eleitor", label="Tipo de Eleitor"
                # , requires=IS_IN_SET(tipo_eleitor) ),
                # Field("tipo_candidato", label="Tipo de Candidato"
                # , requires=IS_IN_SET(tipo_eleitor) ),
                Field("documentos_requeridos_eleitor", "list:reference tipo_documento",  label="Documentos requeridos ao eleitor"),
                Field("documentos_requeridos_cargo", "list:reference tipo_documento",  label="Documentos adicionais requeridos ao candidato"),
                Field("nome", label="Nome do Cargo", notnull=True, requires=IS_NOT_EMPTY()),
                Field("atribuicoes", "text", label="Atribuições", writable=False,readable=False),
                Field("cadeiras",'integer',label="Nº de cadeiras",comment="Número de representantes a serem eleitos nesse segmento"),
                auth.signature,
                format="%(nome)s"
                )

db.cargo.documentos_requeridos_eleitor.widget = SQLFORM.widgets.checkboxes.widget
# Na lista de adicionais ele precisa remover a opção, pois já foi escolhido acima.
db.cargo.documentos_requeridos_cargo.widget = SQLFORM.widgets.checkboxes.widget


db.define_table("habilitacao",
                Field("eleitor", "reference auth_user", label="Eleitor"),
                Field("eleicao",'reference eleicao'),
                Field("cargo", "reference cargo", label="Segmento de Atuação"),
                Field("entidade_representada", "reference entidade"
                    , requires=IS_EMPTY_OR(
                        IS_IN_DB(db(db.entidade.representante_legal==auth.user_id)
                            , "entidade.id", "%(nome)s"))),
                Field("status",requires=IS_IN_SET(status_habilitacao), default="inscricao_solicitada" )
            )


def representa_candidato(row):
    return (
        db((db.candidato.id == row.id)
            & (db.candidato.candidato == db.auth_user.id)
           ).select(db.auth_user.first_name).first()
    )


db.define_table("candidato",
                Field("candidato", "reference auth_user", writable=False),
                Field("entidade", "reference entidade", requires=IS_EMPTY_OR(
                        IS_IN_DB(db(db.entidade.representante_legal==auth.user_id)
                            , "entidade.id", "%(nome)s"))),
                Field("eleicao", "reference eleicao"),
                Field("cargo", "reference cargo", notnull=True,
                      label="Segmento de Atuação"),
                Field("status", requires=IS_IN_SET(status_candidatura)),
                auth.signature,
                format=lambda row: representa_candidato(row)['first_name']
                )

status_documento_ico = {'':"", "nao_enviado": "text-danger", "aguardando_aprovacao": "text-info", "rejeitado":"text-danger", "aprovado":"text-success"}
status_documento = {"aguardando_aprovacao":"AGUARDANDO ANÁLISE", "rejeitado":"REJEITADO", "aprovado":"APROVADO", "nao_enviado":"NÃO ENVIADO"}
# Documentos que o candidato precisa apresentar
db.define_table("documentos",
                Field("user_id", "reference auth_user"),
                Field("nome_arquivo", label="Nome do Arquivo"),
                Field("tipo_arquivo", "reference tipo_documento", label="Tipo do Arquivo"),
                Field("arquivo", "upload", uploadseparate=True, requires=IS_FILE(
                    extension=['pdf', 'jpg', 'png', "gif"], error_message="Entre com um arquivo dos tipos -> 'pdf', 'jpg', 'png', 'gif'")),
                Field("status", requires=IS_IN_SET(status_documento)),  # widget=bt_aprova),
                Field("observacoes", "text", label="Descreva os motivos da rejeição"), # Use para fazer comentários
                auth.signature,
                )

db.define_table("voto",
                Field("entidade", "reference entidade"),
                Field("eleicao", "reference eleicao"),
                Field("cargo", "reference cargo"),
                Field("candidato", "reference candidato"),
                auth.signature,
                )

eleicoes_publicadas = db(db.eleicao.publicar==True)
db.define_table('recurso',
    Field('titulo', label=ob('Título'), requires=IS_NOT_EMPTY(error_message="Digite um título para o seu recurso")),
    Field('tipo', label = ob('Categoria'),
        requires=IS_IN_SET(['Edital','Habilitação','Candidatura','Resultado da Eleição'],error_message="Escolha a categoria do recurso")),
    Field('eleicao','reference eleicao',
        label = ob('Eleição'),
     requires = IS_IN_DB(eleicoes_publicadas, 'eleicao.id', '%(nome)s',error_message='Escolha a eleição a que se refere o recurso')),
    Field('descricao','text', label=ob('Recurso'), requires=IS_NOT_EMPTY(error_message="Preencha o conteúdo do seu recurso")),
    auth.signature,
    format = '(%(id)s) %(titulo)s'
)

db.define_table('documento_recurso',
    Field('recurso','reference recurso'),
    Field('nome',label=ob('Nome do arquivo'),requires=IS_NOT_EMPTY(error_message="Descreva sinteticamente o conteúdo do arquivo")),
    Field('arquivo','upload',uploadseparate=True, requires=IS_FILE(extension=['jpg','jpeg','pdf','png']),
            comment="Arquivos jpg,jpeg,pdf ou png de até 10MB"
    ),
    auth.signature,
)

db.define_table('resposta_recurso',
    Field('recurso','reference recurso'),
    Field('resposta','text'),
    auth.signature,
)

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
#auth.enable_record_versioning(db)