#-*- coding: utf-8 -*-
# ----------------------------------------------------------------------------------------------------------------- #
# Uso preferencial deste arquivo para funções auxiliares que possam ser utilizadas em todo o app.
# Validadores prefira usar 0_validators.py.
# ----------------------------------------------------------------------------------------------------------------- #




def check_candidato(eleicao, candidato):
    candidatado = db((db.candidato.candidato == candidato)
                        & (db.candidato.eleicao == eleicao)).select().first()
    return candidatado

# Refatorar para envio geral
def email_registro(esta_eleicao, nome_eleicao, mensagem, mensagem_comite=None):
    """Função de envio de e-mail \n
        -[] Deve ser padronizada para uso geral de envios de e-mail (exceto controle de acesso?)\n
        -[] Deve permitir envio principal e, opcionalmente, envios de aviso a gestores\n
        -[] Modificar para receber a mensagem a ser enviada e a mensagem opcional\n
        """
    usuario = db.auth_user(db.auth_user.id == auth.user_id)
    email_gestor = db((db.eleicao.id == esta_eleicao) & (
        db.auth_user.id == db.eleicao.created_by)).select(db.auth_user.email).first()['email']
    envia_emails(para=usuario.email,
                 assunto=f'Candidatura efetuada para {nome_eleicao}', mensagem=mensagem)
    if mensagem_comite is not None:
        envia_emails(para=email_gestor, assunto='Candidato inscrito',
                     mensagem=mensagem_comite)

def check_docs(register_form, docs_registrados, docs_obrigatorios, esta_eleicao, nome_eleicao):
    obrigatorios = set(docs_obrigatorios)
    registrados = set(docs_registrados or [])

    if obrigatorios == registrados:
        usuario = db.auth_user(db.auth_user.id == auth.user_id)
        mensagem = f'Sua candidatura para a {nome_eleicao} foi efetuada com sucesso!'
        mensagem_comite = f'O usuário {usuario.first_name} se candidatou para a eleição {nome_eleicao}!'
        email_registro(esta_eleicao, nome_eleicao,
                       mensagem, mensagem_comite)
        response.flash = "Inscrição Solicitada"
        register_form.record.update_record(status='Inscrição Solicitada')
        db.commit()

    else:
        db.rollback()
        register_form.errors.eleitor = 'Insira os documentos'
        response.flash = "TODOS OS DOCUMENTOS NECESSÁRIOS DEVEM SER INSERIDOS"                     

def email_habilitacao(esta_eleicao, nome_eleicao, nome_eleitor):
    """Função de envio de e-mail \n
        -[] Deve ser padronizada para uso geral de envios de e-mail (exceto controle de acesso?)\n
        -[] Deve permitir envio principal e, opcionalmente, envios de aviso a gestores\n
        -[] Modificar para receber a mensagem a ser enviada e a mensagem opcional\n
        """
    email_eleitor = db(db.auth_user.id == auth.user_id).select(
        'email').first()['email']
    email_gestor = db((db.eleicao.id == esta_eleicao) & (
        db.auth_user.id == db.eleicao.created_by)).select(db.auth_user.email).first()['email']
    mensagem = f'Sua inscrição para a {nome_eleicao} foi efetuada com sucesso!'
    mensagem_comite = f'O usuário {nome_eleitor} se inscreveu para a eleição {nome_eleicao}!'
    envia_emails(para=email_eleitor,
                 assunto=f'Inscrição efetuada para {nome_eleicao}', mensagem=mensagem)
    envia_emails(para=email_gestor, assunto='Eleitor inscrito',
                 mensagem=mensagem_comite)

def cria_grupo(form):

    # checa se o grupo existe
    group = db(db.auth_group.role=='eleicao_{}'.format(form.vars.id)).select().first()
    if not group:
        id_grupo = auth.add_group('eleicao_{}'.format(form.vars.id),
                                'grupo de admin da eleicao de id = {}'.format(form.vars.id))
        auth.add_membership(id_grupo, auth.user_id)
    else:
        # grupo existe checa permissão
        membership = db((db.auth_membership.group_id==group.id) & (db.auth_membership.user_id==auth.user_id))
        if not membership:
            auth.add_membership(id_grupo, auth.user_id)


def envia_emails(para, assunto, mensagem):
    mail = auth.settings.mailer
    if mail:
        if db.queue.insert(status='pending',
                           email=para,
                           subject=assunto,
                           msg=mensagem):
            
            response.flash = 'Email programado para envio.'
        else:
            response.flash = 'Falha na programação do envio'

        # Codigo inserido a fim de fazer o envio dos emails pendentes assim que programar na fila
        # Não é uma boa prática, o ideal é deixar rodando como um serviço.
        import datetime
        rows = db(db.queue.status=='pending').select()
        data = make_datatime(datetime.datetime.today())
        for row in rows:
            if mail.send(to=row.email,
                subject= row.subject,
                message= ('',response.render('generic_email.html', dict(msg=row.msg))),
                headers= {'Content_Type': 'multipart'}):
                row.update_record(status='sent')
                print(f'E-mail enviado com sucesso para {row.email} em {data}')
            else:
                row.update_record(status='failed')
                print('Falha no envio agendado para {row.email} em {data}')
            db.commit()
    else:
        response.flash = 'Não foi capaz de enviar o email'
    return response.flash

def documentos_requeridos(eleicao, modo):
    """
    Devolve uma lista de ids dos documentos requeridos para o tipo de eleitor e para o candidato
    eleicao: <Row> de eleição
    cargo: <Row> de cargo
    modo: [candidatar | votar ]
        votar: para listar documentos do eleitor
        candidatar: para listar documentos do candidato

    returns list<int> ids requeridos        
    """    
    
    lista = []
    if eleicao.obrigatorio:
        lista += eleicao.obrigatorio
    
    # Acrescenta os documentos requeridos para os candidatos
    if modo=='candidatar':
        # return modo
        lista += eleicao.documentos_requeridos_cargo
        # return len(lista)
        if eleicao.tipo_candidato=='usuario':
            # precisa garantir tipos de documentos para tipo de eleitor do cargo
            t = db((db.tipo_documento.relativo_a==eleicao.tipo_candidato) & (db.tipo_documento.id.belongs(lista))
                ).select(db.tipo_documento.id)

            return [p.id for p in t]
    
    elif modo=='votar':
        # lista += cargo.documentos_requeridos_eleitor

        if eleicao.tipo_eleitor=='usuario':
            # precisa garantir tipos de documentos para tipo de eleitor do cargo
            t = db((db.tipo_documento.relativo_a==eleicao.tipo_eleitor) & (db.tipo_documento.id.belongs(lista))
                ).select(db.tipo_documento.id)

            return [p.id for p in t]
    
    return lista

def documentos_cadastrados(docs, user_id):
    """
    Função responsável por inserir os documentos que forem exigidos na tabela de documentos dos usuários.
    Compara se um documento está na tabela de documentos e o insere.
    Durante o processo de envio basta fazer um select somente nos documentos obrigatórios.

    docs: lista de ids dos documentos requeridos
    user_id: "Dono" dos documentos

    """
    # pesquisa no banco se o eleitor tem documentos
    query = db.documentos.user_id==user_id
    query &= db.documentos.tipo_arquivo.belongs(docs)
    docs_cadastrados = [d.tipo_arquivo for d in db(query).select(db.documentos.tipo_arquivo)]

    cadastrar = list(set(docs) - set(docs_cadastrados))

    for d in cadastrar:
        record = dict(
            status = "nao_enviado",
            tipo_arquivo = d,
            nome_arquivo = d,
            user_id = user_id
        )
        db.documentos.insert(**record)
    # Consulta novamente os documentos cadastrados
    query = db.documentos.user_id==user_id
    query &= db.documentos.tipo_arquivo.belongs(docs)
    return db(query).select(orderby=db.documentos.tipo_arquivo)


def form_documentos_requeridos():
    """
        Constroi uma lista de formulários para envio de documentos requeridos
    """
    pass

def relatorio_documentos():
    pass

def eleicoes_comite():
    """
    Devolve ids das eleicoes cujo usuario logado é comite
    returns list ids
    """
    rows = db.executesql(
        f"""select id from eleicao 
        where id in(select replace(auth_group.role, 'eleicao_', '')::integer as eleicao_id from auth_membership, auth_group 
            where auth_membership.user_id={auth.user_id} and auth_membership.group_id=auth_group.id and auth_group.role like 'eleicao_%' )
        
        """
        ,as_dict=True
    )
    
    return [r['id'] for r in rows] 

def check_pendencias():
    """
    Checa pendencias
    1 - Pendencias de Eleitor
    2 - Pendencias de Cancidato
    """
    
    query = db.candidato.status=='rejeitado'
    query &= db.candidato.candidato==auth.user_id
    query &= db.eleicao.inicio_eleicao >= request.now # somente eleicoes que nao iniciaram
    
    
    join =[ 
        db.eleitor.on(db.habilitacao.eleitor==db.eleitor.id) 
        ,db.eleicao.on(db.eleitor.eleicao==db.eleicao.id)
        ]

    candidato = db(query).select(db.candidato.ALL, join=join)

    query = db.habilitacao.status=='rejeitado'
    query &= db.eleitor.eleitor==auth.user_id
    # query &= db.habilitacao.eleitor==db.eleitor.id
    query &= db.eleicao.inicio_eleicao >= request.now # somente eleicoes que nao iniciaram
    
    join =[ 
        db.eleitor.on(db.habilitacao.eleitor==db.eleitor.id) 
        ,db.eleicao.on(db.eleitor.eleicao==db.eleicao.id)
        ]

    eleitor = db(query).select(db.habilitacao.ALL,
        join=join
    )

    relatorio = []
    from gluon.storage import Storage

    for c in candidato:
        relatorio.append(Storage(**{"message":f"Olá verifique sua candidatura para eleicao <strong>{c.eleicao.nome}</strong>", "eleicao": c.eleicao}))

    for e in eleitor:
        relatorio.append(Storage(**{"message":f"Olá verifique sua habilitacao para eleicao <strong>{e.eleitor.eleicao.nome}</strong>", "eleicao": e.eleitor.eleicao}))        

    session.relatorio = relatorio



def check_eleitor(eleicao, eleitor):
    alistado = db((db.eleitor.eleitor == eleitor)
                    & (db.eleitor.eleicao == eleicao)).select().first()
    return alistado

def situacao_votar(periodo, eleicao, alistado):  # sourcery no-metrics
    import datetime 
    hj = datetime.datetime.now()
    situacao = None
    habilitar_situacao = A('INSCREVA-SE',
                                _href=URL(c='habilitar_eleitor', f='index', vars=dict(eleicao=eleicao.id)),_class="btn btn-primary",_style="letter-spacing: 2px;font-weight: 900;")

    # if not alistado and periodo.fim_eleicao > hj:
    
    #     situacao = A('ENTRAR',
    #                         _href=URL(c='default', f='user/login', vars=dict(eleicao=eleicao.id)),_class="btn btn-primary", _style="letter-spacing: 3px;font-weight: 900;")
    #     return situacao
    # print('alistamento', eleicao.id, periodo.inicio_alistamento, periodo.fim_alistamento,
    #         periodo.inicio_eleicao, periodo.fim_eleicao, hj)
    if periodo.inicio_alistamento > hj:
        # print('antes do alistamento', eleicao)
        situacao = 'Aguardando período de alistamento'
    elif periodo.fim_eleicao < hj:
        # print('apos eleição', eleicao)
        situacao = A('Resultados', _href=URL(
            f='resultados', vars={'esta_eleicao': eleicao.id}))

    # periodo alistamento
    elif periodo.inicio_alistamento <= hj and periodo.fim_alistamento >= hj:
        # print('durante o alistamento', eleicao)
        if alistado:
            if alistado.status == 'Inscrição Solicitada' or alistado.status is None:
                situacao = A('Inscrito Aguardando Aprovação', _href=URL(c='alistamento', f='index'
                , vars=dict(eleicao=eleicao.id)))
            elif alistado.status == 'aprovado':
                # checar candidatura primeiro
                candidato = check_candidato(eleicao, alistado.eleitor)
                if not candidato or candidato.status == "Aprovado":
                    situacao = 'Aprovado, Aguarde abertura da votação'
                else:
                    situacao = habilitar_situacao
            elif alistado.status == 'rejeitado':
                # situacao= A('Rejeitado: Revise aqui sua documentação',_href=URL(f='alistar',args=[eleicao.id]))
                situacao = 'Rejeitado'
            else:
                situacao = habilitar_situacao
        else:
            situacao = habilitar_situacao

    elif periodo.fim_alistamento <= hj and periodo.inicio_eleicao > hj:
        situacao = 'Inscrições  Encerradas'
        if alistado:
            if alistado.status == 'aprovado':
                situacao = 'Aprovado, Aguarde período de votação'
            elif alistado.status == 'rejeitado':
                situacao = 'Rejeitado'


    # periodo eleicao
    elif periodo.inicio_eleicao <= hj and periodo.fim_eleicao >= hj:
        # print('durante eleicao', eleicao)
        if alistado and alistado.status != 'aprovado':
            situacao = 'A Inscrição Não foi Aprovada, aguarde resultados'
        elif alistado and alistado.status == 'aprovado':
            if votou_eleicao(eleicao, alistado.eleitor):
                texto = 'CONFIRA SEU VOTO'
            else:
                texto = 'VOTAR'
            situacao = A(texto, _href=URL(
                f='votar', vars={'esta_eleicao': eleicao.id}),_class="btn btn-primary",_style="letter-spacing: 3px;font-weight: 900;")
        elif auth.user_id:
            situacao = f'Não está inscrito para votar, aguarde final da eleição para ver os resultados'
        else:
            situacao = A('ENTRAR',
                            _href=URL(c='default', f='user/login', vars=dict(_next=f'/voto/default/votar/?esta_eleicao={eleicao.id}')),_class="btn btn-primary", _style="letter-spacing: 3px;font-weight: 900;")
   



    return situacao