#-*- coding: utf-8 -*-

@auth.requires_login()
def eleicoes():
    
    # response.view="generic.html"
    lista = db( db.eleicao.id.belongs(eleicoes_comite()) ).select()
    
    return dict(lista=lista)

@auth.requires_login()
def index():
    """
    args(0) == <eleicao>
    """
    
    eleicao = db(db.eleicao.id == request.args(0)).select().first()
    if not eleicao:
        redirect(URL('comite', 'eleicoes'))
    
    if not auth.has_membership("eleicao_%s" % request.args(0)):
        redirect(URL('comite', 'eleicoes'))

    grupo = db.auth_group(db.auth_group.role.contains("_%s"%request.args(0)))
    # garante que usuario é do comite
    eleitores = db(
        (db.eleitor.eleicao==eleicao.id)
        & (db.eleitor.status == 'inscricao_solicitada')
        & (db.eleitor.eleitor != auth.user_id)
        ).select()
    # r = db._lastsql
    eleitores_rejeitados = db(
        (db.eleitor.eleicao==eleicao.id)
        & (db.eleitor.status == 'rejeitado')
        & (db.eleitor.eleitor != auth.user_id)
        ).select()

    eleitores_aprovados = db(
        (db.eleitor.eleicao==eleicao.id)
        & (db.eleitor.status == 'aprovado')
        & (db.eleitor.eleitor != auth.user_id)
        ).select()

    candidatos = db(
        (db.candidato.eleicao==eleicao.id)
        & (db.candidato.status == 'inscricao_solicitada')   
        & (db.candidato.candidato != auth.user_id)
        ).select()
    candidatos_aprovados = db(
        (db.candidato.eleicao==eleicao.id)
        & (db.candidato.status == 'aprovado')        
        & (db.candidato.candidato != auth.user_id)
        ).select()
    candidatos_rejeitados = db(
        (db.candidato.eleicao==eleicao.id)
        & (db.candidato.status == 'rejeitado')        
        & (db.candidato.candidato != auth.user_id)
        ).select()

    docs_eleitor = None

    # return dict(eleitores=eleitores, candidatos=candidatos, eleicao=eleicao, docs_eleitor=docs_eleitor)
    return locals()

@auth.requires_login()
def checar_candidato():

    if not request.args:
        redirect(URL('comite', 'index'))
    status_documento = {"aguardando_aprovacao":"Aguardando Análise", "rejeitado":"Rejeitado", "aprovado":"Aprovado"}
    db.documentos.status.requires=IS_IN_SET(status_documento)
    candidato = db.candidato(request.args(0))
    
    docs = documentos_requeridos(candidato.eleicao, 'candidatar')
    docs_cadastrados = documentos_cadastrados(docs, candidato.candidato)
    
    documentos = []
    
    relatorio = ""

    if len(request.args)>1 and request.args(1) == 'finalizar':
        status = 'aprovado'
        for dc in docs_cadastrados:
            if dc.status == "rejeitado":
                relatorio += f"<li>{dc.tipo_arquivo.tipo} {status_documento.get(dc.status)} por {dc.observacoes}</li>"
                status = 'rejeitado'
        if status == 'rejeitado':
            mensagem = f"""
            <p class="text-center"> Olá, {candidato.candidato.first_name}. A comissão eleitoral avaliou os documentos encaminhados no processo eleitoral {candidato.cargo.eleicao.nome} para se candidatar ao cargo {candidato.cargo.nome}.<br>
            Os documentos abaixo foram rejeitados. Verifique os motivos e se for o caso, reenvie  os documentos com as correções necessárias e aguarde a reanálise da comissão eleitoral. <br>
            Documentos:
            <uL>
            {relatorio}
            <ul>
            <br>
            Fique atento ao sistema e seu email.<p>
            <br><br>
            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
            <p class="text-danger">Não responda este email</p>
            """
            envia_emails(candidato.candidato.email, "Análise de documentação para processo eleitoral", mensagem)
        elif status == 'aprovado':
            mensagem = f"""
            <p class="text-center"> Olá, {candidato.candidato.first_name}. A inscrição  no  processo eleitoral {candidato.cargo.eleicao.nome} para se candidatar ao cargo {candidato.cargo.nome} foi aprovada
            pela comissão eleitoral. <br>
            Aguarde o período de votação.<p>
            <br><br>
            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
            <p class="text-danger">Não responda este email</p>
            """
            envia_emails(candidato.candidato.email, "Aprovação no processo eleitoral", mensagem)       

        candidato.status = status
        candidato.update_record()
        
        
        redirect(URL('comite', 'index', args=candidato.cargo.eleicao))

    fields = ["nome_arquivo", "tipo_arquivo", "arquivo", "status", "observacoes", "modified_by", "modified_on"]
        
    for d in docs_cadastrados:
        
        # Garante que somente aguardando aprovação poderá se mechido
        db.documentos.status.writable = False
        db.documentos.observacoes.writable = False
        db.documentos.arquivo.writable = False
        
        db.documentos.tipo_arquivo.writable = False
        db.documentos.nome_arquivo.writable = False

        # Garante que somente aguardando aprovação poderá se mechido
        db.documentos.status.writable = d.status=="aguardando_aprovacao"
        db.documentos.observacoes.writable = d.status=="aguardando_aprovacao"

        form = SQLFORM(db.documentos, record=d.id, deletable=True, upload=URL('download'), fields=fields,
            submit_button="Registrar avaliação do documento"
        )
        if form.process().accepted:
            # db(db.documentos.id==form.vars.id).update(status="aguardando_aprovacao")
            redirect(URL('comite', 'checar_candidato', args=request.args))
            response.flash = "Atualizado"
            if form.status=="rejeitado":
                candidato.status="rejeitado"
                candidato.update_record()        

        elif form.errors:
            response.flash = 'há erros no formulário'
        else:
            # response.flash = 'Gentileza enviar os documentos'
            pass
        
        documentos.append(form)
    
    return dict(documentos=documentos, candidato=candidato)

@auth.requires_login()
def checar_eleitor():

    if not request.args:
        redirect(URL('comite', 'index'))


    # habilitacao = db.habilitacao(request.args(0))
    eleitor = db.eleitor(int(request.args(0)))
    
    docs = documentos_requeridos(eleitor.eleicao,  'votar')
    docs_cadastrados = documentos_cadastrados(docs, eleitor.eleitor)
    documentos = []
    status_documento = {"aguardando_aprovacao":"Aguardando Análise", "rejeitado":"Rejeitado", "aprovado":"Aprovado"}
    db.documentos.status.requires=IS_IN_SET(status_documento)
    
    fields = ["nome_arquivo", "tipo_arquivo", "arquivo", "status", "observacoes", "modified_on", "modified_by"]
    
    relatorio = ""

    if request.args(1) and request.args(1) == 'finalizar':
        status = 'aprovado'
        for dc in docs_cadastrados:
            if dc.status == "rejeitado":
                status = 'rejeitado'
                relatorio += f"<li>{dc.tipo_arquivo.tipo} {status_documento.get(dc.status)} por {dc.observacoes}</li>"
        if status == 'rejeitado':
            mensagem = f"""
            <p class="text-center"> Olá, {eleitor.eleitor.first_name}. A comissão eleitoral analisou a documentação enviada e identificou erros nos documentos apresentados. <br>
            Verifique os documentos abaixo para prosseguir no processo eleitoral {eleitor.eleicao.nome}.<br>
            Agora você precisa reenviar os documentos com as adequações solicitadas e aguardar aprovação da comissão eleitoral. <br>
            Documentos:
            <uL>
            {relatorio}
            <ul>
            <br>
            Fique atento ao sistema e seu email.<p>
            <br><br>
            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
            <p class="text-danger">Não responda este email</p>
            """
            envia_emails(eleitor.eleitor.email, "Inscrição Solicitada para processo eleitoral", XML(mensagem))

        elif status == 'aprovado':
            habilitacoes = db( (db.habilitacao.eleitor==eleitor.eleitor) 
                & (db.habilitacao.eleicao == eleitor.eleicao)
                # & (db.habilitacao.entidade_representada==None)
            ).select()    
            for h in habilitacoes:    
                h.update_record(status='aprovado')    
                db.commit()
                response.flash = f'aprovado {h}'

            mensagem = f"""
            <p class="text-center"> Olá, {eleitor.eleitor.first_name}. A comissão eleitoral analisou e aprovou a documentação enviada para o processo eleitoral {eleitor.eleicao.nome}.<br>
            Agora você só precisa aguardar o período eleitoral, de {eleitor.eleicao.inicio_eleicao} a {eleitor.eleicao.fim_eleicao} para votar. <br>
            <br>
            <br><br>
            <p>Dúvidas: sistemavoto@social.mg.gov.br</p>
            <p class="text-danger">Não responda este email</p>
            """
            envia_emails(eleitor.eleitor.email, "Aprovação de Inscrição para processo eleitoral", XML(mensagem))


        
        eleitor.status = status
        eleitor.update_record()
            
        redirect(URL('comite', 'index', args=eleitor.eleicao))

    for d in docs_cadastrados:
        
        # Garante que somente aguardando aprovação poderá se mechido
        db.documentos.status.writable = False
        db.documentos.observacoes.writable = False
        
        db.documentos.arquivo.writable = False
        
        db.documentos.tipo_arquivo.writable = False
        db.documentos.nome_arquivo.writable = False
        
        db.documentos.status.writable = d.status=="aguardando_aprovacao"
        db.documentos.observacoes.writable = d.status=="aguardando_aprovacao"
        
        form = SQLFORM(db.documentos, record=d.id, deletable=True, upload=URL('download'), fields=fields, 
            submit_button="Registrar Avaliação do Documento")
        if form.process().accepted:
            # db(db.documentos.id==form.vars.id).update(status="aguardando_aprovacao")
            
            response.flash = "Atualizado!"        
            
            # Garante modified on
            documento = db.documentos(form.vars.id)
            documento.modified_by = auth.user_id
            documento.modified_on = request.now
            documento.update_record()

            # if form.vars.status=="rejeitado":
            #     eleitor.status="rejeitado"
            #     habilitacao.update_record()

            redirect(URL('comite', 'checar_eleitor', args=request.args))
        elif form.errors:
            response.flash = 'há erros no formulário'
        # else:
        #     response.flash = 'Gentileza enviar os documentos'
        
        documentos.append(form)
    
    return dict(documentos=documentos, eleitor=eleitor)

def download():
    return response.download(request, db, attachment=False)    

def resultados():
    esta_eleicao = int(request.vars['esta_eleicao'])
    eleicao = db.eleicao(esta_eleicao)
    if eleicao.fim_eleicao > datetime.now():
        session.flash = 'A eleição ainda não foi encerrada'
        redirect(URL(f='index',args=[esta_eleicao]))

    nome_eleicao = eleicao.nome

    votos_desta_eleicao = db((db.voto.eleicao == esta_eleicao))

    total_votos = votos_desta_eleicao.count()

    # votos_por_candidato = votos_desta_eleicao.select(
    #     db.voto.ALL, orderby=db.voto.id, groupby=db.voto.id)

    eleitores = db.executesql(f'''select distinct created_by
        from voto 
        where eleicao = {eleicao.id}
    ''',as_dict=True)
    total_eleitores = len(eleitores)

    cargos_eleicao = db((db.cargo.eleicao == esta_eleicao)).select()

    total_por_cargos = {}
    for cargo in cargos_eleicao:
        candidatos_eleicao = db(
            (db.candidato.cargo == cargo)
            &(db.candidato.status == 'aprovado')
            ).select()
        total_por_candidatos = []
        for candidato in candidatos_eleicao:
            if eleicao.tipo_candidato == 'entidade':
                # candidato_nome = db((db.candidato.id == candidato) & (
                #     db.auth_user.id == db.candidato.candidato)).select(db.auth_user.first_name, distinct=True).first()
                nome_candidato = candidato.entidade.nome.upper()
            else:
                # candidato_nome = db((db.candidato.id == candidato) & (
                #     db.auth_user.id == db.candidato.candidato)).select(db.auth_user.first_name, distinct=True).first()
                nome_candidato = candidato.candidato.first_name.upper()
            candidato_votos = db((db.voto.eleicao == esta_eleicao)
                                 & (db.voto.candidato == candidato.id)).count()
            total_por_candidatos.append(
                dict(Candidato=nome_candidato, Votos=candidato_votos))

        total_por_candidatos = sorted(
            total_por_candidatos, key=lambda k: k['Votos'], reverse=True)
        
        total_por_cargos[cargo.id] = total_por_candidatos

    # return dict(nome_eleicao=nome_eleicao, total_votos=total_votos, total_por_cargos=total_por_cargos,
    #             # candidatos_eleicao=candidatos_eleicao if cargos_eleicao else None)
    return locals()

@auth.requires_login()
def recursos():
    if request.args(0) and int(request.args(0))> 0:
        session.eleicao_id = int(request.args(0))
        eleicao = db.eleicao(session.eleicao_id)
    T.force('re-br')
    q = db(db.recurso.eleicao == session.eleicao_id)
    # if not q:
    #     q = db.recurso
    db.recurso.eleicao.readable=False
    form = SQLFORM.grid(q,
        maxtextlength=200,
        deletable=False,
        details=False,
        editable=False,
        create=False,
        csv=False,
        links = [dict(header=['Acompanhar'], body=lambda row:
                          [DIV(A('Acompanhar', _href=URL(c='comite', f='recurso', args=row.id,user_signature=True)))])
                    ]
    )

    return locals()

@auth.requires_login()
def recurso():
    T.force('do-br')
    if request.args(0) and request.args(0) == 'new':
        pass
    elif request.args(0) and int(request.args(0)) > 0:
        recurso_id = int(request.args(0))
        session.recurso_id = recurso_id
    elif session.recurso_id:
        pass
    else:
        redirect(URL('recursos'))
    recurso = db((db.recurso.eleicao == session.eleicao_id)
        &(db.recurso.id==session.recurso_id)
        ).select().first()
    if not recurso:
        redirect(URL('recursos'))
    eleicao = db.eleicao(session.eleicao_id)
    # if not q:
    #     q = db.recurso
    
    form = SQLFORM(db.recurso,session.recurso_id, readonly=True)
    # q = db((db.documento_recurso.recurso==recurso_id))
    db.documento_recurso.recurso.default=session.recurso_id
    db.documento_recurso.recurso.writable=False
    db.documento_recurso.recurso.readable=False
    form1 = SQLFORM.grid(db.documento_recurso,
        deletable=False,
        csv=False,
        details=False,
        editable=False,
        maxtextlength=200,
    )
    q2 = db(db.resposta_recurso.recurso==session.recurso_id)
    db.resposta_recurso.created_by.readable=True
    db.resposta_recurso.created_by.label="Criado por"
    db.resposta_recurso.recurso.readable=False
    form2 = SQLFORM.grid(q2,
        deletable=False,
        csv=False,
        details=False,
        editable=False,
        maxtextlength=200,
        create=False,
        searchable=False,
    )
    db.resposta_recurso.recurso.default=session.recurso_id
    db.resposta_recurso.recurso.writable=False
    form3 = SQLFORM(db.resposta_recurso)

    if form3.process().accepted:
        response.flash = 'form accepted'
    elif form3.errors:
        pass
    else:
        pass

    return locals()