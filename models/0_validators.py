# coding: utf8

from decimal import *
import re


# -*- coding: utf-8 -*-
# flavio@casacurta.com
from datetime import date, datetime, timedelta
# hoje = date.today() - timedelta(days=1)
hoje = date.today()
ontem = date.today() - timedelta(days=3)
patern = re.compile('([^\d]+)')
def UNMASK(num): return re.sub(patern, '', num or '')


class MASK_DECIMAL(object):
    """
    Edit the a value mask

    If "Decimal point is comma" was defined, comma separator is dot

    example::

        db.mytable.mycolumn.represent = lambda value, row: MASK_DECIMAL()(value, 0)

        >>> MASK_DECIMAL()('.12', 2)
        '0.12'
        >>> MASK_DECIMAL(dot=',')(',12', 2)
        '0,12'
        >>> MASK_DECIMAL()('12345.67', 3)
        '12,345.670'
        >>> MASK_DECIMAL(dot=',')('1234,567', 3)
        '1.234,567'
    """

    def __init__(self, dot='.'):
        self.dot = dot
        self.comma = ',' if dot == '.' else '.'

    def __call__(self, value, dec=0):
        value = str(value)
        sign = ''
        if float(value.replace(',', '.')) < 0:
            sign = '-'
            value = value[1:]
        if dec:
            pdot = value.find(self.dot) + 1
            z = dec - (len(value) - (pdot)) if pdot else dec
            value = value + ('0' * z)
        value = value.replace(".", "").replace(",", "")
        if len(value) == dec:
            value = '0' + value
        q_int = len(value)-dec
        r = q_int % 3
        mask = eval("'{}{}{}{}'.format('{}' * r if r else ''\
                                     ,self.comma if q_int > 3 and r else ''\
                                     ,(('{}{}{}' + self.comma) * (q_int/3))[:-1]\
                                     ,self.dot + '{}' * dec if dec else '')").format(*value)

        return sign + mask


class MASK_MONEY(object):
    """
    Edit the a value money mask

    example::

        db.mytable.mycolumn.represent = lambda value, row: MASK_MONEY(
            symbol='R$')(value, 0)

        >>> MASK_MONEY()('.12', 2)
        'R$ 0,12'
        >>> MASK_MONEY(dot='.', symbol='$')(',12', 2)
        '$ 0.12'
        >>> MASK_MONEY()('12345.67', 3)
        'R$ 12.345,670'
        >>> MASK_MONEY(dot='.', symbol='R$')('1234,567', 3)
        'R$ 1.234,567'
        >>> MASK_MONEY(LC_ALL='usa')('1234567', 2)
        '$ 1,234,567.00'
    """

    def __init__(self, LC_ALL='', dot='', symbol=''):
        import locale

        locale.setlocale(locale.LC_ALL, LC_ALL)
        if not dot:
            self.dot = locale.localeconv()['decimal_point']
        else:
            self.dot = dot
        if not symbol:
            self.symbol = locale.localeconv()['currency_symbol']
        else:
            self.symbol = symbol

    def __call__(self, value, dec=0):
        rep = ',' if self.dot == '.' else '.'
        value = str(value).replace(rep, self.dot).replace(self.symbol, '')
        return '{} {}'.format(self.symbol, MASK_DECIMAL(dot=self.dot)(value, dec))


class MASK_CPF(object):
    """
    Edit the a CPF code mask

    example::

        db.mytable.mycolumn.represent = lambda value, row: MASK_CPF()(value)

        >>> MASK_CPF()('12345678909')
        '123.456.789-09'
        >>> MASK_CPF()('123456797')
        '001.234.567-97'

    """

    def __init__(self):
        pass

    def __call__(self, cpf):
        if not isinstance(cpf, (list, str)):
            cpf = str(cpf)
        if isinstance(cpf, str):
            cpf = UNMASK(cpf)
            cpf = '0' * (11 - len(cpf)) + cpf
        return '{}{}{}.{}{}{}.{}{}{}-{}{}'.format(*cpf)


class MASK_CNPJ(object):
    """
    Edit the a CNPJ code mask

    example::

        db.mytable.mycolumn.represent = lambda value, row: MASK_CNPJ()(value)

        >>> MASK_CNPJ()('12345678000195')
        '12.345.678/0001-95'
        >>> MASK_CNPJ()('123456000149')
        '00.123.456/0001-49'
    """

    def __init__(self):
        pass

    def __call__(self, cnpj):
        if not isinstance(cnpj, (list, str)):
            cnpj = str(cnpj)
        if isinstance(cnpj, str):
            cnpj = UNMASK(cnpj)
            cnpj = '0' * (14 - len(cnpj)) + cnpj
        return '{}{}.{}{}{}.{}{}{}/{}{}{}{}-{}{}'.format(*cnpj)


class MASK_DV(object):
    """
    Edit the a digit checker

    example::

        db.mytable.mycolumn.represent = lambda value, row: MASK_DV('/')(value)

        >>> MASK_DV('-')('12345678000195')
        '1234567800019-5'
    """

    def __init__(self, mask=''):
        self.mask = mask

    def __call__(self, value):
        if not isinstance(value, (list, str)):
            value = str(value)
        if isinstance(value, str):
            value = UNMASK(value)
        return '{}{}{}'.format(value[:-1], self.mask, value[-1])


def make_data(field):
    if field != None:
        try:
            return field.strftime("%d/%m/%Y")
        except:
            return field
    else:
        return ''


data = IS_NULL_OR(IS_DATE(format=T('%d/%m/%Y')))


def isCpfValid(cpf):
    """ If cpf in the Brazilian format is valid, it returns True, otherwise, it returns False. """

    # Check if type is str
    if not isinstance(cpf, str):
        return False

    # Remove some unwanted characters
    cpf = re.sub("[^0-9]", '', cpf)

    # Checks if string has 11 characters
    if len(cpf) != 11:
        return False

    sum = 0
    weight = 10

    """ Calculating the first cpf check digit. """
    for n in range(9):
        sum = sum + int(cpf[n]) * weight

        # Decrement weight
        weight = weight - 1

    verifyingDigit = 11 - sum % 11

    if verifyingDigit > 9:
        firstVerifyingDigit = 0
    else:
        firstVerifyingDigit = verifyingDigit

    """ Calculating the second check digit of cpf. """
    sum = 0
    weight = 11
    for n in range(10):
        sum = sum + int(cpf[n]) * weight

        # Decrement weight
        weight = weight - 1

    verifyingDigit = 11 - sum % 11

    if verifyingDigit > 9:
        secondVerifyingDigit = 0
    else:
        secondVerifyingDigit = verifyingDigit

    if cpf[-2:] == "%s%s" % (firstVerifyingDigit, secondVerifyingDigit):
        return True
    return False


class TO_UPPER(object):

    def __init__(self, format=True, error_message=T('Digite algum texto!')):
        self.error_message = error_message
        self.format = format

    def __call__(self, value):
        return value.upper()

    def doformat(self, value):
        return value.upper()


class IS_CELULAR(object):
    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            telefone = str(value)
            # return(cpf,'aquiok'+str(len(cpf)==11))
            if len(telefone) >= 12:
                # return (value, 'cpf acima de 11')
                c = []
                for d in telefone:
                    if d.isdigit():
                        c.append(d)
                cl = str(''.join(c))
                # return (value, 'cpf incorreto'+str(cl))
                if len(cl) in [11, 12, 13]:
                    return (str(cl), None)
                elif len(cl) < 11:
                    return (value, 'telefone incompleto')
                else:
                    return (value, 'o telefone tem mais de 14 dígitos')
            else:
                return (value, 'O telefone deve estar no formato  0 00-90000-0000, inclua o código do país')
            # return(cpf,'aquiok'+str(len(cpf)==11))
        except:
            return (value, 'algum erro'+str(value))

    def formatter(self, value):
        formatado = value
        if value and len(value) >= 11:
            if len(value) == 12:
                formatado = value[0:1] + \
                    ' (' + value[1:3]+') '+value[3:8]+'-'+value[8:12]
            elif len(value) == 13:
                formatado = value[0:2] + \
                    ' (' + value[2:4]+') '+value[4:9]+'-'+value[9:13]
            elif len(value) == 14:
                formatado = value[0:3]+' (' + value[3:5]+') ' + \
                    value[5:10]+'-'+value[10:14]

        return formatado

# -*- coding: utf-8 -*-
# flavio@casacurta.com


class IS_DECIMAL(IS_DECIMAL_IN_RANGE):

    def __call__(self, value):

        comma = ',' if self.dot == '.' else '.'
        import decimal
        try:
            if isinstance(value, decimal.Decimal):
                v = value
            else:
                v = decimal.Decimal(str(value).replace(
                    comma, '').replace(self.dot, '.'))
            if self.minimum is None:
                if self.maximum is None or v <= self.maximum:
                    return (v, None)
            elif self.maximum is None:
                if v >= self.minimum:
                    return (v, None)
            elif self.minimum <= v <= self.maximum:
                return (v, None)
        except (ValueError, TypeError, decimal.InvalidOperation):
            pass
        return (value, self.error_message)


class IS_MONEY(object):

    """
    Checks if field's value is a valid decimal money

    Examples::

        INPUT(_type='text', _name='name', requires=IS_MONEY())

        >>> IS_MONEY(0, 999.99, dot=',', symbol='R$')('R$ 123,45')
        (Decimal('123.45'), None)
        >>> IS_MONEY()('$ 123.45')
        (Decimal('123.45'), None)

    """

    def __init__(self, minimum=None, maximum=None, error_message=None, dot='.', symbol='$'):

        self.minimum = minimum
        self.maximum = maximum
        self.error_message = error_message
        self.dot = dot
        self.symbol = symbol

    def __call__(self, money):

        value = str(money).replace(self.symbol, '').strip()
        return IS_DECIMAL(minimum=self.minimum, maximum=self.maximum, error_message=self.error_message, dot=self.dot)(value)


class IS_CEP(object):

    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            # return (value, 'cpf incorreto'+str(value))
            # return (value, 'cpf incorreto'+str(cl))
            c = []
            for d in value:
                if d.isdigit():
                    c.append(d)
            cl = str(''.join(c))
            # return (value, 'cpf incorreto'+str(cl))
            if len(cl) == 8:
                cep = cl
                return(str(cep), None)
            else:
                return (value, 'Número de dígitos incorreto para CEP')

        except:
            return (value, 'algum erro'+str(value))

    def formatter(self, value):
        formatado = value[0:2]+'.'+value[2:5]+'-'+value[5:8]
        return formatado


def verifica_vencimento(user_id=None):
    # from datetime import date
    # print(auth.user)
    if not user_id:
        if auth.user and auth.user.id:
            user_id = auth.user.id
    if user_id:
        # print(auth.user.id)
        row = db((db.pagamento.cliente == user_id)
                 & (db.pagamento.status == 'Aprovado')
                 & (db.pagamento.plano.belongs([2, 3, 4, 5]))
                 ).select(orderby=~db.pagamento.vencimento).first()
        # print(rows)
        if not row:
            db.pagamento.insert(
                cliente=user_id,
                tipo='Voucher',
                plano=4,
                voucher='7-DIASGRATIS-UID1-HFN24N',
                status='Aprovado',
                inicio=hoje,
                vencimento=hoje+timedelta(days=7),
            )
            db.commit()
            row = db(db.pagamento.cliente == user_id).select(
                orderby=~db.pagamento.vencimento).first()
        if user_id in (1,):
            # vencimento = hoje
            return hoje+timedelta(days=7), 5
        return row.vencimento, row.plano
    else:
        return None, None


def aviso_plano(vencimento, plano):
    d = vencimento - hoje
    falta = d.days
    debug = True
    p = db.plano(plano)
    nome_plano = "({}) {} - {} ".format(p.id, p.nome, p.valor)
    aviso = ''
    if falta < 0:
        aviso = "<strong style='color:red;'>Seu plano {1} venceu em {0}</strong>".format(
            make_data(vencimento),  nome_plano)
    else:
        aviso = "<strong style='color:red;'>Seu plano {1} vence em {0} dias</strong><br>".format(
            falta, nome_plano)

    return aviso


def advanced_editor(field, value):
    return TEXTAREA(_id=str(field).replace('.', '_'),
                    _name=field.name, _class='text ckeditor', value=value, _cols=80, _rows=10)


def to_decimal(value):
    value = value.replace('R$', '')
    without_dot = value.replace('.', '')
    decimal = without_dot.replace(',', '.')
    return decimal


class IS_CPF_OR_CNPJ(object):
    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            # return (value, 'cpf incorreto'+str(value))
            # return (value, 'cpf incorreto'+str(cl))
            c = []
            for d in value:
                if d.isdigit():
                    c.append(d)
            cl = str(''.join(c))
            # return (value, 'cpf incorreto'+str(cl))
            if len(cl) == 11:
                cpf = cl
                cnpj = None
            elif len(cl) == 14:
                cpf = None
                cnpj = cl
            else:
                return (value, 'Número de dígitos incorreto para CPF ou CNPJ')

            # return(cpf,'aquiok'+str(len(cpf)==11))
            if cpf:

                def valida(value):

                    def calcdv(numb):
                        result = int()
                        seq = reversed(
                            ((range(9, id_type[1], -1) * 2)[:len(numb)]))
                        # return (value,'to fundo1')
                        for digit, base in zip(numb, seq):
                            result += int(digit) * int(base)

                        dv = result % 11
                        # return (value,'to fundo1'+str(dv))
                        return (dv-10) and dv or 0

                    id_type = ['CPF', -1]

                    numb, xdv = value[:-2], value[-2:]

                    dv1 = calcdv(numb)
                    # return (value,'entrei'+str(dv1))
                    dv2 = calcdv(numb+str(dv1))
                    return (('%d%d' % (dv1, dv2) == xdv and True or False), id_type[0])

                try:
                    cpf = str(value)
                    # return(cpf,'aquiok'+str(len(cpf)==11))
                    if len(cpf) >= 11:

                        # return (value, 'cpf acima de 11')
                        c = []
                        for d in cpf:
                            if d.isdigit():
                                c.append(d)
                        cl = str(''.join(c))
                        # return (value, 'cpf incorreto'+str(cl))
                        if len(cl) == 11:
                            if valida(cl)[0] == True:
                                return (value, None)
                            else:
                                return (value, 'cpf inválido')
                        elif len(cl) < 11:
                            return (value, 'cpf incompleto')
                        else:
                            return (value, 'cpf tem mais de 11 dígitos')
                        if cpf[3] != '.' or cpf[7] != '.' or cpf[11] != '-':
                            return (value, 'cpf deve estar no formato 000.000.000-00'+cpf[11])
                    else:
                        return (value, 'cpf deve estar no formato 000.000.000-00')
                    # return(cpf,'aquiok'+str(len(cpf)==11))
                except:
                    return (value, 'algum erro'+str(value))
            elif cnpj:

                """ Pega apenas os 12 primeiros dígitos do CNPJ e gera os 2 dígitos que faltam """
                inteiros = map(int, cnpj)
                novoCnpj = inteiros[:12]

                prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
                while len(novoCnpj) < 14:
                    r = sum([x * y for (x, y) in zip(novoCnpj, prod)]) % 11
                    if r > 1:
                        f = 11-r
                    else:
                        f = 0
                    novoCnpj.append(f)
                    prod.insert(0, 6)
                # return(str(novoCnpj),'aquiok')
                """ Se o número gerado coincidir com o número original, é válido """
                if novoCnpj == inteiros:
                    # cnpj = ''.join(novoCnpj)

                    return (str(cnpj), None)

                else:
                    return (value, 'CNPJ não é válido')

        except:
            return (value, 'algum erro'+str(value))

    def formatter(self, value):
        if len(value) == 11:
            formatado = value[0:3]+'.'+value[3:6] + \
                '.'+value[6:9]+'-'+value[9:11]
        elif len(value) == 14:
            formatado = value[0:2]+'.'+value[2:5]+'.' + \
                value[5:8]+'/'+value[8:12]+'-'+value[12:14]
        else:
            formatado = value
        return formatado


class IS_TELEFONE(object):
    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            telefone = str(value)
            # return(cpf,'aquiok'+str(len(cpf)==11))
            if len(telefone) >= 10:
                # return (value, 'cpf acima de 11')
                c = []
                for d in telefone:
                    if d.isdigit():
                        c.append(d)
                cl = str(''.join(c))
                # return (value, 'cpf incorreto'+str(cl))
                if len(cl) == 10:
                    return (str(cl), None)
                elif len(cl) < 10:
                    return (value, 'telefone incompleto')
                else:
                    return (value, 'o telefone tem mais de 10 dígitos')
                if cpf[2] != '-' or cpf[7] != '-':
                    return (value, 'o telefone deve estar no formato 00-0000-0000')
            else:
                return (value, 'O telefone deve estar no formato 00-0000-0000')
            # return(cpf,'aquiok'+str(len(cpf)==11))
        except:
            return (value, 'algum erro'+str(value))

    def formatter(self, value):
        if value and len(value) == 10:
            formatado = value[0:2]+'-'+value[2:6]+'-'+value[6:10]
        else:
            formatado = value
        return formatado


class IS_CELULAR(object):
    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            telefone = str(value)
            # return(cpf,'aquiok'+str(len(cpf)==11))
            if len(telefone) >= 11:
                # return (value, 'cpf acima de 11')
                c = []
                for d in telefone:
                    if d.isdigit():
                        c.append(d)
                cl = str(''.join(c))
                # return (value, 'cpf incorreto'+str(cl))
                if len(cl) == 11:
                    return (str(cl), None)
                elif len(cl) < 11:
                    return (value, 'telefone incompleto')
                else:
                    return (value, 'o telefone tem mais de 11 dígitos')
                if cpf[2] != '-' or cpf[7] != '-':
                    return (value, 'o telefone deve estar no formato 00-90000-0000')
            else:
                return (value, 'O telefone deve estar no formato 00-90000-0000')
            # return(cpf,'aquiok'+str(len(cpf)==11))
        except:
            return (value, 'algum erro'+str(value))

    def formatter(self, value):
        if value and len(value) == 10:
            formatado = value[0:2]+'-'+value[2:6]+'-'+value[6:10]
        else:
            formatado = value
        return formatado


def to_telefone(value):
    if value and len(value) == 10:
        formatado = '('+value[0:2]+') '+value[2:6]+'-'+value[6:10]
    elif value and len(value) == 11:
        formatado = '('+value[0:2]+') '+value[2]+' '+value[3:7]+'-'+value[7:11]
    else:
        formatado = ''
    return formatado


def isCpfValid(cpf):
    """ If cpf in the Brazilian format is valid, it returns True, otherwise, it returns False. """

    # Check if type is str
    if not isinstance(cpf, str):
        return False

    # Remove some unwanted characters
    cpf = re.sub("[^0-9]", '', cpf)

    # Checks if string has 11 characters
    if len(cpf) != 11:
        return False

    sum = 0
    weight = 10

    """ Calculating the first cpf check digit. """
    for n in range(9):
        sum = sum + int(cpf[n]) * weight

        # Decrement weight
        weight = weight - 1

    verifyingDigit = 11 - sum % 11

    if verifyingDigit > 9:
        firstVerifyingDigit = 0
    else:
        firstVerifyingDigit = verifyingDigit

    """ Calculating the second check digit of cpf. """
    sum = 0
    weight = 11
    for n in range(10):
        sum = sum + int(cpf[n]) * weight

        # Decrement weight
        weight = weight - 1

    verifyingDigit = 11 - sum % 11

    if verifyingDigit > 9:
        secondVerifyingDigit = 0
    else:
        secondVerifyingDigit = verifyingDigit

    if cpf[-2:] == "%s%s" % (firstVerifyingDigit, secondVerifyingDigit):
        return True
    return False


class IS_CPF(object):

    def __init__(self, format=False, error_message=T('Informe um cpf válido!')):
        self.error_message = error_message
        self.format = format

    def __call__(self, value):
        try:
            cl = str(''.join(c for c in value if c.isdigit()))

            if len(cl) == 11:
                cpf = cl
            else:
                return value, self.error_message

            if cpf:
                check = isCpfValid(cpf)
                if check:
                    return self.doformat(cpf) if self.format else cpf, None
                else:
                    return cpf, T('CPF inválido')

        except Exception as erro:
            return value, self.error_message + '{}'.format(erro)

    def doformat(self, value):
        if len(value) == 11:
            result = value[0:3] + '.' + value[3:6] + '.' + value[6:9] + \
                '-' + value[9:11]
        else:
            result = value
        return result


'''
dinheiro = lambda amount: "%s%s%s" %(
    "-" if amount < 0 else "",
    'R$',
    ('{:%d,.2f}'%(len(str(amount))+3)).format(abs(amount)).lstrip())

'''


def dinheiro(valor):
    s = str(valor)
    if '.' in s:
        cents = s.split('.')[1]
        # print(cents)
        if len(cents) == 1:
            cents = ',{}0'.format(cents)
        elif len(cents) == 2:
            cents = ',{}'.format(cents)
        elif len(cents) == 0:
            cents = ',00'.format(cents)
    else:
        # print(s)
        cents = '{},00'.format(s)

    inteiros = s.split('.')[0]
    i = len(inteiros)
    # print(i)
    n = 0
    partes = []
    if i % 3 != 0:
        r = i % 3
        # 'print(r)

        n = r
        partes.append('{}'.format(inteiros[0:n]))

    else:
        s = ''
    if i > 3:

        while n < i:
            pt = n+3
            partes.append(inteiros[n:pt])
            n = pt
    elif i == 3:
        s = partes.append(inteiros)

    s = "R$ "+'.'.join(partes)
    return s+cents


def lt(s):
    try:
        s = unicode(s, 'utf-8').encode('latin-1')
    except:
        s = str(s)
    return s


def tl(s):
    try:
        data = s
        udata = data.decode("utf-8")
        return udata.encode("latin-1", "ignore")
    except:
        return ''


def strip_mail(email):
    if len(email) > 20:
        return email[:15]+'...'
    else:
        return email


data = IS_NULL_OR(IS_DATE(format=T('%d/%m/%Y')))


def make_data(field):
    if field != None:
        try:
            return field.strftime("%d/%m/%Y")
        except:
            return field
    else:
        return ''


def make_datatime(field):
    if field != None:
        try:
            return field.strftime("%d/%m/%Y - %H:%M:%S")
        except:
            return field
    else:
        return ''


def get_municipio(cod):
    # m = db(db.municipio.id==cod).select(db.municipio.municipio).first()
    return db.municipio[cod].municipio


# Field('first_name', length=128, default='', required=True, label=T('Nome')),
# Field('last_name', length=128, default='',required=True, label=T('Sobrenome')),


locale_code = "pt_BR.UTF-8"  # para unix


def money(value):
    if value < 0:
        value = 0

    import locale
    locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')
    value = locale.currency(value, grouping=True, symbol=True)

    return value


def votou(eleicao, eleitor, cargo):
    votou = db((db.voto.eleicao == eleicao) & (db.voto.created_by ==
               eleitor) & (db.voto.cargo == cargo)).select().first()
    return votou


def votou_eleicao(eleicao, eleitor):
    habilitacoes = db((db.habilitacao.eleicao == eleicao)
                &(db.habilitacao.eleitor== eleitor)
                & (db.habilitacao.status == 'aprovado')
    ).select()
    v = 0
    for h in habilitacoes:
        votou = db((db.voto.eleicao == eleicao) & (db.voto.created_by ==
               eleitor) & (db.voto.cargo == h.cargo)).select().first()
        if votou:
            v += 1

    if len(habilitacoes) == v:
        return True
    else:
        return False
        
        


def participacao(eleicao_id):

    habilitados = len(db((db.eleitor.eleicao == eleicao_id) &
                      (db.eleitor.status == 'Aprovado')).select())
    votos = db.executesql("""select distinct created_by from voto where
            eleicao = {0}

            """.format(eleicao_id), as_dict=True)

    if votos:
        participantes = len(votos)
    else:
        participantes = 0

    print(participantes, '/', habilitados)
    return '{} votaram/{} habilitados'.format(participantes, habilitados)


def documentacao_eleitor(eleitor_id):
    aprovados = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        db.documentos_eleitor.status == 'Aprovado')).select('tipo_arquivo').as_dict()
    rejeitados = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        db.documentos_eleitor.status == 'Rejeitado')).select('tipo_arquivo').as_dict()

    if aprovados:
        aprovados = len(aprovados)
    else:
        aprovados = 0
    if rejeitados:
        rejeitados = len(rejeitados)
    else:
        rejeitados = 0

    print(aprovados, '/', rejeitados)
    return '{} aprovados/ {} rejeitados'.format(aprovados, rejeitados)


def check_documentacao_eleitor(eleitor_id):
    eleicao = db.eleitor(db.eleitor.id == eleitor_id).eleicao
    necessarios = db.eleicao(
        db.eleicao.id == eleicao).obrigatorio
    aprovados = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        db.documentos_eleitor.status == 'Aprovado')).select('tipo_arquivo').as_dict()

    if set(aprovados) == set(necessarios):
        return True
    else:
        return False


def documentacao_candidato(candidato_id):

    cargo = db.candidato(db.candidato.id == candidato_id).cargo
    necessarios = len(db.cargo(db.cargo.id == cargo).documentos_requeridos)
    aprovados = db((db.documentos.candidato == candidato_id) & (
        db.documentos.status == 'Aprovado')).select('tipo_arquivo').as_dict()

    if aprovados:
        aprovados = len(aprovados)
    else:
        aprovados = 0

    print(aprovados, '/', necessarios)
    return '{} necessarios/{} aprovados'.format(necessarios, aprovados)


def check_documentacao_candidato(candidato_id):

    cargo = db.candidato(db.candidato.id == candidato_id).cargo
    necessarios = db.cargo(db.cargo.id == cargo).documentos_requeridos
    aprovados = db((db.documentos.candidato == candidato_id) & (
        db.documentos.status == 'Aprovado')).select('tipo_arquivo').column()

    if set(aprovados) == set(necessarios):
        return True
    else:
        return False


class IS_CNPJ(object):
    def __init__(self, format=True, error_message='Digite apenas os números!'):
        self.format = format
        self.error_message = error_message

    def __call__(self, value):
        try:
            # return (value, 'cpf incorreto'+str(value))
            # return (value, 'cpf incorreto'+str(cl))
            c = []
            for d in value:
                if d.isdigit():
                    c.append(d)
            cl = str(''.join(c))
            # return (value, 'cpf incorreto'+str(cl))
            if len(cl) == 14:
                cnpj = cl
            else:
                return (value, 'Número de dígitos incorreto para CNPJ')

            if cnpj:

                """ Pega apenas os 12 primeiros dígitos do CNPJ e gera os 2 dígitos que faltam """
                # inteiros = map(int, cnpj)
                inteiros = []
                for n in cnpj:
                    inteiros.append(int(n))

                novoCnpj = inteiros[:12]

                prod = [5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2]
                while len(novoCnpj) < 14:
                    r = sum([x * y for (x, y) in zip(novoCnpj, prod)]) % 11
                    if r > 1:
                        f = 11 - r
                    else:
                        f = 0
                    novoCnpj.append(f)
                    prod.insert(0, 6)
                    # return(str(novoCnpj),'aquiok')
                """ Se o número gerado coincidir com o número original, é válido """
                if novoCnpj == inteiros:
                    #cnpj = ''.join(novoCnpj)

                    return (str(cnpj), None)

                else:
                    return (value, 'CNPJ não é válido')

        except:
            return (value, 'algum erro' + str(value))

    def formatter(self, value):
        formatado = value[0:2] + '.' + value[2:5] + '.' + \
            value[5:8] + '/ ' + value[8:12] + '-' + value[12:14]
        return formatado


def menu_inteligente(id):
    menu = '<table><tr>'
    if id == None:
        bbasic = '#007fff'

        menu += '<td style="background-color:%s;"><a href=%s style="color:white;" >1. Entidade </a><td>' % (
            bbasic, URL(r=request,
                        f='dados_entidade', user_signature=True))
        menu += '</tr></table>'

    else:
        bbasic = '#007fff'
        bmetas = '#766b6b'
        bdespesas = '#766b6b'
        blocais = '#766b6b'
        bdocumentos = '#766b6b'
        bfinalizar = '#766b6b'

        menu += '<td style="background-color:%s;"><a href=%s style="color:white;" >1. Dados da Entidade</a><td>' % (
            bbasic, URL(r=request,
                        f='dados_entidade', args=[id], user_signature=True))

        # nmetas = check_metas(id)
        # if nmetas > 0:
        #     bmetas = '#007fff'

        # menu += '<td style="background:%s; "><a href=%s style="color:white;" >2. Metas  </a><td> ' % (
        #     bmetas, URL(r=request, f='metas',
        #                 args=[request.args(0)], user_signature=True))

        # ndespesas = check_despesas(id)
        # if ndespesas > 0:
        #     bdespesas = '#007fff'
        # if nmetas > 0:
        #     menu += '<td style="background:%s;"><a href=%s style="color:white;" >3.Despesas  </a><td>' % (
        #         bdespesas, URL(r=request, f='despesas', args=[request.args(0)], user_signature=True))

        # nlocais = check_locais(id)
        # if nlocais > 0:
        #     blocais = '#007fff'
        # if ndespesas > 0 and nmetas > 0:
        #     menu += '<td style="background:%s;"><a href=%s style="color:white;" >4. Locais  </a><td>' % (
        #         blocais, URL(r=request, f='local', args=[request.args(0)], user_signature=True),)

        # doc = check_documentos(id)
        # if doc['valido'] == True:
        #     bdocumentos = '#007fff'
        # if nlocais > 0 and ndespesas > 0 and nmetas > 0:
        #     menu += '<td style="background:%s; "><a href=%s style="color:white;" >5. Documentos  </a><td>' % (
        #         bdocumentos, URL(r=request, f='documentos', args=[request.args(0)], user_signature=True))
        # if doc['valido'] == True and nlocais > 0 and ndespesas > 0 and nmetas > 0:
        #     menu += '<td style="background:%s;"><a href=%s style="color:white;" >6. Verificar </a><td>' % (
        #         bfinalizar, URL(r=request, f='status', args=[request.args(0)]))

        # if checklist(id, short=True) == 'Pronto para Protocolo':
        #     menu += '<td style="background:%s;"><a href=%s style="color:white;" >7. Finalizar </a><td>' % (
        #         bfinalizar, URL(r=request, f='protocolo', args=[request.args(0)]))

        # historico = db(db.historico.projeto == id).select(orderby=~db.historico.id).first()
        # link = ''
        # if historico:
        #     if (historico.ocorrencia == 'Em reajuste' or
        #         historico.ocorrencia[:11] == 'Diligência' or
        #         historico.ocorrencia[4:15] == 'Diligência'):
        #         menu += '<td style="background:%s;"><a href=%s style="color:white;" >8. Histórico de Tramitação </a></td>' % (
        #             bfinalizar, URL(r=request, f='detalhes', args=[request.args(0)]))
        #         menu += '</tr></table>'
        #         if historico.arquivo and len(historico.arquivo) > 0:
        #             link = "<a href=" + URL(r=request, f='download', args=[
        #                 historico.arquivo]) + " style='color:white;'>Arquivo com informações adicionais</a>"
        #         if historico.detalhamento:
        #             d = historico.detalhamento
        #         else:
        #             d = ''
        #         menu += "<p style='background:red;color:white'>%s:<br>%s <br>%s </p>" % (historico.ocorrencia, d, link)
        #     else:
        #         menu += '</tr></table>'
        # else:
        menu += '</tr></table>'
    return XML(menu)


def ob(label):
    return XML(label + " <FONT color=red>*</FONT>")