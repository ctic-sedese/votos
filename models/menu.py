# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

# ----------------------------------------------------------------------------------------------------------------------
# this is the main application menu add/remove items as required
# ----------------------------------------------------------------------------------------------------------------------
if auth.has_membership("admin"):
    response.menu = [
        (T('Processos Eleitorais'), False, URL('default', 'index'), []),
        (T('Recursos'), False, URL('default', 'recursos'), []),
        (T('Administrativo'), False, URL('default', 'index'), [
            (T('Usuários'), False, URL('default', 'usuarios'), []),
            (T('Entidades'), False, URL('default', 'entidade'), []),
            (T('Eleições'), False, URL('default', 'eleicao'), []),
            (T('Tipos de Documento'), False, URL('default', 'tipo_de_documento'),[]),
            (T('Minhas Entidades'), False, URL('default', 'minhaentidade'), []),
            ]),
    ]
    m = db.executesql(f'''select id from auth_membership
                        where user_id = {auth.user_id}
    
          ''')
    # response.flash = f'{len(m)}'
    response.menu += [('Comissão Eleitoral', False, URL('comite', 'eleicoes'))]
    # if len(m) > 0:
    #     response.menu += [('Comissão Eleitoral', False, URL('comite', 'eleicoes'))]
elif auth.user_id:
    response.menu = [
        (T('Home'), False, URL('default', 'index'), []),
        (T('Recursos'), False, URL('default', 'recursos'), []),
        (T('Minhas Entidades'), False, URL('default', 'minhaentidade'), []),
    ]
    m = db.executesql(f'''select id from auth_membership
                        where user_id = {auth.user_id}
    
          ''')
    # response.flash = f'{len(m)}'
    # response.menu += [(f'Comissão Eleitoral{m}', False, URL('comite', 'eleicoes'))]
    if len(m) > 0:
        response.menu += [('Comissão Eleitoral', False, URL('comite', 'eleicoes'))]

else:
    response.menu = [
        (T('Home'), False, URL('default', 'index'), []),        
    ]
# ----------------------------------------------------------------------------------------------------------------------
# provide shortcuts for development. you can remove everything below in production
# ----------------------------------------------------------------------------------------------------------------------
