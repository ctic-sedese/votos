# -*- coding: utf-8 -*-
# -------------------------------------------------------------------------
# This is a sample controller
# this file is released under public domain and you can use without limitations
# -------------------------------------------------------------------------
from datetime import timedelta
from gluon.tools import callback
from gluon.globals import Request
from time import daylight, sleep

if 0 != 0:
    from gluon import *
    from gluon.tools import *
    from applications.voto.models import db
    from applications.voto.models.db import *
    from applications.voto.models import IS_IN_DB, IS_IN_SET, votou
    from gluon.packages import dal
    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T

# ---- example index page ----
def index():  # sourcery no-metrics
    # response.flash = T("Hello World")
    hj = request.now.today()
    # print(hj)
    response.alert = "Teste!"
    def check_periodo(eleicao):
        periodo = db((db.eleicao.id == eleicao)
            ).select(db.eleicao.inicio_alistamento
                , db.eleicao.fim_alistamento, db.eleicao.inicio_eleicao, db.eleicao.fim_eleicao).first()
        return periodo



    def check_apto_a_votar(eleicao, eleitor):
        apto_a_votar = db((db.eleitor.eleitor == eleitor)
                          & (db.eleitor.eleicao == eleicao) & (db.eleitor.status == 'Aprovado')).select().first()
        # apto_a_votar = db((db.habilitacao.eleitor==eleitor))
        # print(type(apto_a_votar))
        return apto_a_votar
    from datetime import   date
    eleicoes = db((db.eleicao.fim_eleicao >= date.today())
            & (db.eleicao.publicar == True)
            ).select(
        orderby=~ db.eleicao.inicio_eleicao)

    ids = set([e.entidade for e in eleicoes])
    db.entidade.representante_legal.requires=None
    # db.entidade.declaracao.requires=None
    entidades = db((db.entidade.id.belongs(ids)) & (db.entidade.municipio == db.municipio.id))\
        .select(db.entidade.id, db.entidade.nome, db.municipio.nome, db.entidade.segmento_atuacao)
    # form = SQLFORM.grid(db.eleicao,editable=False)

    encerradas = db((db.eleicao.fim_eleicao < date.today())
                & (db.eleicao.fim_eleicao > date.today() - timedelta(weeks=52))
                &(db.eleicao.publicar == True)
    ).select(
        orderby=~ db.eleicao.inicio_eleicao)

    ids = set([e.entidade for e in encerradas])

    entidades_encerradas = db((db.entidade.id.belongs(ids)) & (db.entidade.municipio == db.municipio.id))\
        .select(db.entidade.id, db.entidade.nome, db.municipio.nome, db.entidade.segmento_atuacao)
    # form = SQLFORM.grid(db.eleicao,editable=False)

    return locals()


@auth.requires_membership("admin")
def dados_entidade():

    if request.args(0) and int(request.args(0)) > 0:
        entidade_id = int(request.args(0))
        form = SQLFORM(db.entidade, entidade_id, submit_button="Prosseguir")
    elif request.args(0):
        session.flash = 'Entidade Inválida! Escolha uma da lista ou cadastre uma nova'
        redirect(URL(f='dados_entidade'))
    else:
        form = SQLFORM(db.entidade)
        entidade_id = 0
    entidades = db(db.entidade.id > 0).select(orderby=db.entidade.nome)
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(f='dados_eleicao', vars=dict(entidade=form.vars.id)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'

    return locals()


@auth.requires_membership("admin")
def dados_eleicao():
    if request.get_vars.entidade:
        entidade_id = int(request.get_vars.entidade)
        db.eleicao.entidade.default = entidade_id
        form = SQLFORM(db.eleicao, 
                        upload=URL('download'),
                        submit_button="Prosseguir",
                    )
    if form.process().accepted:
        response.flash = 'entidade selecionada'
        # session.entidade_id = form.vars.id
        redirect(URL(f='dados_eleicao', vars=dict(entidade=form.vars.id)))

    elif form.errors:
        response.flash = 'há erros no formulário'
    else:
        response.flash = 'Gentileza preencher o formulário'
    # response.view='generic.html'
    return locals()


@auth.requires_membership("admin")
def entidade():
    """ Formulário de cadastro de entidades
    """
    def declaracao():
        declaracao = f'Declaro para os devidos fins e sob as penas da Lei que as informações\
                    prestadas e documentos inseridos, neste sistema, para o Processo Eleitoral\
                    deste Conselho, são verídicos e autênticos(fiéis à verdade e\
                    condizentes com a realidade dos fatos à época).'
        return declaracao

    # db.entidade.concordancia.show_if = (
    #     db.entidade.representante_legal != '')
    db.entidade.orgao_colegiado.writable=True
    form_create = SQLFORM(db.entidade, submit_button='Salvar')
    fields = [db.entidade.id, db.entidade.nome, db.entidade.cnpj, db.entidade.data_criacao_entidade,
               db.entidade.segmento_atuacao, db.entidade.municipio, db.entidade.representante_legal]
    form = SQLFORM.grid(db.entidade, fields=fields, 
                        create=False, 
                        details=False, 
                        searchable=True,
                        csv=False, maxtextlength=200,
                        deletable=False)

    if form_create.accepts(request_vars=request.vars, formname='form_create'):
        response.flash = 'Entidade Cadastrada'
    elif form_create.errors:
        response.flash = f'Corrija os campos do formulário {form_create.errors}'
    else:
        pass

    return dict(form=form, form_create=form_create, declaracao=declaracao)


@auth.requires_membership("admin")
def eleicao():
    links_eleicao = [dict(header=['Administrar'], body=lambda row:
                          [DIV(A('Administrar', _href=URL(c='assistente_eleicao', f='dados_eleicao', args=row.id, vars=dict(eleicao=row.id))))])
                    ]
    q = db(db.eleicao.created_by == auth.user_id)
    form = SQLFORM.grid(q, 
                details=False, 
                deletable=False, 
                editable=False,
                csv=False,
                fields=[db.eleicao.id,
                        db.eleicao.entidade,
                        db.eleicao.inicio_alistamento,
                        db.eleicao.inicio_eleicao,
                        db.eleicao.edital,
                    ],
                        oncreate=cria_grupo, links=links_eleicao, maxtextlength=200, create=False)
    return dict(form=form)


@auth.requires_membership('eleicao_{}'.format(request.args(0)))
def admin_eleicao():
    eleicao_id = request.args(0)
    group_eleicao_id = db(db.auth_group.role == 'eleicao_{}'.format(
        request.args(0))).select('id').first()
    membros_eleicao = db(db.auth_membership.group_id == group_eleicao_id)
    form = SQLFORM(db.eleicao, eleicao_id)
    db.auth_membership.group_id.default = group_eleicao_id
    db.auth_membership.group_id.writable = False
    db.auth_membership.group_id.readable = False
    form2 = SQLFORM.grid(membros_eleicao, args=request.args[:1],
                         editable=False, details=False, searchable=False,
                         field_id=db.auth_membership.id, csv=False, maxtextlength=200)
    if form.process().accepted:
        response.flash = "Registro alterado"
    return dict(form=form, form2=form2)


@auth.requires_login()
def eleicoes():  # sourcery no-metrics
    hj = request.now.today()
    print(hj)

    def check_periodo(eleicao):
        periodo = db((db.eleicao.id == eleicao)).select(db.eleicao.inicio_alistamento,
                                                        db.eleicao.fim_alistamento, db.eleicao.inicio_eleicao, db.eleicao.fim_eleicao).first()
        return periodo



    def check_apto_a_votar(eleicao, eleitor):
        apto_a_votar = db((db.eleitor.eleitor == eleitor)
                          & (db.eleitor.eleicao == eleicao) & (db.eleitor.status == 'Aprovado')).select().first()
        return apto_a_votar



    if session.filter == 'on':
        filtro = SQLFORM.factory(
            Field('filter_entity', 'bool', default='off', widget=SQLFORM.widgets.boolean.widget,
                  label='Mostrar apenas a minha entidade'), submit_button='Limpar filtro',
            formstyle='bootstrap3_inline')
        response.flash = "Filtrando pela entidade do usuário"
        entidade = db.auth_user(db.auth_user.id == auth.user_id).entidade
        eleicoes = db(db.eleicao.entidade == entidade).select()
    else:
        filtro = SQLFORM.factory(
            Field('filter_entity', 'bool', default='on', widget=SQLFORM.widgets.boolean.widget,
                  label='Mostrar apenas a minha entidade'), submit_button='Filtrar',
            formstyle='bootstrap3_inline')
        response.flash = "Sem filtros"
        eleicoes = db(db.eleicao.id > 0).select()

    if filtro.process().accepted:
        session.filter = filtro.vars.filter_entity
        redirect(URL(f="eleicoes"), client_side=True)
    # form = SQLFORM.grid(db.eleicao,editable=False)


    def situacao_candidaturas(periodo, eleicao, candidatado):
        situacao_cand = None
        # Antes do período de Alistamento
        if hj < periodo.inicio_alistamento:
            situacao_cand = 'Aguardando período de alistamento'
        # após as eleições
        elif periodo.fim_eleicao < hj:
            situacao_cand = A('Eleição Encerrada, clique para ver Candidatos',
                              _href=URL(f='lista_de_candidatos_a_eleicao', args=[eleicao.id]))
        # periodo de alistamento
        elif periodo.inicio_alistamento <= hj and periodo.fim_alistamento > hj:
            if candidatado:
                if candidatado.status == 'aprovado':
                    situacao_cand = 'Candidatura Aprovado, Aguarde abertura da votação'
                elif candidatado.status == 'Inscrição Solicitada' or candidatado.status == None:
                    situacao_cand = A('Inscrito Aguardando Aprovação', _href=URL(f='candidatar',
                                                                                 args=[eleicao.id]))
                elif candidatado.status == 'rejeitado':
                    situacao_cand = A('Rejeitado: Revise aqui sua documentação',
                                      _href=URL(f='candidatar', args=[eleicao.id]))
                else:
                    situacao_cand = A(
                        'Candidatar-se', _href=URL(f='candidatar', args=[eleicao.id]))
            else:
                situacao_cand = A(
                    'Candidatar-se', _href=URL(f='candidatar', args=[eleicao.id]))
        # depois do fim do alistamento e antes do início da eleição
        elif periodo.fim_alistamento <= hj and periodo.inicio_eleicao > hj:
            if candidatado:
                if candidatado.status == 'aprovado':
                    situacao_cand = A('Ver Candidatos',
                                      _href=URL(f='lista_de_candidatos_a_eleicao',
                                                args=[eleicao.id]))
                    # situacao_cand= A('Candidatura Aprovada, Aguarde abertura da votação' ,_href=URL(f='candidatar',args=[eleicao.id]))
                elif candidatado.status == 'rejeitado':
                    situacao_cand = A('Rejeitado: Revise aqui sua documentação',
                                      _href=URL(f='candidatar',
                                                args=[eleicao.id]))
                else:
                    situacao_cand = 'Alistamento encerrado'
            elif not candidatado:
                situacao_cand = A('Ver Candidatos', _href=URL(f='lista_de_candidatos_a_eleicao',
                                                              args=[eleicao.id]))

        # período de eleição
        elif periodo.inicio_eleicao <= hj and periodo.fim_eleicao >= hj:
            situacao_cand = A('Ver Candidatos', _href=URL(f='lista_de_candidatos_a_eleicao',
                                                          args=[eleicao.id]))
        return situacao_cand

    return locals()


def inscreve_eleitor():
    esta_eleicao = request.vars['esta_eleicao'] or None
    nome_eleicao = request.vars['nome_eleicao']
    nome_eleitor = request.vars['nome_eleitor']
    docs_obrigatorios = request.vars['docs_obrigatorios']
    docs_registrados = request.vars['docs_registrados']
    inscrito = db((db.eleitor.eleitor == auth.user_id)
                  & (db.eleitor.eleicao == esta_eleicao)).select().first()

    def check_docs(docs_registrados, docs_obrigatorios):
        obrigatorios = set(docs_obrigatorios)
        registrados = set(docs_registrados or [])

        if obrigatorios == registrados:
            email_habilitacao(esta_eleicao, nome_eleicao, nome_eleitor)
            response.flash = "Inscrição Solicitada"
            register_form.record.update_record(status='Inscrição Solicitada')
            db.commit()
            redirect(request.env.http_web2py_component_location,
                     client_side=True)
        else:
            db.rollback()
            register_form.errors.eleitor = 'Insira os documentos'
            response.flash = "TODOS OS DOCUMENTOS NECESSÁRIOS DEVEM SER INSERIDOS"

    if inscrito is None:
        register_form = FORM.confirm('Desejo me habilitar para esta eleição')
        if register_form.accepts:
            db.eleitor.insert(eleitor=auth.user_id,
                              eleicao=esta_eleicao, status='Não Inscrito')
            session.flash = "Registrado"
            redirect(request.env.http_web2py_component_location,
                     client_side=True)
        else:
            pass

    elif inscrito.status == 'Não Inscrito' or inscrito.status == 'rejeitado':
        db.eleitor.status.writable = False
        register_form = SQLFORM(db.eleitor, inscrito,
                                submit_button='Solicitar Inscrição', showid=False)
        register_form.status = 'Inscrição Solicitada'
        if register_form.accepts(request.vars, formname='solicita'):
            session.flash = f"Solicitando Inscrição"

            check_docs(docs_registrados, docs_obrigatorios)

        elif register_form.errors:
            session.flash = f"{request.vars} "
        else:
            pass
    elif inscrito.status == 'Inscrição Solicitada' or inscrito.status == 'aprovado':
        register_form = SQLFORM(db.eleitor, inscrito,
                                readonly=True, showid=False)
    else:
        register_form = ''

    return locals()



def alistar():  # sourcery no-metrics
    if request.args(0):
        esta_eleicao = request.args(0)
        nome_eleicao = db.eleicao(db.eleicao.id == esta_eleicao).nome
        inscrito = db((db.eleitor.eleitor == auth.user_id)
                      & (db.eleitor.eleicao == esta_eleicao)).select().first()
        docs_obrigatorios = db(db.eleicao.id == esta_eleicao).select(
            db.eleicao.obrigatorio)
        if inscrito:
            db.auth_user.username.writable = False
            db.auth_user.first_name.writable = False
            db.auth_user.data_nascimento.writable = False
            db.auth_user.identidade.writable = False
            nome_eleitor = db.auth_user(db.auth_user.id == auth.user_id).first_name
            eleitor_insc = inscrito.id
            docs_registrados = db(db.documentos_eleitor.eleitor == eleitor_insc).select(
                'tipo_arquivo').column()
            db.documentos_eleitor.eleitor.default = eleitor_insc
            form_usuario = SQLFORM(db.auth_user,
                                   auth.user_id,
                                   showid=False, submit_button='Atualizar',
                                   formstyle='bootstrap3_stacked',
                                   separator=':  ')

        else:
            auth.messages.verify_email = '''
            <html> <html class="no-js" lang="{{='pt-br'}}"><head>
            <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge{{=not request.is_local and ',chrome=1' or ''}}"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>{{=response.title or request.application}}</title>
            <meta name="application-name" content="{{=request.application}}"> <meta name="google-site-verification" content="">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            <link rel="stylesheet" href="{{=URL('static','css/bootstrap.min.css')}}" />
            <link rel="stylesheet" href="{{=URL('static','css/web2py-bootstrap4.css')}}" />
            <script src="{{=URL('static','js/modernizr-2.8.3.min.js')}}"></script><body>
            <div class="card" style="width: 50rem background-color: rgb(10, 70, 250); color:white; padding:20px;word-wrap:break-word;">
            <div class="card-body" align="center">
            <h1 class="card-title" > Eleições Virtuais da SEDESE</h1> </head> <br><br>
            <h3 class="card-text"> Bem vindo %(first_name)s, Click no link abaixo:<br><br>
            <a href=%(link)s class="btn btn-primary"> Verificar e-mail </a> <br><br> para verificar seu email </h3><br><br></div>    </div> </body>
            <div class="card-footer"> <div class="container center" align="center" style="background-color: rgb(32, 32, 32); color:white; padding:20px;word-wrap:break-word;"> <br><br>
            <a class="display-3" href="{{=URL(c='default',f='index')}}">Clique para ir até Eleições Virtuais</a>
            <h3 class="display-3" > Contato: sistemavoto@social.mg.gov.br </h3> </div> </div> </html> '''
            auth.settings.registration_requires_verification = False
            # auth.settings.registration_requires_approval = False 
            auth.settings.register_next=URL('default', 'eleitor_docs', args = esta_eleicao)
            if 'auth' in session:
                form_usuario = SQLFORM(db.auth_user,
                                       auth.user_id,
                                       showid=False, submit_button='Atualizar',
                                       formstyle='bootstrap3_stacked',
                                       separator=':  ')
            else:
                form_usuario = auth.register()

        if form_usuario.accepted:
            id = db.auth_user.insert(
                **db.auth_user._filter_fields(form_usuario.vars))
            db.eleitor.insert(eleicao=esta_eleicao, eleitor=int(id), status="Inscrição Solicitada")
            session.flash = 'Registrado'
            session.user_vars = form_usuario.vars
            
        elif form_usuario.errors:
            response.flash = 'Verifique o formulário de dados do Eleitor.'
        else:
            pass

    else:
        redirect(URL(f="eleicoes"))

    return dict(form_usuario=form_usuario, esta_eleicao=esta_eleicao, nome_eleicao=nome_eleicao)



def cadastra_entidade():

    esta_eleicao = request.vars['esta_eleicao']
    def declaracao():
        declaracao = f'Declaro para os devidos fins e sob as penas da Lei que as informações\
                    prestadas e documentos inseridos, neste sistema, para o Processo Eleitoral\
                    deste Conselho, são verídicos e autênticos(fiéis à verdade e\
                    condizentes com a realidade dos fatos à época).'
        return declaracao

    form_create = SQLFORM(db.entidade, submit_button='Salvar')

    if form_create.accepts(request_vars=request.vars, formname='form_create', keepvalues=True):
        response.flash = 'Entidade Cadastrada'
    elif form_create.errors:
        response.flash = f'Corrija os campos do formulário {form_create.errors}'
    else:
        pass

    return locals()


def eleitor_docs():
    if request.args(0):
        esta_eleicao = request.args(0)
        nome_eleicao = db.eleicao(db.eleicao.id == esta_eleicao).nome or None
        if auth in session:
            nome_eleitor = db.auth_user(db.auth_user.id == auth.user_id).first_name
        
        inscrito = db((db.eleitor.eleitor == auth.user_id) & (db.eleitor.eleicao == esta_eleicao)).select().first()

        if inscrito:
            docs_obrigatorios = db(db.eleicao.id == esta_eleicao).select(
                db.eleicao.obrigatorio)
            eleitor_insc = inscrito.id
            db.documentos_eleitor.eleitor.writable = False
            db.documentos_eleitor.eleitor.readable = False
            db.documentos_eleitor.status.writable = False
            db.documentos_eleitor.status.default = "Aguardando Aprovação"
            docs = db(db.documentos_eleitor.eleitor == eleitor_insc)
            docs_registrados = db(db.documentos_eleitor.eleitor == eleitor_insc).select(
                'tipo_arquivo').column()
            docs_status = db(db.documentos_eleitor.eleitor == eleitor_insc).select('status')  
            db.documentos_eleitor.eleitor.default = eleitor_insc
            este_doc = request.vars['este_doc']
            form_documentos = SQLFORM(
                db.documentos_eleitor, este_doc or None, formstyle='bootstrap3_stacked',
                separator=':  ')
            form_docs_reg = SQLFORM.grid(docs, create=False,
                                            user_signature=None, args=request.args[:1],
                                            maxtextlength=200, details=False, links=[dict(header='Editar', body=lambda row:
                                                                                        A('Editar', _href=URL(f='eleitor_docs', args=esta_eleicao, vars={
                                                                                            'este_doc': row.id if row.id else None}), _class='button btn btn-default'))],
                                            csv=False, editable=False, searchable=False, field_id=db.documentos_eleitor.id,
                                            formstyle='bootstrap3_stacked', formname='form_docs_reg')
            
            if form_documentos.process(formname='form_documentos').accepted:
                session.flash = 'Documento registrado'
                redirect(URL(f="alistar", args=(esta_eleicao)))
            elif form_documentos.errors:
                response.flash = 'Verifique o formulário de documentos do Eleitor.'
            else:
                pass
        else:
            redirect(URL(f='alistar', args=(esta_eleicao)))
    else:
        redirect(URL(f='index'))

    return locals()    


@auth.requires_login()
def candidatar():
    esta_eleicao = request.args(0)
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    este_eleitor = db((db.eleitor.eleitor == auth.user_id)
                      & (db.eleitor.eleicao == esta_eleicao)).select().first()
    docs_req = db((db.cargo.eleicao == esta_eleicao) & (
        db.candidato.cargo == db.cargo.id)).select('documentos_requeridos').first()

    def form2_onvalidate(form2):
        session.flash = 'Documento registrado'

    if not este_eleitor:
        response.aviso = "Você deve, primeiramente, realizar sua Inscrição como eleitor {}\
             caso queira candidatar-se nesta eleição".format(A("aqui", _href=URL(f="alistar", args=[request.args(0)])))
        # redirect(URL(f="alistar", args=[request.args(0)]))

    elif not db.auth_user.entidade:
        session.aviso = "Você não tem uma entidade cadastrada. Escolha uma da lista ou crie uma {}"\
            .format(A("aqui", _href=URL(f="minhaentidade")))
        redirect(URL(f="user/profile"))

    elif request.args(0):
        inscrito = db((db.candidato.candidato == auth.user_id)
                      & (db.candidato.eleicao == esta_eleicao)).select().first()
        db.candidato.candidato.default = auth.user_id
        cargos = db(db.cargo.eleicao == esta_eleicao)
        # print(cargos)
        db.candidato.cargo.requires = IS_IN_DB(cargos, 'cargo.id', '%(nome)s')
        db.candidato.eleicao.default = esta_eleicao
        entidade = db(db.auth_user.id == auth.user_id).select(
            db.auth_user.entidade).first()
        db.candidato.entidade.default = entidade.entidade
        db.candidato.eleicao.writable = False
        db.candidato.entidade.writable = False
        db.candidato.status.writable = False
        if inscrito:
            docs_registrados = db(db.documentos.candidato == inscrito).select(
                'tipo_arquivo').column()
            db.eleitor.status.writable = False
            form = SQLFORM(db.candidato, inscrito.id, formname='candidato',
                           fields=['candidato', 'foto', 'entidade', 'eleicao', 'cargo',
                                   'pdf', 'perfil', 'propostas', 'status'], showid=False,
                           formstyle='bootstrap3_stacked', separator=':  ', submit_button='Solicitar Inscrição'
                           )
            docs = db((db.documentos.candidato == inscrito.id))
            db.documentos.candidato.default = inscrito.id
            db.documentos.candidato.writable = False
            db.documentos.candidato.readable = False
            db.documentos.status.default = 'Aguardando Aprovação'
            db.documentos.status.writable = False
            # db.documentos.status.readable = False
            db.documentos.id.readable = False
            form2 = SQLFORM.grid(docs, user_signature=None,  formname='documentos',
                                 paginate=10, args=request.args[:1],
                                 exportclasses=dict(
                                     csv_with_hidden_cols=False, json=False, tsv_with_hidden_cols=False, tsv=False, html=False),
                                 onvalidation=form2_onvalidate, searchable=False, details=False, maxtextlength=200)
            # outros_docs = db(db.candidato.id == inscrito.id).select(db.candidato.foto, db.candidato.pdf).first()
            foto = inscrito.foto
            if form.process().accepted:
                docs_requeridos = set(
                    docs_req.documentos_requeridos.split('|'))
                docs_requeridos.discard('')
                session.flash = f"Solicitando Candidatura"
                check_docs(form, docs_registrados,
                           docs_requeridos, esta_eleicao, nome_eleicao)
        else:
            foto = None
            # db.eleitor.status.requires=IS_IN_SET(["Não inscrito","Inscrição Solicitada"])
            form = SQLFORM(db.candidato, fields=['candidato', 'entidade',
                                                 'eleicao', 'cargo', 'pdf', 'perfil', 'propostas', 'foto'],
                           formstyle='bootstrap3_stacked',  separator=':  ', submit_button='Registrar'
                           )
            form.vars.status = 'Não inscrito'
            form2 = None

            if form.process(formname='candidato').accepted:
                session.flash = 'Registro de Candidatura Recebido'
                redirect(URL(f="candidatar", args=[esta_eleicao]))
            elif form.errors:
                response.flash = 'Corrija os Erros no Formulario'
            else:
                pass

    else:
        redirect(URL(f="candidatar", args=[esta_eleicao]))

    return locals()


@auth.requires_login()
def minhaentidade():
    db.entidade.representante_legal.default = auth.user_id
    db.entidade.cnpj.requires=[IS_NOT_IN_DB(db, 'entidade.cnpj', error_message='O CNPJ não pode ser duplicado ou 0000000000'), IS_CNPJ()]
    db.entidade.cnpj.label=ob("CNPJ")
    # db.entidade.texto_concordancia.default=True
    # db.entidade.representante_legal.writable=False
    q = db(db.entidade.representante_legal == auth.user_id)

    form = SQLFORM.grid(q,
        csv=False,
        maxtextlength=500,
        details=False,
        create=False,
        deletable=False,
        fields=[db.entidade.id,db.entidade.nome,db.entidade.cnpj,db.entidade.data_criacao_entidade]
    )

    # e = db(db.entidade.id == auth.user.entidade).select().first()
    # if not e.representante_legal or e.representante_legal == auth.user.id:
    #     form = SQLFORM(db.entidade, auth.user.entidade,
    #                     deletable=False,
    #                     )
    # else:
    entidades = db(db.entidade.representante_legal==auth.user_id).select()

    return locals()


@ auth.requires_membership("admin")
def cargo():
    form = SQLFORM.grid(db.cargo, maxtextlength=200)

    return dict(form=form)


@ auth.requires_membership("admin")
def candidato():
    form = SQLFORM.grid(db.candidato, maxtextlength=200)

    return dict(form=form)


@ auth.requires_login()
def voto():
    cargo_id = request.vars['cargo_id']
    candidato_id = request.vars['candidato_id']
    eleicao_id = request.vars['esta_eleicao']
    candidato_eleicao = db((db.candidato.id == candidato_id) & (db.candidato.eleicao == eleicao_id)).select(
        db.candidato.id, db.candidato.cargo, db.candidato.entidade, db.candidato.candidato).first()
    eleitor = db((db.eleitor.eleitor==auth.user.id)
        &(db.eleitor.eleicao==eleicao_id)
    ).select().first()
    habilitacao = db((db.habilitacao.eleitor==auth.user_id)
                    & (db.habilitacao.eleicao==eleitor.eleicao)
                    & (db.habilitacao.status=='aprovado')
                    & (db.habilitacao.cargo==cargo_id)).select().first()
    db.voto.insert(entidade=eleitor.entidade_representada if eleitor.entidade_representada else eleitor.eleitor,
        eleicao=eleitor.eleicao,
        cargo=habilitacao.cargo,
        candidato=candidato_id, 
    )
    db.commit()
    session.flash="Voto computado"
    redirect(URL(c="default",f="votar",vars=dict(esta_eleicao=eleicao_id)))
    # db.voto.eleicao.default = eleicao_id
    # db.voto.eleicao.writable = False
    # db.voto.entidade.default = candidato_eleicao.entidade
    # db.voto.entidade.writable = False
    # db.voto.cargo.default = candidato_eleicao.cargo
    # db.voto.cargo.writable = False
    # db.voto.candidato.default = candidato_eleicao.id
    # db.voto.candidato.writable = False
    # if candidato_eleicao.entidade:
    #     db.voto.candidato.label="Representante Legal"
    # foto = candidato_eleicao.candidato.foto

    # form = SQLFORM(db.voto, submit_button='Enviar Voto')  # candidato.id)
    # if form.process().accepted:
    #     response.flash = 'Voto Aceito'
    #     redirect(URL(f="index"))
    # elif form.errors:
    #     response.flash = 'Ocorreu um erro no envio.'
    # else:
    #     # response.flash = 'Verifique as informações'
    #     pass
    return locals()

@ auth.requires_login()
def votar():
    esta_eleicao = int(request.vars['esta_eleicao'])
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    habilitacoes = db((db.habilitacao.eleitor==auth.user.id)
                    &(db.habilitacao.eleicao==esta_eleicao)
                    &(db.habilitacao.status=='aprovado')
                    ).select()
    ids = [h.cargo for h in habilitacoes]
    cargos = db((db.cargo.eleicao == esta_eleicao)
                &(db.cargo.id.belongs(ids))
            ).select(db.cargo.id, db.cargo.nome)
    cargos_p_votar = []
    cargos_votados = []
    for cargo in cargos:
        votou_no_cargo = votou(esta_eleicao, auth.user_id, cargo.id)
        if not votou_no_cargo:
            cargos_p_votar.append(TD(A(cargo.nome.upper(), _href=URL('default', 'votar_cargo', vars={
                                  'cargo_id': cargo.id, 'esta_eleicao': esta_eleicao}),_style='color:black;letter-spacing: 2px;font-size:1.3em;' ), _class=' btn-warning',_style='border-color:black;border-style: solid;border-width:1px;'))
        else:
            quando = votou_no_cargo.created_on.strftime("%d/%m/%Y %H:%M:%S")
            cargos_votados.append(
                TD(XML(f'<span style="font-size:1.7em" > Você já votou para  {cargo.nome} em {quando} </span>'),_class='btn-success'))

    return dict(cargos_p_votar=cargos_p_votar, cargos_votados=cargos_votados, esta_eleicao=esta_eleicao, nome_eleicao=nome_eleicao)





# ---- API (example) -----
@ auth.requires_login()
def api_get_user_email():
    if not request.env.request_method == 'GET':
        raise HTTP(403)
    return response.json({'status': 'success', 'email': auth.user.email})

# ---- Smart Grid (example) -----


# can only be accessed by members of admin groupd
@ auth.requires_membership('admin')
def grid():
    response.view = 'generic.html'  # use a generic view
    tablename = request.args(0)
    if not tablename in db.tables:
        raise HTTP(403)
    grid = SQLFORM.smartgrid(db[tablename], args=[
        tablename], deletable=False, editable=False, maxtextlength=200)
    return dict(grid=grid)

# ---- Embedded wiki (example) ----


def wiki():
    auth.wikimenu()  # add the wiki to the menu
    return auth.wiki()

# ---- Action for login/register/etc (required for auth) -----


def user():

    if request.args(0) == "profile":
        db.auth_user.username.writable = False
    if request.args(0) == "login":
        db.auth_user.username.comment="Digite apenas os números do seu CPF"
    if request.args(0) == "impersonate":
        request.post_vars.auth_user = int(request.args(1))
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    also notice there is http://..../[app]/appadmin/manage/auth to allow administrator to manage users
    """
    # mensagem= dict(msg='Bem vindo %(first_name)s! Clique em %(link)s para verificar seu email')
    # auth.messages.verify_email = '{}'.format(response.render('generic_email.html', mensagem))
    auth.messages.verify_email = '''
    <html> <html class="no-js" lang="{{='pt-br'}}"><head>
    <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge{{=not request.is_local and ',chrome=1' or ''}}"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{=response.title or request.application}}</title>
    <meta name="application-name" content="{{=request.application}}"> <meta name="google-site-verification" content="">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{=URL('static','css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{=URL('static','css/web2py-bootstrap4.css')}}" />
    <script src="{{=URL('static','js/modernizr-2.8.3.min.js')}}"></script><body>
    <div class="card" style="width: 50rem background-color: rgb(10, 70, 250); color:white; padding:20px;word-wrap:break-word;">
    <div class="card-body" align="center">
        <h1 class="card-title" > Eleições Virtuais da SEDESE</h1> </head> <br><br>
    <h3 class="card-text"> Bem vindo %(first_name)s, Click no link abaixo:<br><br>
    <a href=%(link)s class="btn btn-primary"> Verificar e-mail </a> <br><br> para verificar seu email </h3><br><br></div>    </div> </body>
    <div class="card-footer"> <div class="container center" align="center" style="background-color: rgb(32, 32, 32); color:white; padding:20px;word-wrap:break-word;"> <br><br>
    <a class="display-3" href="{{=URL(c='default',f='index')}}">Clique para ir até Eleições Virtuais</a>
    <h3 class="display-3" > Contato: sistemavoto@social.mg.gov.br </h3> </div> </div> </html> '''

    auth.messages.reset_password = '''
    <html> <html class="no-js" lang="{{='pt-br'}}"><head>
    <meta charset="utf-8"> <meta http-equiv="X-UA-Compatible" content="IE=edge{{=not request.is_local and ',chrome=1' or ''}}"> <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{=response.title or request.application}}</title>
    <meta name="application-name" content="{{=request.application}}"> <meta name="google-site-verification" content="">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="{{=URL('static','css/bootstrap.min.css')}}" />
    <link rel="stylesheet" href="{{=URL('static','css/web2py-bootstrap4.css')}}" />
    <script src="{{=URL('static','js/modernizr-2.8.3.min.js')}}"></script><body>
    <div class="card" style="width: 50rem background-color: rgb(10, 70, 250); color:white; padding:20px;word-wrap:break-word;">
    <div class="card-body" align="center">
        <h1 class="card-title" > Eleições Virtuais da SEDESE</h1> </head> <br><br>
    <h3 class="card-text"> A redefinição de senha foi solicitada para %(first_name)s. Click no link abaixo:<br><br>
    <a href=%(link)s class="btn btn-primary"> Redefinir senha </a> <br><br> para redefinir sua senha </h3><br><br></div>    </div> </body>
    <div class="card-footer"> <div class="container center" align="center" style="background-color: rgb(32, 32, 32); color:white; padding:20px;word-wrap:break-word;"> <br><br>
    <a class="display-3" href="{{=URL('default','index')}}">Clique para ir até Eleições Virtuais</a>
    <h3 class="display-3" > Contato: sistemavoto@social.mg.gov.br </h3> </div> </div> </html> '''
    form = auth()
    if form:
        form[0].insert(0, aviso_obrigatorio)
    if request.args(0) == 'register':


        if form.errors.first_name:
            form[0][1][1][0]['_style']='border:1px solid red'
        if form.errors.email:
            form[0][3][1][0]['_style']='border:1px solid red'
        if form.errors.username:
            form[0][4][1][0]['_style']='border:1px solid red'
        if form.errors.municipio:
            form[0][5][1][0]['_style']='border:1px solid red'         
        if form.errors.identidade:
            form[0][6][1][0]['_style']='border:1px solid red'      
        if form.errors.password:
            form[0][7][1][0]['_style']='border:1px solid red'  
        
    if request.args(0) == 'login':
        
        
        if form.errors.username:
            form[0][1][1][0]['_style']='border:1px solid red'
        if form.errors.password:
            form[0][2][1][0]['_style']='border:1px solid red'  
        db.auth_user.password.comment = XML(f"""<button type="button" onclick="if (auth_user_password.type == 'text') auth_user_password.type = 'password';
  else auth_user_password.type = 'text';"" style="width:50px;height=50px;" id="eye">
          <img src="https://img.icons8.com/material-sharp/24/000000/visible.png"/>
         </button> A senha deve ter entre 6 e 32 caracteres """)
        
    return dict(form=form)


# ---- action to server uploaded static content (required) ---
@ cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


@ auth.requires_membership('admin')
def usuarios():

    rows = db(db.auth_user.registration_key == None).select()
    if rows:
        for row in rows:
            row.update_record(registration_key='')
            db.commit()
    db.auth_user.registration_key.writable = True
    db.auth_user.registration_key.readable = True
    db.auth_user.registration_key.label = "Revisão Pendente"
    db.auth_user.registration_key.comment = "Apague o valor para aprovar/liberar o acesso"
    form = SQLFORM.grid(
        db.auth_user,
        orderby=~db.auth_user.registration_key,
        links=[dict(header='Grupos', body=lambda row: A(
            'Grupos', _href=URL(f='grupos', args=[row.id], user_signature=True)))],
        maxtextlength=200,
        paginate=100,
        fields=[
            db.auth_user.id,
            db.auth_user.first_name,
            db.auth_user.email,
            db.auth_user.registration_key,
            db.auth_membership.group_id,
            db.auth_membership.user_id,
        ]
    )

    return locals()


@ auth.requires_membership('admin')
def grupos():
    if request.args(0) == 'new':
        db.auth_membership.user_id.default = session.grupos_user_id
        q = db(db.auth_membership.user_id == session.grupos_user_id)
        form = SQLFORM.grid(db.auth_membership)
    else:
        session.grupos_user_id = int(request.args(0))
        q = db(db.auth_membership.user_id == int(request.args(0)))
        form = SQLFORM.grid(q,
                            csv=False,
                            )

    del q
    response.view = 'generic.html'
    return locals()


def bt_aprovacao():
    eleitor_id = request.get_vars.eleitor
    candidato_id = request.get_vars.candidato_id

    def atualiza():
        if tipo == 'eleitor':
            redirect(URL(f='avaliar', args=esta_eleicao), client_side=True)
        elif tipo == 'candidato':
            redirect(URL(f='avaliar_candidatos',
                     args=esta_eleicao), client_side=True)
        elif tipo == 'doc_eleitor':
            redirect(URL(f='avaliar_docs_eleitor',
                     args=esta_eleicao, vars={'eleitor_id': eleitor_id}), client_side=True)
        elif tipo == 'doc_candidato':
            redirect(URL(f='avaliar_docs_candidato',
                     args=esta_eleicao, vars={'candidato_id': candidato_id}), client_side=True)
        else:
            pass

    registro = request.vars.id
    tipo = request.vars.tipo
    esta_eleicao = request.vars.esta_eleicao
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    assunto = 'aprovação em eleição'
    if tipo == 'eleitor':
        reg = db(db.eleitor.id == registro).select().first()
        nome_eleitor = db.auth_user(db.auth_user.id == reg.eleitor).first_name
        mensagem = dict(
            msg1=f'O eleitor {nome_eleitor} foi aprovado para a eleição: {nome_eleicao}')
        email_eleitor = db.auth_user(db.auth_user.id == reg.eleitor).email
        # session.email = email_eleitor
    elif tipo == 'candidato':
        reg = db(db.candidato.id == registro).select().first()
        nome_candidato = db.auth_user(
            db.auth_user.id == reg.candidato).first_name
        mensagem = f'O candidato {nome_candidato} foi aprovado para a eleição: {nome_eleicao}'
        email_candidato = db.auth_user(db.auth_user.id == reg.candidato).email
    elif tipo == 'doc_eleitor':
        reg = db(db.documentos_eleitor.id == registro).select().first()
    elif tipo == 'doc_candidato':
        reg = db(db.documentos.id == registro).select().first()

    else:
        pass

    if tipo == 'eleitor':
        if check_documentacao_eleitor(reg) == True:
            reg.update_record(status='Aprovado')
            envia_emails(email_eleitor, assunto, mensagem)
            session.flash = 'O {} foi {}'.format(tipo, reg.status)
        else:
            session.flash = 'Os documentos de {} não estão aprovados'.format(
                nome_eleitor)
    elif tipo == 'candidato':
        if check_documentacao_candidato(reg) == True:
            reg.update_record(status='Aprovado')
            envia_emails(email_candidato, assunto, mensagem)
            session.flash = 'O {} foi {}'.format(tipo, reg.status)
        else:
            session.flash = 'Os documentos de {} não estão aprovados'.format(
                nome_candidato)

    elif tipo == 'doc_eleitor' or 'doc_candidato':
        reg.update_record(status='Aprovado')
        session.flash = 'O documento foi aprovado'
    else:
        pass

    atualiza()
    return locals()


def bt_rejeitar():
    eleitor_id = request.get_vars.eleitor
    candidato_id = request.get_vars.candidato_id
    motivo = request.vars.motivo

    def atualiza():
        if tipo == 'eleitor':
            redirect(URL(f='avaliar', args=esta_eleicao), client_side=True)
        elif tipo == 'candidato':
            redirect(URL(f='avaliar_candidatos',
                     args=esta_eleicao), client_side=True)
        elif tipo == 'doc_eleitor':
            redirect(URL(f='avaliar_docs_eleitor',
                     args=esta_eleicao, vars={'eleitor_id': eleitor_id}), client_side=True)
        elif tipo == 'doc_candidato':
            redirect(URL(f='avaliar_docs_candidato', args=esta_eleicao,
                         vars={'candidato_id': candidato_id}), client_side=True)
        else:
            pass

    registro = request.vars.id
    tipo = request.vars.tipo
    esta_eleicao = request.vars.esta_eleicao
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    assunto = 'aprovação em eleição'
    if tipo == 'eleitor':
        reg = db(db.eleitor.id == registro).select().first()
        nome_eleitor = db.auth_user(db.auth_user.id == reg.eleitor).first_name
        rejeitados = db((db.documentos_eleitor.eleitor == registro) & (
            db.documentos_eleitor.status == 'rejeitado')).select('tipo_arquivo').column()
        mensagem = f'O usuário {nome_eleitor} foi reprovado para a eleição: {nome_eleicao} \n \
                    Documentos rejeitados: \n \
                    {rejeitados} \n \
                    Motivo: {motivo}'
        email_eleitor = db.auth_user(db.auth_user.id == reg.eleitor).email
    elif tipo == 'candidato':
        reg = db(db.candidato.id == registro).select().first()
        nome_candidato = db.auth_user(
            db.auth_user.id == reg.candidato).first_name
        rejeitados = db((db.documentos.candidato == registro) & (
            db.documentos.status == 'rejeitado')).select('tipo_arquivo').column()
        mensagem = f'O usuário {nome_candidato} foi reprovado para a eleição: {nome_eleicao} \n \
                    Documentos rejeitados: \n \
                    {rejeitados} \n \
                    Motivo: {motivo}'
        email_eleitor = db.auth_user(db.auth_user.id == reg.candidato).email
    elif tipo == 'doc_eleitor':
        reg = db(db.documentos_eleitor.id == registro).select().first()
    elif tipo == 'doc_candidato':
        reg = db(db.documentos.id == registro).select().first()

    else:
        pass
    if tipo == 'eleitor' or tipo == 'candidato':
        if reg.status == "Inscrição Solicitada":

            reg.update_record(status='Rejeitado')
            envia_emails(email_eleitor, assunto, mensagem)
            session.flash = 'O {} foi {}'.format(tipo, reg.status)

    elif tipo == 'doc_eleitor' or 'doc_candidato':
        reg.update_record(status='Rejeitado')
        session.flash = 'O documento foi Rejeitado'
    else:
        pass

    atualiza()
    return locals()


def registra_motivo():
    row = request.vars.id
    esta_eleicao = request.vars.esta_eleicao
    form_motivos = SQLFORM.factory(
        Field('motivo', 'text', requires=IS_NOT_EMPTY()))
    if form_motivos.process().accepted:
        redirect(URL(f='bt_rejeitar', vars={
            'id': row, 'tipo': 'eleitor', 'esta_eleicao': esta_eleicao, 'motivo': form_motivos.vars.motivo}))
    return locals()


@ auth.requires_membership('eleicao_{}'.format(request.args(0)))
def avaliar():
    atrib = {'class': "btn-group btn-group-toggle"}
    esta_eleicao = request.args(0)
    nome_eleicao = db(db.eleicao.id == esta_eleicao).select(
        'nome').first().nome
    eleitores_p_avaliar = db((db.eleitor.eleicao == esta_eleicao) & (
        db.auth_user.id == db.eleitor.eleitor))
    cpf = db.auth_user.username

    eleitores_filds = [db.auth_user.first_name, db.entidade.nome, db.municipio.nome, db.auth_user.email,
                       db.auth_user.username, db.auth_user.gênero, db.auth_user.cor, db.auth_user.mothers_name,
                       db.auth_user.data_nascimento,  db.auth_user.escolaridade, db.auth_user.identidade, db.eleitor.status]
    links_eleitor = [dict(header=['Avaliar'], body=lambda row:
                          [DIV(
                              A('Aprovar', callback=URL(f='bt_aprovacao',
                                                        vars={'id': row.eleitor.id, 'tipo': 'eleitor', 'esta_eleicao': esta_eleicao}),
                                type="radio", name="options", id="option1", autocomplete="off",
                                _class='btn btn-primary active',
                                ),
                              A('Rejeitar', callback=URL(f='registra_motivo', vars={'id': row.eleitor.id, 'tipo': 'eleitor', 'esta_eleicao': esta_eleicao}), type="button", name="options", id="option2", autocomplete="off",
                                _class='btn btn-danger'), options_dict=atrib)]),

                     dict(header=['Documentos'], body=lambda row:
                          [DIV(
                              A('Habilitações', _href=URL(f='habilitar_eleitor', args=esta_eleicao, vars={
                                  'eleitor_id': row.eleitor.id}), _class='button btn btn-default'),

                              A('Documentos', _href=URL(f='avaliar_docs_eleitor', args=esta_eleicao, vars={
                                  'eleitor_id': row.eleitor.id}), _class='button btn btn-default'),

                              P(documentacao_eleitor(row.eleitor.id))
                          )])]

    grid_eleitor = SQLFORM.smartgrid(db.eleitor, linked_tables=['auth_user'], constraints={
        'eleitor': ((db.eleitor.eleicao == esta_eleicao) & (db.auth_user.id == db.eleitor.eleitor) &
                    (db.auth_user.municipio == db.municipio.id) & (db.auth_user.entidade == db.entidade.id))},
        user_signature=None, formname='p_avaliar', fields=eleitores_filds,  args=request.args[:1],
        create=False, editable=False, deletable=False, details=False, searchable=False, links=links_eleitor,
        field_id=db.eleitor.id, csv=False, maxtextlength=200)

    return locals()


@ auth.requires_membership('eleicao_{}'.format(request.args(0)))
def habilitar_eleitor():
    este_eleitor = request.vars.eleitor
    eleitor_nome = db((db.eleitor.id == este_eleitor) & (
        db.eleitor.eleitor == db.auth_user.id)).select().first()['auth_user'].first_name
    esta_eleicao = request.args(0)
    cargos_eleicao = db(db.cargo.eleicao == esta_eleicao)

    # linhas = []
    # for cargo in cargos_eleicao.select():
    #     linha = DIV(LABEL(cargo.nome),
    #                   INPUT(_name='cargos', value='1', _type='checkbox'))
    #     linhas.append(linha)
    # form = FORM(
    #     LABEL('Eleitor'),BR(),LABEL(eleitor_nome),BR(),LABEL('Cargos'),BR(),
    #     # linhas,
    #     INPUT(_name="submit", value="Enviar", type="submit")
    #     )
    # return form
    # return cargos_eleicao.select()
    # form = SQLFORM.factory(
    #     Field('eleitor' ,default=eleitor_nome, writable=False),
    #     # Field('cargos', 'list:integer', requires=IS_IN_DB(db, 'cargo.id','%(nome)s', multiple=True, zero="Todos"),
    #     # widget = SQLFORM.widgets.checkboxes.widget
    #     # )
    #     )
    # print(form)
    mensagem = []

    def iter_cagos(eleitor, cargos):
        cargo_habilitado = db((db.habilitacao.eleitor == eleitor) & (
            db.cargo.id == db.habilitacao.cargo) & (db.cargo.eleicao == esta_eleicao))
        for cargo in cargos:
            cargo_reg = db((db.habilitacao.eleitor == eleitor) & (
                db.habilitacao.cargo == cargo)).select().first()
            if not cargo_reg:
                db.habilitacao.insert(eleitor_id=eleitor, cargo=int(cargo))
                db.commit()
            # cargos_list.append(db.habilitacao.cargo.nome)
        # rows = cargo_habilitado.select()
        # response.view = 'generic.html'
        # return locals()
        # for row in cargo_habilitado.select():
        #     if row.habilitacao.cargo not in cargos:
        #         db(db.habilitacao.id == row.habilitacao.id).delete()
        #         db.commit()

        cargos_nome = cargo_habilitado.select(
            db.cargo.id, db.cargo.nome, distinct=True).as_dict()
        cargos_list = []
        cargos_list.append([n['nome'] for n in cargos_nome.values()])

        mensagem.append(
            f'Eleitor {eleitor_nome} está habilitado nesta eleição ao(s) cargo(s) {cargos_list}')

    if request.post_vars.cargos:
        cargos = []
        if ',' in str(request.post_vars.cargos):
            for cargo in request.post_vars.cargos:
                cargos.append(cargo)
        else:
            cargos.append(request.post_vars.cargos)

        # return str(type(request.post_vars.cargos))
        iter_cagos(este_eleitor, cargos)
        # cargos_eleitor = db((db.habilitacao.eleitor == este_eleitor) & (db.cargo.id == db.habilitacao.cargo))

        # response.flash='Eleitor {} habilitado nesta eleição ao(s) cargo(s) {}'.format(eleitor_nome,cargos_list)
        response.flash = mensagem

    return locals()


@ auth.requires_membership('admin')
def desabilitar_eleitor_cargo():
    if request.args(0) and request.args(1):
        row = db(db.habilitacao.id == int(request.args(0))).select().first()

        if row:
            url = URL(
                f=f'habilitar_eleitor/{request.args(1)}?eleitor_id={row.eleitor}')
            # return url
            db(db.habilitacao.id == row.id).delete()
            db.commit()
            redirect(url)
        else:
            return 'o registro não existe, volte à página anterior'
    else:
        return 'não foram passadas informações suficientes'


@ auth.requires_membership('admin')
def avaliar_candidatos():
    atrib = {'class': "btn-group btn-group-toggle", 'data-toggle': "buttons"}
    esta_eleicao = request.args(0)
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    candidatos_p_avaliar = db((db.candidato.eleicao == esta_eleicao) & (
        db.auth_user.id == db.candidato.candidato))
    candidatos_filds = [db.auth_user.first_name, db.candidato.cargo, db.entidade.nome, db.municipio.nome, db.auth_user.email,
                        db.auth_user.username, db.auth_user.gênero, db.auth_user.cor, db.auth_user.mothers_name,
                        db.auth_user.data_nascimento,  db.auth_user.escolaridade, db.auth_user.identidade, db.candidato.status]
    links_candidadato = [dict(header=['Documentos '], body=lambda row:

                              DIV(A('Aprovar', callback=URL(f='bt_aprovacao', vars={'id': row.candidato.id, 'tipo': 'candidato', 'esta_eleicao': esta_eleicao}),
                                    type="radio", name="options", id="option1", autocomplete="off", _class='btn btn-primary active'),
                                  A('Rejeitar', callback=URL(f='bt_rejeitar', vars={'id': row.candidato.id, 'tipo': 'candidato', 'esta_eleicao': esta_eleicao}),
                                    type="radio", name="options", id="option2", autocomplete="off", _class='btn btn-danger'),
                                  options_dict=atrib)),
                         dict(header=['Avaliar'], body=lambda row:
                              [A(BEAUTIFY('Documentos'), _href=URL(f='avaliar_docs_candidato', args=esta_eleicao, vars={
                                  'candidato_id': row.candidato.id}), _class='button btn btn-default', _style="font-size: 1em"),
                              P(documentacao_candidato(row.candidato.id))])
                         ]

    grid_candidato = SQLFORM.smartgrid(db.candidato, linked_tables=['auth_user'], constraints={
        'candidato': ((db.candidato.eleicao == esta_eleicao) & (db.auth_user.id == db.candidato.candidato) &
                      (db.auth_user.municipio == db.municipio.id) & (db.auth_user.entidade == db.entidade.id))},
        user_signature=None, formname='p_avaliar', fields=candidatos_filds, args=request.args[:1],
        create=False, editable=False, deletable=False, details=False, searchable=False, links=links_candidadato,
        field_id=db.candidato.id, csv=False, maxtextlength=200)

    return locals()


# @auth.requires_membership('eleicao_{}'.format(request.args(0))) #Trocar para algo como 'membro da comissão'
@ auth.requires_membership('admin')  
def avaliar_docs_candidato():

    esta_eleicao = request.args(0)

    def form_onupdate(form):
        doc_status = form.vars.status
        session.flash = 'O Documento do candidato foi {}'.format(doc_status)
        # redirect(URL(f="avaliar_documentos", args=[esta_eleicao]))
    docs_req = db((db.cargo.eleicao == esta_eleicao) & (
        db.candidato.cargo == db.cargo.id)).select('documentos_requeridos').first()

    candidato_id = request.vars.candidato_id
    candidato_c_docs_p_avaliar = db((db.candidato.eleicao == esta_eleicao)
                                    & (db.documentos.candidato == candidato_id)
                                    & ((db.documentos.status == 'Aguardando Aprovação') | (db.documentos.status == None))
                                    & (db.documentos.candidato == db.candidato.id)
                                    )  # .select()
    candidato_c_docs_aprovados = db((db.candidato.eleicao == esta_eleicao)
                                    & (db.candidato.id == candidato_id)
                                    & (db.documentos.candidato == db.candidato.id)
                                    & (db.documentos.status == 'Aprovado'))  # .select()
    candidato_c_docs_reprovado = db((db.candidato.eleicao == esta_eleicao)
                                    & (db.candidato.id == candidato_id)
                                    & (db.documentos.status == 'rejeitado')
                                    & (db.documentos.candidato == db.candidato.id)
                                    )  # .select()

    candidato_doc = db((db.candidato.eleicao == esta_eleicao) & (
        db.candidato.id == candidato_id)).select().first()

    url = URL('download')
    db.documentos.id.readable = False
    db.documentos.candidato.writable = False
    db.documentos.nome_arquivo.writable = False
    db.documentos.tipo_arquivo.writable = False
    db.documentos.arquivo.writable = False
    db.documentos.status.requires = IS_IN_SET(['Aprovado', 'Rejeitado'])
    db.documentos.status.widget = SQLFORM.widgets.radio.widget
    # headers=[db.documentos.candidato, db.candidato.entidade, db.documentos.nome_arquivo, db.documentos.arquivo, db.documentos.status]
    fields = [db.documentos.candidato, db.candidato.entidade, db.candidato.cargo,
              db.documentos.nome_arquivo, db.documentos.arquivo, db.documentos.status]
    # links_doc_candidato = [dict(header='Aprovação', body=lambda row: A('Aprovar', callback=URL(
    #     f='bt_aprovacao', args=esta_eleicao, vars={'id': row.documentos.id}), _class='button btn btn-default'))]
    atrib = {'class': "btn-group btn-group-toggle", 'data-toggle': "buttons"}
    links_doc_candidato = [dict(header='Avaliar', body=lambda row:
                                [DIV(A('Aprovar', callback=URL(f='bt_aprovacao', vars={'id': row.documentos.id,
                                                                                       'candidato_id': candidato_id, 'tipo': 'doc_candidato', 'esta_eleicao': esta_eleicao}),
                                       type="radio", name="options", id="option1", autocomplete="off",
                                       _class='btn btn-primary active', _id=f'BtAprovaDoc_{row.documentos.id}'),
                                     A('Rejeitar', callback=URL(f='bt_rejeitar', vars={'id': row.documentos.id,
                                                                                       'candidato_id': candidato_id, 'tipo': 'doc_candidato', 'esta_eleicao': esta_eleicao}),
                                       type="radio", name="options", id="option2", autocomplete="off",
                                       _class='btn btn-danger'), options_dict=atrib),
                                 DIV(A('Ver', _href=URL(c='default', f='avaliar_docs_candidato', args=esta_eleicao,
                                                        vars={'doc': row.documentos.id, 'candidato_id': candidato_id}),
                                       _class='btn btn-secondary'))])]
    links_doc_rejetados = [dict(header='Avaliar', body=lambda row:
                                [DIV(A('Aprovar', callback=URL(f='bt_aprovacao', vars={'id': row.documentos.id,
                                                                                       'candidato_id': candidato_id, 'tipo': 'doc_candidato', 'esta_eleicao': esta_eleicao}),
                                       type="radio", name="options", id="option1", autocomplete="off",
                                       _class='btn btn-primary active', _id=f'BtAprovaDoc_{row.documentos.id}'),
                                     options_dict=atrib),
                                 DIV(A('Ver', _href=URL(c='default', f='avaliar_docs_candidato', args=esta_eleicao,
                                                        vars={'doc': row.documentos.id, 'candidato_id': candidato_id}),
                                       _class='btn btn-secondary'))])]
    links_doc_aprovados = [dict(header='Avaliar', body=lambda row:
                                [DIV(A('Rejeitar', callback=URL(f='bt_rejeitar', vars={'id': row.documentos.id,
                                                                                       'candidato_id': candidato_id, 'tipo': 'doc_candidato', 'esta_eleicao': esta_eleicao}),
                                       type="radio", name="options", id="option2", autocomplete="off",
                                       _class='btn btn-danger'), options_dict=atrib),
                                 DIV(A('Ver', _href=URL(c='default', f='avaliar_docs_candidato', args=esta_eleicao,
                                                        vars={'doc': row.documentos.id, 'candidato_id': candidato_id}),
                                       _class='btn btn-secondary'))])]

    form = SQLFORM.grid(candidato_c_docs_p_avaliar, user_signature=None, formname='p_avaliar', fields=fields, args=request.args[:1],
                        create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                        field_id=db.documentos.id, csv=False, maxtextlength=200, links=links_doc_candidato)

    form2 = SQLFORM.grid(candidato_c_docs_aprovados, user_signature=None, formname='aprovados', fields=fields,  args=request.args[: 1],
                         create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                         field_id=db.documentos.id, csv=False, maxtextlength=200, links=links_doc_aprovados)

    form3 = SQLFORM.grid(candidato_c_docs_reprovado, user_signature=None, formname='rejeitados', fields=fields,   args=request.args[:1],
                         create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                         field_id=db.documentos.id, csv=False, maxtextlength=200, links=links_doc_rejetados)
    if request.vars['doc']:
        doc = request.vars['doc']
        form_doc = SQLFORM(db.documentos, doc, upload=url, readonly=True)

    return locals()


# auth.requires_membership('eleicao_{}'.format(request.args(0))) #Trocar para algo como 'membro da comissão'
@ auth.requires_membership('admin')  # Trocar para algo como 'membro da comissão'
def avaliar_docs_eleitor():

    def form_onupdate(form):
        doc_status = form.vars.status
        session.flash = 'O Documento do eleitor foi {}'.format(doc_status)
        # redirect(URL(f="avaliar_documentos", args=[esta_eleicao]))

    eleitor_id = request.vars.eleitor
    esta_eleicao = request.args(0)
    eleitor_c_docs_p_avaliar = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        (db.documentos_eleitor.status == 'Aguardando Aprovação') | (db.documentos_eleitor.status == None)))  # .select()
    eleitor_c_docs_aprovados = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        db.documentos_eleitor.status == 'Aprovado'))  # .select()
    eleitor_c_docs_reprovado = db((db.documentos_eleitor.eleitor == eleitor_id) & (
        db.documentos_eleitor.status == 'rejeitado'))  # .select()
    eleitor_doc = db((db.eleitor.eleicao == esta_eleicao) & (
        db.eleitor.id == eleitor_id)).select().first()
    url = URL('download')

    db.documentos_eleitor.id.readable = False
    db.documentos_eleitor.eleitor.writable = False
    db.documentos_eleitor.nome_arquivo.writable = False
    db.documentos_eleitor.tipo_arquivo.writable = False
    db.documentos_eleitor.arquivo.writable = False
    db.documentos_eleitor.status.requires = IS_IN_SET(
        ['Aprovado', 'Rejeitado'])
    db.documentos_eleitor.status.widget = SQLFORM.widgets.radio.widget
    # headers=[db.documentos.candidato, db.candidato.entidade, db.documentos.nome_arquivo, db.documentos.arquivo, db.documentos.status]
    fields = [db.documentos_eleitor.eleitor, db.documentos_eleitor.nome_arquivo, db.documentos_eleitor.tipo_arquivo,
              db.documentos_eleitor.arquivo, db.documentos_eleitor.status]

    # links_doc_eleitor = [dict(header='Aprovação', body=lambda row: A('Aprovar', callback=URL(
    #     f='bt_aprovacao', args=row['id']), _class='button btn btn-default')), ]
    atrib = {'class': "btn-group btn-group-toggle", 'data-toggle': "buttons"}
    links_doc_eleitor = [dict(header=['Avaliar'], body=lambda row:
                              [DIV(A('Aprovar', callback=URL(f='bt_aprovacao',
                                                             vars={'id': row['id'], 'eleitor_id': eleitor_id, 'tipo': 'doc_eleitor', 'esta_eleicao': esta_eleicao}),
                                     type="radio", name="options", id="option1", autocomplete="off",
                                     _class='btn btn-primary active',
                                     _onclick="$('.web2py_table_selectable_actions').on('click', function(e){ if (!confirm('Confirmar a ação?')){ e.preventDefault();  e.stopPropagation(); })"),

                                   A('Rejeitar', callback=URL(f='bt_rejeitar', vars={'id': row['id'], 'eleitor_id': eleitor_id, 'tipo': 'doc_eleitor', 'esta_eleicao': esta_eleicao}),
                                     type="radio", name="options", id="option2", autocomplete="off",
                                     _class='btn btn-danger'),
                                   options_dict=atrib),
                              DIV(A('Ver', _href=URL(c='default', f='avaliar_docs_eleitor', args=esta_eleicao,
                                                     vars={'doc': row['id'], 'eleitor_id': eleitor_id}), _class='btn btn-secondary'))
                               ])]

    form = SQLFORM.grid(eleitor_c_docs_p_avaliar, user_signature=None, formname='p_avaliar', fields=fields, args=request.args[: 1],
                        create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                        field_id=db.documentos_eleitor.id, csv=False, maxtextlength=200, links=links_doc_eleitor)

    form2 = SQLFORM.grid(eleitor_c_docs_aprovados, user_signature=None, formname='aprovados', fields=fields,  args=request.args[:1],
                         create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                         field_id=db.documentos_eleitor.id, csv=False, maxtextlength=200, links=links_doc_eleitor)

    form3 = SQLFORM.grid(eleitor_c_docs_reprovado, user_signature=None, formname='rejeitados', fields=fields,   args=request.args[:1],
                         create=False, editable=False, deletable=False, details=False, upload=url, onupdate=form_onupdate, searchable=False,
                         field_id=db.documentos_eleitor.id, csv=False, maxtextlength=200, links=links_doc_eleitor)
    if request.vars['doc']:
        doc = request.vars['doc']
        form_doc = SQLFORM(db.documentos_eleitor, doc,
                           upload=url, readonly=True)
    else:
        pass

    return locals()


def lista_de_cargos_na_eleicao():
    esta_eleicao = request.args(0)
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    cargos_eleicao = db((db.cargo.eleicao == esta_eleicao)).select()
    lst_cargos = []
    for cargo in cargos_eleicao:
        lst_cargos.append(cargo)

    return dict(lst_cargos=lst_cargos, nome_eleicao=nome_eleicao, esta_eleicao=esta_eleicao)


def lista_de_candidatos_a_eleicao():
    esta_eleicao = request.args(0)
    # esta_eleicao = request.vars['eleicao_id']
    # cargo = request.vars['cargo']

    esta_eleicao = request.args(0)
    nome_eleicao = db.eleicao(int(esta_eleicao)).nome
    cargos = db(db.cargo.eleicao == esta_eleicao).select(
        db.cargo.id, db.cargo.nome)
    # cargos_p_votar = []
    # cargos_votados = []
    # for cargo in cargos:

    # nome_eleicao = db(db.eleicao.id == esta_eleicao).select(db.eleicao.nome).first().nome

    # cargos_eleicao = db((db.cargo.eleicao == esta_eleicao)).select()
    lst_grids = []

    #  fields = [db.auth_user.first_name, db.auth_user.data_nascimento, db.auth_user.escolaridade, db.cargo.nome , db.candidato.propostas, db.candidato.perfil, db.candidato.foto]

    for cargo in cargos:

        candidato_eleicao = db((db.candidato.cargo == cargo.id)
                               & (db.candidato.status == 'aprovado')
                               & (db.candidato.cargo == db.cargo.id)
                               & (db.candidato.eleicao == esta_eleicao)
                               & (db.auth_user.id == db.candidato.candidato)).select(distinct=True)
        if candidato_eleicao:
            for candidato in candidato_eleicao:
                lst_grids.append(candidato)

    return dict(lst_grids=lst_grids, nome_eleicao=nome_eleicao)


def resultados():
    esta_eleicao = int(request.vars['esta_eleicao'])
    eleicao = db.eleicao(esta_eleicao)
    if eleicao.fim_eleicao > datetime.now():
        session.flash = 'A eleição ainda não foi encerrada'
        redirect(URL('index'))
    nome_eleicao = eleicao.nome

    votos_desta_eleicao = db((db.voto.eleicao == esta_eleicao))

    total_votos = votos_desta_eleicao.count()

    # votos_por_candidato = votos_desta_eleicao.select(
    #     db.voto.ALL, orderby=db.voto.id, groupby=db.voto.id)

    eleitores = db.executesql(f'''select distinct created_by
        from voto 
        where eleicao = {eleicao.id}
    ''',as_dict=True)
    total_eleitores = len(eleitores)

    cargos_eleicao = db((db.cargo.eleicao == esta_eleicao)).select()

    total_por_cargos = {}
    for cargo in cargos_eleicao:
        candidatos_eleicao = db(
            (db.candidato.cargo == cargo)
            &(db.candidato.status == 'aprovado')
            ).select()
        total_por_candidatos = []
        for candidato in candidatos_eleicao:
            if eleicao.tipo_candidato == 'entidade':
                # candidato_nome = db((db.candidato.id == candidato) & (
                #     db.auth_user.id == db.candidato.candidato)).select(db.auth_user.first_name, distinct=True).first()
                nome_candidato = candidato.entidade.nome
            else:
                # candidato_nome = db((db.candidato.id == candidato) & (
                #     db.auth_user.id == db.candidato.candidato)).select(db.auth_user.first_name, distinct=True).first()
                nome_candidato = candidato.candidato.first_name
            candidato_votos = db((db.voto.eleicao == esta_eleicao)
                                 & (db.voto.candidato == candidato.id)).count()
            total_por_candidatos.append(
                dict(Candidato=nome_candidato, Votos=candidato_votos))

        total_por_candidatos = sorted(
            total_por_candidatos, key=lambda k: k['Votos'], reverse=True)
        
        total_por_cargos[cargo.id] = total_por_candidatos

    # return dict(nome_eleicao=nome_eleicao, total_votos=total_votos, total_por_cargos=total_por_cargos,
    #             # candidatos_eleicao=candidatos_eleicao if cargos_eleicao else None)
    return locals()

@ auth.requires_membership('admin')
def tipo_de_documento():

    form_doc = SQLFORM(db.tipo_documento)
    if form_doc.process().accepted:
        session.flash = 'Tipo de Documento Registrado'
    elif form_doc.errors:
        response.flash = 'Ocorreu um erro no registro.'
    else:
        response.flash = 'Verifique as informações'
    # q = db(db.tipo_documento.id>0).select(orderby=db.tipo_documento.tipo)
    grid = SQLFORM.grid(db.tipo_documento, fields=[db.tipo_documento.tipo], create=False,
                        editable=True, deletable=False, details=False, csv=False, maxtextlength=200,
                        links = [dict(header=['Será exigido de'], body=lambda row: tipo_eleitor[db.tipo_documento(row.id).relativo_a])],
                        orderby=db.tipo_documento.tipo,
                        )

    return locals()


def votar_cargo():
    esta_eleicao = int(request.vars['esta_eleicao'])
    eleicao = db.eleicao(esta_eleicao)
    cargo_id = int(request.vars['cargo_id'])
    cargo = db.cargo(cargo_id)
    # eleitor_id = db((db.eleitor.eleitor == auth.user_id) & (
    #     # db.eleitor.eleicao == esta_eleicao)).select('id').first()
    habilitado = db((db.habilitacao.eleitor==auth.user_id)
                    & (db.habilitacao.eleicao==eleicao.id)
                    & (db.habilitacao.status=='aprovado')
                    & (db.habilitacao.cargo==cargo.id)).select().first()
    lst_candidatos_ao_cargo = []

    if not habilitado or votou(esta_eleicao, auth.user_id, cargo.id):
        redirect(URL(c="default",f="votar",vars=dict(esta_eleicao=eleicao.id)))

    if eleicao.tipo_candidato == 'entidade':
        # return eleicao.tipo_candidato
        candidatos_ao_cargo = db((db.candidato.eleicao == esta_eleicao) 
                            & (db.candidato.cargo == cargo_id) 
                            & (db.candidato.entidade == db.entidade.id)
                            & (db.candidato.status == 'aprovado')
                                ).select(orderby=db.entidade.nome)
        for c in candidatos_ao_cargo:
            lst_candidatos_ao_cargo.append(A(c.entidade.nome.upper(), _href=URL('default', 'voto', vars={
                                            'candidato_id': c.candidato.id, 'esta_eleicao': esta_eleicao, 'cargo_id': cargo_id}),
                                            _style="font-size:1.7em",
                                            _onclick="confirm('Confirma o seu voto em {{=c.entidade.nome.upper()}}? )"),
                                            )
    else:
        candidatos_ao_cargo = db((db.candidato.eleicao == esta_eleicao) & (db.candidato.cargo == cargo_id) & (
        db.candidato.candidato == db.auth_user.id)).select(orderby=db.auth_user.first_name)
        for c in candidatos_ao_cargo:
            lst_candidatos_ao_cargo.append(A(c.auth_user.first_nome.upper(), _href=URL('default', 'voto', vars={
                                            'candidato_id': c.candidato.id, 'esta_eleicao': esta_eleicao, 'cargo_id': cargo_id}),
                                            _style="font-size:1.7em"))


    # return dict(lst_candidatos_ao_cargo=lst_candidatos_ao_cargo, habilitado=habilitado)
    return locals()


# def envia_emails(para, assunto, mensagem):
#     mail = auth.settings.mailer
#     if mail:
#         if db.queue.insert(status='pending',
#                            email=para,
#                            subject=assunto,
#                            msg=mensagem):
#             response.flash = 'Email programado para envio.'
#         else:
#             response.flash = 'Falha na programação do envio'
#     else:
#         response.flash = 'Não foi capaz de enviar o email'
#     return response.flash


@auth.requires_membership('dev')
def teste():
    rows = db(db.auth_user.id > 0).select()

    for row in rows:
        if len(row.username) == 11:
            cpf = f'{row.username[:3]}.{row.username[3:6]}.{row.username[6:9]}-{row.username[9:]}'
            print(cpf)
            row.update_record(username=cpf)


def check_usuario():
    """
    Checa o candidato e eleitor

    Fazer varredura nas tabelas candidato e habilitação
    """
    check_pendencias()
    
    return dict()

@auth.requires_login()
def recursos():
    T.force('re-br')
    q = db(db.recurso.created_by == auth.user_id)
    # if not q:
    #     q = db.recurso

    form = SQLFORM.grid(q,
        maxtextlength=200,
        deletable=False,
        details=False,
        editable=False,
        orderby=~db.recurso.id,
        csv=False,
        links = [dict(header=['Acompanhar'], body=lambda row:
                          [DIV(A('Acompanhar', _href=URL(c='default', f='recurso', args=row.id,user_signature=True)))])
                    ]
    )

    return locals()

@auth.requires_login()
def recurso():
    T.force('do-br')
    if request.args(0) and request.args(0) in ('new','download'):
        pass
    elif request.args(0) and int(request.args(0)) > 0:
        recurso_id = int(request.args(0))
        session.recurso_id = recurso_id
    elif session.recurso_id:
        pass
    else:
        redirect(URL('recursos'))
    recurso = db((db.recurso.created_by == auth.user_id)
        &(db.recurso.id==session.recurso_id)
        ).select().first()
    if not recurso:
        redirect(URL('recursos'))

    # if not q:
    #     q = db.recurso
    
    form = SQLFORM(db.recurso,session.recurso_id, readonly=True)
    # q = db((db.documento_recurso.recurso==recurso_id))
    db.documento_recurso.recurso.default=session.recurso_id
    db.documento_recurso.recurso.writable=False
    db.documento_recurso.recurso.readable=False
    q1 = db(db.documento_recurso.recurso==session.recurso_id)
    form1 = SQLFORM.grid(q1,
        deletable=False,
        csv=False,
        details=False,
        editable=False,
        maxtextlength=200,
    )
    q2 = db(db.resposta_recurso.recurso==session.recurso_id)
    db.resposta_recurso.created_by.readable=True
    db.resposta_recurso.created_by.label="Criado por"
    db.resposta_recurso.recurso.readable=False
    form2 = SQLFORM.grid(q2,
        deletable=False,
        csv=False,
        details=False,
        editable=False,
        maxtextlength=200,
        create=False,
        searchable=False,
    )
    db.resposta_recurso.recurso.default=session.recurso_id
    db.resposta_recurso.recurso.writable=False
    db.resposta_recurso.created_by.represent=lambda v: db.auth_user(v).first_name
    form3 = SQLFORM(db.resposta_recurso)

    if form3.process().accepted:
        response.flash = 'form accepted'
    elif form3.errors:
        pass
    else:
        pass

    return locals()