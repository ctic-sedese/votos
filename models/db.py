# -*- coding: utf-8 -*-
if 0 != 0:
    from gluon import *
    from gluon.tools import IS_IN_DB, IS_IN_SET, IS_NOT_IN_DB, IS_EMPTY_OR, IS_FILE, IS_LENGTH, IS_NOT_EMPTY, IS_IMAGE, IS_CPF, IS_CPF_OR_CNPJ, data, auth
    request = current.request
    response = current.response
    session = current.session
    cache = current.cache
    T = current.T
# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
configuration = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(configuration.get('db.uri'),
             pool_size=configuration.get('db.pool_size'),
             fake_migrate_all=configuration.get('db.fake_migrate_all'),
             fake_migrate=configuration.get('db.fake_migrate'),
             migrate_enabled=True, #configuration.get('db.migrate_enabled'),
             migrate=True, #configuration.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------


# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = []
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')


# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=configuration.get('host.names'))

# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------
#auth.settings.extra_fields['auth_user'] = []


db.define_table('estado',
                Field('uf'),
                Field('nome'),
                Field('latitude', 'double'),
                Field('longitude', 'double'),
                format='%(uf)s'
                )

db.define_table('municipio',
                Field('nome', label="Município"),
                Field('latitude', 'double'),
                Field('longitude', 'double'),
                Field('capital',),
                Field('estado', 'reference estado'),
                Field('uf'),
                format='%(nome)s - %(uf)s'
                )

mmg = db(db.municipio.uf == 'MG')

url_registro = URL(f='usuarios/new/auth_user',user_signature=True)

# 
# db.define_table("entidade",
#                 Field("nome", label=ob("Órgão")),
#                 Field('cnpj', length=200, 
#                     label="CNPJ", 
#                     #requires=[IS_NOT_IN_DB(db, 'entidade.cnpj', error_message='O CNPJ não pode ser duplicado ou 0000000000'), IS_CNPJ()]
#                     ),
#                 Field('cartao_cnpj', "upload", 
#                         uploadseparate=True, 
#                         requires=[IS_EMPTY_OR(IS_FILE(extension=['pdf', 'jpg', 'png', "gif"])), IS_LENGTH(maxsize=2096576, error_message='O arquivo pode ter no máximo 2MB')]
#                         ),
#                 Field('comprovante_endereco', "upload", 
#                         uploadseparate=True, 
#                         requires=[IS_EMPTY_OR(IS_FILE(extension=['pdf', 'jpg', 'png', "gif"])), IS_LENGTH(maxsize=2096576, error_message='O arquivo pode ter no máximo 2MB')]
#                         ),
#                 Field('data_criacao_entidade', 'date', 
#                      label='Data de Criação da Entidade',
#                       requires=data, widget=SQLFORM.widgets.string.widget),
#                 Field("carta_apresentacao", "text",),
#                 Field("segmento_atuacao", "text"),
#                 Field('municipio', 'integer', 
#                     requires=IS_IN_DB(mmg, 'municipio.id', '%(nome)s')
#                     ),
#                 Field("representante_legal", 'integer', 
#                     comment=XML(f'''<a href="{url_registro}" target="_blank">cadastrar novo representante</a>''')
#                     ),
#                 Field("concordancia", widget=SQLFORM.widgets.radio.widget, 
#                     requires=IS_IN_SET(
#                     ['Concordo', 'Não Concordo']), label='Declaração :',  
#                     comment=XML(f'''<b>Declaro para os devidos fins e sob as penas da Lei que as informações prestadas\
#                      e documentos inseridos, neste sistema, para o Processo Eleitoral deste Conselho, são verídicos\
#                      e autênticos(fiéis à verdade e condizentes com a realidade dos fatos à época).</b>''')),
#                 Field('orgao_colegiado','boolean', 
#                     default=False, 
#                     writable=False, 
#                     readable=False, 
#                     label="Órgão Colegiado"),
                
#                 # auth.signature,
#                 # migrate='entidade.table',
#                 format="%(nome)s"
#                 )

db.define_table("entidade",
                Field("nome", label=ob("Órgão"), requires=IS_LENGTH(512,6,error_message="É necessário informar o nome")),
                Field('cnpj', length=200, 
                    label="CNPJ", 
                    requires=[IS_NOT_IN_DB(db, 'entidade.cnpj', error_message='O CNPJ não pode ser duplicado ou 0000000000'), IS_CNPJ()]
                    ),
                # Field('cartao_cnpj', "upload", 
                #         uploadseparate=True, 
                #         requires=[IS_EMPTY_OR(IS_FILE(extension=['pdf', 'jpg', 'png', "gif"])), IS_LENGTH(maxsize=2096576, error_message='O arquivo pode ter no máximo 2MB')]
                #         ),
                # Field('comprovante_endereco', "upload", 
                #         uploadseparate=True, 
                #         requires=[IS_EMPTY_OR(IS_FILE(extension=['pdf', 'jpg', 'png', "gif"])), IS_LENGTH(maxsize=2096576, error_message='O arquivo pode ter no máximo 2MB')]
                #         ),
                Field('data_criacao_entidade', 'date', 
                     label=ob('Data de Criação'),
                     requires=IS_DATE(format=T('%d/%m/%Y'),error_message='a data deve estar no formato 01/01/1990!') ,
                      widget=SQLFORM.widgets.string.widget),
                #Field("carta_apresentacao", "text",),
                Field("legislacao_criacao", "text", label="Legislação de Criação"),
                Field("segmento_atuacao", "text", label="Segmentos de atuação"),
                Field('municipio', 'integer', label=ob("Município"),
                    requires=IS_IN_DB(mmg, 'municipio.id', '%(nome)s')
                    ),
                Field("representante_legal", 'integer' ),
                Field("texto_concordancia", 'boolean', readable=True,  label="", default=True,
                    requires=IS_IN_SET(['on'],error_message="É obrigatório concordar com a declaração"),
                     
                    comment=XML(f'''<b>Declaro para os devidos fins e sob as penas da Lei que as informações prestadas\
                     e documentos inseridos, neste sistema, para o Processo Eleitoral deste Conselho, são verídicos\
                     e autênticos(fiéis à verdade e condizentes com a realidade dos fatos à época).</b>''')),

                # Field("concordancia", widget=SQLFORM.widgets.radio.widget, 
                #     requires=IS_IN_SET(['Concordo', 'Não Concordo']), label='Concordância :'),
                Field('orgao_colegiado','boolean', 
                    default=False, 
                    writable=False, 
                    readable=False, 
                    label="Órgão Colegiado"),
                
                # auth.signature,
                # migrate='entidade.table',
                format="%(nome)s"
                )


db.define_table(
    auth.settings.table_user_name,
    Field('first_name', 
        length=256, 
        default='', 
        label=ob('Nome completo'),
        requires=IS_LENGTH(256,5,error_message='Informe seu nome completo. Minímo  6 e máximo 256 caracteres')
        ),
    # Field('userp_id','integer',writable=False,readable=False),
    Field('last_name', 
        length=128, 
        default='', 
        label=T('Nome Social'), 
        writable=True, readable=True),
    Field('email', 
        length=128, 
        default='',
        label=ob('Email'),
        requires=[IS_LOWER(), IS_EMAIL(error_message='Informe um email válido'),
        IS_NOT_IN_DB(db,'auth_user.email', error_message='Email já cadastrado')],
        unique=True),

    Field('username', 
        length=200, 
        label=ob("CPF"), 
        requires=[IS_CPF(), IS_NOT_IN_DB(db, 'auth_user.username',  
        error_message='O CPF não pode ser duplicado ou 0000000000')],
        comment='Digite apenas os números do seu CPF'
        ),
    # Field('entidade', 'integer',
    #       requires=IS_EMPTY_OR(IS_IN_DB(db, 'entidade.id', '%(nome)s')),
    #       comment="Você poderá cadastrar sua entidade depois"
    #       #comment=A('Cadastrar', _class='btn btn-primary', _type="submit", _href=URL(c='default', f='cadastra_entidade', args=request.args(0),vars={'form_usuario': request.vars}))
    #       ),
    Field('municipio', 'integer',
          requires=IS_IN_DB(mmg, 'municipio.id', '%(nome)s'),
          label=ob("Município de Residência"), 
        #   comment="Município de Residência",
          ),
    # Field('mothers_name', 
    #     length=128, 
    #     default='', 
    #     label=ob('Nome da Mãe'),
    #     requires=IS_NOT_EMPTY()
    #     ),
    # Field('gênero', 
    #     requires=IS_EMPTY_OR(IS_IN_SET(['Masculino', 'Feminino', 'Outros'], zero='Informe seu Gênero')),
    #     ),
    # Field('cor', 
    #     requires=IS_EMPTY_OR(IS_IN_SET(['Branca', 'Preta', 'Parda', 'Indígena', 'Amarela', 'Não Desejo Informar'], zero='Informe sua Cor'))
    #     ),
    # Field('data_nascimento', 'date', 
    #         label=ob('Data de Nascimento'),
    #         requires=IS_DATE(format=T('%d/%m/%Y'),
    #                error_message='a data deve estar no formato 01/01/1990!') ,
    #         widget=SQLFORM.widgets.string.widget, 
    #         comment="Data no formato 01/01/1900"),
    Field("identidade", 
        label=ob("Documento de Identidade"), 
        requires=IS_NOT_EMPTY(error_message='Informe o número do seu documento de identidade')
        ),
    # Field("foto", "upload", 
    #     uploadseparate=True, 
    #     requires=[IS_EMPTY_OR(IS_FILE(extension=[
    #     'jpg', 'jpeg', 'png'], error_message='O arquivo deve estar no formato jpg, jpeg ou png')), IS_LENGTH(maxsize=2096576, error_message='O arquivo pode ter no máximo 2MB')],
    #     comment=' Envie uma foto atual em um dos formatos: jpg, jpeg ou png'
    #     ),
    # Field("escolaridade", 
    #         requires=IS_EMPTY_OR(IS_IN_SET(['Sem Instrução', 'Fundamental Incompleto', 'Fundamental Completo',
    #       'Médio Incompleto', 'Médio Completo', 'Superior Incompleto', 'Superior Completo'], zero='Informe sua Escolaridade')),
    #       ),
    Field('password', 'password', length=512,
          readable=False, label=ob('Senha'),
          requires=IS_LENGTH(32,5),
          comment=XML(f"""<button type="button" onclick="if (auth_user_password.type == 'text') auth_user_password.type = 'password';
  else auth_user_password.type = 'text';"" style="width:50px;height=50px;" id="eye">
          <img src="https://img.icons8.com/material-sharp/24/000000/visible.png"/>
         </button> A senha deve ter entre 6 e 32 caracteres """),
          ),
    Field('registration_key', length=512,
          writable=False, readable=False, default=''),
    Field('reset_password_key', length=512,
          writable=False, readable=False, default=''),
    Field('registration_id', length=512,
          writable=False, readable=False, default=''),
    auth.signature,
    format='%(first_name)s',

)
def label_representante(id):
    if id:
        u  = db.auth_user(id)
        if u:
            return f"{u.first_name}"
        else:
            return None
    else:
        return ''
db.auth_user.password.requires = IS_NOT_EMPTY(error_message='A senha é obrigatória')
db.entidade.representante_legal.requires = IS_IN_DB(db, 'auth_user.id', '%(first_name)s')

db.entidade.representante_legal.represent = lambda value, row: label_representante(value)


auth.define_tables(username=True, signature=False)
auth.settings.create_user_groups = None
auth.settings.registration_requires_verification = True
# auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True


# -------------------------------------------------------------------------
# configure _next
# -------------------------------------------------------------------------
# auth.settings.login_next = URL('default','check_usuario')
# auth.settings.logout_next = URL('index')
# auth.settings.profile_next = URL('index')
# auth.settings.register_next = URL('user', args='login')
# auth.settings.retrieve_username_next = URL('index')
# auth.settings.retrieve_password_next = URL('index')
# auth.settings.change_password_next = URL('index')
# auth.settings.request_reset_password_next = URL('user', args='login')
# auth.settings.reset_password_next = URL('user', args='login')
# auth.settings.verify_email_next = URL('user', args='login')

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = configuration.get('smtp.server')
mail.settings.sender = configuration.get('smtp.sender')
mail.settings.login = configuration.get('smtp.login')
mail.settings.tls = configuration.get('smtp.tls') or False
mail.settings.ssl = configuration.get('smtp.ssl') or False

# Definição da tabela fila de emails
db.define_table('queue',
                Field('status'),
                Field('email'),
                Field('subject'),
                Field('msg', 'text')
            )


# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = True
# auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------
# read more at http://dev.w3.org/html5/markup/meta.name.html
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')
response.show_toolbar = configuration.get('app.toolbar')

# -------------------------------------------------------------------------
# your http://google.com/analytics id
# -------------------------------------------------------------------------
response.google_analytics_id = configuration.get('google.analytics_id')

# -------------------------------------------------------------------------
# maybe use the scheduler
# -------------------------------------------------------------------------
if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(
        db, heartbeat=configuration.get('scheduler.heartbeat'))

T.force('pt-br')
aviso_obrigatorio = (LABEL(XML('Os campos com asterisco (<span style="color:red">*</span>) são obrigatórios')))